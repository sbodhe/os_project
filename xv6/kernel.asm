
kernel:     file format elf32-i386


Disassembly of section .text:

00100000 <multiboot_header>:
  100000:	02 b0 ad 1b 01 00    	add    0x11bad(%eax),%dh
  100006:	01 00                	add    %eax,(%eax)
  100008:	fd                   	std    
  100009:	4f                   	dec    %edi
  10000a:	51                   	push   %ecx
  10000b:	e4 00                	in     $0x0,%al
  10000d:	00 10                	add    %dl,(%eax)
  10000f:	00 00                	add    %al,(%eax)
  100011:	00 10                	add    %dl,(%eax)
  100013:	00 06                	add    %al,(%esi)
  100015:	78 10                	js     100027 <multiboot_entry+0x7>
  100017:	00 a4 e8 10 00 20 00 	add    %ah,0x200010(%eax,%ebp,8)
  10001e:	10 00                	adc    %al,(%eax)

00100020 <multiboot_entry>:
# Multiboot entry point.  Machine is mostly set up.
# Configure the GDT to match the environment that our usual
# boot loader - bootasm.S - sets up.
.globl multiboot_entry
multiboot_entry:
  lgdt gdtdesc
  100020:	0f 01 15 64 00 10 00 	lgdtl  0x100064
  ljmp $(SEG_KCODE<<3), $mbstart32
  100027:	ea 2e 00 10 00 08 00 	ljmp   $0x8,$0x10002e

0010002e <mbstart32>:

mbstart32:
  # Set up the protected-mode data segment registers
  movw    $(SEG_KDATA<<3), %ax    # Our data segment selector
  10002e:	66 b8 10 00          	mov    $0x10,%ax
  movw    %ax, %ds                # -> DS: Data Segment
  100032:	8e d8                	mov    %eax,%ds
  movw    %ax, %es                # -> ES: Extra Segment
  100034:	8e c0                	mov    %eax,%es
  movw    %ax, %ss                # -> SS: Stack Segment
  100036:	8e d0                	mov    %eax,%ss
  movw    $0, %ax                 # Zero segments not ready for use
  100038:	66 b8 00 00          	mov    $0x0,%ax
  movw    %ax, %fs                # -> FS
  10003c:	8e e0                	mov    %eax,%fs
  movw    %ax, %gs                # -> GS
  10003e:	8e e8                	mov    %eax,%gs

  # Set up the stack pointer and call into C.
  movl $(stack + STACK), %esp
  100040:	bc e0 88 10 00       	mov    $0x1088e0,%esp
  call main
  100045:	e8 76 28 00 00       	call   1028c0 <main>

0010004a <spin>:
spin:
  jmp spin
  10004a:	eb fe                	jmp    10004a <spin>

0010004c <gdt>:
	...
  100054:	ff                   	(bad)  
  100055:	ff 00                	incl   (%eax)
  100057:	00 00                	add    %al,(%eax)
  100059:	9a cf 00 ff ff 00 00 	lcall  $0x0,$0xffff00cf
  100060:	00 92 cf 00 17 00    	add    %dl,0x1700cf(%edx)

00100064 <gdtdesc>:
  100064:	17                   	pop    %ss
  100065:	00 4c 00 10          	add    %cl,0x10(%eax,%eax,1)
  100069:	00 90 90 90 90 90    	add    %dl,-0x6f6f6f70(%eax)
  10006f:	90                   	nop

00100070 <brelse>:
}

// Release the buffer b.
void
brelse(struct buf *b)
{
  100070:	55                   	push   %ebp
  100071:	89 e5                	mov    %esp,%ebp
  100073:	53                   	push   %ebx
  100074:	83 ec 14             	sub    $0x14,%esp
  100077:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((b->flags & B_BUSY) == 0)
  10007a:	f6 03 01             	testb  $0x1,(%ebx)
  10007d:	74 57                	je     1000d6 <brelse+0x66>
    panic("brelse");

  acquire(&bcache.lock);
  10007f:	c7 04 24 e0 88 10 00 	movl   $0x1088e0,(%esp)
  100086:	e8 a5 3a 00 00       	call   103b30 <acquire>

  b->next->prev = b->prev;
  10008b:	8b 43 10             	mov    0x10(%ebx),%eax
  10008e:	8b 53 0c             	mov    0xc(%ebx),%edx
  100091:	89 50 0c             	mov    %edx,0xc(%eax)
  b->prev->next = b->next;
  100094:	8b 43 0c             	mov    0xc(%ebx),%eax
  100097:	8b 53 10             	mov    0x10(%ebx),%edx
  10009a:	89 50 10             	mov    %edx,0x10(%eax)
  b->next = bcache.head.next;
  10009d:	a1 14 9e 10 00       	mov    0x109e14,%eax
  b->prev = &bcache.head;
  1000a2:	c7 43 0c 04 9e 10 00 	movl   $0x109e04,0xc(%ebx)

  acquire(&bcache.lock);

  b->next->prev = b->prev;
  b->prev->next = b->next;
  b->next = bcache.head.next;
  1000a9:	89 43 10             	mov    %eax,0x10(%ebx)
  b->prev = &bcache.head;
  bcache.head.next->prev = b;
  1000ac:	a1 14 9e 10 00       	mov    0x109e14,%eax
  1000b1:	89 58 0c             	mov    %ebx,0xc(%eax)
  bcache.head.next = b;
  1000b4:	89 1d 14 9e 10 00    	mov    %ebx,0x109e14

  b->flags &= ~B_BUSY;
  1000ba:	83 23 fe             	andl   $0xfffffffe,(%ebx)
  wakeup(b);
  1000bd:	89 1c 24             	mov    %ebx,(%esp)
  1000c0:	e8 7b 30 00 00       	call   103140 <wakeup>

  release(&bcache.lock);
  1000c5:	c7 45 08 e0 88 10 00 	movl   $0x1088e0,0x8(%ebp)
}
  1000cc:	83 c4 14             	add    $0x14,%esp
  1000cf:	5b                   	pop    %ebx
  1000d0:	5d                   	pop    %ebp
  bcache.head.next = b;

  b->flags &= ~B_BUSY;
  wakeup(b);

  release(&bcache.lock);
  1000d1:	e9 0a 3a 00 00       	jmp    103ae0 <release>
// Release the buffer b.
void
brelse(struct buf *b)
{
  if((b->flags & B_BUSY) == 0)
    panic("brelse");
  1000d6:	c7 04 24 80 64 10 00 	movl   $0x106480,(%esp)
  1000dd:	e8 2e 08 00 00       	call   100910 <panic>
  1000e2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  1000e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

001000f0 <bwrite>:
}

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
  1000f0:	55                   	push   %ebp
  1000f1:	89 e5                	mov    %esp,%ebp
  1000f3:	83 ec 18             	sub    $0x18,%esp
  1000f6:	8b 45 08             	mov    0x8(%ebp),%eax
  if((b->flags & B_BUSY) == 0)
  1000f9:	8b 10                	mov    (%eax),%edx
  1000fb:	f6 c2 01             	test   $0x1,%dl
  1000fe:	74 0e                	je     10010e <bwrite+0x1e>
    panic("bwrite");
  b->flags |= B_DIRTY;
  100100:	83 ca 04             	or     $0x4,%edx
  100103:	89 10                	mov    %edx,(%eax)
  iderw(b);
  100105:	89 45 08             	mov    %eax,0x8(%ebp)
}
  100108:	c9                   	leave  
bwrite(struct buf *b)
{
  if((b->flags & B_BUSY) == 0)
    panic("bwrite");
  b->flags |= B_DIRTY;
  iderw(b);
  100109:	e9 42 1e 00 00       	jmp    101f50 <iderw>
// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
  if((b->flags & B_BUSY) == 0)
    panic("bwrite");
  10010e:	c7 04 24 87 64 10 00 	movl   $0x106487,(%esp)
  100115:	e8 f6 07 00 00       	call   100910 <panic>
  10011a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00100120 <bread>:
}

// Return a B_BUSY buf with the contents of the indicated disk sector.
struct buf*
bread(uint dev, uint sector)
{
  100120:	55                   	push   %ebp
  100121:	89 e5                	mov    %esp,%ebp
  100123:	57                   	push   %edi
  100124:	56                   	push   %esi
  100125:	53                   	push   %ebx
  100126:	83 ec 1c             	sub    $0x1c,%esp
  100129:	8b 75 08             	mov    0x8(%ebp),%esi
  10012c:	8b 7d 0c             	mov    0xc(%ebp),%edi
static struct buf*
bget(uint dev, uint sector)
{
  struct buf *b;

  acquire(&bcache.lock);
  10012f:	c7 04 24 e0 88 10 00 	movl   $0x1088e0,(%esp)
  100136:	e8 f5 39 00 00       	call   103b30 <acquire>

 loop:
  // Try for cached block.
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
  10013b:	8b 1d 14 9e 10 00    	mov    0x109e14,%ebx
  100141:	81 fb 04 9e 10 00    	cmp    $0x109e04,%ebx
  100147:	75 12                	jne    10015b <bread+0x3b>
  100149:	eb 35                	jmp    100180 <bread+0x60>
  10014b:	90                   	nop
  10014c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  100150:	8b 5b 10             	mov    0x10(%ebx),%ebx
  100153:	81 fb 04 9e 10 00    	cmp    $0x109e04,%ebx
  100159:	74 25                	je     100180 <bread+0x60>
    if(b->dev == dev && b->sector == sector){
  10015b:	3b 73 04             	cmp    0x4(%ebx),%esi
  10015e:	75 f0                	jne    100150 <bread+0x30>
  100160:	3b 7b 08             	cmp    0x8(%ebx),%edi
  100163:	75 eb                	jne    100150 <bread+0x30>
      if(!(b->flags & B_BUSY)){
  100165:	8b 03                	mov    (%ebx),%eax
  100167:	a8 01                	test   $0x1,%al
  100169:	74 64                	je     1001cf <bread+0xaf>
        b->flags |= B_BUSY;
        release(&bcache.lock);
        return b;
      }
      sleep(b, &bcache.lock);
  10016b:	c7 44 24 04 e0 88 10 	movl   $0x1088e0,0x4(%esp)
  100172:	00 
  100173:	89 1c 24             	mov    %ebx,(%esp)
  100176:	e8 e5 30 00 00       	call   103260 <sleep>
  10017b:	eb be                	jmp    10013b <bread+0x1b>
  10017d:	8d 76 00             	lea    0x0(%esi),%esi
      goto loop;
    }
  }

  // Allocate fresh block.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
  100180:	8b 1d 10 9e 10 00    	mov    0x109e10,%ebx
  100186:	81 fb 04 9e 10 00    	cmp    $0x109e04,%ebx
  10018c:	75 0d                	jne    10019b <bread+0x7b>
  10018e:	eb 52                	jmp    1001e2 <bread+0xc2>
  100190:	8b 5b 0c             	mov    0xc(%ebx),%ebx
  100193:	81 fb 04 9e 10 00    	cmp    $0x109e04,%ebx
  100199:	74 47                	je     1001e2 <bread+0xc2>
    if((b->flags & B_BUSY) == 0){
  10019b:	f6 03 01             	testb  $0x1,(%ebx)
  10019e:	75 f0                	jne    100190 <bread+0x70>
      b->dev = dev;
  1001a0:	89 73 04             	mov    %esi,0x4(%ebx)
      b->sector = sector;
  1001a3:	89 7b 08             	mov    %edi,0x8(%ebx)
      b->flags = B_BUSY;
  1001a6:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
      release(&bcache.lock);
  1001ac:	c7 04 24 e0 88 10 00 	movl   $0x1088e0,(%esp)
  1001b3:	e8 28 39 00 00       	call   103ae0 <release>
bread(uint dev, uint sector)
{
  struct buf *b;

  b = bget(dev, sector);
  if(!(b->flags & B_VALID))
  1001b8:	f6 03 02             	testb  $0x2,(%ebx)
  1001bb:	75 08                	jne    1001c5 <bread+0xa5>
    iderw(b);
  1001bd:	89 1c 24             	mov    %ebx,(%esp)
  1001c0:	e8 8b 1d 00 00       	call   101f50 <iderw>
  return b;
}
  1001c5:	83 c4 1c             	add    $0x1c,%esp
  1001c8:	89 d8                	mov    %ebx,%eax
  1001ca:	5b                   	pop    %ebx
  1001cb:	5e                   	pop    %esi
  1001cc:	5f                   	pop    %edi
  1001cd:	5d                   	pop    %ebp
  1001ce:	c3                   	ret    
 loop:
  // Try for cached block.
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
    if(b->dev == dev && b->sector == sector){
      if(!(b->flags & B_BUSY)){
        b->flags |= B_BUSY;
  1001cf:	83 c8 01             	or     $0x1,%eax
  1001d2:	89 03                	mov    %eax,(%ebx)
        release(&bcache.lock);
  1001d4:	c7 04 24 e0 88 10 00 	movl   $0x1088e0,(%esp)
  1001db:	e8 00 39 00 00       	call   103ae0 <release>
  1001e0:	eb d6                	jmp    1001b8 <bread+0x98>
      b->flags = B_BUSY;
      release(&bcache.lock);
      return b;
    }
  }
  panic("bget: no buffers");
  1001e2:	c7 04 24 8e 64 10 00 	movl   $0x10648e,(%esp)
  1001e9:	e8 22 07 00 00       	call   100910 <panic>
  1001ee:	66 90                	xchg   %ax,%ax

001001f0 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
  1001f0:	55                   	push   %ebp
  1001f1:	89 e5                	mov    %esp,%ebp
  1001f3:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  initlock(&bcache.lock, "bcache");
  1001f6:	c7 44 24 04 9f 64 10 	movl   $0x10649f,0x4(%esp)
  1001fd:	00 
  1001fe:	c7 04 24 e0 88 10 00 	movl   $0x1088e0,(%esp)
  100205:	e8 96 37 00 00       	call   1039a0 <initlock>
  // head.next is most recently used.
  struct buf head;
} bcache;

void
binit(void)
  10020a:	ba 04 9e 10 00       	mov    $0x109e04,%edx
  10020f:	b8 14 89 10 00       	mov    $0x108914,%eax
  struct buf *b;

  initlock(&bcache.lock, "bcache");

  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  100214:	c7 05 10 9e 10 00 04 	movl   $0x109e04,0x109e10
  10021b:	9e 10 00 
  bcache.head.next = &bcache.head;
  10021e:	c7 05 14 9e 10 00 04 	movl   $0x109e04,0x109e14
  100225:	9e 10 00 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
    b->next = bcache.head.next;
  100228:	89 50 10             	mov    %edx,0x10(%eax)
    b->prev = &bcache.head;
  10022b:	c7 40 0c 04 9e 10 00 	movl   $0x109e04,0xc(%eax)
    b->dev = -1;
  100232:	c7 40 04 ff ff ff ff 	movl   $0xffffffff,0x4(%eax)
    bcache.head.next->prev = b;
  100239:	8b 15 14 9e 10 00    	mov    0x109e14,%edx
  10023f:	89 42 0c             	mov    %eax,0xc(%edx)
  initlock(&bcache.lock, "bcache");

  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
  100242:	89 c2                	mov    %eax,%edx
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  100244:	a3 14 9e 10 00       	mov    %eax,0x109e14
  initlock(&bcache.lock, "bcache");

  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
  100249:	05 18 02 00 00       	add    $0x218,%eax
  10024e:	3d 04 9e 10 00       	cmp    $0x109e04,%eax
  100253:	75 d3                	jne    100228 <binit+0x38>
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
  100255:	c9                   	leave  
  100256:	c3                   	ret    
  100257:	90                   	nop
  100258:	90                   	nop
  100259:	90                   	nop
  10025a:	90                   	nop
  10025b:	90                   	nop
  10025c:	90                   	nop
  10025d:	90                   	nop
  10025e:	90                   	nop
  10025f:	90                   	nop

00100260 <consoleinit>:
  return n;
}

void
consoleinit(void)
{
  100260:	55                   	push   %ebp
  100261:	89 e5                	mov    %esp,%ebp
  100263:	83 ec 18             	sub    $0x18,%esp
  initlock(&cons.lock, "console");
  100266:	c7 44 24 04 a6 64 10 	movl   $0x1064a6,0x4(%esp)
  10026d:	00 
  10026e:	c7 04 24 40 78 10 00 	movl   $0x107840,(%esp)
  100275:	e8 26 37 00 00       	call   1039a0 <initlock>
  initlock(&input.lock, "input");
  10027a:	c7 44 24 04 ae 64 10 	movl   $0x1064ae,0x4(%esp)
  100281:	00 
  100282:	c7 04 24 20 a0 10 00 	movl   $0x10a020,(%esp)
  100289:	e8 12 37 00 00       	call   1039a0 <initlock>

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  picenable(IRQ_KBD);
  10028e:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
consoleinit(void)
{
  initlock(&cons.lock, "console");
  initlock(&input.lock, "input");

  devsw[CONSOLE].write = consolewrite;
  100295:	c7 05 8c aa 10 00 40 	movl   $0x100440,0x10aa8c
  10029c:	04 10 00 
  devsw[CONSOLE].read = consoleread;
  10029f:	c7 05 88 aa 10 00 90 	movl   $0x100690,0x10aa88
  1002a6:	06 10 00 
  cons.locking = 1;
  1002a9:	c7 05 74 78 10 00 01 	movl   $0x1,0x107874
  1002b0:	00 00 00 

  picenable(IRQ_KBD);
  1002b3:	e8 e8 28 00 00       	call   102ba0 <picenable>
  ioapicenable(IRQ_KBD, 0);
  1002b8:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1002bf:	00 
  1002c0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  1002c7:	e8 84 1e 00 00       	call   102150 <ioapicenable>
}
  1002cc:	c9                   	leave  
  1002cd:	c3                   	ret    
  1002ce:	66 90                	xchg   %ax,%ax

001002d0 <consputc>:
  crt[pos] = ' ' | 0x0700;
}

void
consputc(int c)
{
  1002d0:	55                   	push   %ebp
  1002d1:	89 e5                	mov    %esp,%ebp
  1002d3:	57                   	push   %edi
  1002d4:	56                   	push   %esi
  1002d5:	89 c6                	mov    %eax,%esi
  1002d7:	53                   	push   %ebx
  1002d8:	83 ec 1c             	sub    $0x1c,%esp
  if(panicked){
  1002db:	83 3d 20 78 10 00 00 	cmpl   $0x0,0x107820
  1002e2:	74 03                	je     1002e7 <consputc+0x17>
}

static inline void
cli(void)
{
  asm volatile("cli");
  1002e4:	fa                   	cli    
  1002e5:	eb fe                	jmp    1002e5 <consputc+0x15>
    cli();
    for(;;)
      ;
  }

  if(c == BACKSPACE){
  1002e7:	3d 00 01 00 00       	cmp    $0x100,%eax
  1002ec:	0f 84 a0 00 00 00    	je     100392 <consputc+0xc2>
    uartputc('\b'); uartputc(' '); uartputc('\b');
  } else
    uartputc(c);
  1002f2:	89 04 24             	mov    %eax,(%esp)
  1002f5:	e8 96 4d 00 00       	call   105090 <uartputc>
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  1002fa:	b9 d4 03 00 00       	mov    $0x3d4,%ecx
  1002ff:	b8 0e 00 00 00       	mov    $0xe,%eax
  100304:	89 ca                	mov    %ecx,%edx
  100306:	ee                   	out    %al,(%dx)
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  100307:	bf d5 03 00 00       	mov    $0x3d5,%edi
  10030c:	89 fa                	mov    %edi,%edx
  10030e:	ec                   	in     (%dx),%al
{
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
  pos = inb(CRTPORT+1) << 8;
  10030f:	0f b6 d8             	movzbl %al,%ebx
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  100312:	89 ca                	mov    %ecx,%edx
  100314:	c1 e3 08             	shl    $0x8,%ebx
  100317:	b8 0f 00 00 00       	mov    $0xf,%eax
  10031c:	ee                   	out    %al,(%dx)
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  10031d:	89 fa                	mov    %edi,%edx
  10031f:	ec                   	in     (%dx),%al
  outb(CRTPORT, 15);
  pos |= inb(CRTPORT+1);
  100320:	0f b6 c0             	movzbl %al,%eax
  100323:	09 c3                	or     %eax,%ebx

  if(c == '\n')
  100325:	83 fe 0a             	cmp    $0xa,%esi
  100328:	0f 84 ee 00 00 00    	je     10041c <consputc+0x14c>
    pos += 80 - pos%80;
  else if(c == BACKSPACE){
  10032e:	81 fe 00 01 00 00    	cmp    $0x100,%esi
  100334:	0f 84 cb 00 00 00    	je     100405 <consputc+0x135>
    if(pos > 0) --pos;
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
  10033a:	66 81 e6 ff 00       	and    $0xff,%si
  10033f:	66 81 ce 00 07       	or     $0x700,%si
  100344:	66 89 b4 1b 00 80 0b 	mov    %si,0xb8000(%ebx,%ebx,1)
  10034b:	00 
  10034c:	83 c3 01             	add    $0x1,%ebx
  
  if((pos/80) >= 24){  // Scroll up.
  10034f:	81 fb 7f 07 00 00    	cmp    $0x77f,%ebx
  100355:	8d 8c 1b 00 80 0b 00 	lea    0xb8000(%ebx,%ebx,1),%ecx
  10035c:	7f 5d                	jg     1003bb <consputc+0xeb>
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  10035e:	be d4 03 00 00       	mov    $0x3d4,%esi
  100363:	b8 0e 00 00 00       	mov    $0xe,%eax
  100368:	89 f2                	mov    %esi,%edx
  10036a:	ee                   	out    %al,(%dx)
  10036b:	bf d5 03 00 00       	mov    $0x3d5,%edi
  100370:	89 d8                	mov    %ebx,%eax
  100372:	c1 f8 08             	sar    $0x8,%eax
  100375:	89 fa                	mov    %edi,%edx
  100377:	ee                   	out    %al,(%dx)
  100378:	b8 0f 00 00 00       	mov    $0xf,%eax
  10037d:	89 f2                	mov    %esi,%edx
  10037f:	ee                   	out    %al,(%dx)
  100380:	89 d8                	mov    %ebx,%eax
  100382:	89 fa                	mov    %edi,%edx
  100384:	ee                   	out    %al,(%dx)
  
  outb(CRTPORT, 14);
  outb(CRTPORT+1, pos>>8);
  outb(CRTPORT, 15);
  outb(CRTPORT+1, pos);
  crt[pos] = ' ' | 0x0700;
  100385:	66 c7 01 20 07       	movw   $0x720,(%ecx)
  if(c == BACKSPACE){
    uartputc('\b'); uartputc(' '); uartputc('\b');
  } else
    uartputc(c);
  cgaputc(c);
}
  10038a:	83 c4 1c             	add    $0x1c,%esp
  10038d:	5b                   	pop    %ebx
  10038e:	5e                   	pop    %esi
  10038f:	5f                   	pop    %edi
  100390:	5d                   	pop    %ebp
  100391:	c3                   	ret    
    for(;;)
      ;
  }

  if(c == BACKSPACE){
    uartputc('\b'); uartputc(' '); uartputc('\b');
  100392:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  100399:	e8 f2 4c 00 00       	call   105090 <uartputc>
  10039e:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  1003a5:	e8 e6 4c 00 00       	call   105090 <uartputc>
  1003aa:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  1003b1:	e8 da 4c 00 00       	call   105090 <uartputc>
  1003b6:	e9 3f ff ff ff       	jmp    1002fa <consputc+0x2a>
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
  
  if((pos/80) >= 24){  // Scroll up.
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
    pos -= 80;
  1003bb:	83 eb 50             	sub    $0x50,%ebx
    if(pos > 0) --pos;
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
  
  if((pos/80) >= 24){  // Scroll up.
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
  1003be:	c7 44 24 08 60 0e 00 	movl   $0xe60,0x8(%esp)
  1003c5:	00 
    pos -= 80;
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
  1003c6:	8d b4 1b 00 80 0b 00 	lea    0xb8000(%ebx,%ebx,1),%esi
    if(pos > 0) --pos;
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
  
  if((pos/80) >= 24){  // Scroll up.
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
  1003cd:	c7 44 24 04 a0 80 0b 	movl   $0xb80a0,0x4(%esp)
  1003d4:	00 
  1003d5:	c7 04 24 00 80 0b 00 	movl   $0xb8000,(%esp)
  1003dc:	e8 6f 38 00 00       	call   103c50 <memmove>
    pos -= 80;
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
  1003e1:	b8 80 07 00 00       	mov    $0x780,%eax
  1003e6:	29 d8                	sub    %ebx,%eax
  1003e8:	01 c0                	add    %eax,%eax
  1003ea:	89 44 24 08          	mov    %eax,0x8(%esp)
  1003ee:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1003f5:	00 
  1003f6:	89 34 24             	mov    %esi,(%esp)
  1003f9:	e8 d2 37 00 00       	call   103bd0 <memset>
  outb(CRTPORT+1, pos);
  crt[pos] = ' ' | 0x0700;
}

void
consputc(int c)
  1003fe:	89 f1                	mov    %esi,%ecx
  100400:	e9 59 ff ff ff       	jmp    10035e <consputc+0x8e>
  pos |= inb(CRTPORT+1);

  if(c == '\n')
    pos += 80 - pos%80;
  else if(c == BACKSPACE){
    if(pos > 0) --pos;
  100405:	85 db                	test   %ebx,%ebx
  100407:	8d 8c 1b 00 80 0b 00 	lea    0xb8000(%ebx,%ebx,1),%ecx
  10040e:	0f 8e 4a ff ff ff    	jle    10035e <consputc+0x8e>
  100414:	83 eb 01             	sub    $0x1,%ebx
  100417:	e9 33 ff ff ff       	jmp    10034f <consputc+0x7f>
  pos = inb(CRTPORT+1) << 8;
  outb(CRTPORT, 15);
  pos |= inb(CRTPORT+1);

  if(c == '\n')
    pos += 80 - pos%80;
  10041c:	89 da                	mov    %ebx,%edx
  10041e:	89 d8                	mov    %ebx,%eax
  100420:	b9 50 00 00 00       	mov    $0x50,%ecx
  100425:	83 c3 50             	add    $0x50,%ebx
  100428:	c1 fa 1f             	sar    $0x1f,%edx
  10042b:	f7 f9                	idiv   %ecx
  10042d:	29 d3                	sub    %edx,%ebx
  10042f:	e9 1b ff ff ff       	jmp    10034f <consputc+0x7f>
  100434:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  10043a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00100440 <consolewrite>:
  return target - n;
}

int
consolewrite(struct inode *ip, char *buf, int n)
{
  100440:	55                   	push   %ebp
  100441:	89 e5                	mov    %esp,%ebp
  100443:	57                   	push   %edi
  100444:	56                   	push   %esi
  100445:	53                   	push   %ebx
  100446:	83 ec 1c             	sub    $0x1c,%esp
  int i;

  iunlock(ip);
  100449:	8b 45 08             	mov    0x8(%ebp),%eax
  return target - n;
}

int
consolewrite(struct inode *ip, char *buf, int n)
{
  10044c:	8b 75 10             	mov    0x10(%ebp),%esi
  10044f:	8b 7d 0c             	mov    0xc(%ebp),%edi
  int i;

  iunlock(ip);
  100452:	89 04 24             	mov    %eax,(%esp)
  100455:	e8 26 13 00 00       	call   101780 <iunlock>
  acquire(&cons.lock);
  10045a:	c7 04 24 40 78 10 00 	movl   $0x107840,(%esp)
  100461:	e8 ca 36 00 00       	call   103b30 <acquire>
  for(i = 0; i < n; i++)
  100466:	85 f6                	test   %esi,%esi
  100468:	7e 16                	jle    100480 <consolewrite+0x40>
  10046a:	31 db                	xor    %ebx,%ebx
  10046c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    consputc(buf[i] & 0xff);
  100470:	0f b6 04 1f          	movzbl (%edi,%ebx,1),%eax
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
  100474:	83 c3 01             	add    $0x1,%ebx
    consputc(buf[i] & 0xff);
  100477:	e8 54 fe ff ff       	call   1002d0 <consputc>
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
  10047c:	39 de                	cmp    %ebx,%esi
  10047e:	7f f0                	jg     100470 <consolewrite+0x30>
    consputc(buf[i] & 0xff);
  release(&cons.lock);
  100480:	c7 04 24 40 78 10 00 	movl   $0x107840,(%esp)
  100487:	e8 54 36 00 00       	call   103ae0 <release>
  ilock(ip);
  10048c:	8b 45 08             	mov    0x8(%ebp),%eax
  10048f:	89 04 24             	mov    %eax,(%esp)
  100492:	e8 29 17 00 00       	call   101bc0 <ilock>

  return n;
}
  100497:	83 c4 1c             	add    $0x1c,%esp
  10049a:	89 f0                	mov    %esi,%eax
  10049c:	5b                   	pop    %ebx
  10049d:	5e                   	pop    %esi
  10049e:	5f                   	pop    %edi
  10049f:	5d                   	pop    %ebp
  1004a0:	c3                   	ret    
  1004a1:	eb 0d                	jmp    1004b0 <printint>
  1004a3:	90                   	nop
  1004a4:	90                   	nop
  1004a5:	90                   	nop
  1004a6:	90                   	nop
  1004a7:	90                   	nop
  1004a8:	90                   	nop
  1004a9:	90                   	nop
  1004aa:	90                   	nop
  1004ab:	90                   	nop
  1004ac:	90                   	nop
  1004ad:	90                   	nop
  1004ae:	90                   	nop
  1004af:	90                   	nop

001004b0 <printint>:
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
  1004b0:	55                   	push   %ebp
  1004b1:	89 e5                	mov    %esp,%ebp
  1004b3:	57                   	push   %edi
  1004b4:	56                   	push   %esi
  1004b5:	89 d6                	mov    %edx,%esi
  1004b7:	53                   	push   %ebx
  1004b8:	83 ec 1c             	sub    $0x1c,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
  1004bb:	85 c9                	test   %ecx,%ecx
  1004bd:	74 04                	je     1004c3 <printint+0x13>
  1004bf:	85 c0                	test   %eax,%eax
  1004c1:	78 55                	js     100518 <printint+0x68>
    x = -xx;
  else
    x = xx;
  1004c3:	31 ff                	xor    %edi,%edi
  1004c5:	31 c9                	xor    %ecx,%ecx
  1004c7:	8d 5d d8             	lea    -0x28(%ebp),%ebx
  1004ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

  i = 0;
  do{
    buf[i++] = digits[x % base];
  1004d0:	31 d2                	xor    %edx,%edx
  1004d2:	f7 f6                	div    %esi
  1004d4:	0f b6 92 ce 64 10 00 	movzbl 0x1064ce(%edx),%edx
  1004db:	88 14 0b             	mov    %dl,(%ebx,%ecx,1)
  1004de:	83 c1 01             	add    $0x1,%ecx
  }while((x /= base) != 0);
  1004e1:	85 c0                	test   %eax,%eax
  1004e3:	75 eb                	jne    1004d0 <printint+0x20>

  if(sign)
  1004e5:	85 ff                	test   %edi,%edi
  1004e7:	74 08                	je     1004f1 <printint+0x41>
    buf[i++] = '-';
  1004e9:	c6 44 0d d8 2d       	movb   $0x2d,-0x28(%ebp,%ecx,1)
  1004ee:	83 c1 01             	add    $0x1,%ecx

  while(--i >= 0)
  1004f1:	8d 71 ff             	lea    -0x1(%ecx),%esi
  1004f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    consputc(buf[i]);
  1004f8:	0f be 04 33          	movsbl (%ebx,%esi,1),%eax
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
  1004fc:	83 ee 01             	sub    $0x1,%esi
    consputc(buf[i]);
  1004ff:	e8 cc fd ff ff       	call   1002d0 <consputc>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
  100504:	83 fe ff             	cmp    $0xffffffff,%esi
  100507:	75 ef                	jne    1004f8 <printint+0x48>
    consputc(buf[i]);
}
  100509:	83 c4 1c             	add    $0x1c,%esp
  10050c:	5b                   	pop    %ebx
  10050d:	5e                   	pop    %esi
  10050e:	5f                   	pop    %edi
  10050f:	5d                   	pop    %ebp
  100510:	c3                   	ret    
  100511:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
    x = -xx;
  100518:	f7 d8                	neg    %eax
  10051a:	bf 01 00 00 00       	mov    $0x1,%edi
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
  10051f:	eb a4                	jmp    1004c5 <printint+0x15>
  100521:	eb 0d                	jmp    100530 <cprintf>
  100523:	90                   	nop
  100524:	90                   	nop
  100525:	90                   	nop
  100526:	90                   	nop
  100527:	90                   	nop
  100528:	90                   	nop
  100529:	90                   	nop
  10052a:	90                   	nop
  10052b:	90                   	nop
  10052c:	90                   	nop
  10052d:	90                   	nop
  10052e:	90                   	nop
  10052f:	90                   	nop

00100530 <cprintf>:
}

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
  100530:	55                   	push   %ebp
  100531:	89 e5                	mov    %esp,%ebp
  100533:	57                   	push   %edi
  100534:	56                   	push   %esi
  100535:	53                   	push   %ebx
  100536:	83 ec 2c             	sub    $0x2c,%esp
  int i, c, state, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
  100539:	8b 3d 74 78 10 00    	mov    0x107874,%edi
  if(locking)
  10053f:	85 ff                	test   %edi,%edi
  100541:	0f 85 29 01 00 00    	jne    100670 <cprintf+0x140>
    acquire(&cons.lock);

  argp = (uint*)(void*)(&fmt + 1);
  state = 0;
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
  100547:	8b 4d 08             	mov    0x8(%ebp),%ecx
  10054a:	0f b6 01             	movzbl (%ecx),%eax
  10054d:	85 c0                	test   %eax,%eax
  10054f:	0f 84 8b 00 00 00    	je     1005e0 <cprintf+0xb0>

  locking = cons.locking;
  if(locking)
    acquire(&cons.lock);

  argp = (uint*)(void*)(&fmt + 1);
  100555:	8d 75 0c             	lea    0xc(%ebp),%esi
  100558:	31 db                	xor    %ebx,%ebx
  10055a:	eb 3f                	jmp    10059b <cprintf+0x6b>
  10055c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
    switch(c){
  100560:	83 fa 25             	cmp    $0x25,%edx
  100563:	0f 84 af 00 00 00    	je     100618 <cprintf+0xe8>
  100569:	83 fa 64             	cmp    $0x64,%edx
  10056c:	0f 84 86 00 00 00    	je     1005f8 <cprintf+0xc8>
    case '%':
      consputc('%');
      break;
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
  100572:	b8 25 00 00 00       	mov    $0x25,%eax
  100577:	89 55 e0             	mov    %edx,-0x20(%ebp)
  10057a:	e8 51 fd ff ff       	call   1002d0 <consputc>
      consputc(c);
  10057f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  100582:	89 d0                	mov    %edx,%eax
  100584:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  100588:	e8 43 fd ff ff       	call   1002d0 <consputc>
  10058d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  if(locking)
    acquire(&cons.lock);

  argp = (uint*)(void*)(&fmt + 1);
  state = 0;
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
  100590:	83 c3 01             	add    $0x1,%ebx
  100593:	0f b6 04 19          	movzbl (%ecx,%ebx,1),%eax
  100597:	85 c0                	test   %eax,%eax
  100599:	74 45                	je     1005e0 <cprintf+0xb0>
    if(c != '%'){
  10059b:	83 f8 25             	cmp    $0x25,%eax
  10059e:	75 e8                	jne    100588 <cprintf+0x58>
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
  1005a0:	83 c3 01             	add    $0x1,%ebx
  1005a3:	0f b6 14 19          	movzbl (%ecx,%ebx,1),%edx
    if(c == 0)
  1005a7:	85 d2                	test   %edx,%edx
  1005a9:	74 35                	je     1005e0 <cprintf+0xb0>
      break;
    switch(c){
  1005ab:	83 fa 70             	cmp    $0x70,%edx
  1005ae:	74 0c                	je     1005bc <cprintf+0x8c>
  1005b0:	7e ae                	jle    100560 <cprintf+0x30>
  1005b2:	83 fa 73             	cmp    $0x73,%edx
  1005b5:	74 79                	je     100630 <cprintf+0x100>
  1005b7:	83 fa 78             	cmp    $0x78,%edx
  1005ba:	75 b6                	jne    100572 <cprintf+0x42>
    case 'd':
      printint(*argp++, 10, 1);
      break;
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
  1005bc:	8b 06                	mov    (%esi),%eax
  1005be:	31 c9                	xor    %ecx,%ecx
  1005c0:	ba 10 00 00 00       	mov    $0x10,%edx
  if(locking)
    acquire(&cons.lock);

  argp = (uint*)(void*)(&fmt + 1);
  state = 0;
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
  1005c5:	83 c3 01             	add    $0x1,%ebx
    case 'd':
      printint(*argp++, 10, 1);
      break;
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
  1005c8:	83 c6 04             	add    $0x4,%esi
  1005cb:	e8 e0 fe ff ff       	call   1004b0 <printint>
  1005d0:	8b 4d 08             	mov    0x8(%ebp),%ecx
  if(locking)
    acquire(&cons.lock);

  argp = (uint*)(void*)(&fmt + 1);
  state = 0;
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
  1005d3:	0f b6 04 19          	movzbl (%ecx,%ebx,1),%eax
  1005d7:	85 c0                	test   %eax,%eax
  1005d9:	75 c0                	jne    10059b <cprintf+0x6b>
  1005db:	90                   	nop
  1005dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      consputc(c);
      break;
    }
  }

  if(locking)
  1005e0:	85 ff                	test   %edi,%edi
  1005e2:	74 0c                	je     1005f0 <cprintf+0xc0>
    release(&cons.lock);
  1005e4:	c7 04 24 40 78 10 00 	movl   $0x107840,(%esp)
  1005eb:	e8 f0 34 00 00       	call   103ae0 <release>
}
  1005f0:	83 c4 2c             	add    $0x2c,%esp
  1005f3:	5b                   	pop    %ebx
  1005f4:	5e                   	pop    %esi
  1005f5:	5f                   	pop    %edi
  1005f6:	5d                   	pop    %ebp
  1005f7:	c3                   	ret    
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
    switch(c){
    case 'd':
      printint(*argp++, 10, 1);
  1005f8:	8b 06                	mov    (%esi),%eax
  1005fa:	b9 01 00 00 00       	mov    $0x1,%ecx
  1005ff:	ba 0a 00 00 00       	mov    $0xa,%edx
  100604:	83 c6 04             	add    $0x4,%esi
  100607:	e8 a4 fe ff ff       	call   1004b0 <printint>
  10060c:	8b 4d 08             	mov    0x8(%ebp),%ecx
      break;
  10060f:	e9 7c ff ff ff       	jmp    100590 <cprintf+0x60>
  100614:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        s = "(null)";
      for(; *s; s++)
        consputc(*s);
      break;
    case '%':
      consputc('%');
  100618:	b8 25 00 00 00       	mov    $0x25,%eax
  10061d:	e8 ae fc ff ff       	call   1002d0 <consputc>
  100622:	8b 4d 08             	mov    0x8(%ebp),%ecx
      break;
  100625:	e9 66 ff ff ff       	jmp    100590 <cprintf+0x60>
  10062a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
  100630:	8b 16                	mov    (%esi),%edx
  100632:	b8 b4 64 10 00       	mov    $0x1064b4,%eax
  100637:	83 c6 04             	add    $0x4,%esi
  10063a:	85 d2                	test   %edx,%edx
  10063c:	0f 44 d0             	cmove  %eax,%edx
        s = "(null)";
      for(; *s; s++)
  10063f:	0f b6 02             	movzbl (%edx),%eax
  100642:	84 c0                	test   %al,%al
  100644:	0f 84 46 ff ff ff    	je     100590 <cprintf+0x60>
  10064a:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  10064d:	89 d3                	mov    %edx,%ebx
  10064f:	90                   	nop
        consputc(*s);
  100650:	0f be c0             	movsbl %al,%eax
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
  100653:	83 c3 01             	add    $0x1,%ebx
        consputc(*s);
  100656:	e8 75 fc ff ff       	call   1002d0 <consputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
  10065b:	0f b6 03             	movzbl (%ebx),%eax
  10065e:	84 c0                	test   %al,%al
  100660:	75 ee                	jne    100650 <cprintf+0x120>
  100662:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
  100665:	8b 4d 08             	mov    0x8(%ebp),%ecx
  100668:	e9 23 ff ff ff       	jmp    100590 <cprintf+0x60>
  10066d:	8d 76 00             	lea    0x0(%esi),%esi
  uint *argp;
  char *s;

  locking = cons.locking;
  if(locking)
    acquire(&cons.lock);
  100670:	c7 04 24 40 78 10 00 	movl   $0x107840,(%esp)
  100677:	e8 b4 34 00 00       	call   103b30 <acquire>
  10067c:	e9 c6 fe ff ff       	jmp    100547 <cprintf+0x17>
  100681:	eb 0d                	jmp    100690 <consoleread>
  100683:	90                   	nop
  100684:	90                   	nop
  100685:	90                   	nop
  100686:	90                   	nop
  100687:	90                   	nop
  100688:	90                   	nop
  100689:	90                   	nop
  10068a:	90                   	nop
  10068b:	90                   	nop
  10068c:	90                   	nop
  10068d:	90                   	nop
  10068e:	90                   	nop
  10068f:	90                   	nop

00100690 <consoleread>:
  release(&input.lock);
}

int
consoleread(struct inode *ip, char *dst, int n)
{
  100690:	55                   	push   %ebp
  100691:	89 e5                	mov    %esp,%ebp
  100693:	57                   	push   %edi
  100694:	56                   	push   %esi
  100695:	53                   	push   %ebx
  100696:	83 ec 3c             	sub    $0x3c,%esp
  100699:	8b 5d 10             	mov    0x10(%ebp),%ebx
  10069c:	8b 7d 08             	mov    0x8(%ebp),%edi
  10069f:	8b 75 0c             	mov    0xc(%ebp),%esi
  uint target;
  int c;

  iunlock(ip);
  1006a2:	89 3c 24             	mov    %edi,(%esp)
  1006a5:	e8 d6 10 00 00       	call   101780 <iunlock>
  target = n;
  1006aa:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  acquire(&input.lock);
  1006ad:	c7 04 24 20 a0 10 00 	movl   $0x10a020,(%esp)
  1006b4:	e8 77 34 00 00       	call   103b30 <acquire>
  while(n > 0){
  1006b9:	85 db                	test   %ebx,%ebx
  1006bb:	7f 2c                	jg     1006e9 <consoleread+0x59>
  1006bd:	e9 c0 00 00 00       	jmp    100782 <consoleread+0xf2>
  1006c2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    while(input.r == input.w){
      if(proc->killed){
  1006c8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  1006ce:	8b 40 24             	mov    0x24(%eax),%eax
  1006d1:	85 c0                	test   %eax,%eax
  1006d3:	75 5b                	jne    100730 <consoleread+0xa0>
        release(&input.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &input.lock);
  1006d5:	c7 44 24 04 20 a0 10 	movl   $0x10a020,0x4(%esp)
  1006dc:	00 
  1006dd:	c7 04 24 d4 a0 10 00 	movl   $0x10a0d4,(%esp)
  1006e4:	e8 77 2b 00 00       	call   103260 <sleep>

  iunlock(ip);
  target = n;
  acquire(&input.lock);
  while(n > 0){
    while(input.r == input.w){
  1006e9:	a1 d4 a0 10 00       	mov    0x10a0d4,%eax
  1006ee:	3b 05 d8 a0 10 00    	cmp    0x10a0d8,%eax
  1006f4:	74 d2                	je     1006c8 <consoleread+0x38>
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &input.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
  1006f6:	89 c2                	mov    %eax,%edx
  1006f8:	83 e2 7f             	and    $0x7f,%edx
  1006fb:	0f b6 8a 54 a0 10 00 	movzbl 0x10a054(%edx),%ecx
  100702:	0f be d1             	movsbl %cl,%edx
  100705:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  100708:	8d 50 01             	lea    0x1(%eax),%edx
    if(c == C('D')){  // EOF
  10070b:	83 7d d4 04          	cmpl   $0x4,-0x2c(%ebp)
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &input.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
  10070f:	89 15 d4 a0 10 00    	mov    %edx,0x10a0d4
    if(c == C('D')){  // EOF
  100715:	74 3a                	je     100751 <consoleread+0xc1>
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
    }
    *dst++ = c;
  100717:	88 0e                	mov    %cl,(%esi)
    --n;
  100719:	83 eb 01             	sub    $0x1,%ebx
    if(c == '\n')
  10071c:	83 7d d4 0a          	cmpl   $0xa,-0x2c(%ebp)
  100720:	74 39                	je     10075b <consoleread+0xcb>
  int c;

  iunlock(ip);
  target = n;
  acquire(&input.lock);
  while(n > 0){
  100722:	85 db                	test   %ebx,%ebx
  100724:	7e 35                	jle    10075b <consoleread+0xcb>
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
    }
    *dst++ = c;
  100726:	83 c6 01             	add    $0x1,%esi
  100729:	eb be                	jmp    1006e9 <consoleread+0x59>
  10072b:	90                   	nop
  10072c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  target = n;
  acquire(&input.lock);
  while(n > 0){
    while(input.r == input.w){
      if(proc->killed){
        release(&input.lock);
  100730:	c7 04 24 20 a0 10 00 	movl   $0x10a020,(%esp)
  100737:	e8 a4 33 00 00       	call   103ae0 <release>
        ilock(ip);
  10073c:	89 3c 24             	mov    %edi,(%esp)
  10073f:	e8 7c 14 00 00       	call   101bc0 <ilock>
  }
  release(&input.lock);
  ilock(ip);

  return target - n;
}
  100744:	83 c4 3c             	add    $0x3c,%esp
  acquire(&input.lock);
  while(n > 0){
    while(input.r == input.w){
      if(proc->killed){
        release(&input.lock);
        ilock(ip);
  100747:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  release(&input.lock);
  ilock(ip);

  return target - n;
}
  10074c:	5b                   	pop    %ebx
  10074d:	5e                   	pop    %esi
  10074e:	5f                   	pop    %edi
  10074f:	5d                   	pop    %ebp
  100750:	c3                   	ret    
      }
      sleep(&input.r, &input.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
    if(c == C('D')){  // EOF
      if(n < target){
  100751:	39 5d e4             	cmp    %ebx,-0x1c(%ebp)
  100754:	76 05                	jbe    10075b <consoleread+0xcb>
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
  100756:	a3 d4 a0 10 00       	mov    %eax,0x10a0d4
  10075b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  10075e:	29 d8                	sub    %ebx,%eax
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
  }
  release(&input.lock);
  100760:	89 45 e0             	mov    %eax,-0x20(%ebp)
  100763:	c7 04 24 20 a0 10 00 	movl   $0x10a020,(%esp)
  10076a:	e8 71 33 00 00       	call   103ae0 <release>
  ilock(ip);
  10076f:	89 3c 24             	mov    %edi,(%esp)
  100772:	e8 49 14 00 00       	call   101bc0 <ilock>
  100777:	8b 45 e0             	mov    -0x20(%ebp),%eax

  return target - n;
}
  10077a:	83 c4 3c             	add    $0x3c,%esp
  10077d:	5b                   	pop    %ebx
  10077e:	5e                   	pop    %esi
  10077f:	5f                   	pop    %edi
  100780:	5d                   	pop    %ebp
  100781:	c3                   	ret    
  iunlock(ip);
  target = n;
  acquire(&input.lock);
  while(n > 0){
    while(input.r == input.w){
      if(proc->killed){
  100782:	31 c0                	xor    %eax,%eax
  100784:	eb da                	jmp    100760 <consoleread+0xd0>
  100786:	8d 76 00             	lea    0x0(%esi),%esi
  100789:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00100790 <consoleintr>:

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
  100790:	55                   	push   %ebp
  100791:	89 e5                	mov    %esp,%ebp
  100793:	57                   	push   %edi
      }
      break;
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
        c = (c == '\r') ? '\n' : c;
        input.buf[input.e++ % INPUT_BUF] = c;
  100794:	bf 50 a0 10 00       	mov    $0x10a050,%edi

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
  100799:	56                   	push   %esi
  10079a:	53                   	push   %ebx
  10079b:	83 ec 1c             	sub    $0x1c,%esp
  10079e:	8b 75 08             	mov    0x8(%ebp),%esi
  int c;

  acquire(&input.lock);
  1007a1:	c7 04 24 20 a0 10 00 	movl   $0x10a020,(%esp)
  1007a8:	e8 83 33 00 00       	call   103b30 <acquire>
  1007ad:	8d 76 00             	lea    0x0(%esi),%esi
  while((c = getc()) >= 0){
  1007b0:	ff d6                	call   *%esi
  1007b2:	85 c0                	test   %eax,%eax
  1007b4:	89 c3                	mov    %eax,%ebx
  1007b6:	0f 88 9c 00 00 00    	js     100858 <consoleintr+0xc8>
    switch(c){
  1007bc:	83 fb 10             	cmp    $0x10,%ebx
  1007bf:	90                   	nop
  1007c0:	0f 84 1a 01 00 00    	je     1008e0 <consoleintr+0x150>
  1007c6:	0f 8f a4 00 00 00    	jg     100870 <consoleintr+0xe0>
  1007cc:	83 fb 08             	cmp    $0x8,%ebx
  1007cf:	90                   	nop
  1007d0:	0f 84 a8 00 00 00    	je     10087e <consoleintr+0xee>
        input.e--;
        consputc(BACKSPACE);
      }
      break;
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
  1007d6:	85 db                	test   %ebx,%ebx
  1007d8:	74 d6                	je     1007b0 <consoleintr+0x20>
  1007da:	a1 dc a0 10 00       	mov    0x10a0dc,%eax
  1007df:	89 c2                	mov    %eax,%edx
  1007e1:	2b 15 d4 a0 10 00    	sub    0x10a0d4,%edx
  1007e7:	83 fa 7f             	cmp    $0x7f,%edx
  1007ea:	77 c4                	ja     1007b0 <consoleintr+0x20>
        c = (c == '\r') ? '\n' : c;
  1007ec:	83 fb 0d             	cmp    $0xd,%ebx
  1007ef:	0f 84 f5 00 00 00    	je     1008ea <consoleintr+0x15a>
        input.buf[input.e++ % INPUT_BUF] = c;
  1007f5:	89 c2                	mov    %eax,%edx
  1007f7:	83 c0 01             	add    $0x1,%eax
  1007fa:	83 e2 7f             	and    $0x7f,%edx
  1007fd:	88 5c 17 04          	mov    %bl,0x4(%edi,%edx,1)
  100801:	a3 dc a0 10 00       	mov    %eax,0x10a0dc
        consputc(c);
  100806:	89 d8                	mov    %ebx,%eax
  100808:	e8 c3 fa ff ff       	call   1002d0 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
  10080d:	83 fb 04             	cmp    $0x4,%ebx
  100810:	0f 84 f0 00 00 00    	je     100906 <consoleintr+0x176>
  100816:	83 fb 0a             	cmp    $0xa,%ebx
  100819:	0f 84 e7 00 00 00    	je     100906 <consoleintr+0x176>
  10081f:	8b 15 d4 a0 10 00    	mov    0x10a0d4,%edx
  100825:	a1 dc a0 10 00       	mov    0x10a0dc,%eax
  10082a:	83 ea 80             	sub    $0xffffff80,%edx
  10082d:	39 d0                	cmp    %edx,%eax
  10082f:	0f 85 7b ff ff ff    	jne    1007b0 <consoleintr+0x20>
          input.w = input.e;
  100835:	a3 d8 a0 10 00       	mov    %eax,0x10a0d8
          wakeup(&input.r);
  10083a:	c7 04 24 d4 a0 10 00 	movl   $0x10a0d4,(%esp)
  100841:	e8 fa 28 00 00       	call   103140 <wakeup>
consoleintr(int (*getc)(void))
{
  int c;

  acquire(&input.lock);
  while((c = getc()) >= 0){
  100846:	ff d6                	call   *%esi
  100848:	85 c0                	test   %eax,%eax
  10084a:	89 c3                	mov    %eax,%ebx
  10084c:	0f 89 6a ff ff ff    	jns    1007bc <consoleintr+0x2c>
  100852:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        }
      }
      break;
    }
  }
  release(&input.lock);
  100858:	c7 45 08 20 a0 10 00 	movl   $0x10a020,0x8(%ebp)
}
  10085f:	83 c4 1c             	add    $0x1c,%esp
  100862:	5b                   	pop    %ebx
  100863:	5e                   	pop    %esi
  100864:	5f                   	pop    %edi
  100865:	5d                   	pop    %ebp
        }
      }
      break;
    }
  }
  release(&input.lock);
  100866:	e9 75 32 00 00       	jmp    103ae0 <release>
  10086b:	90                   	nop
  10086c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  int c;

  acquire(&input.lock);
  while((c = getc()) >= 0){
    switch(c){
  100870:	83 fb 15             	cmp    $0x15,%ebx
  100873:	74 57                	je     1008cc <consoleintr+0x13c>
  100875:	83 fb 7f             	cmp    $0x7f,%ebx
  100878:	0f 85 58 ff ff ff    	jne    1007d6 <consoleintr+0x46>
        input.e--;
        consputc(BACKSPACE);
      }
      break;
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
  10087e:	a1 dc a0 10 00       	mov    0x10a0dc,%eax
  100883:	3b 05 d8 a0 10 00    	cmp    0x10a0d8,%eax
  100889:	0f 84 21 ff ff ff    	je     1007b0 <consoleintr+0x20>
        input.e--;
  10088f:	83 e8 01             	sub    $0x1,%eax
  100892:	a3 dc a0 10 00       	mov    %eax,0x10a0dc
        consputc(BACKSPACE);
  100897:	b8 00 01 00 00       	mov    $0x100,%eax
  10089c:	e8 2f fa ff ff       	call   1002d0 <consputc>
  1008a1:	e9 0a ff ff ff       	jmp    1007b0 <consoleintr+0x20>
  1008a6:	66 90                	xchg   %ax,%ax
    case C('P'):  // Process listing.
      procdump();
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
  1008a8:	83 e8 01             	sub    $0x1,%eax
  1008ab:	89 c2                	mov    %eax,%edx
  1008ad:	83 e2 7f             	and    $0x7f,%edx
    switch(c){
    case C('P'):  // Process listing.
      procdump();
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
  1008b0:	80 ba 54 a0 10 00 0a 	cmpb   $0xa,0x10a054(%edx)
  1008b7:	0f 84 f3 fe ff ff    	je     1007b0 <consoleintr+0x20>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
  1008bd:	a3 dc a0 10 00       	mov    %eax,0x10a0dc
        consputc(BACKSPACE);
  1008c2:	b8 00 01 00 00       	mov    $0x100,%eax
  1008c7:	e8 04 fa ff ff       	call   1002d0 <consputc>
    switch(c){
    case C('P'):  // Process listing.
      procdump();
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
  1008cc:	a1 dc a0 10 00       	mov    0x10a0dc,%eax
  1008d1:	3b 05 d8 a0 10 00    	cmp    0x10a0d8,%eax
  1008d7:	75 cf                	jne    1008a8 <consoleintr+0x118>
  1008d9:	e9 d2 fe ff ff       	jmp    1007b0 <consoleintr+0x20>
  1008de:	66 90                	xchg   %ax,%ax

  acquire(&input.lock);
  while((c = getc()) >= 0){
    switch(c){
    case C('P'):  // Process listing.
      procdump();
  1008e0:	e8 fb 26 00 00       	call   102fe0 <procdump>
      break;
  1008e5:	e9 c6 fe ff ff       	jmp    1007b0 <consoleintr+0x20>
      }
      break;
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
        c = (c == '\r') ? '\n' : c;
        input.buf[input.e++ % INPUT_BUF] = c;
  1008ea:	89 c2                	mov    %eax,%edx
  1008ec:	83 c0 01             	add    $0x1,%eax
  1008ef:	83 e2 7f             	and    $0x7f,%edx
  1008f2:	c6 44 17 04 0a       	movb   $0xa,0x4(%edi,%edx,1)
  1008f7:	a3 dc a0 10 00       	mov    %eax,0x10a0dc
        consputc(c);
  1008fc:	b8 0a 00 00 00       	mov    $0xa,%eax
  100901:	e8 ca f9 ff ff       	call   1002d0 <consputc>
  100906:	a1 dc a0 10 00       	mov    0x10a0dc,%eax
  10090b:	e9 25 ff ff ff       	jmp    100835 <consoleintr+0xa5>

00100910 <panic>:
    release(&cons.lock);
}

void
panic(char *s)
{
  100910:	55                   	push   %ebp
  100911:	89 e5                	mov    %esp,%ebp
  100913:	56                   	push   %esi
  100914:	53                   	push   %ebx
  100915:	83 ec 40             	sub    $0x40,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
  100918:	fa                   	cli    
  int i;
  uint pcs[10];
  
  cli();
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  100919:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, pcs);
  10091f:	8d 75 d0             	lea    -0x30(%ebp),%esi
  100922:	31 db                	xor    %ebx,%ebx
{
  int i;
  uint pcs[10];
  
  cli();
  cons.locking = 0;
  100924:	c7 05 74 78 10 00 00 	movl   $0x0,0x107874
  10092b:	00 00 00 
  cprintf("cpu%d: panic: ", cpu->id);
  10092e:	0f b6 00             	movzbl (%eax),%eax
  100931:	c7 04 24 bb 64 10 00 	movl   $0x1064bb,(%esp)
  100938:	89 44 24 04          	mov    %eax,0x4(%esp)
  10093c:	e8 ef fb ff ff       	call   100530 <cprintf>
  cprintf(s);
  100941:	8b 45 08             	mov    0x8(%ebp),%eax
  100944:	89 04 24             	mov    %eax,(%esp)
  100947:	e8 e4 fb ff ff       	call   100530 <cprintf>
  cprintf("\n");
  10094c:	c7 04 24 d6 68 10 00 	movl   $0x1068d6,(%esp)
  100953:	e8 d8 fb ff ff       	call   100530 <cprintf>
  getcallerpcs(&s, pcs);
  100958:	8d 45 08             	lea    0x8(%ebp),%eax
  10095b:	89 74 24 04          	mov    %esi,0x4(%esp)
  10095f:	89 04 24             	mov    %eax,(%esp)
  100962:	e8 59 30 00 00       	call   1039c0 <getcallerpcs>
  100967:	90                   	nop
  for(i=0; i<10; i++)
    cprintf(" %p", pcs[i]);
  100968:	8b 04 9e             	mov    (%esi,%ebx,4),%eax
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, pcs);
  for(i=0; i<10; i++)
  10096b:	83 c3 01             	add    $0x1,%ebx
    cprintf(" %p", pcs[i]);
  10096e:	c7 04 24 ca 64 10 00 	movl   $0x1064ca,(%esp)
  100975:	89 44 24 04          	mov    %eax,0x4(%esp)
  100979:	e8 b2 fb ff ff       	call   100530 <cprintf>
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, pcs);
  for(i=0; i<10; i++)
  10097e:	83 fb 0a             	cmp    $0xa,%ebx
  100981:	75 e5                	jne    100968 <panic+0x58>
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
  100983:	c7 05 20 78 10 00 01 	movl   $0x1,0x107820
  10098a:	00 00 00 
  10098d:	eb fe                	jmp    10098d <panic+0x7d>
  10098f:	90                   	nop

00100990 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
  100990:	55                   	push   %ebp
  100991:	89 e5                	mov    %esp,%ebp
  100993:	57                   	push   %edi
  100994:	56                   	push   %esi
  100995:	53                   	push   %ebx
  100996:	81 ec 2c 01 00 00    	sub    $0x12c,%esp
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  if((ip = namei(path)) == 0)
  10099c:	8b 45 08             	mov    0x8(%ebp),%eax
  10099f:	89 04 24             	mov    %eax,(%esp)
  1009a2:	e8 b9 14 00 00       	call   101e60 <namei>
  1009a7:	85 c0                	test   %eax,%eax
  1009a9:	89 c7                	mov    %eax,%edi
  1009ab:	0f 84 1d 01 00 00    	je     100ace <exec+0x13e>
    return -1;
  ilock(ip);
  1009b1:	89 04 24             	mov    %eax,(%esp)
  1009b4:	e8 07 12 00 00       	call   101bc0 <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
  1009b9:	8d 45 94             	lea    -0x6c(%ebp),%eax
  1009bc:	c7 44 24 0c 34 00 00 	movl   $0x34,0xc(%esp)
  1009c3:	00 
  1009c4:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  1009cb:	00 
  1009cc:	89 44 24 04          	mov    %eax,0x4(%esp)
  1009d0:	89 3c 24             	mov    %edi,(%esp)
  1009d3:	e8 98 09 00 00       	call   101370 <readi>
  1009d8:	83 f8 33             	cmp    $0x33,%eax
  1009db:	0f 86 c7 01 00 00    	jbe    100ba8 <exec+0x218>
    goto bad;
  if(elf.magic != ELF_MAGIC)
  1009e1:	81 7d 94 7f 45 4c 46 	cmpl   $0x464c457f,-0x6c(%ebp)
  1009e8:	0f 85 ba 01 00 00    	jne    100ba8 <exec+0x218>
    goto bad;

  if((pgdir = setupkvm()) == 0)
  1009ee:	e8 1d 54 00 00       	call   105e10 <setupkvm>
  1009f3:	85 c0                	test   %eax,%eax
  1009f5:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
  1009fb:	0f 84 a7 01 00 00    	je     100ba8 <exec+0x218>
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
  100a01:	66 83 7d c0 00       	cmpw   $0x0,-0x40(%ebp)
  100a06:	8b 75 b0             	mov    -0x50(%ebp),%esi
  100a09:	0f 84 b5 02 00 00    	je     100cc4 <exec+0x334>
  100a0f:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  100a16:	00 00 00 
  100a19:	31 db                	xor    %ebx,%ebx
  100a1b:	eb 15                	jmp    100a32 <exec+0xa2>
  100a1d:	8d 76 00             	lea    0x0(%esi),%esi
  100a20:	0f b7 45 c0          	movzwl -0x40(%ebp),%eax
  100a24:	83 c3 01             	add    $0x1,%ebx
  100a27:	39 d8                	cmp    %ebx,%eax
  100a29:	0f 8e b1 00 00 00    	jle    100ae0 <exec+0x150>
  100a2f:	83 c6 20             	add    $0x20,%esi
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
  100a32:	8d 55 c8             	lea    -0x38(%ebp),%edx
  100a35:	c7 44 24 0c 20 00 00 	movl   $0x20,0xc(%esp)
  100a3c:	00 
  100a3d:	89 74 24 08          	mov    %esi,0x8(%esp)
  100a41:	89 54 24 04          	mov    %edx,0x4(%esp)
  100a45:	89 3c 24             	mov    %edi,(%esp)
  100a48:	e8 23 09 00 00       	call   101370 <readi>
  100a4d:	83 f8 20             	cmp    $0x20,%eax
  100a50:	75 66                	jne    100ab8 <exec+0x128>
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
  100a52:	83 7d c8 01          	cmpl   $0x1,-0x38(%ebp)
  100a56:	75 c8                	jne    100a20 <exec+0x90>
      continue;
    if(ph.memsz < ph.filesz)
  100a58:	8b 45 dc             	mov    -0x24(%ebp),%eax
  100a5b:	3b 45 d8             	cmp    -0x28(%ebp),%eax
  100a5e:	72 58                	jb     100ab8 <exec+0x128>
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.va + ph.memsz)) == 0)
  100a60:	03 45 d0             	add    -0x30(%ebp),%eax
  100a63:	8b 8d f4 fe ff ff    	mov    -0x10c(%ebp),%ecx
  100a69:	89 44 24 08          	mov    %eax,0x8(%esp)
  100a6d:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  100a73:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  100a77:	89 04 24             	mov    %eax,(%esp)
  100a7a:	e8 91 56 00 00       	call   106110 <allocuvm>
  100a7f:	85 c0                	test   %eax,%eax
  100a81:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)
  100a87:	74 2f                	je     100ab8 <exec+0x128>
      goto bad;
    if(loaduvm(pgdir, (char*)ph.va, ip, ph.offset, ph.filesz) < 0)
  100a89:	8b 45 d8             	mov    -0x28(%ebp),%eax
  100a8c:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
  100a92:	89 7c 24 08          	mov    %edi,0x8(%esp)
  100a96:	89 44 24 10          	mov    %eax,0x10(%esp)
  100a9a:	8b 45 cc             	mov    -0x34(%ebp),%eax
  100a9d:	89 14 24             	mov    %edx,(%esp)
  100aa0:	89 44 24 0c          	mov    %eax,0xc(%esp)
  100aa4:	8b 45 d0             	mov    -0x30(%ebp),%eax
  100aa7:	89 44 24 04          	mov    %eax,0x4(%esp)
  100aab:	e8 30 57 00 00       	call   1061e0 <loaduvm>
  100ab0:	85 c0                	test   %eax,%eax
  100ab2:	0f 89 68 ff ff ff    	jns    100a20 <exec+0x90>

  return 0;

 bad:
  if(pgdir)
    freevm(pgdir);
  100ab8:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  100abe:	89 04 24             	mov    %eax,(%esp)
  100ac1:	e8 0a 55 00 00       	call   105fd0 <freevm>
  if(ip)
  100ac6:	85 ff                	test   %edi,%edi
  100ac8:	0f 85 da 00 00 00    	jne    100ba8 <exec+0x218>
    iunlockput(ip);
  100ace:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return -1;
}
  100ad3:	81 c4 2c 01 00 00    	add    $0x12c,%esp
  100ad9:	5b                   	pop    %ebx
  100ada:	5e                   	pop    %esi
  100adb:	5f                   	pop    %edi
  100adc:	5d                   	pop    %ebp
  100add:	c3                   	ret    
  100ade:	66 90                	xchg   %ax,%ax
  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
  100ae0:	8b 9d f4 fe ff ff    	mov    -0x10c(%ebp),%ebx
  100ae6:	81 c3 ff 0f 00 00    	add    $0xfff,%ebx
  100aec:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  100af2:	8d b3 00 10 00 00    	lea    0x1000(%ebx),%esi
    if((sz = allocuvm(pgdir, sz, ph.va + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.va, ip, ph.offset, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
  100af8:	89 3c 24             	mov    %edi,(%esp)
  100afb:	e8 d0 0f 00 00       	call   101ad0 <iunlockput>
  ip = 0;

  // Allocate a one-page stack at the next page boundary
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + PGSIZE)) == 0)
  100b00:	8b 8d f0 fe ff ff    	mov    -0x110(%ebp),%ecx
  100b06:	89 74 24 08          	mov    %esi,0x8(%esp)
  100b0a:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  100b0e:	89 0c 24             	mov    %ecx,(%esp)
  100b11:	e8 fa 55 00 00       	call   106110 <allocuvm>
  100b16:	85 c0                	test   %eax,%eax
  100b18:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)
  100b1e:	74 7f                	je     100b9f <exec+0x20f>
    goto bad;

  // Push argument strings, prepare rest of stack in ustack.
  sp = sz;
  for(argc = 0; argv[argc]; argc++) {
  100b20:	8b 55 0c             	mov    0xc(%ebp),%edx
  100b23:	8b 02                	mov    (%edx),%eax
  100b25:	85 c0                	test   %eax,%eax
  100b27:	0f 84 78 01 00 00    	je     100ca5 <exec+0x315>
  100b2d:	8b 7d 0c             	mov    0xc(%ebp),%edi
  100b30:	31 f6                	xor    %esi,%esi
  100b32:	8b 9d f4 fe ff ff    	mov    -0x10c(%ebp),%ebx
  100b38:	eb 28                	jmp    100b62 <exec+0x1d2>
  100b3a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      goto bad;
    sp -= strlen(argv[argc]) + 1;
    sp &= ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  100b40:	89 9c b5 10 ff ff ff 	mov    %ebx,-0xf0(%ebp,%esi,4)
#include "defs.h"
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
  100b47:	8b 45 0c             	mov    0xc(%ebp),%eax
  if((sz = allocuvm(pgdir, sz, sz + PGSIZE)) == 0)
    goto bad;

  // Push argument strings, prepare rest of stack in ustack.
  sp = sz;
  for(argc = 0; argv[argc]; argc++) {
  100b4a:	83 c6 01             	add    $0x1,%esi
      goto bad;
    sp -= strlen(argv[argc]) + 1;
    sp &= ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  100b4d:	8d 95 04 ff ff ff    	lea    -0xfc(%ebp),%edx
#include "defs.h"
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
  100b53:	8d 3c b0             	lea    (%eax,%esi,4),%edi
  if((sz = allocuvm(pgdir, sz, sz + PGSIZE)) == 0)
    goto bad;

  // Push argument strings, prepare rest of stack in ustack.
  sp = sz;
  for(argc = 0; argv[argc]; argc++) {
  100b56:	8b 04 b0             	mov    (%eax,%esi,4),%eax
  100b59:	85 c0                	test   %eax,%eax
  100b5b:	74 5d                	je     100bba <exec+0x22a>
    if(argc >= MAXARG)
  100b5d:	83 fe 20             	cmp    $0x20,%esi
  100b60:	74 3d                	je     100b9f <exec+0x20f>
      goto bad;
    sp -= strlen(argv[argc]) + 1;
  100b62:	89 04 24             	mov    %eax,(%esp)
  100b65:	e8 46 32 00 00       	call   103db0 <strlen>
  100b6a:	f7 d0                	not    %eax
  100b6c:	8d 1c 18             	lea    (%eax,%ebx,1),%ebx
    sp &= ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
  100b6f:	8b 07                	mov    (%edi),%eax
  sp = sz;
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
    sp -= strlen(argv[argc]) + 1;
    sp &= ~3;
  100b71:	83 e3 fc             	and    $0xfffffffc,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
  100b74:	89 04 24             	mov    %eax,(%esp)
  100b77:	e8 34 32 00 00       	call   103db0 <strlen>
  100b7c:	8b 8d f0 fe ff ff    	mov    -0x110(%ebp),%ecx
  100b82:	83 c0 01             	add    $0x1,%eax
  100b85:	89 44 24 0c          	mov    %eax,0xc(%esp)
  100b89:	8b 07                	mov    (%edi),%eax
  100b8b:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  100b8f:	89 0c 24             	mov    %ecx,(%esp)
  100b92:	89 44 24 08          	mov    %eax,0x8(%esp)
  100b96:	e8 55 51 00 00       	call   105cf0 <copyout>
  100b9b:	85 c0                	test   %eax,%eax
  100b9d:	79 a1                	jns    100b40 <exec+0x1b0>

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip)
    iunlockput(ip);
  100b9f:	31 ff                	xor    %edi,%edi
  100ba1:	e9 12 ff ff ff       	jmp    100ab8 <exec+0x128>
  100ba6:	66 90                	xchg   %ax,%ax
  100ba8:	89 3c 24             	mov    %edi,(%esp)
  100bab:	e8 20 0f 00 00       	call   101ad0 <iunlockput>
  100bb0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  100bb5:	e9 19 ff ff ff       	jmp    100ad3 <exec+0x143>
  if((sz = allocuvm(pgdir, sz, sz + PGSIZE)) == 0)
    goto bad;

  // Push argument strings, prepare rest of stack in ustack.
  sp = sz;
  for(argc = 0; argv[argc]; argc++) {
  100bba:	8d 4e 03             	lea    0x3(%esi),%ecx
  100bbd:	8d 3c b5 04 00 00 00 	lea    0x4(,%esi,4),%edi
  100bc4:	8d 04 b5 10 00 00 00 	lea    0x10(,%esi,4),%eax
    sp &= ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;
  100bcb:	c7 84 8d 04 ff ff ff 	movl   $0x0,-0xfc(%ebp,%ecx,4)
  100bd2:	00 00 00 00 

  ustack[0] = 0xffffffff;  // fake return PC
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer
  100bd6:	89 d9                	mov    %ebx,%ecx

  sp -= (3+argc+1) * 4;
  100bd8:	29 c3                	sub    %eax,%ebx
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
  100bda:	89 44 24 0c          	mov    %eax,0xc(%esp)
  100bde:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  }
  ustack[3+argc] = 0;

  ustack[0] = 0xffffffff;  // fake return PC
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer
  100be4:	29 f9                	sub    %edi,%ecx
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;

  ustack[0] = 0xffffffff;  // fake return PC
  100be6:	c7 85 04 ff ff ff ff 	movl   $0xffffffff,-0xfc(%ebp)
  100bed:	ff ff ff 
  ustack[1] = argc;
  100bf0:	89 b5 08 ff ff ff    	mov    %esi,-0xf8(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
  100bf6:	89 8d 0c ff ff ff    	mov    %ecx,-0xf4(%ebp)

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
  100bfc:	89 54 24 08          	mov    %edx,0x8(%esp)
  100c00:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  100c04:	89 04 24             	mov    %eax,(%esp)
  100c07:	e8 e4 50 00 00       	call   105cf0 <copyout>
  100c0c:	85 c0                	test   %eax,%eax
  100c0e:	78 8f                	js     100b9f <exec+0x20f>
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
  100c10:	8b 4d 08             	mov    0x8(%ebp),%ecx
  100c13:	0f b6 11             	movzbl (%ecx),%edx
  100c16:	84 d2                	test   %dl,%dl
  100c18:	74 16                	je     100c30 <exec+0x2a0>
  100c1a:	89 c8                	mov    %ecx,%eax
  100c1c:	83 c0 01             	add    $0x1,%eax
  100c1f:	90                   	nop
    if(*s == '/')
  100c20:	80 fa 2f             	cmp    $0x2f,%dl
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
  100c23:	0f b6 10             	movzbl (%eax),%edx
    if(*s == '/')
  100c26:	0f 44 c8             	cmove  %eax,%ecx
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
  100c29:	83 c0 01             	add    $0x1,%eax
  100c2c:	84 d2                	test   %dl,%dl
  100c2e:	75 f0                	jne    100c20 <exec+0x290>
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
  100c30:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  100c36:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  100c3a:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
  100c41:	00 
  100c42:	83 c0 6c             	add    $0x6c,%eax
  100c45:	89 04 24             	mov    %eax,(%esp)
  100c48:	e8 23 31 00 00       	call   103d70 <safestrcpy>

  // Commit to the user image.
  oldpgdir = proc->pgdir;
  100c4d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  proc->pgdir = pgdir;
  100c53:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));

  // Commit to the user image.
  oldpgdir = proc->pgdir;
  100c59:	8b 70 04             	mov    0x4(%eax),%esi
  proc->pgdir = pgdir;
  100c5c:	89 50 04             	mov    %edx,0x4(%eax)
  proc->sz = sz;
  100c5f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  100c65:	8b 8d f4 fe ff ff    	mov    -0x10c(%ebp),%ecx
  100c6b:	89 08                	mov    %ecx,(%eax)
  proc->tf->eip = elf.entry;  // main
  100c6d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  100c73:	8b 55 ac             	mov    -0x54(%ebp),%edx
  100c76:	8b 40 18             	mov    0x18(%eax),%eax
  100c79:	89 50 38             	mov    %edx,0x38(%eax)
  proc->tf->esp = sp;
  100c7c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  100c82:	8b 40 18             	mov    0x18(%eax),%eax
  100c85:	89 58 44             	mov    %ebx,0x44(%eax)
  switchuvm(proc);
  100c88:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  100c8e:	89 04 24             	mov    %eax,(%esp)
  100c91:	e8 0a 56 00 00       	call   1062a0 <switchuvm>
  freevm(oldpgdir);
  100c96:	89 34 24             	mov    %esi,(%esp)
  100c99:	e8 32 53 00 00       	call   105fd0 <freevm>
  100c9e:	31 c0                	xor    %eax,%eax

  return 0;
  100ca0:	e9 2e fe ff ff       	jmp    100ad3 <exec+0x143>
  if((sz = allocuvm(pgdir, sz, sz + PGSIZE)) == 0)
    goto bad;

  // Push argument strings, prepare rest of stack in ustack.
  sp = sz;
  for(argc = 0; argv[argc]; argc++) {
  100ca5:	8b 9d f4 fe ff ff    	mov    -0x10c(%ebp),%ebx
  100cab:	b0 10                	mov    $0x10,%al
  100cad:	bf 04 00 00 00       	mov    $0x4,%edi
  100cb2:	b9 03 00 00 00       	mov    $0x3,%ecx
  100cb7:	31 f6                	xor    %esi,%esi
  100cb9:	8d 95 04 ff ff ff    	lea    -0xfc(%ebp),%edx
  100cbf:	e9 07 ff ff ff       	jmp    100bcb <exec+0x23b>
  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
  100cc4:	be 00 10 00 00       	mov    $0x1000,%esi
  100cc9:	31 db                	xor    %ebx,%ebx
  100ccb:	e9 28 fe ff ff       	jmp    100af8 <exec+0x168>

00100cd0 <filewrite>:
}

// Write to file f.  Addr is kernel address.
int
filewrite(struct file *f, char *addr, int n)
{
  100cd0:	55                   	push   %ebp
  100cd1:	89 e5                	mov    %esp,%ebp
  100cd3:	83 ec 38             	sub    $0x38,%esp
  100cd6:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  100cd9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  100cdc:	89 75 f8             	mov    %esi,-0x8(%ebp)
  100cdf:	8b 75 0c             	mov    0xc(%ebp),%esi
  100ce2:	89 7d fc             	mov    %edi,-0x4(%ebp)
  100ce5:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->writable == 0)
  100ce8:	80 7b 09 00          	cmpb   $0x0,0x9(%ebx)
  100cec:	74 5a                	je     100d48 <filewrite+0x78>
    return -1;
  if(f->type == FD_PIPE)
  100cee:	8b 03                	mov    (%ebx),%eax
  100cf0:	83 f8 01             	cmp    $0x1,%eax
  100cf3:	74 6b                	je     100d60 <filewrite+0x90>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
  100cf5:	83 f8 02             	cmp    $0x2,%eax
  100cf8:	75 7d                	jne    100d77 <filewrite+0xa7>
    ilock(f->ip);
  100cfa:	8b 43 10             	mov    0x10(%ebx),%eax
  100cfd:	89 04 24             	mov    %eax,(%esp)
  100d00:	e8 bb 0e 00 00       	call   101bc0 <ilock>
    if((r = writei(f->ip, addr, f->off, n)) > 0)
  100d05:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  100d09:	8b 43 14             	mov    0x14(%ebx),%eax
  100d0c:	89 74 24 04          	mov    %esi,0x4(%esp)
  100d10:	89 44 24 08          	mov    %eax,0x8(%esp)
  100d14:	8b 43 10             	mov    0x10(%ebx),%eax
  100d17:	89 04 24             	mov    %eax,(%esp)
  100d1a:	e8 f1 07 00 00       	call   101510 <writei>
  100d1f:	85 c0                	test   %eax,%eax
  100d21:	7e 03                	jle    100d26 <filewrite+0x56>
      f->off += r;
  100d23:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
  100d26:	8b 53 10             	mov    0x10(%ebx),%edx
  100d29:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  100d2c:	89 14 24             	mov    %edx,(%esp)
  100d2f:	e8 4c 0a 00 00       	call   101780 <iunlock>
    return r;
  100d34:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  }
  panic("filewrite");
}
  100d37:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  100d3a:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100d3d:	8b 7d fc             	mov    -0x4(%ebp),%edi
  100d40:	89 ec                	mov    %ebp,%esp
  100d42:	5d                   	pop    %ebp
  100d43:	c3                   	ret    
  100d44:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  100d48:	8b 5d f4             	mov    -0xc(%ebp),%ebx
    if((r = writei(f->ip, addr, f->off, n)) > 0)
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("filewrite");
  100d4b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  100d50:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100d53:	8b 7d fc             	mov    -0x4(%ebp),%edi
  100d56:	89 ec                	mov    %ebp,%esp
  100d58:	5d                   	pop    %ebp
  100d59:	c3                   	ret    
  100d5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  int r;

  if(f->writable == 0)
    return -1;
  if(f->type == FD_PIPE)
    return pipewrite(f->pipe, addr, n);
  100d60:	8b 43 0c             	mov    0xc(%ebx),%eax
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("filewrite");
}
  100d63:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100d66:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  100d69:	8b 7d fc             	mov    -0x4(%ebp),%edi
  int r;

  if(f->writable == 0)
    return -1;
  if(f->type == FD_PIPE)
    return pipewrite(f->pipe, addr, n);
  100d6c:	89 45 08             	mov    %eax,0x8(%ebp)
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("filewrite");
}
  100d6f:	89 ec                	mov    %ebp,%esp
  100d71:	5d                   	pop    %ebp
  int r;

  if(f->writable == 0)
    return -1;
  if(f->type == FD_PIPE)
    return pipewrite(f->pipe, addr, n);
  100d72:	e9 f9 1f 00 00       	jmp    102d70 <pipewrite>
    if((r = writei(f->ip, addr, f->off, n)) > 0)
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("filewrite");
  100d77:	c7 04 24 df 64 10 00 	movl   $0x1064df,(%esp)
  100d7e:	e8 8d fb ff ff       	call   100910 <panic>
  100d83:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  100d89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00100d90 <fileread>:
}

// Read from file f.  Addr is kernel address.
int
fileread(struct file *f, char *addr, int n)
{
  100d90:	55                   	push   %ebp
  100d91:	89 e5                	mov    %esp,%ebp
  100d93:	83 ec 38             	sub    $0x38,%esp
  100d96:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  100d99:	8b 5d 08             	mov    0x8(%ebp),%ebx
  100d9c:	89 75 f8             	mov    %esi,-0x8(%ebp)
  100d9f:	8b 75 0c             	mov    0xc(%ebp),%esi
  100da2:	89 7d fc             	mov    %edi,-0x4(%ebp)
  100da5:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->readable == 0)
  100da8:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
  100dac:	74 5a                	je     100e08 <fileread+0x78>
    return -1;
  if(f->type == FD_PIPE)
  100dae:	8b 03                	mov    (%ebx),%eax
  100db0:	83 f8 01             	cmp    $0x1,%eax
  100db3:	74 6b                	je     100e20 <fileread+0x90>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
  100db5:	83 f8 02             	cmp    $0x2,%eax
  100db8:	75 7d                	jne    100e37 <fileread+0xa7>
    ilock(f->ip);
  100dba:	8b 43 10             	mov    0x10(%ebx),%eax
  100dbd:	89 04 24             	mov    %eax,(%esp)
  100dc0:	e8 fb 0d 00 00       	call   101bc0 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
  100dc5:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  100dc9:	8b 43 14             	mov    0x14(%ebx),%eax
  100dcc:	89 74 24 04          	mov    %esi,0x4(%esp)
  100dd0:	89 44 24 08          	mov    %eax,0x8(%esp)
  100dd4:	8b 43 10             	mov    0x10(%ebx),%eax
  100dd7:	89 04 24             	mov    %eax,(%esp)
  100dda:	e8 91 05 00 00       	call   101370 <readi>
  100ddf:	85 c0                	test   %eax,%eax
  100de1:	7e 03                	jle    100de6 <fileread+0x56>
      f->off += r;
  100de3:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
  100de6:	8b 53 10             	mov    0x10(%ebx),%edx
  100de9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  100dec:	89 14 24             	mov    %edx,(%esp)
  100def:	e8 8c 09 00 00       	call   101780 <iunlock>
    return r;
  100df4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  }
  panic("fileread");
}
  100df7:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  100dfa:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100dfd:	8b 7d fc             	mov    -0x4(%ebp),%edi
  100e00:	89 ec                	mov    %ebp,%esp
  100e02:	5d                   	pop    %ebp
  100e03:	c3                   	ret    
  100e04:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  100e08:	8b 5d f4             	mov    -0xc(%ebp),%ebx
    if((r = readi(f->ip, addr, f->off, n)) > 0)
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("fileread");
  100e0b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  100e10:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100e13:	8b 7d fc             	mov    -0x4(%ebp),%edi
  100e16:	89 ec                	mov    %ebp,%esp
  100e18:	5d                   	pop    %ebp
  100e19:	c3                   	ret    
  100e1a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  int r;

  if(f->readable == 0)
    return -1;
  if(f->type == FD_PIPE)
    return piperead(f->pipe, addr, n);
  100e20:	8b 43 0c             	mov    0xc(%ebx),%eax
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("fileread");
}
  100e23:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100e26:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  100e29:	8b 7d fc             	mov    -0x4(%ebp),%edi
  int r;

  if(f->readable == 0)
    return -1;
  if(f->type == FD_PIPE)
    return piperead(f->pipe, addr, n);
  100e2c:	89 45 08             	mov    %eax,0x8(%ebp)
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("fileread");
}
  100e2f:	89 ec                	mov    %ebp,%esp
  100e31:	5d                   	pop    %ebp
  int r;

  if(f->readable == 0)
    return -1;
  if(f->type == FD_PIPE)
    return piperead(f->pipe, addr, n);
  100e32:	e9 39 1e 00 00       	jmp    102c70 <piperead>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
      f->off += r;
    iunlock(f->ip);
    return r;
  }
  panic("fileread");
  100e37:	c7 04 24 e9 64 10 00 	movl   $0x1064e9,(%esp)
  100e3e:	e8 cd fa ff ff       	call   100910 <panic>
  100e43:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  100e49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00100e50 <filestat>:
}

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
  100e50:	55                   	push   %ebp
  if(f->type == FD_INODE){
  100e51:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
  100e56:	89 e5                	mov    %esp,%ebp
  100e58:	53                   	push   %ebx
  100e59:	83 ec 14             	sub    $0x14,%esp
  100e5c:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
  100e5f:	83 3b 02             	cmpl   $0x2,(%ebx)
  100e62:	74 0c                	je     100e70 <filestat+0x20>
    stati(f->ip, st);
    iunlock(f->ip);
    return 0;
  }
  return -1;
}
  100e64:	83 c4 14             	add    $0x14,%esp
  100e67:	5b                   	pop    %ebx
  100e68:	5d                   	pop    %ebp
  100e69:	c3                   	ret    
  100e6a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
  if(f->type == FD_INODE){
    ilock(f->ip);
  100e70:	8b 43 10             	mov    0x10(%ebx),%eax
  100e73:	89 04 24             	mov    %eax,(%esp)
  100e76:	e8 45 0d 00 00       	call   101bc0 <ilock>
    stati(f->ip, st);
  100e7b:	8b 45 0c             	mov    0xc(%ebp),%eax
  100e7e:	89 44 24 04          	mov    %eax,0x4(%esp)
  100e82:	8b 43 10             	mov    0x10(%ebx),%eax
  100e85:	89 04 24             	mov    %eax,(%esp)
  100e88:	e8 e3 01 00 00       	call   101070 <stati>
    iunlock(f->ip);
  100e8d:	8b 43 10             	mov    0x10(%ebx),%eax
  100e90:	89 04 24             	mov    %eax,(%esp)
  100e93:	e8 e8 08 00 00       	call   101780 <iunlock>
    return 0;
  }
  return -1;
}
  100e98:	83 c4 14             	add    $0x14,%esp
filestat(struct file *f, struct stat *st)
{
  if(f->type == FD_INODE){
    ilock(f->ip);
    stati(f->ip, st);
    iunlock(f->ip);
  100e9b:	31 c0                	xor    %eax,%eax
    return 0;
  }
  return -1;
}
  100e9d:	5b                   	pop    %ebx
  100e9e:	5d                   	pop    %ebp
  100e9f:	c3                   	ret    

00100ea0 <filedup>:
}

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
  100ea0:	55                   	push   %ebp
  100ea1:	89 e5                	mov    %esp,%ebp
  100ea3:	53                   	push   %ebx
  100ea4:	83 ec 14             	sub    $0x14,%esp
  100ea7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
  100eaa:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  100eb1:	e8 7a 2c 00 00       	call   103b30 <acquire>
  if(f->ref < 1)
  100eb6:	8b 43 04             	mov    0x4(%ebx),%eax
  100eb9:	85 c0                	test   %eax,%eax
  100ebb:	7e 1a                	jle    100ed7 <filedup+0x37>
    panic("filedup");
  f->ref++;
  100ebd:	83 c0 01             	add    $0x1,%eax
  100ec0:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
  100ec3:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  100eca:	e8 11 2c 00 00       	call   103ae0 <release>
  return f;
}
  100ecf:	89 d8                	mov    %ebx,%eax
  100ed1:	83 c4 14             	add    $0x14,%esp
  100ed4:	5b                   	pop    %ebx
  100ed5:	5d                   	pop    %ebp
  100ed6:	c3                   	ret    
struct file*
filedup(struct file *f)
{
  acquire(&ftable.lock);
  if(f->ref < 1)
    panic("filedup");
  100ed7:	c7 04 24 f2 64 10 00 	movl   $0x1064f2,(%esp)
  100ede:	e8 2d fa ff ff       	call   100910 <panic>
  100ee3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  100ee9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00100ef0 <filealloc>:
}

// Allocate a file structure.
struct file*
filealloc(void)
{
  100ef0:	55                   	push   %ebp
  100ef1:	89 e5                	mov    %esp,%ebp
  100ef3:	53                   	push   %ebx
  initlock(&ftable.lock, "ftable");
}

// Allocate a file structure.
struct file*
filealloc(void)
  100ef4:	bb 2c a1 10 00       	mov    $0x10a12c,%ebx
{
  100ef9:	83 ec 14             	sub    $0x14,%esp
  struct file *f;

  acquire(&ftable.lock);
  100efc:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  100f03:	e8 28 2c 00 00       	call   103b30 <acquire>
  for(f = ftable.file; f < ftable.file + NFILE; f++){
    if(f->ref == 0){
  100f08:	8b 15 18 a1 10 00    	mov    0x10a118,%edx
  100f0e:	85 d2                	test   %edx,%edx
  100f10:	75 11                	jne    100f23 <filealloc+0x33>
  100f12:	eb 4a                	jmp    100f5e <filealloc+0x6e>
  100f14:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
filealloc(void)
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
  100f18:	83 c3 18             	add    $0x18,%ebx
  100f1b:	81 fb 74 aa 10 00    	cmp    $0x10aa74,%ebx
  100f21:	74 25                	je     100f48 <filealloc+0x58>
    if(f->ref == 0){
  100f23:	8b 43 04             	mov    0x4(%ebx),%eax
  100f26:	85 c0                	test   %eax,%eax
  100f28:	75 ee                	jne    100f18 <filealloc+0x28>
      f->ref = 1;
  100f2a:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
  100f31:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  100f38:	e8 a3 2b 00 00       	call   103ae0 <release>
      return f;
    }
  }
  release(&ftable.lock);
  return 0;
}
  100f3d:	89 d8                	mov    %ebx,%eax
  100f3f:	83 c4 14             	add    $0x14,%esp
  100f42:	5b                   	pop    %ebx
  100f43:	5d                   	pop    %ebp
  100f44:	c3                   	ret    
  100f45:	8d 76 00             	lea    0x0(%esi),%esi
      f->ref = 1;
      release(&ftable.lock);
      return f;
    }
  }
  release(&ftable.lock);
  100f48:	31 db                	xor    %ebx,%ebx
  100f4a:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  100f51:	e8 8a 2b 00 00       	call   103ae0 <release>
  return 0;
}
  100f56:	89 d8                	mov    %ebx,%eax
  100f58:	83 c4 14             	add    $0x14,%esp
  100f5b:	5b                   	pop    %ebx
  100f5c:	5d                   	pop    %ebp
  100f5d:	c3                   	ret    
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
    if(f->ref == 0){
  100f5e:	bb 14 a1 10 00       	mov    $0x10a114,%ebx
  100f63:	eb c5                	jmp    100f2a <filealloc+0x3a>
  100f65:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  100f69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00100f70 <fileclose>:
}

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
  100f70:	55                   	push   %ebp
  100f71:	89 e5                	mov    %esp,%ebp
  100f73:	83 ec 38             	sub    $0x38,%esp
  100f76:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  100f79:	8b 5d 08             	mov    0x8(%ebp),%ebx
  100f7c:	89 75 f8             	mov    %esi,-0x8(%ebp)
  100f7f:	89 7d fc             	mov    %edi,-0x4(%ebp)
  struct file ff;

  acquire(&ftable.lock);
  100f82:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  100f89:	e8 a2 2b 00 00       	call   103b30 <acquire>
  if(f->ref < 1)
  100f8e:	8b 43 04             	mov    0x4(%ebx),%eax
  100f91:	85 c0                	test   %eax,%eax
  100f93:	0f 8e 9c 00 00 00    	jle    101035 <fileclose+0xc5>
    panic("fileclose");
  if(--f->ref > 0){
  100f99:	83 e8 01             	sub    $0x1,%eax
  100f9c:	85 c0                	test   %eax,%eax
  100f9e:	89 43 04             	mov    %eax,0x4(%ebx)
  100fa1:	74 1d                	je     100fc0 <fileclose+0x50>
    release(&ftable.lock);
  100fa3:	c7 45 08 e0 a0 10 00 	movl   $0x10a0e0,0x8(%ebp)
  
  if(ff.type == FD_PIPE)
    pipeclose(ff.pipe, ff.writable);
  else if(ff.type == FD_INODE)
    iput(ff.ip);
}
  100faa:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  100fad:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100fb0:	8b 7d fc             	mov    -0x4(%ebp),%edi
  100fb3:	89 ec                	mov    %ebp,%esp
  100fb5:	5d                   	pop    %ebp

  acquire(&ftable.lock);
  if(f->ref < 1)
    panic("fileclose");
  if(--f->ref > 0){
    release(&ftable.lock);
  100fb6:	e9 25 2b 00 00       	jmp    103ae0 <release>
  100fbb:	90                   	nop
  100fbc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return;
  }
  ff = *f;
  100fc0:	8b 43 0c             	mov    0xc(%ebx),%eax
  100fc3:	8b 7b 10             	mov    0x10(%ebx),%edi
  100fc6:	89 45 e0             	mov    %eax,-0x20(%ebp)
  100fc9:	0f b6 43 09          	movzbl 0x9(%ebx),%eax
  100fcd:	88 45 e7             	mov    %al,-0x19(%ebp)
  100fd0:	8b 33                	mov    (%ebx),%esi
  f->ref = 0;
  100fd2:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
  f->type = FD_NONE;
  100fd9:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  release(&ftable.lock);
  100fdf:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  100fe6:	e8 f5 2a 00 00       	call   103ae0 <release>
  
  if(ff.type == FD_PIPE)
  100feb:	83 fe 01             	cmp    $0x1,%esi
  100fee:	74 30                	je     101020 <fileclose+0xb0>
    pipeclose(ff.pipe, ff.writable);
  else if(ff.type == FD_INODE)
  100ff0:	83 fe 02             	cmp    $0x2,%esi
  100ff3:	74 13                	je     101008 <fileclose+0x98>
    iput(ff.ip);
}
  100ff5:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  100ff8:	8b 75 f8             	mov    -0x8(%ebp),%esi
  100ffb:	8b 7d fc             	mov    -0x4(%ebp),%edi
  100ffe:	89 ec                	mov    %ebp,%esp
  101000:	5d                   	pop    %ebp
  101001:	c3                   	ret    
  101002:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  release(&ftable.lock);
  
  if(ff.type == FD_PIPE)
    pipeclose(ff.pipe, ff.writable);
  else if(ff.type == FD_INODE)
    iput(ff.ip);
  101008:	89 7d 08             	mov    %edi,0x8(%ebp)
}
  10100b:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  10100e:	8b 75 f8             	mov    -0x8(%ebp),%esi
  101011:	8b 7d fc             	mov    -0x4(%ebp),%edi
  101014:	89 ec                	mov    %ebp,%esp
  101016:	5d                   	pop    %ebp
  release(&ftable.lock);
  
  if(ff.type == FD_PIPE)
    pipeclose(ff.pipe, ff.writable);
  else if(ff.type == FD_INODE)
    iput(ff.ip);
  101017:	e9 74 08 00 00       	jmp    101890 <iput>
  10101c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  f->ref = 0;
  f->type = FD_NONE;
  release(&ftable.lock);
  
  if(ff.type == FD_PIPE)
    pipeclose(ff.pipe, ff.writable);
  101020:	0f be 45 e7          	movsbl -0x19(%ebp),%eax
  101024:	89 44 24 04          	mov    %eax,0x4(%esp)
  101028:	8b 45 e0             	mov    -0x20(%ebp),%eax
  10102b:	89 04 24             	mov    %eax,(%esp)
  10102e:	e8 2d 1e 00 00       	call   102e60 <pipeclose>
  101033:	eb c0                	jmp    100ff5 <fileclose+0x85>
{
  struct file ff;

  acquire(&ftable.lock);
  if(f->ref < 1)
    panic("fileclose");
  101035:	c7 04 24 fa 64 10 00 	movl   $0x1064fa,(%esp)
  10103c:	e8 cf f8 ff ff       	call   100910 <panic>
  101041:	eb 0d                	jmp    101050 <fileinit>
  101043:	90                   	nop
  101044:	90                   	nop
  101045:	90                   	nop
  101046:	90                   	nop
  101047:	90                   	nop
  101048:	90                   	nop
  101049:	90                   	nop
  10104a:	90                   	nop
  10104b:	90                   	nop
  10104c:	90                   	nop
  10104d:	90                   	nop
  10104e:	90                   	nop
  10104f:	90                   	nop

00101050 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
  101050:	55                   	push   %ebp
  101051:	89 e5                	mov    %esp,%ebp
  101053:	83 ec 18             	sub    $0x18,%esp
  initlock(&ftable.lock, "ftable");
  101056:	c7 44 24 04 04 65 10 	movl   $0x106504,0x4(%esp)
  10105d:	00 
  10105e:	c7 04 24 e0 a0 10 00 	movl   $0x10a0e0,(%esp)
  101065:	e8 36 29 00 00       	call   1039a0 <initlock>
}
  10106a:	c9                   	leave  
  10106b:	c3                   	ret    
  10106c:	90                   	nop
  10106d:	90                   	nop
  10106e:	90                   	nop
  10106f:	90                   	nop

00101070 <stati>:
}

// Copy stat information from inode.
void
stati(struct inode *ip, struct stat *st)
{
  101070:	55                   	push   %ebp
  101071:	89 e5                	mov    %esp,%ebp
  101073:	8b 55 08             	mov    0x8(%ebp),%edx
  101076:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
  101079:	8b 0a                	mov    (%edx),%ecx
  10107b:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
  10107e:	8b 4a 04             	mov    0x4(%edx),%ecx
  101081:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
  101084:	0f b7 4a 10          	movzwl 0x10(%edx),%ecx
  101088:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
  10108b:	0f b7 4a 16          	movzwl 0x16(%edx),%ecx
  10108f:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
  101093:	8b 52 18             	mov    0x18(%edx),%edx
  101096:	89 50 10             	mov    %edx,0x10(%eax)
}
  101099:	5d                   	pop    %ebp
  10109a:	c3                   	ret    
  10109b:	90                   	nop
  10109c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

001010a0 <idup>:

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
  1010a0:	55                   	push   %ebp
  1010a1:	89 e5                	mov    %esp,%ebp
  1010a3:	53                   	push   %ebx
  1010a4:	83 ec 14             	sub    $0x14,%esp
  1010a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
  1010aa:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  1010b1:	e8 7a 2a 00 00       	call   103b30 <acquire>
  ip->ref++;
  1010b6:	83 43 08 01          	addl   $0x1,0x8(%ebx)
  release(&icache.lock);
  1010ba:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  1010c1:	e8 1a 2a 00 00       	call   103ae0 <release>
  return ip;
}
  1010c6:	89 d8                	mov    %ebx,%eax
  1010c8:	83 c4 14             	add    $0x14,%esp
  1010cb:	5b                   	pop    %ebx
  1010cc:	5d                   	pop    %ebp
  1010cd:	c3                   	ret    
  1010ce:	66 90                	xchg   %ax,%ax

001010d0 <iget>:

// Find the inode with number inum on device dev
// and return the in-memory copy.
static struct inode*
iget(uint dev, uint inum)
{
  1010d0:	55                   	push   %ebp
  1010d1:	89 e5                	mov    %esp,%ebp
  1010d3:	57                   	push   %edi
  1010d4:	89 d7                	mov    %edx,%edi
  1010d6:	56                   	push   %esi
}

// Find the inode with number inum on device dev
// and return the in-memory copy.
static struct inode*
iget(uint dev, uint inum)
  1010d7:	31 f6                	xor    %esi,%esi
{
  1010d9:	53                   	push   %ebx
  1010da:	89 c3                	mov    %eax,%ebx
  1010dc:	83 ec 2c             	sub    $0x2c,%esp
  struct inode *ip, *empty;

  acquire(&icache.lock);
  1010df:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  1010e6:	e8 45 2a 00 00       	call   103b30 <acquire>
}

// Find the inode with number inum on device dev
// and return the in-memory copy.
static struct inode*
iget(uint dev, uint inum)
  1010eb:	b8 14 ab 10 00       	mov    $0x10ab14,%eax
  1010f0:	eb 14                	jmp    101106 <iget+0x36>
  1010f2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
  1010f8:	85 f6                	test   %esi,%esi
  1010fa:	74 3c                	je     101138 <iget+0x68>

  acquire(&icache.lock);

  // Try for cached inode.
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
  1010fc:	83 c0 50             	add    $0x50,%eax
  1010ff:	3d b4 ba 10 00       	cmp    $0x10bab4,%eax
  101104:	74 42                	je     101148 <iget+0x78>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
  101106:	8b 48 08             	mov    0x8(%eax),%ecx
  101109:	85 c9                	test   %ecx,%ecx
  10110b:	7e eb                	jle    1010f8 <iget+0x28>
  10110d:	39 18                	cmp    %ebx,(%eax)
  10110f:	75 e7                	jne    1010f8 <iget+0x28>
  101111:	39 78 04             	cmp    %edi,0x4(%eax)
  101114:	75 e2                	jne    1010f8 <iget+0x28>
      ip->ref++;
  101116:	83 c1 01             	add    $0x1,%ecx
  101119:	89 48 08             	mov    %ecx,0x8(%eax)
      release(&icache.lock);
  10111c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  10111f:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  101126:	e8 b5 29 00 00       	call   103ae0 <release>
      return ip;
  10112b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  ip->ref = 1;
  ip->flags = 0;
  release(&icache.lock);

  return ip;
}
  10112e:	83 c4 2c             	add    $0x2c,%esp
  101131:	5b                   	pop    %ebx
  101132:	5e                   	pop    %esi
  101133:	5f                   	pop    %edi
  101134:	5d                   	pop    %ebp
  101135:	c3                   	ret    
  101136:	66 90                	xchg   %ax,%ax
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
  101138:	85 c9                	test   %ecx,%ecx
  10113a:	0f 44 f0             	cmove  %eax,%esi

  acquire(&icache.lock);

  // Try for cached inode.
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
  10113d:	83 c0 50             	add    $0x50,%eax
  101140:	3d b4 ba 10 00       	cmp    $0x10bab4,%eax
  101145:	75 bf                	jne    101106 <iget+0x36>
  101147:	90                   	nop
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Allocate fresh inode.
  if(empty == 0)
  101148:	85 f6                	test   %esi,%esi
  10114a:	74 29                	je     101175 <iget+0xa5>
    panic("iget: no inodes");

  ip = empty;
  ip->dev = dev;
  10114c:	89 1e                	mov    %ebx,(%esi)
  ip->inum = inum;
  10114e:	89 7e 04             	mov    %edi,0x4(%esi)
  ip->ref = 1;
  101151:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->flags = 0;
  101158:	c7 46 0c 00 00 00 00 	movl   $0x0,0xc(%esi)
  release(&icache.lock);
  10115f:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  101166:	e8 75 29 00 00       	call   103ae0 <release>

  return ip;
}
  10116b:	83 c4 2c             	add    $0x2c,%esp
  ip = empty;
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->flags = 0;
  release(&icache.lock);
  10116e:	89 f0                	mov    %esi,%eax

  return ip;
}
  101170:	5b                   	pop    %ebx
  101171:	5e                   	pop    %esi
  101172:	5f                   	pop    %edi
  101173:	5d                   	pop    %ebp
  101174:	c3                   	ret    
      empty = ip;
  }

  // Allocate fresh inode.
  if(empty == 0)
    panic("iget: no inodes");
  101175:	c7 04 24 0b 65 10 00 	movl   $0x10650b,(%esp)
  10117c:	e8 8f f7 ff ff       	call   100910 <panic>
  101181:	eb 0d                	jmp    101190 <readsb>
  101183:	90                   	nop
  101184:	90                   	nop
  101185:	90                   	nop
  101186:	90                   	nop
  101187:	90                   	nop
  101188:	90                   	nop
  101189:	90                   	nop
  10118a:	90                   	nop
  10118b:	90                   	nop
  10118c:	90                   	nop
  10118d:	90                   	nop
  10118e:	90                   	nop
  10118f:	90                   	nop

00101190 <readsb>:
static void itrunc(struct inode*);

// Read the super block.
static void
readsb(int dev, struct superblock *sb)
{
  101190:	55                   	push   %ebp
  101191:	89 e5                	mov    %esp,%ebp
  101193:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  
  bp = bread(dev, 1);
  101196:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  10119d:	00 
static void itrunc(struct inode*);

// Read the super block.
static void
readsb(int dev, struct superblock *sb)
{
  10119e:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  1011a1:	89 75 fc             	mov    %esi,-0x4(%ebp)
  1011a4:	89 d6                	mov    %edx,%esi
  struct buf *bp;
  
  bp = bread(dev, 1);
  1011a6:	89 04 24             	mov    %eax,(%esp)
  1011a9:	e8 72 ef ff ff       	call   100120 <bread>
  memmove(sb, bp->data, sizeof(*sb));
  1011ae:	89 34 24             	mov    %esi,(%esp)
  1011b1:	c7 44 24 08 0c 00 00 	movl   $0xc,0x8(%esp)
  1011b8:	00 
static void
readsb(int dev, struct superblock *sb)
{
  struct buf *bp;
  
  bp = bread(dev, 1);
  1011b9:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
  1011bb:	8d 40 18             	lea    0x18(%eax),%eax
  1011be:	89 44 24 04          	mov    %eax,0x4(%esp)
  1011c2:	e8 89 2a 00 00       	call   103c50 <memmove>
  brelse(bp);
  1011c7:	89 1c 24             	mov    %ebx,(%esp)
  1011ca:	e8 a1 ee ff ff       	call   100070 <brelse>
}
  1011cf:	8b 5d f8             	mov    -0x8(%ebp),%ebx
  1011d2:	8b 75 fc             	mov    -0x4(%ebp),%esi
  1011d5:	89 ec                	mov    %ebp,%esp
  1011d7:	5d                   	pop    %ebp
  1011d8:	c3                   	ret    
  1011d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

001011e0 <balloc>:
// Blocks. 

// Allocate a disk block.
static uint
balloc(uint dev)
{
  1011e0:	55                   	push   %ebp
  1011e1:	89 e5                	mov    %esp,%ebp
  1011e3:	57                   	push   %edi
  1011e4:	56                   	push   %esi
  1011e5:	53                   	push   %ebx
  1011e6:	83 ec 3c             	sub    $0x3c,%esp
  int b, bi, m;
  struct buf *bp;
  struct superblock sb;

  bp = 0;
  readsb(dev, &sb);
  1011e9:	8d 55 dc             	lea    -0x24(%ebp),%edx
// Blocks. 

// Allocate a disk block.
static uint
balloc(uint dev)
{
  1011ec:	89 45 d0             	mov    %eax,-0x30(%ebp)
  int b, bi, m;
  struct buf *bp;
  struct superblock sb;

  bp = 0;
  readsb(dev, &sb);
  1011ef:	e8 9c ff ff ff       	call   101190 <readsb>
  for(b = 0; b < sb.size; b += BPB){
  1011f4:	8b 45 dc             	mov    -0x24(%ebp),%eax
  1011f7:	85 c0                	test   %eax,%eax
  1011f9:	0f 84 9c 00 00 00    	je     10129b <balloc+0xbb>
  1011ff:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
    bp = bread(dev, BBLOCK(b, sb.ninodes));
  101206:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  101209:	31 db                	xor    %ebx,%ebx
  10120b:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  10120e:	c1 e8 03             	shr    $0x3,%eax
  101211:	c1 fa 0c             	sar    $0xc,%edx
  101214:	8d 44 10 03          	lea    0x3(%eax,%edx,1),%eax
  101218:	89 44 24 04          	mov    %eax,0x4(%esp)
  10121c:	8b 45 d0             	mov    -0x30(%ebp),%eax
  10121f:	89 04 24             	mov    %eax,(%esp)
  101222:	e8 f9 ee ff ff       	call   100120 <bread>
  101227:	89 c6                	mov    %eax,%esi
  101229:	eb 10                	jmp    10123b <balloc+0x5b>
  10122b:	90                   	nop
  10122c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    for(bi = 0; bi < BPB; bi++){
  101230:	83 c3 01             	add    $0x1,%ebx
  101233:	81 fb 00 10 00 00    	cmp    $0x1000,%ebx
  101239:	74 45                	je     101280 <balloc+0xa0>
      m = 1 << (bi % 8);
  10123b:	89 d9                	mov    %ebx,%ecx
  10123d:	b8 01 00 00 00       	mov    $0x1,%eax
  101242:	83 e1 07             	and    $0x7,%ecx
  101245:	d3 e0                	shl    %cl,%eax
  101247:	89 c1                	mov    %eax,%ecx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
  101249:	89 d8                	mov    %ebx,%eax
  10124b:	c1 f8 03             	sar    $0x3,%eax
  10124e:	0f b6 54 06 18       	movzbl 0x18(%esi,%eax,1),%edx
  101253:	0f b6 fa             	movzbl %dl,%edi
  101256:	85 cf                	test   %ecx,%edi
  101258:	75 d6                	jne    101230 <balloc+0x50>
        bp->data[bi/8] |= m;  // Mark block in use on disk.
  10125a:	09 d1                	or     %edx,%ecx
  10125c:	88 4c 06 18          	mov    %cl,0x18(%esi,%eax,1)
        bwrite(bp);
  101260:	89 34 24             	mov    %esi,(%esp)
  101263:	e8 88 ee ff ff       	call   1000f0 <bwrite>
        brelse(bp);
  101268:	89 34 24             	mov    %esi,(%esp)
  10126b:	e8 00 ee ff ff       	call   100070 <brelse>
  101270:	8b 45 d4             	mov    -0x2c(%ebp),%eax
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}
  101273:	83 c4 3c             	add    $0x3c,%esp
    for(bi = 0; bi < BPB; bi++){
      m = 1 << (bi % 8);
      if((bp->data[bi/8] & m) == 0){  // Is block free?
        bp->data[bi/8] |= m;  // Mark block in use on disk.
        bwrite(bp);
        brelse(bp);
  101276:	8d 04 03             	lea    (%ebx,%eax,1),%eax
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}
  101279:	5b                   	pop    %ebx
  10127a:	5e                   	pop    %esi
  10127b:	5f                   	pop    %edi
  10127c:	5d                   	pop    %ebp
  10127d:	c3                   	ret    
  10127e:	66 90                	xchg   %ax,%ax
        bwrite(bp);
        brelse(bp);
        return b + bi;
      }
    }
    brelse(bp);
  101280:	89 34 24             	mov    %esi,(%esp)
  101283:	e8 e8 ed ff ff       	call   100070 <brelse>
  struct buf *bp;
  struct superblock sb;

  bp = 0;
  readsb(dev, &sb);
  for(b = 0; b < sb.size; b += BPB){
  101288:	81 45 d4 00 10 00 00 	addl   $0x1000,-0x2c(%ebp)
  10128f:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  101292:	39 45 dc             	cmp    %eax,-0x24(%ebp)
  101295:	0f 87 6b ff ff ff    	ja     101206 <balloc+0x26>
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
  10129b:	c7 04 24 1b 65 10 00 	movl   $0x10651b,(%esp)
  1012a2:	e8 69 f6 ff ff       	call   100910 <panic>
  1012a7:	89 f6                	mov    %esi,%esi
  1012a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

001012b0 <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
  1012b0:	55                   	push   %ebp
  1012b1:	89 e5                	mov    %esp,%ebp
  1012b3:	83 ec 38             	sub    $0x38,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
  1012b6:	83 fa 0b             	cmp    $0xb,%edx

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
  1012b9:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  1012bc:	89 c3                	mov    %eax,%ebx
  1012be:	89 75 f8             	mov    %esi,-0x8(%ebp)
  1012c1:	89 7d fc             	mov    %edi,-0x4(%ebp)
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
  1012c4:	77 1a                	ja     1012e0 <bmap+0x30>
    if((addr = ip->addrs[bn]) == 0)
  1012c6:	8d 7a 04             	lea    0x4(%edx),%edi
  1012c9:	8b 44 b8 0c          	mov    0xc(%eax,%edi,4),%eax
  1012cd:	85 c0                	test   %eax,%eax
  1012cf:	74 6f                	je     101340 <bmap+0x90>
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}
  1012d1:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  1012d4:	8b 75 f8             	mov    -0x8(%ebp),%esi
  1012d7:	8b 7d fc             	mov    -0x4(%ebp),%edi
  1012da:	89 ec                	mov    %ebp,%esp
  1012dc:	5d                   	pop    %ebp
  1012dd:	c3                   	ret    
  1012de:	66 90                	xchg   %ax,%ax
  if(bn < NDIRECT){
    if((addr = ip->addrs[bn]) == 0)
      ip->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;
  1012e0:	8d 7a f4             	lea    -0xc(%edx),%edi

  if(bn < NINDIRECT){
  1012e3:	83 ff 7f             	cmp    $0x7f,%edi
  1012e6:	77 7c                	ja     101364 <bmap+0xb4>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
  1012e8:	8b 40 4c             	mov    0x4c(%eax),%eax
  1012eb:	85 c0                	test   %eax,%eax
  1012ed:	74 69                	je     101358 <bmap+0xa8>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
  1012ef:	89 44 24 04          	mov    %eax,0x4(%esp)
  1012f3:	8b 03                	mov    (%ebx),%eax
  1012f5:	89 04 24             	mov    %eax,(%esp)
  1012f8:	e8 23 ee ff ff       	call   100120 <bread>
    a = (uint*)bp->data;
    if((addr = a[bn]) == 0){
  1012fd:	8d 7c b8 18          	lea    0x18(%eax,%edi,4),%edi

  if(bn < NINDIRECT){
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
  101301:	89 c6                	mov    %eax,%esi
    a = (uint*)bp->data;
    if((addr = a[bn]) == 0){
  101303:	8b 07                	mov    (%edi),%eax
  101305:	85 c0                	test   %eax,%eax
  101307:	75 17                	jne    101320 <bmap+0x70>
      a[bn] = addr = balloc(ip->dev);
  101309:	8b 03                	mov    (%ebx),%eax
  10130b:	e8 d0 fe ff ff       	call   1011e0 <balloc>
  101310:	89 07                	mov    %eax,(%edi)
      bwrite(bp);
  101312:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  101315:	89 34 24             	mov    %esi,(%esp)
  101318:	e8 d3 ed ff ff       	call   1000f0 <bwrite>
  10131d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    }
    brelse(bp);
  101320:	89 34 24             	mov    %esi,(%esp)
  101323:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  101326:	e8 45 ed ff ff       	call   100070 <brelse>
    return addr;
  10132b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  }

  panic("bmap: out of range");
}
  10132e:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  101331:	8b 75 f8             	mov    -0x8(%ebp),%esi
  101334:	8b 7d fc             	mov    -0x4(%ebp),%edi
  101337:	89 ec                	mov    %ebp,%esp
  101339:	5d                   	pop    %ebp
  10133a:	c3                   	ret    
  10133b:	90                   	nop
  10133c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
    if((addr = ip->addrs[bn]) == 0)
      ip->addrs[bn] = addr = balloc(ip->dev);
  101340:	8b 03                	mov    (%ebx),%eax
  101342:	e8 99 fe ff ff       	call   1011e0 <balloc>
  101347:	89 44 bb 0c          	mov    %eax,0xc(%ebx,%edi,4)
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}
  10134b:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  10134e:	8b 75 f8             	mov    -0x8(%ebp),%esi
  101351:	8b 7d fc             	mov    -0x4(%ebp),%edi
  101354:	89 ec                	mov    %ebp,%esp
  101356:	5d                   	pop    %ebp
  101357:	c3                   	ret    
  bn -= NDIRECT;

  if(bn < NINDIRECT){
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
  101358:	8b 03                	mov    (%ebx),%eax
  10135a:	e8 81 fe ff ff       	call   1011e0 <balloc>
  10135f:	89 43 4c             	mov    %eax,0x4c(%ebx)
  101362:	eb 8b                	jmp    1012ef <bmap+0x3f>
    }
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
  101364:	c7 04 24 31 65 10 00 	movl   $0x106531,(%esp)
  10136b:	e8 a0 f5 ff ff       	call   100910 <panic>

00101370 <readi>:
}

// Read data from inode.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
  101370:	55                   	push   %ebp
  101371:	89 e5                	mov    %esp,%ebp
  101373:	83 ec 38             	sub    $0x38,%esp
  101376:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  101379:	8b 5d 08             	mov    0x8(%ebp),%ebx
  10137c:	89 75 f8             	mov    %esi,-0x8(%ebp)
  10137f:	8b 4d 14             	mov    0x14(%ebp),%ecx
  101382:	89 7d fc             	mov    %edi,-0x4(%ebp)
  101385:	8b 75 10             	mov    0x10(%ebp),%esi
  101388:	8b 7d 0c             	mov    0xc(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
  10138b:	66 83 7b 10 03       	cmpw   $0x3,0x10(%ebx)
  101390:	74 1e                	je     1013b0 <readi+0x40>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  }

  if(off > ip->size || off + n < off)
  101392:	8b 43 18             	mov    0x18(%ebx),%eax
  101395:	39 f0                	cmp    %esi,%eax
  101397:	73 3f                	jae    1013d8 <readi+0x68>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
  101399:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  10139e:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  1013a1:	8b 75 f8             	mov    -0x8(%ebp),%esi
  1013a4:	8b 7d fc             	mov    -0x4(%ebp),%edi
  1013a7:	89 ec                	mov    %ebp,%esp
  1013a9:	5d                   	pop    %ebp
  1013aa:	c3                   	ret    
  1013ab:	90                   	nop
  1013ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
  1013b0:	0f b7 43 12          	movzwl 0x12(%ebx),%eax
  1013b4:	66 83 f8 09          	cmp    $0x9,%ax
  1013b8:	77 df                	ja     101399 <readi+0x29>
  1013ba:	98                   	cwtl   
  1013bb:	8b 04 c5 80 aa 10 00 	mov    0x10aa80(,%eax,8),%eax
  1013c2:	85 c0                	test   %eax,%eax
  1013c4:	74 d3                	je     101399 <readi+0x29>
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  1013c6:	89 4d 10             	mov    %ecx,0x10(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
}
  1013c9:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  1013cc:	8b 75 f8             	mov    -0x8(%ebp),%esi
  1013cf:	8b 7d fc             	mov    -0x4(%ebp),%edi
  1013d2:	89 ec                	mov    %ebp,%esp
  1013d4:	5d                   	pop    %ebp
  struct buf *bp;

  if(ip->type == T_DEV){
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  1013d5:	ff e0                	jmp    *%eax
  1013d7:	90                   	nop
  }

  if(off > ip->size || off + n < off)
  1013d8:	89 ca                	mov    %ecx,%edx
  1013da:	01 f2                	add    %esi,%edx
  1013dc:	89 55 e0             	mov    %edx,-0x20(%ebp)
  1013df:	72 b8                	jb     101399 <readi+0x29>
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;
  1013e1:	89 c2                	mov    %eax,%edx
  1013e3:	29 f2                	sub    %esi,%edx
  1013e5:	3b 45 e0             	cmp    -0x20(%ebp),%eax
  1013e8:	0f 42 ca             	cmovb  %edx,%ecx

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
  1013eb:	85 c9                	test   %ecx,%ecx
  1013ed:	74 7e                	je     10146d <readi+0xfd>
  1013ef:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
  1013f6:	89 7d e0             	mov    %edi,-0x20(%ebp)
  1013f9:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  1013fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
  101400:	89 f2                	mov    %esi,%edx
  101402:	89 d8                	mov    %ebx,%eax
  101404:	c1 ea 09             	shr    $0x9,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
  101407:	bf 00 02 00 00       	mov    $0x200,%edi
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
  10140c:	e8 9f fe ff ff       	call   1012b0 <bmap>
  101411:	89 44 24 04          	mov    %eax,0x4(%esp)
  101415:	8b 03                	mov    (%ebx),%eax
  101417:	89 04 24             	mov    %eax,(%esp)
  10141a:	e8 01 ed ff ff       	call   100120 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
  10141f:	8b 4d dc             	mov    -0x24(%ebp),%ecx
  101422:	2b 4d e4             	sub    -0x1c(%ebp),%ecx
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
  101425:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
  101427:	89 f0                	mov    %esi,%eax
  101429:	25 ff 01 00 00       	and    $0x1ff,%eax
  10142e:	29 c7                	sub    %eax,%edi
    memmove(dst, bp->data + off%BSIZE, m);
  101430:	8d 44 02 18          	lea    0x18(%edx,%eax,1),%eax
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
  101434:	39 cf                	cmp    %ecx,%edi
    memmove(dst, bp->data + off%BSIZE, m);
  101436:	89 44 24 04          	mov    %eax,0x4(%esp)
  10143a:	8b 45 e0             	mov    -0x20(%ebp),%eax
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
  10143d:	0f 47 f9             	cmova  %ecx,%edi
    memmove(dst, bp->data + off%BSIZE, m);
  101440:	89 55 d8             	mov    %edx,-0x28(%ebp)
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
  101443:	01 fe                	add    %edi,%esi
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
  101445:	89 7c 24 08          	mov    %edi,0x8(%esp)
  101449:	89 04 24             	mov    %eax,(%esp)
  10144c:	e8 ff 27 00 00       	call   103c50 <memmove>
    brelse(bp);
  101451:	8b 55 d8             	mov    -0x28(%ebp),%edx
  101454:	89 14 24             	mov    %edx,(%esp)
  101457:	e8 14 ec ff ff       	call   100070 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
  10145c:	01 7d e4             	add    %edi,-0x1c(%ebp)
  10145f:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  101462:	01 7d e0             	add    %edi,-0x20(%ebp)
  101465:	39 55 dc             	cmp    %edx,-0x24(%ebp)
  101468:	77 96                	ja     101400 <readi+0x90>
  10146a:	8b 4d dc             	mov    -0x24(%ebp),%ecx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
  10146d:	89 c8                	mov    %ecx,%eax
  10146f:	e9 2a ff ff ff       	jmp    10139e <readi+0x2e>
  101474:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  10147a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00101480 <iupdate>:
}

// Copy inode, which has changed, from memory to disk.
void
iupdate(struct inode *ip)
{
  101480:	55                   	push   %ebp
  101481:	89 e5                	mov    %esp,%ebp
  101483:	56                   	push   %esi
  101484:	53                   	push   %ebx
  101485:	83 ec 10             	sub    $0x10,%esp
  101488:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum));
  10148b:	8b 43 04             	mov    0x4(%ebx),%eax
  10148e:	c1 e8 03             	shr    $0x3,%eax
  101491:	83 c0 02             	add    $0x2,%eax
  101494:	89 44 24 04          	mov    %eax,0x4(%esp)
  101498:	8b 03                	mov    (%ebx),%eax
  10149a:	89 04 24             	mov    %eax,(%esp)
  10149d:	e8 7e ec ff ff       	call   100120 <bread>
  dip = (struct dinode*)bp->data + ip->inum%IPB;
  dip->type = ip->type;
  1014a2:	0f b7 53 10          	movzwl 0x10(%ebx),%edx
iupdate(struct inode *ip)
{
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum));
  1014a6:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
  1014a8:	8b 43 04             	mov    0x4(%ebx),%eax
  1014ab:	83 e0 07             	and    $0x7,%eax
  1014ae:	c1 e0 06             	shl    $0x6,%eax
  1014b1:	8d 44 06 18          	lea    0x18(%esi,%eax,1),%eax
  dip->type = ip->type;
  1014b5:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
  1014b8:	0f b7 53 12          	movzwl 0x12(%ebx),%edx
  1014bc:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
  1014c0:	0f b7 53 14          	movzwl 0x14(%ebx),%edx
  1014c4:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
  1014c8:	0f b7 53 16          	movzwl 0x16(%ebx),%edx
  1014cc:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
  1014d0:	8b 53 18             	mov    0x18(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
  1014d3:	83 c3 1c             	add    $0x1c,%ebx
  dip = (struct dinode*)bp->data + ip->inum%IPB;
  dip->type = ip->type;
  dip->major = ip->major;
  dip->minor = ip->minor;
  dip->nlink = ip->nlink;
  dip->size = ip->size;
  1014d6:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
  1014d9:	83 c0 0c             	add    $0xc,%eax
  1014dc:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  1014e0:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
  1014e7:	00 
  1014e8:	89 04 24             	mov    %eax,(%esp)
  1014eb:	e8 60 27 00 00       	call   103c50 <memmove>
  bwrite(bp);
  1014f0:	89 34 24             	mov    %esi,(%esp)
  1014f3:	e8 f8 eb ff ff       	call   1000f0 <bwrite>
  brelse(bp);
  1014f8:	89 75 08             	mov    %esi,0x8(%ebp)
}
  1014fb:	83 c4 10             	add    $0x10,%esp
  1014fe:	5b                   	pop    %ebx
  1014ff:	5e                   	pop    %esi
  101500:	5d                   	pop    %ebp
  dip->minor = ip->minor;
  dip->nlink = ip->nlink;
  dip->size = ip->size;
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
  bwrite(bp);
  brelse(bp);
  101501:	e9 6a eb ff ff       	jmp    100070 <brelse>
  101506:	8d 76 00             	lea    0x0(%esi),%esi
  101509:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00101510 <writei>:
}

// Write data to inode.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
  101510:	55                   	push   %ebp
  101511:	89 e5                	mov    %esp,%ebp
  101513:	83 ec 38             	sub    $0x38,%esp
  101516:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  101519:	8b 5d 08             	mov    0x8(%ebp),%ebx
  10151c:	89 75 f8             	mov    %esi,-0x8(%ebp)
  10151f:	8b 4d 14             	mov    0x14(%ebp),%ecx
  101522:	89 7d fc             	mov    %edi,-0x4(%ebp)
  101525:	8b 75 10             	mov    0x10(%ebp),%esi
  101528:	8b 7d 0c             	mov    0xc(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
  10152b:	66 83 7b 10 03       	cmpw   $0x3,0x10(%ebx)
  101530:	74 1e                	je     101550 <writei+0x40>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  }

  if(off > ip->size || off + n < off)
  101532:	39 73 18             	cmp    %esi,0x18(%ebx)
  101535:	73 41                	jae    101578 <writei+0x68>

  if(n > 0 && off > ip->size){
    ip->size = off;
    iupdate(ip);
  }
  return n;
  101537:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  10153c:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  10153f:	8b 75 f8             	mov    -0x8(%ebp),%esi
  101542:	8b 7d fc             	mov    -0x4(%ebp),%edi
  101545:	89 ec                	mov    %ebp,%esp
  101547:	5d                   	pop    %ebp
  101548:	c3                   	ret    
  101549:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
{
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
  101550:	0f b7 43 12          	movzwl 0x12(%ebx),%eax
  101554:	66 83 f8 09          	cmp    $0x9,%ax
  101558:	77 dd                	ja     101537 <writei+0x27>
  10155a:	98                   	cwtl   
  10155b:	8b 04 c5 84 aa 10 00 	mov    0x10aa84(,%eax,8),%eax
  101562:	85 c0                	test   %eax,%eax
  101564:	74 d1                	je     101537 <writei+0x27>
      return -1;
    return devsw[ip->major].write(ip, src, n);
  101566:	89 4d 10             	mov    %ecx,0x10(%ebp)
  if(n > 0 && off > ip->size){
    ip->size = off;
    iupdate(ip);
  }
  return n;
}
  101569:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  10156c:	8b 75 f8             	mov    -0x8(%ebp),%esi
  10156f:	8b 7d fc             	mov    -0x4(%ebp),%edi
  101572:	89 ec                	mov    %ebp,%esp
  101574:	5d                   	pop    %ebp
  struct buf *bp;

  if(ip->type == T_DEV){
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  101575:	ff e0                	jmp    *%eax
  101577:	90                   	nop
  }

  if(off > ip->size || off + n < off)
  101578:	89 c8                	mov    %ecx,%eax
  10157a:	01 f0                	add    %esi,%eax
  10157c:	72 b9                	jb     101537 <writei+0x27>
    return -1;
  if(off + n > MAXFILE*BSIZE)
  10157e:	3d 00 18 01 00       	cmp    $0x11800,%eax
  101583:	76 07                	jbe    10158c <writei+0x7c>
    n = MAXFILE*BSIZE - off;
  101585:	b9 00 18 01 00       	mov    $0x11800,%ecx
  10158a:	29 f1                	sub    %esi,%ecx

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
  10158c:	85 c9                	test   %ecx,%ecx
  10158e:	0f 84 91 00 00 00    	je     101625 <writei+0x115>
  101594:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
  10159b:	89 7d e0             	mov    %edi,-0x20(%ebp)
  10159e:	89 4d dc             	mov    %ecx,-0x24(%ebp)
  1015a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
  if(off + n > MAXFILE*BSIZE)
    n = MAXFILE*BSIZE - off;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
  1015a8:	89 f2                	mov    %esi,%edx
  1015aa:	89 d8                	mov    %ebx,%eax
  1015ac:	c1 ea 09             	shr    $0x9,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
  1015af:	bf 00 02 00 00       	mov    $0x200,%edi
    return -1;
  if(off + n > MAXFILE*BSIZE)
    n = MAXFILE*BSIZE - off;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
  1015b4:	e8 f7 fc ff ff       	call   1012b0 <bmap>
  1015b9:	89 44 24 04          	mov    %eax,0x4(%esp)
  1015bd:	8b 03                	mov    (%ebx),%eax
  1015bf:	89 04 24             	mov    %eax,(%esp)
  1015c2:	e8 59 eb ff ff       	call   100120 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
  1015c7:	8b 4d dc             	mov    -0x24(%ebp),%ecx
  1015ca:	2b 4d e4             	sub    -0x1c(%ebp),%ecx
    return -1;
  if(off + n > MAXFILE*BSIZE)
    n = MAXFILE*BSIZE - off;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
  1015cd:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
  1015cf:	89 f0                	mov    %esi,%eax
  1015d1:	25 ff 01 00 00       	and    $0x1ff,%eax
  1015d6:	29 c7                	sub    %eax,%edi
  1015d8:	39 cf                	cmp    %ecx,%edi
  1015da:	0f 47 f9             	cmova  %ecx,%edi
    memmove(bp->data + off%BSIZE, src, m);
  1015dd:	8b 4d e0             	mov    -0x20(%ebp),%ecx
  1015e0:	8d 44 02 18          	lea    0x18(%edx,%eax,1),%eax
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    n = MAXFILE*BSIZE - off;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
  1015e4:	01 fe                	add    %edi,%esi
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(bp->data + off%BSIZE, src, m);
  1015e6:	89 55 d8             	mov    %edx,-0x28(%ebp)
  1015e9:	89 7c 24 08          	mov    %edi,0x8(%esp)
  1015ed:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  1015f1:	89 04 24             	mov    %eax,(%esp)
  1015f4:	e8 57 26 00 00       	call   103c50 <memmove>
    bwrite(bp);
  1015f9:	8b 55 d8             	mov    -0x28(%ebp),%edx
  1015fc:	89 14 24             	mov    %edx,(%esp)
  1015ff:	e8 ec ea ff ff       	call   1000f0 <bwrite>
    brelse(bp);
  101604:	8b 55 d8             	mov    -0x28(%ebp),%edx
  101607:	89 14 24             	mov    %edx,(%esp)
  10160a:	e8 61 ea ff ff       	call   100070 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    n = MAXFILE*BSIZE - off;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
  10160f:	01 7d e4             	add    %edi,-0x1c(%ebp)
  101612:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  101615:	01 7d e0             	add    %edi,-0x20(%ebp)
  101618:	39 45 dc             	cmp    %eax,-0x24(%ebp)
  10161b:	77 8b                	ja     1015a8 <writei+0x98>
    memmove(bp->data + off%BSIZE, src, m);
    bwrite(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
  10161d:	3b 73 18             	cmp    0x18(%ebx),%esi
  101620:	8b 4d dc             	mov    -0x24(%ebp),%ecx
  101623:	77 07                	ja     10162c <writei+0x11c>
    ip->size = off;
    iupdate(ip);
  }
  return n;
  101625:	89 c8                	mov    %ecx,%eax
  101627:	e9 10 ff ff ff       	jmp    10153c <writei+0x2c>
    bwrite(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
    ip->size = off;
  10162c:	89 73 18             	mov    %esi,0x18(%ebx)
    iupdate(ip);
  10162f:	89 4d d8             	mov    %ecx,-0x28(%ebp)
  101632:	89 1c 24             	mov    %ebx,(%esp)
  101635:	e8 46 fe ff ff       	call   101480 <iupdate>
  10163a:	8b 4d d8             	mov    -0x28(%ebp),%ecx
  }
  return n;
  10163d:	89 c8                	mov    %ecx,%eax
  10163f:	e9 f8 fe ff ff       	jmp    10153c <writei+0x2c>
  101644:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  10164a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00101650 <namecmp>:

// Directories

int
namecmp(const char *s, const char *t)
{
  101650:	55                   	push   %ebp
  101651:	89 e5                	mov    %esp,%ebp
  101653:	83 ec 18             	sub    $0x18,%esp
  return strncmp(s, t, DIRSIZ);
  101656:	8b 45 0c             	mov    0xc(%ebp),%eax
  101659:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
  101660:	00 
  101661:	89 44 24 04          	mov    %eax,0x4(%esp)
  101665:	8b 45 08             	mov    0x8(%ebp),%eax
  101668:	89 04 24             	mov    %eax,(%esp)
  10166b:	e8 50 26 00 00       	call   103cc0 <strncmp>
}
  101670:	c9                   	leave  
  101671:	c3                   	ret    
  101672:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  101679:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00101680 <dirlookup>:
// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
// Caller must have already locked dp.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
  101680:	55                   	push   %ebp
  101681:	89 e5                	mov    %esp,%ebp
  101683:	57                   	push   %edi
  101684:	56                   	push   %esi
  101685:	53                   	push   %ebx
  101686:	83 ec 3c             	sub    $0x3c,%esp
  101689:	8b 45 08             	mov    0x8(%ebp),%eax
  10168c:	8b 55 10             	mov    0x10(%ebp),%edx
  10168f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  101692:	89 45 dc             	mov    %eax,-0x24(%ebp)
  101695:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  uint off, inum;
  struct buf *bp;
  struct dirent *de;

  if(dp->type != T_DIR)
  101698:	66 83 78 10 01       	cmpw   $0x1,0x10(%eax)
  10169d:	0f 85 d0 00 00 00    	jne    101773 <dirlookup+0xf3>
    panic("dirlookup not DIR");
  1016a3:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)

  for(off = 0; off < dp->size; off += BSIZE){
  1016aa:	8b 48 18             	mov    0x18(%eax),%ecx
  1016ad:	85 c9                	test   %ecx,%ecx
  1016af:	0f 84 b4 00 00 00    	je     101769 <dirlookup+0xe9>
    bp = bread(dp->dev, bmap(dp, off / BSIZE));
  1016b5:	8b 55 e0             	mov    -0x20(%ebp),%edx
  1016b8:	8b 45 dc             	mov    -0x24(%ebp),%eax
  1016bb:	c1 ea 09             	shr    $0x9,%edx
  1016be:	e8 ed fb ff ff       	call   1012b0 <bmap>
  1016c3:	8b 4d dc             	mov    -0x24(%ebp),%ecx
  1016c6:	89 44 24 04          	mov    %eax,0x4(%esp)
  1016ca:	8b 01                	mov    (%ecx),%eax
  1016cc:	89 04 24             	mov    %eax,(%esp)
  1016cf:	e8 4c ea ff ff       	call   100120 <bread>
  1016d4:	89 45 e4             	mov    %eax,-0x1c(%ebp)

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
// Caller must have already locked dp.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
  1016d7:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += BSIZE){
    bp = bread(dp->dev, bmap(dp, off / BSIZE));
    for(de = (struct dirent*)bp->data;
  1016da:	83 c0 18             	add    $0x18,%eax
  1016dd:	89 45 d8             	mov    %eax,-0x28(%ebp)
  1016e0:	89 c6                	mov    %eax,%esi

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
// Caller must have already locked dp.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
  1016e2:	81 c7 18 02 00 00    	add    $0x218,%edi
  1016e8:	eb 0d                	jmp    1016f7 <dirlookup+0x77>
  1016ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

  for(off = 0; off < dp->size; off += BSIZE){
    bp = bread(dp->dev, bmap(dp, off / BSIZE));
    for(de = (struct dirent*)bp->data;
        de < (struct dirent*)(bp->data + BSIZE);
        de++){
  1016f0:	83 c6 10             	add    $0x10,%esi
  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += BSIZE){
    bp = bread(dp->dev, bmap(dp, off / BSIZE));
    for(de = (struct dirent*)bp->data;
  1016f3:	39 fe                	cmp    %edi,%esi
  1016f5:	74 51                	je     101748 <dirlookup+0xc8>
        de < (struct dirent*)(bp->data + BSIZE);
        de++){
      if(de->inum == 0)
  1016f7:	66 83 3e 00          	cmpw   $0x0,(%esi)
  1016fb:	74 f3                	je     1016f0 <dirlookup+0x70>
        continue;
      if(namecmp(name, de->name) == 0){
  1016fd:	8d 46 02             	lea    0x2(%esi),%eax
  101700:	89 44 24 04          	mov    %eax,0x4(%esp)
  101704:	89 1c 24             	mov    %ebx,(%esp)
  101707:	e8 44 ff ff ff       	call   101650 <namecmp>
  10170c:	85 c0                	test   %eax,%eax
  10170e:	75 e0                	jne    1016f0 <dirlookup+0x70>
        // entry matches path element
        if(poff)
  101710:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  101713:	85 d2                	test   %edx,%edx
  101715:	74 0e                	je     101725 <dirlookup+0xa5>
          *poff = off + (uchar*)de - bp->data;
  101717:	8b 45 e0             	mov    -0x20(%ebp),%eax
  10171a:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  10171d:	8d 04 06             	lea    (%esi,%eax,1),%eax
  101720:	2b 45 d8             	sub    -0x28(%ebp),%eax
  101723:	89 02                	mov    %eax,(%edx)
        inum = de->inum;
        brelse(bp);
  101725:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
        continue;
      if(namecmp(name, de->name) == 0){
        // entry matches path element
        if(poff)
          *poff = off + (uchar*)de - bp->data;
        inum = de->inum;
  101728:	0f b7 1e             	movzwl (%esi),%ebx
        brelse(bp);
  10172b:	89 0c 24             	mov    %ecx,(%esp)
  10172e:	e8 3d e9 ff ff       	call   100070 <brelse>
        return iget(dp->dev, inum);
  101733:	8b 4d dc             	mov    -0x24(%ebp),%ecx
  101736:	89 da                	mov    %ebx,%edx
  101738:	8b 01                	mov    (%ecx),%eax
      }
    }
    brelse(bp);
  }
  return 0;
}
  10173a:	83 c4 3c             	add    $0x3c,%esp
  10173d:	5b                   	pop    %ebx
  10173e:	5e                   	pop    %esi
  10173f:	5f                   	pop    %edi
  101740:	5d                   	pop    %ebp
        // entry matches path element
        if(poff)
          *poff = off + (uchar*)de - bp->data;
        inum = de->inum;
        brelse(bp);
        return iget(dp->dev, inum);
  101741:	e9 8a f9 ff ff       	jmp    1010d0 <iget>
  101746:	66 90                	xchg   %ax,%ax
      }
    }
    brelse(bp);
  101748:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  10174b:	89 04 24             	mov    %eax,(%esp)
  10174e:	e8 1d e9 ff ff       	call   100070 <brelse>
  struct dirent *de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += BSIZE){
  101753:	8b 55 dc             	mov    -0x24(%ebp),%edx
  101756:	81 45 e0 00 02 00 00 	addl   $0x200,-0x20(%ebp)
  10175d:	8b 4d e0             	mov    -0x20(%ebp),%ecx
  101760:	39 4a 18             	cmp    %ecx,0x18(%edx)
  101763:	0f 87 4c ff ff ff    	ja     1016b5 <dirlookup+0x35>
      }
    }
    brelse(bp);
  }
  return 0;
}
  101769:	83 c4 3c             	add    $0x3c,%esp
  10176c:	31 c0                	xor    %eax,%eax
  10176e:	5b                   	pop    %ebx
  10176f:	5e                   	pop    %esi
  101770:	5f                   	pop    %edi
  101771:	5d                   	pop    %ebp
  101772:	c3                   	ret    
  uint off, inum;
  struct buf *bp;
  struct dirent *de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");
  101773:	c7 04 24 44 65 10 00 	movl   $0x106544,(%esp)
  10177a:	e8 91 f1 ff ff       	call   100910 <panic>
  10177f:	90                   	nop

00101780 <iunlock>:
}

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
  101780:	55                   	push   %ebp
  101781:	89 e5                	mov    %esp,%ebp
  101783:	53                   	push   %ebx
  101784:	83 ec 14             	sub    $0x14,%esp
  101787:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
  10178a:	85 db                	test   %ebx,%ebx
  10178c:	74 36                	je     1017c4 <iunlock+0x44>
  10178e:	f6 43 0c 01          	testb  $0x1,0xc(%ebx)
  101792:	74 30                	je     1017c4 <iunlock+0x44>
  101794:	8b 43 08             	mov    0x8(%ebx),%eax
  101797:	85 c0                	test   %eax,%eax
  101799:	7e 29                	jle    1017c4 <iunlock+0x44>
    panic("iunlock");

  acquire(&icache.lock);
  10179b:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  1017a2:	e8 89 23 00 00       	call   103b30 <acquire>
  ip->flags &= ~I_BUSY;
  1017a7:	83 63 0c fe          	andl   $0xfffffffe,0xc(%ebx)
  wakeup(ip);
  1017ab:	89 1c 24             	mov    %ebx,(%esp)
  1017ae:	e8 8d 19 00 00       	call   103140 <wakeup>
  release(&icache.lock);
  1017b3:	c7 45 08 e0 aa 10 00 	movl   $0x10aae0,0x8(%ebp)
}
  1017ba:	83 c4 14             	add    $0x14,%esp
  1017bd:	5b                   	pop    %ebx
  1017be:	5d                   	pop    %ebp
    panic("iunlock");

  acquire(&icache.lock);
  ip->flags &= ~I_BUSY;
  wakeup(ip);
  release(&icache.lock);
  1017bf:	e9 1c 23 00 00       	jmp    103ae0 <release>
// Unlock the given inode.
void
iunlock(struct inode *ip)
{
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
    panic("iunlock");
  1017c4:	c7 04 24 56 65 10 00 	movl   $0x106556,(%esp)
  1017cb:	e8 40 f1 ff ff       	call   100910 <panic>

001017d0 <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
  1017d0:	55                   	push   %ebp
  1017d1:	89 e5                	mov    %esp,%ebp
  1017d3:	57                   	push   %edi
  1017d4:	56                   	push   %esi
  1017d5:	89 c6                	mov    %eax,%esi
  1017d7:	53                   	push   %ebx
  1017d8:	89 d3                	mov    %edx,%ebx
  1017da:	83 ec 2c             	sub    $0x2c,%esp
static void
bzero(int dev, int bno)
{
  struct buf *bp;
  
  bp = bread(dev, bno);
  1017dd:	89 54 24 04          	mov    %edx,0x4(%esp)
  1017e1:	89 04 24             	mov    %eax,(%esp)
  1017e4:	e8 37 e9 ff ff       	call   100120 <bread>
  memset(bp->data, 0, BSIZE);
  1017e9:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
  1017f0:	00 
  1017f1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1017f8:	00 
static void
bzero(int dev, int bno)
{
  struct buf *bp;
  
  bp = bread(dev, bno);
  1017f9:	89 c7                	mov    %eax,%edi
  memset(bp->data, 0, BSIZE);
  1017fb:	8d 40 18             	lea    0x18(%eax),%eax
  1017fe:	89 04 24             	mov    %eax,(%esp)
  101801:	e8 ca 23 00 00       	call   103bd0 <memset>
  bwrite(bp);
  101806:	89 3c 24             	mov    %edi,(%esp)
  101809:	e8 e2 e8 ff ff       	call   1000f0 <bwrite>
  brelse(bp);
  10180e:	89 3c 24             	mov    %edi,(%esp)
  101811:	e8 5a e8 ff ff       	call   100070 <brelse>
  struct superblock sb;
  int bi, m;

  bzero(dev, b);

  readsb(dev, &sb);
  101816:	89 f0                	mov    %esi,%eax
  101818:	8d 55 dc             	lea    -0x24(%ebp),%edx
  10181b:	e8 70 f9 ff ff       	call   101190 <readsb>
  bp = bread(dev, BBLOCK(b, sb.ninodes));
  101820:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  101823:	89 da                	mov    %ebx,%edx
  101825:	c1 ea 0c             	shr    $0xc,%edx
  101828:	89 34 24             	mov    %esi,(%esp)
  bi = b % BPB;
  m = 1 << (bi % 8);
  10182b:	be 01 00 00 00       	mov    $0x1,%esi
  int bi, m;

  bzero(dev, b);

  readsb(dev, &sb);
  bp = bread(dev, BBLOCK(b, sb.ninodes));
  101830:	c1 e8 03             	shr    $0x3,%eax
  101833:	8d 44 10 03          	lea    0x3(%eax,%edx,1),%eax
  101837:	89 44 24 04          	mov    %eax,0x4(%esp)
  10183b:	e8 e0 e8 ff ff       	call   100120 <bread>
  bi = b % BPB;
  101840:	89 da                	mov    %ebx,%edx
  m = 1 << (bi % 8);
  101842:	89 d9                	mov    %ebx,%ecx

  bzero(dev, b);

  readsb(dev, &sb);
  bp = bread(dev, BBLOCK(b, sb.ninodes));
  bi = b % BPB;
  101844:	81 e2 ff 0f 00 00    	and    $0xfff,%edx
  m = 1 << (bi % 8);
  10184a:	83 e1 07             	and    $0x7,%ecx
  if((bp->data[bi/8] & m) == 0)
  10184d:	c1 fa 03             	sar    $0x3,%edx
  bzero(dev, b);

  readsb(dev, &sb);
  bp = bread(dev, BBLOCK(b, sb.ninodes));
  bi = b % BPB;
  m = 1 << (bi % 8);
  101850:	d3 e6                	shl    %cl,%esi
  if((bp->data[bi/8] & m) == 0)
  101852:	0f b6 4c 10 18       	movzbl 0x18(%eax,%edx,1),%ecx
  int bi, m;

  bzero(dev, b);

  readsb(dev, &sb);
  bp = bread(dev, BBLOCK(b, sb.ninodes));
  101857:	89 c7                	mov    %eax,%edi
  bi = b % BPB;
  m = 1 << (bi % 8);
  if((bp->data[bi/8] & m) == 0)
  101859:	0f b6 c1             	movzbl %cl,%eax
  10185c:	85 f0                	test   %esi,%eax
  10185e:	74 22                	je     101882 <bfree+0xb2>
    panic("freeing free block");
  bp->data[bi/8] &= ~m;  // Mark block free on disk.
  101860:	89 f0                	mov    %esi,%eax
  101862:	f7 d0                	not    %eax
  101864:	21 c8                	and    %ecx,%eax
  101866:	88 44 17 18          	mov    %al,0x18(%edi,%edx,1)
  bwrite(bp);
  10186a:	89 3c 24             	mov    %edi,(%esp)
  10186d:	e8 7e e8 ff ff       	call   1000f0 <bwrite>
  brelse(bp);
  101872:	89 3c 24             	mov    %edi,(%esp)
  101875:	e8 f6 e7 ff ff       	call   100070 <brelse>
}
  10187a:	83 c4 2c             	add    $0x2c,%esp
  10187d:	5b                   	pop    %ebx
  10187e:	5e                   	pop    %esi
  10187f:	5f                   	pop    %edi
  101880:	5d                   	pop    %ebp
  101881:	c3                   	ret    
  readsb(dev, &sb);
  bp = bread(dev, BBLOCK(b, sb.ninodes));
  bi = b % BPB;
  m = 1 << (bi % 8);
  if((bp->data[bi/8] & m) == 0)
    panic("freeing free block");
  101882:	c7 04 24 5e 65 10 00 	movl   $0x10655e,(%esp)
  101889:	e8 82 f0 ff ff       	call   100910 <panic>
  10188e:	66 90                	xchg   %ax,%ax

00101890 <iput>:
}

// Caller holds reference to unlocked ip.  Drop reference.
void
iput(struct inode *ip)
{
  101890:	55                   	push   %ebp
  101891:	89 e5                	mov    %esp,%ebp
  101893:	57                   	push   %edi
  101894:	56                   	push   %esi
  101895:	53                   	push   %ebx
  101896:	83 ec 2c             	sub    $0x2c,%esp
  101899:	8b 75 08             	mov    0x8(%ebp),%esi
  acquire(&icache.lock);
  10189c:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  1018a3:	e8 88 22 00 00       	call   103b30 <acquire>
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
  1018a8:	8b 46 08             	mov    0x8(%esi),%eax
  1018ab:	83 f8 01             	cmp    $0x1,%eax
  1018ae:	0f 85 a1 00 00 00    	jne    101955 <iput+0xc5>
  1018b4:	8b 56 0c             	mov    0xc(%esi),%edx
  1018b7:	f6 c2 02             	test   $0x2,%dl
  1018ba:	0f 84 95 00 00 00    	je     101955 <iput+0xc5>
  1018c0:	66 83 7e 16 00       	cmpw   $0x0,0x16(%esi)
  1018c5:	0f 85 8a 00 00 00    	jne    101955 <iput+0xc5>
    // inode is no longer used: truncate and free inode.
    if(ip->flags & I_BUSY)
  1018cb:	f6 c2 01             	test   $0x1,%dl
  1018ce:	0f 85 fa 00 00 00    	jne    1019ce <iput+0x13e>
      panic("iput busy");
    ip->flags |= I_BUSY;
  1018d4:	83 ca 01             	or     $0x1,%edx
    release(&icache.lock);
  1018d7:	89 f3                	mov    %esi,%ebx
  acquire(&icache.lock);
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
    // inode is no longer used: truncate and free inode.
    if(ip->flags & I_BUSY)
      panic("iput busy");
    ip->flags |= I_BUSY;
  1018d9:	89 56 0c             	mov    %edx,0xc(%esi)
  release(&icache.lock);
}

// Caller holds reference to unlocked ip.  Drop reference.
void
iput(struct inode *ip)
  1018dc:	8d 7e 30             	lea    0x30(%esi),%edi
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
    // inode is no longer used: truncate and free inode.
    if(ip->flags & I_BUSY)
      panic("iput busy");
    ip->flags |= I_BUSY;
    release(&icache.lock);
  1018df:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  1018e6:	e8 f5 21 00 00       	call   103ae0 <release>
  1018eb:	eb 0a                	jmp    1018f7 <iput+0x67>
  1018ed:	8d 76 00             	lea    0x0(%esi),%esi
  uint *a;

  for(i = 0; i < NDIRECT; i++){
    if(ip->addrs[i]){
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
  1018f0:	83 c3 04             	add    $0x4,%ebx
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
  1018f3:	39 fb                	cmp    %edi,%ebx
  1018f5:	74 1c                	je     101913 <iput+0x83>
    if(ip->addrs[i]){
  1018f7:	8b 53 1c             	mov    0x1c(%ebx),%edx
  1018fa:	85 d2                	test   %edx,%edx
  1018fc:	74 f2                	je     1018f0 <iput+0x60>
      bfree(ip->dev, ip->addrs[i]);
  1018fe:	8b 06                	mov    (%esi),%eax
  101900:	e8 cb fe ff ff       	call   1017d0 <bfree>
      ip->addrs[i] = 0;
  101905:	c7 43 1c 00 00 00 00 	movl   $0x0,0x1c(%ebx)
  10190c:	83 c3 04             	add    $0x4,%ebx
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
  10190f:	39 fb                	cmp    %edi,%ebx
  101911:	75 e4                	jne    1018f7 <iput+0x67>
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }
  
  if(ip->addrs[NDIRECT]){
  101913:	8b 46 4c             	mov    0x4c(%esi),%eax
  101916:	85 c0                	test   %eax,%eax
  101918:	75 56                	jne    101970 <iput+0xe0>
    brelse(bp);
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  ip->size = 0;
  10191a:	c7 46 18 00 00 00 00 	movl   $0x0,0x18(%esi)
  iupdate(ip);
  101921:	89 34 24             	mov    %esi,(%esp)
  101924:	e8 57 fb ff ff       	call   101480 <iupdate>
    if(ip->flags & I_BUSY)
      panic("iput busy");
    ip->flags |= I_BUSY;
    release(&icache.lock);
    itrunc(ip);
    ip->type = 0;
  101929:	66 c7 46 10 00 00    	movw   $0x0,0x10(%esi)
    iupdate(ip);
  10192f:	89 34 24             	mov    %esi,(%esp)
  101932:	e8 49 fb ff ff       	call   101480 <iupdate>
    acquire(&icache.lock);
  101937:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  10193e:	e8 ed 21 00 00       	call   103b30 <acquire>
    ip->flags = 0;
  101943:	c7 46 0c 00 00 00 00 	movl   $0x0,0xc(%esi)
    wakeup(ip);
  10194a:	89 34 24             	mov    %esi,(%esp)
  10194d:	e8 ee 17 00 00       	call   103140 <wakeup>
  101952:	8b 46 08             	mov    0x8(%esi),%eax
  }
  ip->ref--;
  101955:	83 e8 01             	sub    $0x1,%eax
  101958:	89 46 08             	mov    %eax,0x8(%esi)
  release(&icache.lock);
  10195b:	c7 45 08 e0 aa 10 00 	movl   $0x10aae0,0x8(%ebp)
}
  101962:	83 c4 2c             	add    $0x2c,%esp
  101965:	5b                   	pop    %ebx
  101966:	5e                   	pop    %esi
  101967:	5f                   	pop    %edi
  101968:	5d                   	pop    %ebp
    acquire(&icache.lock);
    ip->flags = 0;
    wakeup(ip);
  }
  ip->ref--;
  release(&icache.lock);
  101969:	e9 72 21 00 00       	jmp    103ae0 <release>
  10196e:	66 90                	xchg   %ax,%ax
      ip->addrs[i] = 0;
    }
  }
  
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
  101970:	89 44 24 04          	mov    %eax,0x4(%esp)
  101974:	8b 06                	mov    (%esi),%eax
    a = (uint*)bp->data;
  101976:	31 db                	xor    %ebx,%ebx
      ip->addrs[i] = 0;
    }
  }
  
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
  101978:	89 04 24             	mov    %eax,(%esp)
  10197b:	e8 a0 e7 ff ff       	call   100120 <bread>
    a = (uint*)bp->data;
  101980:	89 c7                	mov    %eax,%edi
      ip->addrs[i] = 0;
    }
  }
  
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
  101982:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    a = (uint*)bp->data;
  101985:	83 c7 18             	add    $0x18,%edi
  101988:	31 c0                	xor    %eax,%eax
  10198a:	eb 11                	jmp    10199d <iput+0x10d>
  10198c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    for(j = 0; j < NINDIRECT; j++){
  101990:	83 c3 01             	add    $0x1,%ebx
  101993:	81 fb 80 00 00 00    	cmp    $0x80,%ebx
  101999:	89 d8                	mov    %ebx,%eax
  10199b:	74 10                	je     1019ad <iput+0x11d>
      if(a[j])
  10199d:	8b 14 87             	mov    (%edi,%eax,4),%edx
  1019a0:	85 d2                	test   %edx,%edx
  1019a2:	74 ec                	je     101990 <iput+0x100>
        bfree(ip->dev, a[j]);
  1019a4:	8b 06                	mov    (%esi),%eax
  1019a6:	e8 25 fe ff ff       	call   1017d0 <bfree>
  1019ab:	eb e3                	jmp    101990 <iput+0x100>
    }
    brelse(bp);
  1019ad:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  1019b0:	89 04 24             	mov    %eax,(%esp)
  1019b3:	e8 b8 e6 ff ff       	call   100070 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
  1019b8:	8b 56 4c             	mov    0x4c(%esi),%edx
  1019bb:	8b 06                	mov    (%esi),%eax
  1019bd:	e8 0e fe ff ff       	call   1017d0 <bfree>
    ip->addrs[NDIRECT] = 0;
  1019c2:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  1019c9:	e9 4c ff ff ff       	jmp    10191a <iput+0x8a>
{
  acquire(&icache.lock);
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
    // inode is no longer used: truncate and free inode.
    if(ip->flags & I_BUSY)
      panic("iput busy");
  1019ce:	c7 04 24 71 65 10 00 	movl   $0x106571,(%esp)
  1019d5:	e8 36 ef ff ff       	call   100910 <panic>
  1019da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

001019e0 <dirlink>:
}

// Write a new directory entry (name, inum) into the directory dp.
int
dirlink(struct inode *dp, char *name, uint inum)
{
  1019e0:	55                   	push   %ebp
  1019e1:	89 e5                	mov    %esp,%ebp
  1019e3:	57                   	push   %edi
  1019e4:	56                   	push   %esi
  1019e5:	53                   	push   %ebx
  1019e6:	83 ec 2c             	sub    $0x2c,%esp
  1019e9:	8b 75 08             	mov    0x8(%ebp),%esi
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
  1019ec:	8b 45 0c             	mov    0xc(%ebp),%eax
  1019ef:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  1019f6:	00 
  1019f7:	89 34 24             	mov    %esi,(%esp)
  1019fa:	89 44 24 04          	mov    %eax,0x4(%esp)
  1019fe:	e8 7d fc ff ff       	call   101680 <dirlookup>
  101a03:	85 c0                	test   %eax,%eax
  101a05:	0f 85 89 00 00 00    	jne    101a94 <dirlink+0xb4>
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
  101a0b:	8b 56 18             	mov    0x18(%esi),%edx
  101a0e:	85 d2                	test   %edx,%edx
  101a10:	0f 84 8d 00 00 00    	je     101aa3 <dirlink+0xc3>
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
    iput(ip);
    return -1;
  101a16:	8d 7d d8             	lea    -0x28(%ebp),%edi
  101a19:	31 db                	xor    %ebx,%ebx
  101a1b:	eb 0b                	jmp    101a28 <dirlink+0x48>
  101a1d:	8d 76 00             	lea    0x0(%esi),%esi
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
  101a20:	83 c3 10             	add    $0x10,%ebx
  101a23:	39 5e 18             	cmp    %ebx,0x18(%esi)
  101a26:	76 24                	jbe    101a4c <dirlink+0x6c>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
  101a28:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
  101a2f:	00 
  101a30:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  101a34:	89 7c 24 04          	mov    %edi,0x4(%esp)
  101a38:	89 34 24             	mov    %esi,(%esp)
  101a3b:	e8 30 f9 ff ff       	call   101370 <readi>
  101a40:	83 f8 10             	cmp    $0x10,%eax
  101a43:	75 65                	jne    101aaa <dirlink+0xca>
      panic("dirlink read");
    if(de.inum == 0)
  101a45:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
  101a4a:	75 d4                	jne    101a20 <dirlink+0x40>
      break;
  }

  strncpy(de.name, name, DIRSIZ);
  101a4c:	8b 45 0c             	mov    0xc(%ebp),%eax
  101a4f:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
  101a56:	00 
  101a57:	89 44 24 04          	mov    %eax,0x4(%esp)
  101a5b:	8d 45 da             	lea    -0x26(%ebp),%eax
  101a5e:	89 04 24             	mov    %eax,(%esp)
  101a61:	e8 ba 22 00 00       	call   103d20 <strncpy>
  de.inum = inum;
  101a66:	8b 45 10             	mov    0x10(%ebp),%eax
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
  101a69:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
  101a70:	00 
  101a71:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  101a75:	89 7c 24 04          	mov    %edi,0x4(%esp)
    if(de.inum == 0)
      break;
  }

  strncpy(de.name, name, DIRSIZ);
  de.inum = inum;
  101a79:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
  101a7d:	89 34 24             	mov    %esi,(%esp)
  101a80:	e8 8b fa ff ff       	call   101510 <writei>
  101a85:	83 f8 10             	cmp    $0x10,%eax
  101a88:	75 2c                	jne    101ab6 <dirlink+0xd6>
    panic("dirlink");
  101a8a:	31 c0                	xor    %eax,%eax
  
  return 0;
}
  101a8c:	83 c4 2c             	add    $0x2c,%esp
  101a8f:	5b                   	pop    %ebx
  101a90:	5e                   	pop    %esi
  101a91:	5f                   	pop    %edi
  101a92:	5d                   	pop    %ebp
  101a93:	c3                   	ret    
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
    iput(ip);
  101a94:	89 04 24             	mov    %eax,(%esp)
  101a97:	e8 f4 fd ff ff       	call   101890 <iput>
  101a9c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    return -1;
  101aa1:	eb e9                	jmp    101a8c <dirlink+0xac>
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
  101aa3:	8d 7d d8             	lea    -0x28(%ebp),%edi
  101aa6:	31 db                	xor    %ebx,%ebx
  101aa8:	eb a2                	jmp    101a4c <dirlink+0x6c>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
  101aaa:	c7 04 24 7b 65 10 00 	movl   $0x10657b,(%esp)
  101ab1:	e8 5a ee ff ff       	call   100910 <panic>
  }

  strncpy(de.name, name, DIRSIZ);
  de.inum = inum;
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
    panic("dirlink");
  101ab6:	c7 04 24 1e 6b 10 00 	movl   $0x106b1e,(%esp)
  101abd:	e8 4e ee ff ff       	call   100910 <panic>
  101ac2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  101ac9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00101ad0 <iunlockput>:
}

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
  101ad0:	55                   	push   %ebp
  101ad1:	89 e5                	mov    %esp,%ebp
  101ad3:	53                   	push   %ebx
  101ad4:	83 ec 14             	sub    $0x14,%esp
  101ad7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  iunlock(ip);
  101ada:	89 1c 24             	mov    %ebx,(%esp)
  101add:	e8 9e fc ff ff       	call   101780 <iunlock>
  iput(ip);
  101ae2:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
  101ae5:	83 c4 14             	add    $0x14,%esp
  101ae8:	5b                   	pop    %ebx
  101ae9:	5d                   	pop    %ebp
// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
  iunlock(ip);
  iput(ip);
  101aea:	e9 a1 fd ff ff       	jmp    101890 <iput>
  101aef:	90                   	nop

00101af0 <ialloc>:
static struct inode* iget(uint dev, uint inum);

// Allocate a new inode with the given type on device dev.
struct inode*
ialloc(uint dev, short type)
{
  101af0:	55                   	push   %ebp
  101af1:	89 e5                	mov    %esp,%ebp
  101af3:	57                   	push   %edi
  101af4:	56                   	push   %esi
  101af5:	53                   	push   %ebx
  101af6:	83 ec 3c             	sub    $0x3c,%esp
  101af9:	0f b7 45 0c          	movzwl 0xc(%ebp),%eax
  int inum;
  struct buf *bp;
  struct dinode *dip;
  struct superblock sb;

  readsb(dev, &sb);
  101afd:	8d 55 dc             	lea    -0x24(%ebp),%edx
static struct inode* iget(uint dev, uint inum);

// Allocate a new inode with the given type on device dev.
struct inode*
ialloc(uint dev, short type)
{
  101b00:	66 89 45 d6          	mov    %ax,-0x2a(%ebp)
  int inum;
  struct buf *bp;
  struct dinode *dip;
  struct superblock sb;

  readsb(dev, &sb);
  101b04:	8b 45 08             	mov    0x8(%ebp),%eax
  101b07:	e8 84 f6 ff ff       	call   101190 <readsb>
  for(inum = 1; inum < sb.ninodes; inum++){  // loop over inode blocks
  101b0c:	83 7d e4 01          	cmpl   $0x1,-0x1c(%ebp)
  101b10:	0f 86 96 00 00 00    	jbe    101bac <ialloc+0xbc>
  101b16:	be 01 00 00 00       	mov    $0x1,%esi
  101b1b:	bb 01 00 00 00       	mov    $0x1,%ebx
  101b20:	eb 18                	jmp    101b3a <ialloc+0x4a>
  101b22:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  101b28:	83 c3 01             	add    $0x1,%ebx
      dip->type = type;
      bwrite(bp);   // mark it allocated on the disk
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  101b2b:	89 3c 24             	mov    %edi,(%esp)
  struct buf *bp;
  struct dinode *dip;
  struct superblock sb;

  readsb(dev, &sb);
  for(inum = 1; inum < sb.ninodes; inum++){  // loop over inode blocks
  101b2e:	89 de                	mov    %ebx,%esi
      dip->type = type;
      bwrite(bp);   // mark it allocated on the disk
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  101b30:	e8 3b e5 ff ff       	call   100070 <brelse>
  struct buf *bp;
  struct dinode *dip;
  struct superblock sb;

  readsb(dev, &sb);
  for(inum = 1; inum < sb.ninodes; inum++){  // loop over inode blocks
  101b35:	39 5d e4             	cmp    %ebx,-0x1c(%ebp)
  101b38:	76 72                	jbe    101bac <ialloc+0xbc>
    bp = bread(dev, IBLOCK(inum));
  101b3a:	89 f0                	mov    %esi,%eax
  101b3c:	c1 e8 03             	shr    $0x3,%eax
  101b3f:	83 c0 02             	add    $0x2,%eax
  101b42:	89 44 24 04          	mov    %eax,0x4(%esp)
  101b46:	8b 45 08             	mov    0x8(%ebp),%eax
  101b49:	89 04 24             	mov    %eax,(%esp)
  101b4c:	e8 cf e5 ff ff       	call   100120 <bread>
  101b51:	89 c7                	mov    %eax,%edi
    dip = (struct dinode*)bp->data + inum%IPB;
  101b53:	89 f0                	mov    %esi,%eax
  101b55:	83 e0 07             	and    $0x7,%eax
  101b58:	c1 e0 06             	shl    $0x6,%eax
  101b5b:	8d 54 07 18          	lea    0x18(%edi,%eax,1),%edx
    if(dip->type == 0){  // a free inode
  101b5f:	66 83 3a 00          	cmpw   $0x0,(%edx)
  101b63:	75 c3                	jne    101b28 <ialloc+0x38>
      memset(dip, 0, sizeof(*dip));
  101b65:	89 14 24             	mov    %edx,(%esp)
  101b68:	89 55 d0             	mov    %edx,-0x30(%ebp)
  101b6b:	c7 44 24 08 40 00 00 	movl   $0x40,0x8(%esp)
  101b72:	00 
  101b73:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  101b7a:	00 
  101b7b:	e8 50 20 00 00       	call   103bd0 <memset>
      dip->type = type;
  101b80:	8b 55 d0             	mov    -0x30(%ebp),%edx
  101b83:	0f b7 45 d6          	movzwl -0x2a(%ebp),%eax
  101b87:	66 89 02             	mov    %ax,(%edx)
      bwrite(bp);   // mark it allocated on the disk
  101b8a:	89 3c 24             	mov    %edi,(%esp)
  101b8d:	e8 5e e5 ff ff       	call   1000f0 <bwrite>
      brelse(bp);
  101b92:	89 3c 24             	mov    %edi,(%esp)
  101b95:	e8 d6 e4 ff ff       	call   100070 <brelse>
      return iget(dev, inum);
  101b9a:	8b 45 08             	mov    0x8(%ebp),%eax
  101b9d:	89 f2                	mov    %esi,%edx
  101b9f:	e8 2c f5 ff ff       	call   1010d0 <iget>
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
}
  101ba4:	83 c4 3c             	add    $0x3c,%esp
  101ba7:	5b                   	pop    %ebx
  101ba8:	5e                   	pop    %esi
  101ba9:	5f                   	pop    %edi
  101baa:	5d                   	pop    %ebp
  101bab:	c3                   	ret    
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
  101bac:	c7 04 24 88 65 10 00 	movl   $0x106588,(%esp)
  101bb3:	e8 58 ed ff ff       	call   100910 <panic>
  101bb8:	90                   	nop
  101bb9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00101bc0 <ilock>:
}

// Lock the given inode.
void
ilock(struct inode *ip)
{
  101bc0:	55                   	push   %ebp
  101bc1:	89 e5                	mov    %esp,%ebp
  101bc3:	56                   	push   %esi
  101bc4:	53                   	push   %ebx
  101bc5:	83 ec 10             	sub    $0x10,%esp
  101bc8:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
  101bcb:	85 db                	test   %ebx,%ebx
  101bcd:	0f 84 e5 00 00 00    	je     101cb8 <ilock+0xf8>
  101bd3:	8b 4b 08             	mov    0x8(%ebx),%ecx
  101bd6:	85 c9                	test   %ecx,%ecx
  101bd8:	0f 8e da 00 00 00    	jle    101cb8 <ilock+0xf8>
    panic("ilock");

  acquire(&icache.lock);
  101bde:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  101be5:	e8 46 1f 00 00       	call   103b30 <acquire>
  while(ip->flags & I_BUSY)
  101bea:	8b 43 0c             	mov    0xc(%ebx),%eax
  101bed:	a8 01                	test   $0x1,%al
  101bef:	74 1e                	je     101c0f <ilock+0x4f>
  101bf1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sleep(ip, &icache.lock);
  101bf8:	c7 44 24 04 e0 aa 10 	movl   $0x10aae0,0x4(%esp)
  101bff:	00 
  101c00:	89 1c 24             	mov    %ebx,(%esp)
  101c03:	e8 58 16 00 00       	call   103260 <sleep>

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquire(&icache.lock);
  while(ip->flags & I_BUSY)
  101c08:	8b 43 0c             	mov    0xc(%ebx),%eax
  101c0b:	a8 01                	test   $0x1,%al
  101c0d:	75 e9                	jne    101bf8 <ilock+0x38>
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
  101c0f:	83 c8 01             	or     $0x1,%eax
  101c12:	89 43 0c             	mov    %eax,0xc(%ebx)
  release(&icache.lock);
  101c15:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  101c1c:	e8 bf 1e 00 00       	call   103ae0 <release>

  if(!(ip->flags & I_VALID)){
  101c21:	f6 43 0c 02          	testb  $0x2,0xc(%ebx)
  101c25:	74 09                	je     101c30 <ilock+0x70>
    brelse(bp);
    ip->flags |= I_VALID;
    if(ip->type == 0)
      panic("ilock: no type");
  }
}
  101c27:	83 c4 10             	add    $0x10,%esp
  101c2a:	5b                   	pop    %ebx
  101c2b:	5e                   	pop    %esi
  101c2c:	5d                   	pop    %ebp
  101c2d:	c3                   	ret    
  101c2e:	66 90                	xchg   %ax,%ax
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
  release(&icache.lock);

  if(!(ip->flags & I_VALID)){
    bp = bread(ip->dev, IBLOCK(ip->inum));
  101c30:	8b 43 04             	mov    0x4(%ebx),%eax
  101c33:	c1 e8 03             	shr    $0x3,%eax
  101c36:	83 c0 02             	add    $0x2,%eax
  101c39:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c3d:	8b 03                	mov    (%ebx),%eax
  101c3f:	89 04 24             	mov    %eax,(%esp)
  101c42:	e8 d9 e4 ff ff       	call   100120 <bread>
  101c47:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
  101c49:	8b 43 04             	mov    0x4(%ebx),%eax
  101c4c:	83 e0 07             	and    $0x7,%eax
  101c4f:	c1 e0 06             	shl    $0x6,%eax
  101c52:	8d 44 06 18          	lea    0x18(%esi,%eax,1),%eax
    ip->type = dip->type;
  101c56:	0f b7 10             	movzwl (%eax),%edx
  101c59:	66 89 53 10          	mov    %dx,0x10(%ebx)
    ip->major = dip->major;
  101c5d:	0f b7 50 02          	movzwl 0x2(%eax),%edx
  101c61:	66 89 53 12          	mov    %dx,0x12(%ebx)
    ip->minor = dip->minor;
  101c65:	0f b7 50 04          	movzwl 0x4(%eax),%edx
  101c69:	66 89 53 14          	mov    %dx,0x14(%ebx)
    ip->nlink = dip->nlink;
  101c6d:	0f b7 50 06          	movzwl 0x6(%eax),%edx
  101c71:	66 89 53 16          	mov    %dx,0x16(%ebx)
    ip->size = dip->size;
  101c75:	8b 50 08             	mov    0x8(%eax),%edx
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
  101c78:	83 c0 0c             	add    $0xc,%eax
    dip = (struct dinode*)bp->data + ip->inum%IPB;
    ip->type = dip->type;
    ip->major = dip->major;
    ip->minor = dip->minor;
    ip->nlink = dip->nlink;
    ip->size = dip->size;
  101c7b:	89 53 18             	mov    %edx,0x18(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
  101c7e:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c82:	8d 43 1c             	lea    0x1c(%ebx),%eax
  101c85:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
  101c8c:	00 
  101c8d:	89 04 24             	mov    %eax,(%esp)
  101c90:	e8 bb 1f 00 00       	call   103c50 <memmove>
    brelse(bp);
  101c95:	89 34 24             	mov    %esi,(%esp)
  101c98:	e8 d3 e3 ff ff       	call   100070 <brelse>
    ip->flags |= I_VALID;
  101c9d:	83 4b 0c 02          	orl    $0x2,0xc(%ebx)
    if(ip->type == 0)
  101ca1:	66 83 7b 10 00       	cmpw   $0x0,0x10(%ebx)
  101ca6:	0f 85 7b ff ff ff    	jne    101c27 <ilock+0x67>
      panic("ilock: no type");
  101cac:	c7 04 24 a0 65 10 00 	movl   $0x1065a0,(%esp)
  101cb3:	e8 58 ec ff ff       	call   100910 <panic>
{
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
    panic("ilock");
  101cb8:	c7 04 24 9a 65 10 00 	movl   $0x10659a,(%esp)
  101cbf:	e8 4c ec ff ff       	call   100910 <panic>
  101cc4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  101cca:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00101cd0 <namex>:
// Look up and return the inode for a path name.
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
static struct inode*
namex(char *path, int nameiparent, char *name)
{
  101cd0:	55                   	push   %ebp
  101cd1:	89 e5                	mov    %esp,%ebp
  101cd3:	57                   	push   %edi
  101cd4:	56                   	push   %esi
  101cd5:	53                   	push   %ebx
  101cd6:	89 c3                	mov    %eax,%ebx
  101cd8:	83 ec 2c             	sub    $0x2c,%esp
  101cdb:	89 55 e0             	mov    %edx,-0x20(%ebp)
  101cde:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  struct inode *ip, *next;

  if(*path == '/')
  101ce1:	80 38 2f             	cmpb   $0x2f,(%eax)
  101ce4:	0f 84 14 01 00 00    	je     101dfe <namex+0x12e>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);
  101cea:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  101cf0:	8b 40 68             	mov    0x68(%eax),%eax
  101cf3:	89 04 24             	mov    %eax,(%esp)
  101cf6:	e8 a5 f3 ff ff       	call   1010a0 <idup>
  101cfb:	89 c7                	mov    %eax,%edi
  101cfd:	eb 04                	jmp    101d03 <namex+0x33>
  101cff:	90                   	nop
{
  char *s;
  int len;

  while(*path == '/')
    path++;
  101d00:	83 c3 01             	add    $0x1,%ebx
skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
  101d03:	0f b6 03             	movzbl (%ebx),%eax
  101d06:	3c 2f                	cmp    $0x2f,%al
  101d08:	74 f6                	je     101d00 <namex+0x30>
    path++;
  if(*path == 0)
  101d0a:	84 c0                	test   %al,%al
  101d0c:	75 1a                	jne    101d28 <namex+0x58>
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
  101d0e:	8b 5d e0             	mov    -0x20(%ebp),%ebx
  101d11:	85 db                	test   %ebx,%ebx
  101d13:	0f 85 0d 01 00 00    	jne    101e26 <namex+0x156>
    iput(ip);
    return 0;
  }
  return ip;
}
  101d19:	83 c4 2c             	add    $0x2c,%esp
  101d1c:	89 f8                	mov    %edi,%eax
  101d1e:	5b                   	pop    %ebx
  101d1f:	5e                   	pop    %esi
  101d20:	5f                   	pop    %edi
  101d21:	5d                   	pop    %ebp
  101d22:	c3                   	ret    
  101d23:	90                   	nop
  101d24:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
  101d28:	3c 2f                	cmp    $0x2f,%al
  101d2a:	0f 84 91 00 00 00    	je     101dc1 <namex+0xf1>
  101d30:	89 de                	mov    %ebx,%esi
  101d32:	eb 08                	jmp    101d3c <namex+0x6c>
  101d34:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  101d38:	3c 2f                	cmp    $0x2f,%al
  101d3a:	74 0a                	je     101d46 <namex+0x76>
    path++;
  101d3c:	83 c6 01             	add    $0x1,%esi
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
  101d3f:	0f b6 06             	movzbl (%esi),%eax
  101d42:	84 c0                	test   %al,%al
  101d44:	75 f2                	jne    101d38 <namex+0x68>
  101d46:	89 f2                	mov    %esi,%edx
  101d48:	29 da                	sub    %ebx,%edx
    path++;
  len = path - s;
  if(len >= DIRSIZ)
  101d4a:	83 fa 0d             	cmp    $0xd,%edx
  101d4d:	7e 79                	jle    101dc8 <namex+0xf8>
    memmove(name, s, DIRSIZ);
  101d4f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  101d52:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
  101d59:	00 
  101d5a:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  101d5e:	89 04 24             	mov    %eax,(%esp)
  101d61:	e8 ea 1e 00 00       	call   103c50 <memmove>
  101d66:	eb 03                	jmp    101d6b <namex+0x9b>
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
    path++;
  101d68:	83 c6 01             	add    $0x1,%esi
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
  101d6b:	80 3e 2f             	cmpb   $0x2f,(%esi)
  101d6e:	74 f8                	je     101d68 <namex+0x98>
  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
  101d70:	85 f6                	test   %esi,%esi
  101d72:	74 9a                	je     101d0e <namex+0x3e>
    ilock(ip);
  101d74:	89 3c 24             	mov    %edi,(%esp)
  101d77:	e8 44 fe ff ff       	call   101bc0 <ilock>
    if(ip->type != T_DIR){
  101d7c:	66 83 7f 10 01       	cmpw   $0x1,0x10(%edi)
  101d81:	75 67                	jne    101dea <namex+0x11a>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
  101d83:	8b 45 e0             	mov    -0x20(%ebp),%eax
  101d86:	85 c0                	test   %eax,%eax
  101d88:	74 09                	je     101d93 <namex+0xc3>
  101d8a:	80 3e 00             	cmpb   $0x0,(%esi)
  101d8d:	0f 84 81 00 00 00    	je     101e14 <namex+0x144>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
  101d93:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  101d96:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  101d9d:	00 
  101d9e:	89 3c 24             	mov    %edi,(%esp)
  101da1:	89 44 24 04          	mov    %eax,0x4(%esp)
  101da5:	e8 d6 f8 ff ff       	call   101680 <dirlookup>
  101daa:	85 c0                	test   %eax,%eax
  101dac:	89 c3                	mov    %eax,%ebx
  101dae:	74 3a                	je     101dea <namex+0x11a>
      iunlockput(ip);
      return 0;
    }
    iunlockput(ip);
  101db0:	89 3c 24             	mov    %edi,(%esp)
  101db3:	89 df                	mov    %ebx,%edi
  101db5:	89 f3                	mov    %esi,%ebx
  101db7:	e8 14 fd ff ff       	call   101ad0 <iunlockput>
  101dbc:	e9 42 ff ff ff       	jmp    101d03 <namex+0x33>
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
  101dc1:	89 de                	mov    %ebx,%esi
  101dc3:	31 d2                	xor    %edx,%edx
  101dc5:	8d 76 00             	lea    0x0(%esi),%esi
    path++;
  len = path - s;
  if(len >= DIRSIZ)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
  101dc8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  101dcb:	89 54 24 08          	mov    %edx,0x8(%esp)
  101dcf:	89 55 dc             	mov    %edx,-0x24(%ebp)
  101dd2:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  101dd6:	89 04 24             	mov    %eax,(%esp)
  101dd9:	e8 72 1e 00 00       	call   103c50 <memmove>
    name[len] = 0;
  101dde:	8b 55 dc             	mov    -0x24(%ebp),%edx
  101de1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  101de4:	c6 04 10 00          	movb   $0x0,(%eax,%edx,1)
  101de8:	eb 81                	jmp    101d6b <namex+0x9b>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
      iunlockput(ip);
  101dea:	89 3c 24             	mov    %edi,(%esp)
  101ded:	31 ff                	xor    %edi,%edi
  101def:	e8 dc fc ff ff       	call   101ad0 <iunlockput>
  if(nameiparent){
    iput(ip);
    return 0;
  }
  return ip;
}
  101df4:	83 c4 2c             	add    $0x2c,%esp
  101df7:	89 f8                	mov    %edi,%eax
  101df9:	5b                   	pop    %ebx
  101dfa:	5e                   	pop    %esi
  101dfb:	5f                   	pop    %edi
  101dfc:	5d                   	pop    %ebp
  101dfd:	c3                   	ret    
namex(char *path, int nameiparent, char *name)
{
  struct inode *ip, *next;

  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  101dfe:	ba 01 00 00 00       	mov    $0x1,%edx
  101e03:	b8 01 00 00 00       	mov    $0x1,%eax
  101e08:	e8 c3 f2 ff ff       	call   1010d0 <iget>
  101e0d:	89 c7                	mov    %eax,%edi
  101e0f:	e9 ef fe ff ff       	jmp    101d03 <namex+0x33>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
      // Stop one level early.
      iunlock(ip);
  101e14:	89 3c 24             	mov    %edi,(%esp)
  101e17:	e8 64 f9 ff ff       	call   101780 <iunlock>
  if(nameiparent){
    iput(ip);
    return 0;
  }
  return ip;
}
  101e1c:	83 c4 2c             	add    $0x2c,%esp
  101e1f:	89 f8                	mov    %edi,%eax
  101e21:	5b                   	pop    %ebx
  101e22:	5e                   	pop    %esi
  101e23:	5f                   	pop    %edi
  101e24:	5d                   	pop    %ebp
  101e25:	c3                   	ret    
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
    iput(ip);
  101e26:	89 3c 24             	mov    %edi,(%esp)
  101e29:	31 ff                	xor    %edi,%edi
  101e2b:	e8 60 fa ff ff       	call   101890 <iput>
    return 0;
  101e30:	e9 e4 fe ff ff       	jmp    101d19 <namex+0x49>
  101e35:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  101e39:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00101e40 <nameiparent>:
  return namex(path, 0, name);
}

struct inode*
nameiparent(char *path, char *name)
{
  101e40:	55                   	push   %ebp
  return namex(path, 1, name);
  101e41:	ba 01 00 00 00       	mov    $0x1,%edx
  return namex(path, 0, name);
}

struct inode*
nameiparent(char *path, char *name)
{
  101e46:	89 e5                	mov    %esp,%ebp
  101e48:	83 ec 08             	sub    $0x8,%esp
  return namex(path, 1, name);
  101e4b:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  101e4e:	8b 45 08             	mov    0x8(%ebp),%eax
}
  101e51:	c9                   	leave  
}

struct inode*
nameiparent(char *path, char *name)
{
  return namex(path, 1, name);
  101e52:	e9 79 fe ff ff       	jmp    101cd0 <namex>
  101e57:	89 f6                	mov    %esi,%esi
  101e59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00101e60 <namei>:
  return ip;
}

struct inode*
namei(char *path)
{
  101e60:	55                   	push   %ebp
  char name[DIRSIZ];
  return namex(path, 0, name);
  101e61:	31 d2                	xor    %edx,%edx
  return ip;
}

struct inode*
namei(char *path)
{
  101e63:	89 e5                	mov    %esp,%ebp
  101e65:	83 ec 18             	sub    $0x18,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
  101e68:	8b 45 08             	mov    0x8(%ebp),%eax
  101e6b:	8d 4d ea             	lea    -0x16(%ebp),%ecx
  101e6e:	e8 5d fe ff ff       	call   101cd0 <namex>
}
  101e73:	c9                   	leave  
  101e74:	c3                   	ret    
  101e75:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  101e79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00101e80 <iinit>:
  struct inode inode[NINODE];
} icache;

void
iinit(void)
{
  101e80:	55                   	push   %ebp
  101e81:	89 e5                	mov    %esp,%ebp
  101e83:	83 ec 18             	sub    $0x18,%esp
  initlock(&icache.lock, "icache");
  101e86:	c7 44 24 04 af 65 10 	movl   $0x1065af,0x4(%esp)
  101e8d:	00 
  101e8e:	c7 04 24 e0 aa 10 00 	movl   $0x10aae0,(%esp)
  101e95:	e8 06 1b 00 00       	call   1039a0 <initlock>
}
  101e9a:	c9                   	leave  
  101e9b:	c3                   	ret    
  101e9c:	90                   	nop
  101e9d:	90                   	nop
  101e9e:	90                   	nop
  101e9f:	90                   	nop

00101ea0 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
  101ea0:	55                   	push   %ebp
  101ea1:	89 e5                	mov    %esp,%ebp
  101ea3:	56                   	push   %esi
  101ea4:	89 c6                	mov    %eax,%esi
  101ea6:	83 ec 14             	sub    $0x14,%esp
  if(b == 0)
  101ea9:	85 c0                	test   %eax,%eax
  101eab:	0f 84 8d 00 00 00    	je     101f3e <idestart+0x9e>
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  101eb1:	ba f7 01 00 00       	mov    $0x1f7,%edx
  101eb6:	66 90                	xchg   %ax,%ax
  101eb8:	ec                   	in     (%dx),%al
static int
idewait(int checkerr)
{
  int r;

  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) 
  101eb9:	25 c0 00 00 00       	and    $0xc0,%eax
  101ebe:	83 f8 40             	cmp    $0x40,%eax
  101ec1:	75 f5                	jne    101eb8 <idestart+0x18>
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  101ec3:	ba f6 03 00 00       	mov    $0x3f6,%edx
  101ec8:	31 c0                	xor    %eax,%eax
  101eca:	ee                   	out    %al,(%dx)
  101ecb:	ba f2 01 00 00       	mov    $0x1f2,%edx
  101ed0:	b8 01 00 00 00       	mov    $0x1,%eax
  101ed5:	ee                   	out    %al,(%dx)
    panic("idestart");

  idewait(0);
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, 1);  // number of sectors
  outb(0x1f3, b->sector & 0xff);
  101ed6:	8b 4e 08             	mov    0x8(%esi),%ecx
  101ed9:	b2 f3                	mov    $0xf3,%dl
  101edb:	89 c8                	mov    %ecx,%eax
  101edd:	ee                   	out    %al,(%dx)
  101ede:	89 c8                	mov    %ecx,%eax
  101ee0:	b2 f4                	mov    $0xf4,%dl
  101ee2:	c1 e8 08             	shr    $0x8,%eax
  101ee5:	ee                   	out    %al,(%dx)
  101ee6:	89 c8                	mov    %ecx,%eax
  101ee8:	b2 f5                	mov    $0xf5,%dl
  101eea:	c1 e8 10             	shr    $0x10,%eax
  101eed:	ee                   	out    %al,(%dx)
  101eee:	8b 46 04             	mov    0x4(%esi),%eax
  101ef1:	c1 e9 18             	shr    $0x18,%ecx
  101ef4:	b2 f6                	mov    $0xf6,%dl
  101ef6:	83 e1 0f             	and    $0xf,%ecx
  101ef9:	83 e0 01             	and    $0x1,%eax
  101efc:	c1 e0 04             	shl    $0x4,%eax
  101eff:	09 c8                	or     %ecx,%eax
  101f01:	83 c8 e0             	or     $0xffffffe0,%eax
  101f04:	ee                   	out    %al,(%dx)
  outb(0x1f4, (b->sector >> 8) & 0xff);
  outb(0x1f5, (b->sector >> 16) & 0xff);
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((b->sector>>24)&0x0f));
  if(b->flags & B_DIRTY){
  101f05:	f6 06 04             	testb  $0x4,(%esi)
  101f08:	75 16                	jne    101f20 <idestart+0x80>
  101f0a:	ba f7 01 00 00       	mov    $0x1f7,%edx
  101f0f:	b8 20 00 00 00       	mov    $0x20,%eax
  101f14:	ee                   	out    %al,(%dx)
    outb(0x1f7, IDE_CMD_WRITE);
    outsl(0x1f0, b->data, 512/4);
  } else {
    outb(0x1f7, IDE_CMD_READ);
  }
}
  101f15:	83 c4 14             	add    $0x14,%esp
  101f18:	5e                   	pop    %esi
  101f19:	5d                   	pop    %ebp
  101f1a:	c3                   	ret    
  101f1b:	90                   	nop
  101f1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  101f20:	b2 f7                	mov    $0xf7,%dl
  101f22:	b8 30 00 00 00       	mov    $0x30,%eax
  101f27:	ee                   	out    %al,(%dx)
}

static inline void
outsl(int port, const void *addr, int cnt)
{
  asm volatile("cld; rep outsl" :
  101f28:	b9 80 00 00 00       	mov    $0x80,%ecx
  101f2d:	83 c6 18             	add    $0x18,%esi
  101f30:	ba f0 01 00 00       	mov    $0x1f0,%edx
  101f35:	fc                   	cld    
  101f36:	f3 6f                	rep outsl %ds:(%esi),(%dx)
  101f38:	83 c4 14             	add    $0x14,%esp
  101f3b:	5e                   	pop    %esi
  101f3c:	5d                   	pop    %ebp
  101f3d:	c3                   	ret    
// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
  if(b == 0)
    panic("idestart");
  101f3e:	c7 04 24 b6 65 10 00 	movl   $0x1065b6,(%esp)
  101f45:	e8 c6 e9 ff ff       	call   100910 <panic>
  101f4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00101f50 <iderw>:
// Sync buf with disk. 
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
  101f50:	55                   	push   %ebp
  101f51:	89 e5                	mov    %esp,%ebp
  101f53:	53                   	push   %ebx
  101f54:	83 ec 14             	sub    $0x14,%esp
  101f57:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!(b->flags & B_BUSY))
  101f5a:	8b 03                	mov    (%ebx),%eax
  101f5c:	a8 01                	test   $0x1,%al
  101f5e:	0f 84 90 00 00 00    	je     101ff4 <iderw+0xa4>
    panic("iderw: buf not busy");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
  101f64:	83 e0 06             	and    $0x6,%eax
  101f67:	83 f8 02             	cmp    $0x2,%eax
  101f6a:	0f 84 9c 00 00 00    	je     10200c <iderw+0xbc>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
  101f70:	8b 53 04             	mov    0x4(%ebx),%edx
  101f73:	85 d2                	test   %edx,%edx
  101f75:	74 0d                	je     101f84 <iderw+0x34>
  101f77:	a1 b8 78 10 00       	mov    0x1078b8,%eax
  101f7c:	85 c0                	test   %eax,%eax
  101f7e:	0f 84 7c 00 00 00    	je     102000 <iderw+0xb0>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);
  101f84:	c7 04 24 80 78 10 00 	movl   $0x107880,(%esp)
  101f8b:	e8 a0 1b 00 00       	call   103b30 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)
  101f90:	ba b4 78 10 00       	mov    $0x1078b4,%edx
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);

  // Append b to idequeue.
  b->qnext = 0;
  101f95:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
  101f9c:	a1 b4 78 10 00       	mov    0x1078b4,%eax
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)
  101fa1:	85 c0                	test   %eax,%eax
  101fa3:	74 0d                	je     101fb2 <iderw+0x62>
  101fa5:	8d 76 00             	lea    0x0(%esi),%esi
  101fa8:	8d 50 14             	lea    0x14(%eax),%edx
  101fab:	8b 40 14             	mov    0x14(%eax),%eax
  101fae:	85 c0                	test   %eax,%eax
  101fb0:	75 f6                	jne    101fa8 <iderw+0x58>
    ;
  *pp = b;
  101fb2:	89 1a                	mov    %ebx,(%edx)
  
  // Start disk if necessary.
  if(idequeue == b)
  101fb4:	39 1d b4 78 10 00    	cmp    %ebx,0x1078b4
  101fba:	75 14                	jne    101fd0 <iderw+0x80>
  101fbc:	eb 2d                	jmp    101feb <iderw+0x9b>
  101fbe:	66 90                	xchg   %ax,%ax
    idestart(b);
  
  // Wait for request to finish.
  // Assuming will not sleep too long: ignore proc->killed.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
    sleep(b, &idelock);
  101fc0:	c7 44 24 04 80 78 10 	movl   $0x107880,0x4(%esp)
  101fc7:	00 
  101fc8:	89 1c 24             	mov    %ebx,(%esp)
  101fcb:	e8 90 12 00 00       	call   103260 <sleep>
  if(idequeue == b)
    idestart(b);
  
  // Wait for request to finish.
  // Assuming will not sleep too long: ignore proc->killed.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
  101fd0:	8b 03                	mov    (%ebx),%eax
  101fd2:	83 e0 06             	and    $0x6,%eax
  101fd5:	83 f8 02             	cmp    $0x2,%eax
  101fd8:	75 e6                	jne    101fc0 <iderw+0x70>
    sleep(b, &idelock);
  }

  release(&idelock);
  101fda:	c7 45 08 80 78 10 00 	movl   $0x107880,0x8(%ebp)
}
  101fe1:	83 c4 14             	add    $0x14,%esp
  101fe4:	5b                   	pop    %ebx
  101fe5:	5d                   	pop    %ebp
  // Assuming will not sleep too long: ignore proc->killed.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
    sleep(b, &idelock);
  }

  release(&idelock);
  101fe6:	e9 f5 1a 00 00       	jmp    103ae0 <release>
    ;
  *pp = b;
  
  // Start disk if necessary.
  if(idequeue == b)
    idestart(b);
  101feb:	89 d8                	mov    %ebx,%eax
  101fed:	e8 ae fe ff ff       	call   101ea0 <idestart>
  101ff2:	eb dc                	jmp    101fd0 <iderw+0x80>
iderw(struct buf *b)
{
  struct buf **pp;

  if(!(b->flags & B_BUSY))
    panic("iderw: buf not busy");
  101ff4:	c7 04 24 bf 65 10 00 	movl   $0x1065bf,(%esp)
  101ffb:	e8 10 e9 ff ff       	call   100910 <panic>
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
    panic("iderw: ide disk 1 not present");
  102000:	c7 04 24 e8 65 10 00 	movl   $0x1065e8,(%esp)
  102007:	e8 04 e9 ff ff       	call   100910 <panic>
  struct buf **pp;

  if(!(b->flags & B_BUSY))
    panic("iderw: buf not busy");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
    panic("iderw: nothing to do");
  10200c:	c7 04 24 d3 65 10 00 	movl   $0x1065d3,(%esp)
  102013:	e8 f8 e8 ff ff       	call   100910 <panic>
  102018:	90                   	nop
  102019:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00102020 <ideintr>:
}

// Interrupt handler.
void
ideintr(void)
{
  102020:	55                   	push   %ebp
  102021:	89 e5                	mov    %esp,%ebp
  102023:	57                   	push   %edi
  102024:	53                   	push   %ebx
  102025:	83 ec 10             	sub    $0x10,%esp
  struct buf *b;

  // Take first buffer off queue.
  acquire(&idelock);
  102028:	c7 04 24 80 78 10 00 	movl   $0x107880,(%esp)
  10202f:	e8 fc 1a 00 00       	call   103b30 <acquire>
  if((b = idequeue) == 0){
  102034:	8b 1d b4 78 10 00    	mov    0x1078b4,%ebx
  10203a:	85 db                	test   %ebx,%ebx
  10203c:	74 2d                	je     10206b <ideintr+0x4b>
    release(&idelock);
    // cprintf("spurious IDE interrupt\n");
    return;
  }
  idequeue = b->qnext;
  10203e:	8b 43 14             	mov    0x14(%ebx),%eax
  102041:	a3 b4 78 10 00       	mov    %eax,0x1078b4

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
  102046:	8b 0b                	mov    (%ebx),%ecx
  102048:	f6 c1 04             	test   $0x4,%cl
  10204b:	74 33                	je     102080 <ideintr+0x60>
    insl(0x1f0, b->data, 512/4);
  
  // Wake process waiting for this buf.
  b->flags |= B_VALID;
  b->flags &= ~B_DIRTY;
  10204d:	83 c9 02             	or     $0x2,%ecx
  102050:	83 e1 fb             	and    $0xfffffffb,%ecx
  102053:	89 0b                	mov    %ecx,(%ebx)
  wakeup(b);
  102055:	89 1c 24             	mov    %ebx,(%esp)
  102058:	e8 e3 10 00 00       	call   103140 <wakeup>
  
  // Start disk on next buf in queue.
  if(idequeue != 0)
  10205d:	a1 b4 78 10 00       	mov    0x1078b4,%eax
  102062:	85 c0                	test   %eax,%eax
  102064:	74 05                	je     10206b <ideintr+0x4b>
    idestart(idequeue);
  102066:	e8 35 fe ff ff       	call   101ea0 <idestart>

  release(&idelock);
  10206b:	c7 04 24 80 78 10 00 	movl   $0x107880,(%esp)
  102072:	e8 69 1a 00 00       	call   103ae0 <release>
}
  102077:	83 c4 10             	add    $0x10,%esp
  10207a:	5b                   	pop    %ebx
  10207b:	5f                   	pop    %edi
  10207c:	5d                   	pop    %ebp
  10207d:	c3                   	ret    
  10207e:	66 90                	xchg   %ax,%ax
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  102080:	ba f7 01 00 00       	mov    $0x1f7,%edx
  102085:	8d 76 00             	lea    0x0(%esi),%esi
  102088:	ec                   	in     (%dx),%al
static int
idewait(int checkerr)
{
  int r;

  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) 
  102089:	0f b6 c0             	movzbl %al,%eax
  10208c:	89 c7                	mov    %eax,%edi
  10208e:	81 e7 c0 00 00 00    	and    $0xc0,%edi
  102094:	83 ff 40             	cmp    $0x40,%edi
  102097:	75 ef                	jne    102088 <ideintr+0x68>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
  102099:	a8 21                	test   $0x21,%al
  10209b:	75 b0                	jne    10204d <ideintr+0x2d>
}

static inline void
insl(int port, void *addr, int cnt)
{
  asm volatile("cld; rep insl" :
  10209d:	8d 7b 18             	lea    0x18(%ebx),%edi
  1020a0:	b9 80 00 00 00       	mov    $0x80,%ecx
  1020a5:	ba f0 01 00 00       	mov    $0x1f0,%edx
  1020aa:	fc                   	cld    
  1020ab:	f3 6d                	rep insl (%dx),%es:(%edi)
  1020ad:	8b 0b                	mov    (%ebx),%ecx
  1020af:	eb 9c                	jmp    10204d <ideintr+0x2d>
  1020b1:	eb 0d                	jmp    1020c0 <ideinit>
  1020b3:	90                   	nop
  1020b4:	90                   	nop
  1020b5:	90                   	nop
  1020b6:	90                   	nop
  1020b7:	90                   	nop
  1020b8:	90                   	nop
  1020b9:	90                   	nop
  1020ba:	90                   	nop
  1020bb:	90                   	nop
  1020bc:	90                   	nop
  1020bd:	90                   	nop
  1020be:	90                   	nop
  1020bf:	90                   	nop

001020c0 <ideinit>:
  return 0;
}

void
ideinit(void)
{
  1020c0:	55                   	push   %ebp
  1020c1:	89 e5                	mov    %esp,%ebp
  1020c3:	83 ec 18             	sub    $0x18,%esp
  int i;

  initlock(&idelock, "ide");
  1020c6:	c7 44 24 04 06 66 10 	movl   $0x106606,0x4(%esp)
  1020cd:	00 
  1020ce:	c7 04 24 80 78 10 00 	movl   $0x107880,(%esp)
  1020d5:	e8 c6 18 00 00       	call   1039a0 <initlock>
  picenable(IRQ_IDE);
  1020da:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
  1020e1:	e8 ba 0a 00 00       	call   102ba0 <picenable>
  ioapicenable(IRQ_IDE, ncpu - 1);
  1020e6:	a1 00 c1 10 00       	mov    0x10c100,%eax
  1020eb:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
  1020f2:	83 e8 01             	sub    $0x1,%eax
  1020f5:	89 44 24 04          	mov    %eax,0x4(%esp)
  1020f9:	e8 52 00 00 00       	call   102150 <ioapicenable>
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  1020fe:	ba f7 01 00 00       	mov    $0x1f7,%edx
  102103:	90                   	nop
  102104:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  102108:	ec                   	in     (%dx),%al
static int
idewait(int checkerr)
{
  int r;

  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) 
  102109:	25 c0 00 00 00       	and    $0xc0,%eax
  10210e:	83 f8 40             	cmp    $0x40,%eax
  102111:	75 f5                	jne    102108 <ideinit+0x48>
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  102113:	ba f6 01 00 00       	mov    $0x1f6,%edx
  102118:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
  10211d:	ee                   	out    %al,(%dx)
  10211e:	31 c9                	xor    %ecx,%ecx
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  102120:	b2 f7                	mov    $0xf7,%dl
  102122:	eb 0f                	jmp    102133 <ideinit+0x73>
  102124:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  ioapicenable(IRQ_IDE, ncpu - 1);
  idewait(0);
  
  // Check if disk 1 is present
  outb(0x1f6, 0xe0 | (1<<4));
  for(i=0; i<1000; i++){
  102128:	83 c1 01             	add    $0x1,%ecx
  10212b:	81 f9 e8 03 00 00    	cmp    $0x3e8,%ecx
  102131:	74 0f                	je     102142 <ideinit+0x82>
  102133:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
  102134:	84 c0                	test   %al,%al
  102136:	74 f0                	je     102128 <ideinit+0x68>
      havedisk1 = 1;
  102138:	c7 05 b8 78 10 00 01 	movl   $0x1,0x1078b8
  10213f:	00 00 00 
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  102142:	ba f6 01 00 00       	mov    $0x1f6,%edx
  102147:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
  10214c:	ee                   	out    %al,(%dx)
    }
  }
  
  // Switch back to disk 0.
  outb(0x1f6, 0xe0 | (0<<4));
}
  10214d:	c9                   	leave  
  10214e:	c3                   	ret    
  10214f:	90                   	nop

00102150 <ioapicenable>:
}

void
ioapicenable(int irq, int cpunum)
{
  if(!ismp)
  102150:	8b 15 04 bb 10 00    	mov    0x10bb04,%edx
  }
}

void
ioapicenable(int irq, int cpunum)
{
  102156:	55                   	push   %ebp
  102157:	89 e5                	mov    %esp,%ebp
  102159:	8b 45 08             	mov    0x8(%ebp),%eax
  if(!ismp)
  10215c:	85 d2                	test   %edx,%edx
  10215e:	74 31                	je     102191 <ioapicenable+0x41>
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  102160:	8b 15 b4 ba 10 00    	mov    0x10bab4,%edx
    return;

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  102166:	8d 48 20             	lea    0x20(%eax),%ecx
  102169:	8d 44 00 10          	lea    0x10(%eax,%eax,1),%eax
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  10216d:	89 02                	mov    %eax,(%edx)
  ioapic->data = data;
  10216f:	8b 15 b4 ba 10 00    	mov    0x10bab4,%edx
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  102175:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
  102178:	89 4a 10             	mov    %ecx,0x10(%edx)
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  10217b:	8b 0d b4 ba 10 00    	mov    0x10bab4,%ecx

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
  102181:	8b 55 0c             	mov    0xc(%ebp),%edx
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  102184:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
  102186:	a1 b4 ba 10 00       	mov    0x10bab4,%eax

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
  10218b:	c1 e2 18             	shl    $0x18,%edx

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  ioapic->data = data;
  10218e:	89 50 10             	mov    %edx,0x10(%eax)
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
}
  102191:	5d                   	pop    %ebp
  102192:	c3                   	ret    
  102193:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  102199:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

001021a0 <ioapicinit>:
  ioapic->data = data;
}

void
ioapicinit(void)
{
  1021a0:	55                   	push   %ebp
  1021a1:	89 e5                	mov    %esp,%ebp
  1021a3:	56                   	push   %esi
  1021a4:	53                   	push   %ebx
  1021a5:	83 ec 10             	sub    $0x10,%esp
  int i, id, maxintr;

  if(!ismp)
  1021a8:	8b 0d 04 bb 10 00    	mov    0x10bb04,%ecx
  1021ae:	85 c9                	test   %ecx,%ecx
  1021b0:	0f 84 9e 00 00 00    	je     102254 <ioapicinit+0xb4>
};

static uint
ioapicread(int reg)
{
  ioapic->reg = reg;
  1021b6:	c7 05 00 00 c0 fe 01 	movl   $0x1,0xfec00000
  1021bd:	00 00 00 
  return ioapic->data;
  1021c0:	8b 35 10 00 c0 fe    	mov    0xfec00010,%esi
    return;

  ioapic = (volatile struct ioapic*)IOAPIC;
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
  1021c6:	bb 00 00 c0 fe       	mov    $0xfec00000,%ebx
};

static uint
ioapicread(int reg)
{
  ioapic->reg = reg;
  1021cb:	c7 05 00 00 c0 fe 00 	movl   $0x0,0xfec00000
  1021d2:	00 00 00 
  return ioapic->data;
  1021d5:	a1 10 00 c0 fe       	mov    0xfec00010,%eax
    return;

  ioapic = (volatile struct ioapic*)IOAPIC;
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
  1021da:	0f b6 15 00 bb 10 00 	movzbl 0x10bb00,%edx
  int i, id, maxintr;

  if(!ismp)
    return;

  ioapic = (volatile struct ioapic*)IOAPIC;
  1021e1:	c7 05 b4 ba 10 00 00 	movl   $0xfec00000,0x10bab4
  1021e8:	00 c0 fe 
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  1021eb:	c1 ee 10             	shr    $0x10,%esi
  id = ioapicread(REG_ID) >> 24;
  1021ee:	c1 e8 18             	shr    $0x18,%eax

  if(!ismp)
    return;

  ioapic = (volatile struct ioapic*)IOAPIC;
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  1021f1:	81 e6 ff 00 00 00    	and    $0xff,%esi
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
  1021f7:	39 c2                	cmp    %eax,%edx
  1021f9:	74 12                	je     10220d <ioapicinit+0x6d>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
  1021fb:	c7 04 24 0c 66 10 00 	movl   $0x10660c,(%esp)
  102202:	e8 29 e3 ff ff       	call   100530 <cprintf>
  102207:	8b 1d b4 ba 10 00    	mov    0x10bab4,%ebx
  10220d:	ba 10 00 00 00       	mov    $0x10,%edx
  102212:	31 c0                	xor    %eax,%eax
  102214:	eb 08                	jmp    10221e <ioapicinit+0x7e>
  102216:	66 90                	xchg   %ax,%ax

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
  102218:	8b 1d b4 ba 10 00    	mov    0x10bab4,%ebx
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  10221e:	89 13                	mov    %edx,(%ebx)
  ioapic->data = data;
  102220:	8b 1d b4 ba 10 00    	mov    0x10bab4,%ebx
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
  102226:	8d 48 20             	lea    0x20(%eax),%ecx
  102229:	81 c9 00 00 01 00    	or     $0x10000,%ecx
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
  10222f:	83 c0 01             	add    $0x1,%eax

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  ioapic->data = data;
  102232:	89 4b 10             	mov    %ecx,0x10(%ebx)
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  102235:	8b 0d b4 ba 10 00    	mov    0x10bab4,%ecx
  10223b:	8d 5a 01             	lea    0x1(%edx),%ebx
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
  10223e:	83 c2 02             	add    $0x2,%edx
  102241:	39 c6                	cmp    %eax,%esi
}

static void
ioapicwrite(int reg, uint data)
{
  ioapic->reg = reg;
  102243:	89 19                	mov    %ebx,(%ecx)
  ioapic->data = data;
  102245:	8b 0d b4 ba 10 00    	mov    0x10bab4,%ecx
  10224b:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
  102252:	7d c4                	jge    102218 <ioapicinit+0x78>
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
  102254:	83 c4 10             	add    $0x10,%esp
  102257:	5b                   	pop    %ebx
  102258:	5e                   	pop    %esi
  102259:	5d                   	pop    %ebp
  10225a:	c3                   	ret    
  10225b:	90                   	nop
  10225c:	90                   	nop
  10225d:	90                   	nop
  10225e:	90                   	nop
  10225f:	90                   	nop

00102260 <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
  102260:	55                   	push   %ebp
  102261:	89 e5                	mov    %esp,%ebp
  102263:	53                   	push   %ebx
  102264:	83 ec 14             	sub    $0x14,%esp
  struct run *r;

  acquire(&kmem.lock);
  102267:	c7 04 24 c0 ba 10 00 	movl   $0x10bac0,(%esp)
  10226e:	e8 bd 18 00 00       	call   103b30 <acquire>
  r = kmem.freelist;
  102273:	8b 1d f4 ba 10 00    	mov    0x10baf4,%ebx
  if(r)
  102279:	85 db                	test   %ebx,%ebx
  10227b:	74 07                	je     102284 <kalloc+0x24>
    kmem.freelist = r->next;
  10227d:	8b 03                	mov    (%ebx),%eax
  10227f:	a3 f4 ba 10 00       	mov    %eax,0x10baf4
  release(&kmem.lock);
  102284:	c7 04 24 c0 ba 10 00 	movl   $0x10bac0,(%esp)
  10228b:	e8 50 18 00 00       	call   103ae0 <release>
  return (char*)r;
}
  102290:	89 d8                	mov    %ebx,%eax
  102292:	83 c4 14             	add    $0x14,%esp
  102295:	5b                   	pop    %ebx
  102296:	5d                   	pop    %ebp
  102297:	c3                   	ret    
  102298:	90                   	nop
  102299:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

001022a0 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
  1022a0:	55                   	push   %ebp
  1022a1:	89 e5                	mov    %esp,%ebp
  1022a3:	53                   	push   %ebx
  1022a4:	83 ec 14             	sub    $0x14,%esp
  1022a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE || v < end || (uint)v >= PHYSTOP) 
  1022aa:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
  1022b0:	75 52                	jne    102304 <kfree+0x64>
  1022b2:	81 fb ff ff ff 00    	cmp    $0xffffff,%ebx
  1022b8:	77 4a                	ja     102304 <kfree+0x64>
  1022ba:	81 fb a4 e8 10 00    	cmp    $0x10e8a4,%ebx
  1022c0:	72 42                	jb     102304 <kfree+0x64>
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
  1022c2:	89 1c 24             	mov    %ebx,(%esp)
  1022c5:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  1022cc:	00 
  1022cd:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  1022d4:	00 
  1022d5:	e8 f6 18 00 00       	call   103bd0 <memset>

  acquire(&kmem.lock);
  1022da:	c7 04 24 c0 ba 10 00 	movl   $0x10bac0,(%esp)
  1022e1:	e8 4a 18 00 00       	call   103b30 <acquire>
  r = (struct run*)v;
  r->next = kmem.freelist;
  1022e6:	a1 f4 ba 10 00       	mov    0x10baf4,%eax
  1022eb:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
  1022ed:	89 1d f4 ba 10 00    	mov    %ebx,0x10baf4
  release(&kmem.lock);
  1022f3:	c7 45 08 c0 ba 10 00 	movl   $0x10bac0,0x8(%ebp)
}
  1022fa:	83 c4 14             	add    $0x14,%esp
  1022fd:	5b                   	pop    %ebx
  1022fe:	5d                   	pop    %ebp

  acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
  kmem.freelist = r;
  release(&kmem.lock);
  1022ff:	e9 dc 17 00 00       	jmp    103ae0 <release>
kfree(char *v)
{
  struct run *r;

  if((uint)v % PGSIZE || v < end || (uint)v >= PHYSTOP) 
    panic("kfree");
  102304:	c7 04 24 3e 66 10 00 	movl   $0x10663e,(%esp)
  10230b:	e8 00 e6 ff ff       	call   100910 <panic>

00102310 <kinit>:
extern char end[]; // first address after kernel loaded from ELF file

// Initialize free list of physical pages.
void
kinit(void)
{
  102310:	55                   	push   %ebp
  102311:	89 e5                	mov    %esp,%ebp
  102313:	53                   	push   %ebx
  102314:	83 ec 14             	sub    $0x14,%esp
  char *p;

  initlock(&kmem.lock, "kmem");
  102317:	c7 44 24 04 44 66 10 	movl   $0x106644,0x4(%esp)
  10231e:	00 
  10231f:	c7 04 24 c0 ba 10 00 	movl   $0x10bac0,(%esp)
  102326:	e8 75 16 00 00       	call   1039a0 <initlock>
  p = (char*)PGROUNDUP((uint)end);
  10232b:	ba a3 f8 10 00       	mov    $0x10f8a3,%edx
  102330:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
  for(; p + PGSIZE <= (char*)PHYSTOP; p += PGSIZE)
  102336:	8d 9a 00 10 00 00    	lea    0x1000(%edx),%ebx
  10233c:	81 fb 00 00 00 01    	cmp    $0x1000000,%ebx
  102342:	76 08                	jbe    10234c <kinit+0x3c>
  102344:	eb 1b                	jmp    102361 <kinit+0x51>
  102346:	66 90                	xchg   %ax,%ax
  102348:	89 da                	mov    %ebx,%edx
  10234a:	89 c3                	mov    %eax,%ebx
    kfree(p);
  10234c:	89 14 24             	mov    %edx,(%esp)
  10234f:	e8 4c ff ff ff       	call   1022a0 <kfree>
{
  char *p;

  initlock(&kmem.lock, "kmem");
  p = (char*)PGROUNDUP((uint)end);
  for(; p + PGSIZE <= (char*)PHYSTOP; p += PGSIZE)
  102354:	8d 83 00 10 00 00    	lea    0x1000(%ebx),%eax
  10235a:	3d 00 00 00 01       	cmp    $0x1000000,%eax
  10235f:	76 e7                	jbe    102348 <kinit+0x38>
    kfree(p);
}
  102361:	83 c4 14             	add    $0x14,%esp
  102364:	5b                   	pop    %ebx
  102365:	5d                   	pop    %ebp
  102366:	c3                   	ret    
  102367:	90                   	nop
  102368:	90                   	nop
  102369:	90                   	nop
  10236a:	90                   	nop
  10236b:	90                   	nop
  10236c:	90                   	nop
  10236d:	90                   	nop
  10236e:	90                   	nop
  10236f:	90                   	nop

00102370 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
  102370:	55                   	push   %ebp
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  102371:	ba 64 00 00 00       	mov    $0x64,%edx
  102376:	89 e5                	mov    %esp,%ebp
  102378:	ec                   	in     (%dx),%al
  102379:	89 c2                	mov    %eax,%edx
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
  10237b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  102380:	83 e2 01             	and    $0x1,%edx
  102383:	74 41                	je     1023c6 <kbdgetc+0x56>
  102385:	ba 60 00 00 00       	mov    $0x60,%edx
  10238a:	ec                   	in     (%dx),%al
    return -1;
  data = inb(KBDATAP);
  10238b:	0f b6 c0             	movzbl %al,%eax

  if(data == 0xE0){
  10238e:	3d e0 00 00 00       	cmp    $0xe0,%eax
  102393:	0f 84 7f 00 00 00    	je     102418 <kbdgetc+0xa8>
    shift |= E0ESC;
    return 0;
  } else if(data & 0x80){
  102399:	84 c0                	test   %al,%al
  10239b:	79 2b                	jns    1023c8 <kbdgetc+0x58>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
  10239d:	8b 15 bc 78 10 00    	mov    0x1078bc,%edx
  1023a3:	89 c1                	mov    %eax,%ecx
  1023a5:	83 e1 7f             	and    $0x7f,%ecx
  1023a8:	f6 c2 40             	test   $0x40,%dl
  1023ab:	0f 44 c1             	cmove  %ecx,%eax
    shift &= ~(shiftcode[data] | E0ESC);
  1023ae:	0f b6 80 60 66 10 00 	movzbl 0x106660(%eax),%eax
  1023b5:	83 c8 40             	or     $0x40,%eax
  1023b8:	0f b6 c0             	movzbl %al,%eax
  1023bb:	f7 d0                	not    %eax
  1023bd:	21 d0                	and    %edx,%eax
  1023bf:	a3 bc 78 10 00       	mov    %eax,0x1078bc
  1023c4:	31 c0                	xor    %eax,%eax
      c += 'A' - 'a';
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
  1023c6:	5d                   	pop    %ebp
  1023c7:	c3                   	ret    
  } else if(data & 0x80){
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
  1023c8:	8b 0d bc 78 10 00    	mov    0x1078bc,%ecx
  1023ce:	f6 c1 40             	test   $0x40,%cl
  1023d1:	74 05                	je     1023d8 <kbdgetc+0x68>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
  1023d3:	0c 80                	or     $0x80,%al
    shift &= ~E0ESC;
  1023d5:	83 e1 bf             	and    $0xffffffbf,%ecx
  }

  shift |= shiftcode[data];
  shift ^= togglecode[data];
  1023d8:	0f b6 90 60 66 10 00 	movzbl 0x106660(%eax),%edx
  1023df:	09 ca                	or     %ecx,%edx
  1023e1:	0f b6 88 60 67 10 00 	movzbl 0x106760(%eax),%ecx
  1023e8:	31 ca                	xor    %ecx,%edx
  c = charcode[shift & (CTL | SHIFT)][data];
  1023ea:	89 d1                	mov    %edx,%ecx
  1023ec:	83 e1 03             	and    $0x3,%ecx
  1023ef:	8b 0c 8d 60 68 10 00 	mov    0x106860(,%ecx,4),%ecx
    data |= 0x80;
    shift &= ~E0ESC;
  }

  shift |= shiftcode[data];
  shift ^= togglecode[data];
  1023f6:	89 15 bc 78 10 00    	mov    %edx,0x1078bc
  c = charcode[shift & (CTL | SHIFT)][data];
  if(shift & CAPSLOCK){
  1023fc:	83 e2 08             	and    $0x8,%edx
    shift &= ~E0ESC;
  }

  shift |= shiftcode[data];
  shift ^= togglecode[data];
  c = charcode[shift & (CTL | SHIFT)][data];
  1023ff:	0f b6 04 01          	movzbl (%ecx,%eax,1),%eax
  if(shift & CAPSLOCK){
  102403:	74 c1                	je     1023c6 <kbdgetc+0x56>
    if('a' <= c && c <= 'z')
  102405:	8d 50 9f             	lea    -0x61(%eax),%edx
  102408:	83 fa 19             	cmp    $0x19,%edx
  10240b:	77 1b                	ja     102428 <kbdgetc+0xb8>
      c += 'A' - 'a';
  10240d:	83 e8 20             	sub    $0x20,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
  102410:	5d                   	pop    %ebp
  102411:	c3                   	ret    
  102412:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  if((st & KBS_DIB) == 0)
    return -1;
  data = inb(KBDATAP);

  if(data == 0xE0){
    shift |= E0ESC;
  102418:	30 c0                	xor    %al,%al
  10241a:	83 0d bc 78 10 00 40 	orl    $0x40,0x1078bc
      c += 'A' - 'a';
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
  102421:	5d                   	pop    %ebp
  102422:	c3                   	ret    
  102423:	90                   	nop
  102424:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  shift ^= togglecode[data];
  c = charcode[shift & (CTL | SHIFT)][data];
  if(shift & CAPSLOCK){
    if('a' <= c && c <= 'z')
      c += 'A' - 'a';
    else if('A' <= c && c <= 'Z')
  102428:	8d 48 bf             	lea    -0x41(%eax),%ecx
      c += 'a' - 'A';
  10242b:	8d 50 20             	lea    0x20(%eax),%edx
  10242e:	83 f9 19             	cmp    $0x19,%ecx
  102431:	0f 46 c2             	cmovbe %edx,%eax
  }
  return c;
}
  102434:	5d                   	pop    %ebp
  102435:	c3                   	ret    
  102436:	8d 76 00             	lea    0x0(%esi),%esi
  102439:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00102440 <kbdintr>:

void
kbdintr(void)
{
  102440:	55                   	push   %ebp
  102441:	89 e5                	mov    %esp,%ebp
  102443:	83 ec 18             	sub    $0x18,%esp
  consoleintr(kbdgetc);
  102446:	c7 04 24 70 23 10 00 	movl   $0x102370,(%esp)
  10244d:	e8 3e e3 ff ff       	call   100790 <consoleintr>
}
  102452:	c9                   	leave  
  102453:	c3                   	ret    
  102454:	90                   	nop
  102455:	90                   	nop
  102456:	90                   	nop
  102457:	90                   	nop
  102458:	90                   	nop
  102459:	90                   	nop
  10245a:	90                   	nop
  10245b:	90                   	nop
  10245c:	90                   	nop
  10245d:	90                   	nop
  10245e:	90                   	nop
  10245f:	90                   	nop

00102460 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
  if(lapic)
  102460:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
}

// Acknowledge interrupt.
void
lapiceoi(void)
{
  102465:	55                   	push   %ebp
  102466:	89 e5                	mov    %esp,%ebp
  if(lapic)
  102468:	85 c0                	test   %eax,%eax
  10246a:	74 12                	je     10247e <lapiceoi+0x1e>
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  10246c:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
  102473:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  102476:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  10247b:	8b 40 20             	mov    0x20(%eax),%eax
void
lapiceoi(void)
{
  if(lapic)
    lapicw(EOI, 0);
}
  10247e:	5d                   	pop    %ebp
  10247f:	c3                   	ret    

00102480 <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
  102480:	55                   	push   %ebp
  102481:	89 e5                	mov    %esp,%ebp
}
  102483:	5d                   	pop    %ebp
  102484:	c3                   	ret    
  102485:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  102489:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00102490 <lapicstartap>:

// Start additional processor running bootstrap code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
  102490:	55                   	push   %ebp
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  102491:	ba 70 00 00 00       	mov    $0x70,%edx
  102496:	89 e5                	mov    %esp,%ebp
  102498:	b8 0f 00 00 00       	mov    $0xf,%eax
  10249d:	53                   	push   %ebx
  10249e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  1024a1:	0f b6 5d 08          	movzbl 0x8(%ebp),%ebx
  1024a5:	ee                   	out    %al,(%dx)
  1024a6:	b8 0a 00 00 00       	mov    $0xa,%eax
  1024ab:	b2 71                	mov    $0x71,%dl
  1024ad:	ee                   	out    %al,(%dx)
  // the AP startup code prior to the [universal startup algorithm]."
  outb(IO_RTC, 0xF);  // offset 0xF is shutdown code
  outb(IO_RTC+1, 0x0A);
  wrv = (ushort*)(0x40<<4 | 0x67);  // Warm reset vector
  wrv[0] = 0;
  wrv[1] = addr >> 4;
  1024ae:	89 c8                	mov    %ecx,%eax
  1024b0:	c1 e8 04             	shr    $0x4,%eax
  1024b3:	66 a3 69 04 00 00    	mov    %ax,0x469
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1024b9:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1024be:	c1 e3 18             	shl    $0x18,%ebx
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(IO_RTC, 0xF);  // offset 0xF is shutdown code
  outb(IO_RTC+1, 0x0A);
  wrv = (ushort*)(0x40<<4 | 0x67);  // Warm reset vector
  wrv[0] = 0;
  1024c1:	66 c7 05 67 04 00 00 	movw   $0x0,0x467
  1024c8:	00 00 

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  lapic[ID];  // wait for write to finish, by reading
  1024ca:	c1 e9 0c             	shr    $0xc,%ecx
  1024cd:	80 cd 06             	or     $0x6,%ch
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1024d0:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
  1024d6:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1024db:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1024de:	c7 80 00 03 00 00 00 	movl   $0xc500,0x300(%eax)
  1024e5:	c5 00 00 
  lapic[ID];  // wait for write to finish, by reading
  1024e8:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1024ed:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1024f0:	c7 80 00 03 00 00 00 	movl   $0x8500,0x300(%eax)
  1024f7:	85 00 00 
  lapic[ID];  // wait for write to finish, by reading
  1024fa:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1024ff:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  102502:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
  102508:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  10250d:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  102510:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
  102516:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  10251b:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  10251e:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
  102524:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  102529:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  10252c:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
  102532:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
  102537:	5b                   	pop    %ebx
  102538:	5d                   	pop    %ebp

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  lapic[ID];  // wait for write to finish, by reading
  102539:	8b 40 20             	mov    0x20(%eax),%eax
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
  10253c:	c3                   	ret    
  10253d:	8d 76 00             	lea    0x0(%esi),%esi

00102540 <cpunum>:
  lapicw(TPR, 0);
}

int
cpunum(void)
{
  102540:	55                   	push   %ebp
  102541:	89 e5                	mov    %esp,%ebp
  102543:	83 ec 18             	sub    $0x18,%esp

static inline uint
readeflags(void)
{
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
  102546:	9c                   	pushf  
  102547:	58                   	pop    %eax
  // Cannot call cpu when interrupts are enabled:
  // result not guaranteed to last long enough to be used!
  // Would prefer to panic but even printing is chancy here:
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
  102548:	f6 c4 02             	test   $0x2,%ah
  10254b:	74 12                	je     10255f <cpunum+0x1f>
    static int n;
    if(n++ == 0)
  10254d:	a1 c0 78 10 00       	mov    0x1078c0,%eax
  102552:	8d 50 01             	lea    0x1(%eax),%edx
  102555:	85 c0                	test   %eax,%eax
  102557:	89 15 c0 78 10 00    	mov    %edx,0x1078c0
  10255d:	74 19                	je     102578 <cpunum+0x38>
      cprintf("cpu called from %x with interrupts enabled\n",
        __builtin_return_address(0));
  }

  if(lapic)
  10255f:	8b 15 f8 ba 10 00    	mov    0x10baf8,%edx
  102565:	31 c0                	xor    %eax,%eax
  102567:	85 d2                	test   %edx,%edx
  102569:	74 06                	je     102571 <cpunum+0x31>
    return lapic[ID]>>24;
  10256b:	8b 42 20             	mov    0x20(%edx),%eax
  10256e:	c1 e8 18             	shr    $0x18,%eax
  return 0;
}
  102571:	c9                   	leave  
  102572:	c3                   	ret    
  102573:	90                   	nop
  102574:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
    static int n;
    if(n++ == 0)
      cprintf("cpu called from %x with interrupts enabled\n",
  102578:	8b 45 04             	mov    0x4(%ebp),%eax
  10257b:	c7 04 24 70 68 10 00 	movl   $0x106870,(%esp)
  102582:	89 44 24 04          	mov    %eax,0x4(%esp)
  102586:	e8 a5 df ff ff       	call   100530 <cprintf>
  10258b:	eb d2                	jmp    10255f <cpunum+0x1f>
  10258d:	8d 76 00             	lea    0x0(%esi),%esi

00102590 <lapicinit>:
  lapic[ID];  // wait for write to finish, by reading
}

void
lapicinit(int c)
{
  102590:	55                   	push   %ebp
  102591:	89 e5                	mov    %esp,%ebp
  102593:	83 ec 18             	sub    $0x18,%esp
  cprintf("lapicinit: %d 0x%x\n", c, lapic);
  102596:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  10259b:	c7 04 24 9c 68 10 00 	movl   $0x10689c,(%esp)
  1025a2:	89 44 24 08          	mov    %eax,0x8(%esp)
  1025a6:	8b 45 08             	mov    0x8(%ebp),%eax
  1025a9:	89 44 24 04          	mov    %eax,0x4(%esp)
  1025ad:	e8 7e df ff ff       	call   100530 <cprintf>
  if(!lapic) 
  1025b2:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1025b7:	85 c0                	test   %eax,%eax
  1025b9:	0f 84 0a 01 00 00    	je     1026c9 <lapicinit+0x139>
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1025bf:	c7 80 f0 00 00 00 3f 	movl   $0x13f,0xf0(%eax)
  1025c6:	01 00 00 
  lapic[ID];  // wait for write to finish, by reading
  1025c9:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1025ce:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1025d1:	c7 80 e0 03 00 00 0b 	movl   $0xb,0x3e0(%eax)
  1025d8:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  1025db:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1025e0:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1025e3:	c7 80 20 03 00 00 20 	movl   $0x20020,0x320(%eax)
  1025ea:	00 02 00 
  lapic[ID];  // wait for write to finish, by reading
  1025ed:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1025f2:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1025f5:	c7 80 80 03 00 00 80 	movl   $0x989680,0x380(%eax)
  1025fc:	96 98 00 
  lapic[ID];  // wait for write to finish, by reading
  1025ff:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  102604:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  102607:	c7 80 50 03 00 00 00 	movl   $0x10000,0x350(%eax)
  10260e:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
  102611:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  102616:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  102619:	c7 80 60 03 00 00 00 	movl   $0x10000,0x360(%eax)
  102620:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
  102623:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  102628:	8b 50 20             	mov    0x20(%eax),%edx
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
  10262b:	8b 50 30             	mov    0x30(%eax),%edx
  10262e:	c1 ea 10             	shr    $0x10,%edx
  102631:	80 fa 03             	cmp    $0x3,%dl
  102634:	0f 87 96 00 00 00    	ja     1026d0 <lapicinit+0x140>
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  10263a:	c7 80 70 03 00 00 33 	movl   $0x33,0x370(%eax)
  102641:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  102644:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  102649:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  10264c:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
  102653:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  102656:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  10265b:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  10265e:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
  102665:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  102668:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  10266d:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  102670:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
  102677:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  10267a:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  10267f:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  102682:	c7 80 10 03 00 00 00 	movl   $0x0,0x310(%eax)
  102689:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  10268c:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  102691:	8b 50 20             	mov    0x20(%eax),%edx
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  102694:	c7 80 00 03 00 00 00 	movl   $0x88500,0x300(%eax)
  10269b:	85 08 00 
  lapic[ID];  // wait for write to finish, by reading
  10269e:	8b 0d f8 ba 10 00    	mov    0x10baf8,%ecx
  1026a4:	8b 41 20             	mov    0x20(%ecx),%eax
  1026a7:	8d 91 00 03 00 00    	lea    0x300(%ecx),%edx
  1026ad:	8d 76 00             	lea    0x0(%esi),%esi
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
  1026b0:	8b 02                	mov    (%edx),%eax
  1026b2:	f6 c4 10             	test   $0x10,%ah
  1026b5:	75 f9                	jne    1026b0 <lapicinit+0x120>
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1026b7:	c7 81 80 00 00 00 00 	movl   $0x0,0x80(%ecx)
  1026be:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
  1026c1:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1026c6:	8b 40 20             	mov    0x20(%eax),%eax
  while(lapic[ICRLO] & DELIVS)
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
  1026c9:	c9                   	leave  
  1026ca:	c3                   	ret    
  1026cb:	90                   	nop
  1026cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
  lapic[index] = value;
  1026d0:	c7 80 40 03 00 00 00 	movl   $0x10000,0x340(%eax)
  1026d7:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
  1026da:	a1 f8 ba 10 00       	mov    0x10baf8,%eax
  1026df:	8b 50 20             	mov    0x20(%eax),%edx
  1026e2:	e9 53 ff ff ff       	jmp    10263a <lapicinit+0xaa>
  1026e7:	90                   	nop
  1026e8:	90                   	nop
  1026e9:	90                   	nop
  1026ea:	90                   	nop
  1026eb:	90                   	nop
  1026ec:	90                   	nop
  1026ed:	90                   	nop
  1026ee:	90                   	nop
  1026ef:	90                   	nop

001026f0 <mpmain>:
// Common CPU setup code.
// Bootstrap CPU comes here from mainc().
// Other CPUs jump here from bootother.S.
static void
mpmain(void)
{
  1026f0:	55                   	push   %ebp
  1026f1:	89 e5                	mov    %esp,%ebp
  1026f3:	53                   	push   %ebx
  1026f4:	83 ec 14             	sub    $0x14,%esp
  if(cpunum() != mpbcpu()){
  1026f7:	e8 44 fe ff ff       	call   102540 <cpunum>
  1026fc:	89 c3                	mov    %eax,%ebx
  1026fe:	e8 ed 01 00 00       	call   1028f0 <mpbcpu>
  102703:	39 c3                	cmp    %eax,%ebx
  102705:	74 16                	je     10271d <mpmain+0x2d>
    seginit();
  102707:	e8 44 3c 00 00       	call   106350 <seginit>
  10270c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    lapicinit(cpunum());
  102710:	e8 2b fe ff ff       	call   102540 <cpunum>
  102715:	89 04 24             	mov    %eax,(%esp)
  102718:	e8 73 fe ff ff       	call   102590 <lapicinit>
  }
  vmenable();        // turn on paging
  10271d:	e8 ee 34 00 00       	call   105c10 <vmenable>
  cprintf("cpu%d: starting\n", cpu->id);
  102722:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  102728:	0f b6 00             	movzbl (%eax),%eax
  10272b:	c7 04 24 b0 68 10 00 	movl   $0x1068b0,(%esp)
  102732:	89 44 24 04          	mov    %eax,0x4(%esp)
  102736:	e8 f5 dd ff ff       	call   100530 <cprintf>
  idtinit();       // load idt register
  10273b:	e8 f0 25 00 00       	call   104d30 <idtinit>
  xchg(&cpu->booted, 1); // tell bootothers() we're up
  102740:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
  102747:	b8 01 00 00 00       	mov    $0x1,%eax
  10274c:	f0 87 82 a8 00 00 00 	lock xchg %eax,0xa8(%edx)
  scheduler();     // start running processes
  102753:	e8 18 0c 00 00       	call   103370 <scheduler>
  102758:	90                   	nop
  102759:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00102760 <mainc>:

// Set up hardware and software.
// Runs only on the boostrap processor.
void
mainc(void)
{
  102760:	55                   	push   %ebp
  102761:	89 e5                	mov    %esp,%ebp
  102763:	53                   	push   %ebx
  102764:	83 ec 14             	sub    $0x14,%esp
  cprintf("\ncpu%d: starting xv6\n\n", cpu->id);
  102767:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  10276d:	0f b6 00             	movzbl (%eax),%eax
  102770:	c7 04 24 c1 68 10 00 	movl   $0x1068c1,(%esp)
  102777:	89 44 24 04          	mov    %eax,0x4(%esp)
  10277b:	e8 b0 dd ff ff       	call   100530 <cprintf>
  picinit();       // interrupt controller
  102780:	e8 4b 04 00 00       	call   102bd0 <picinit>
  ioapicinit();    // another interrupt controller
  102785:	e8 16 fa ff ff       	call   1021a0 <ioapicinit>
  consoleinit();   // I/O devices & their interrupts
  10278a:	e8 d1 da ff ff       	call   100260 <consoleinit>
  10278f:	90                   	nop
  uartinit();      // serial port
  102790:	e8 4b 29 00 00       	call   1050e0 <uartinit>
  kvmalloc();      // initialize the kernel page table
  102795:	e8 f6 36 00 00       	call   105e90 <kvmalloc>
  pinit();         // process table
  10279a:	e8 e1 11 00 00       	call   103980 <pinit>
  10279f:	90                   	nop
  tvinit();        // trap vectors
  1027a0:	e8 0b 28 00 00       	call   104fb0 <tvinit>
  binit();         // buffer cache
  1027a5:	e8 46 da ff ff       	call   1001f0 <binit>
  fileinit();      // file table
  1027aa:	e8 a1 e8 ff ff       	call   101050 <fileinit>
  1027af:	90                   	nop
  iinit();         // inode cache
  1027b0:	e8 cb f6 ff ff       	call   101e80 <iinit>
  ideinit();       // disk
  1027b5:	e8 06 f9 ff ff       	call   1020c0 <ideinit>
  if(!ismp)
  1027ba:	a1 04 bb 10 00       	mov    0x10bb04,%eax
  1027bf:	85 c0                	test   %eax,%eax
  1027c1:	0f 84 ae 00 00 00    	je     102875 <mainc+0x115>
    timerinit();   // uniprocessor timer
  userinit();      // first user process
  1027c7:	e8 c4 10 00 00       	call   103890 <userinit>

  // Write bootstrap code to unused memory at 0x7000.
  // The linker has placed the image of bootother.S in
  // _binary_bootother_start.
  code = (uchar*)0x7000;
  memmove(code, _binary_bootother_start, (uint)_binary_bootother_size);
  1027cc:	c7 44 24 08 6a 00 00 	movl   $0x6a,0x8(%esp)
  1027d3:	00 
  1027d4:	c7 44 24 04 9c 77 10 	movl   $0x10779c,0x4(%esp)
  1027db:	00 
  1027dc:	c7 04 24 00 70 00 00 	movl   $0x7000,(%esp)
  1027e3:	e8 68 14 00 00       	call   103c50 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
  1027e8:	69 05 00 c1 10 00 bc 	imul   $0xbc,0x10c100,%eax
  1027ef:	00 00 00 
  1027f2:	05 20 bb 10 00       	add    $0x10bb20,%eax
  1027f7:	3d 20 bb 10 00       	cmp    $0x10bb20,%eax
  1027fc:	76 6d                	jbe    10286b <mainc+0x10b>
  1027fe:	bb 20 bb 10 00       	mov    $0x10bb20,%ebx
  102803:	90                   	nop
  102804:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(c == cpus+cpunum())  // We've started already.
  102808:	e8 33 fd ff ff       	call   102540 <cpunum>
  10280d:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
  102813:	05 20 bb 10 00       	add    $0x10bb20,%eax
  102818:	39 c3                	cmp    %eax,%ebx
  10281a:	74 36                	je     102852 <mainc+0xf2>
      continue;

    // Tell bootother.S what stack to use and the address of mpmain;
    // it expects to find these two addresses stored just before
    // its first instruction.
    stack = kalloc();
  10281c:	e8 3f fa ff ff       	call   102260 <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
    *(void**)(code-8) = mpmain;
  102821:	c7 05 f8 6f 00 00 f0 	movl   $0x1026f0,0x6ff8
  102828:	26 10 00 

    // Tell bootother.S what stack to use and the address of mpmain;
    // it expects to find these two addresses stored just before
    // its first instruction.
    stack = kalloc();
    *(void**)(code-4) = stack + KSTACKSIZE;
  10282b:	05 00 10 00 00       	add    $0x1000,%eax
  102830:	a3 fc 6f 00 00       	mov    %eax,0x6ffc
    *(void**)(code-8) = mpmain;

    lapicstartap(c->id, (uint)code);
  102835:	0f b6 03             	movzbl (%ebx),%eax
  102838:	c7 44 24 04 00 70 00 	movl   $0x7000,0x4(%esp)
  10283f:	00 
  102840:	89 04 24             	mov    %eax,(%esp)
  102843:	e8 48 fc ff ff       	call   102490 <lapicstartap>

    // Wait for cpu to finish mpmain()
    while(c->booted == 0)
  102848:	8b 83 a8 00 00 00    	mov    0xa8(%ebx),%eax
  10284e:	85 c0                	test   %eax,%eax
  102850:	74 f6                	je     102848 <mainc+0xe8>
  // The linker has placed the image of bootother.S in
  // _binary_bootother_start.
  code = (uchar*)0x7000;
  memmove(code, _binary_bootother_start, (uint)_binary_bootother_size);

  for(c = cpus; c < cpus+ncpu; c++){
  102852:	69 05 00 c1 10 00 bc 	imul   $0xbc,0x10c100,%eax
  102859:	00 00 00 
  10285c:	81 c3 bc 00 00 00    	add    $0xbc,%ebx
  102862:	05 20 bb 10 00       	add    $0x10bb20,%eax
  102867:	39 c3                	cmp    %eax,%ebx
  102869:	72 9d                	jb     102808 <mainc+0xa8>
  userinit();      // first user process
  bootothers();    // start other processors

  // Finish setting up this processor in mpmain.
  mpmain();
}
  10286b:	83 c4 14             	add    $0x14,%esp
  10286e:	5b                   	pop    %ebx
  10286f:	5d                   	pop    %ebp
    timerinit();   // uniprocessor timer
  userinit();      // first user process
  bootothers();    // start other processors

  // Finish setting up this processor in mpmain.
  mpmain();
  102870:	e9 7b fe ff ff       	jmp    1026f0 <mpmain>
  binit();         // buffer cache
  fileinit();      // file table
  iinit();         // inode cache
  ideinit();       // disk
  if(!ismp)
    timerinit();   // uniprocessor timer
  102875:	e8 56 24 00 00       	call   104cd0 <timerinit>
  10287a:	e9 48 ff ff ff       	jmp    1027c7 <mainc+0x67>
  10287f:	90                   	nop

00102880 <jmpkstack>:
  jmpkstack();       // call mainc() on a properly-allocated stack 
}

void
jmpkstack(void)
{
  102880:	55                   	push   %ebp
  102881:	89 e5                	mov    %esp,%ebp
  102883:	83 ec 18             	sub    $0x18,%esp
  char *kstack, *top;
  
  kstack = kalloc();
  102886:	e8 d5 f9 ff ff       	call   102260 <kalloc>
  if(kstack == 0)
  10288b:	85 c0                	test   %eax,%eax
  10288d:	74 19                	je     1028a8 <jmpkstack+0x28>
    panic("jmpkstack kalloc");
  top = kstack + PGSIZE;
  asm volatile("movl %0,%%esp; call mainc" : : "r" (top));
  10288f:	05 00 10 00 00       	add    $0x1000,%eax
  102894:	89 c4                	mov    %eax,%esp
  102896:	e8 c5 fe ff ff       	call   102760 <mainc>
  panic("jmpkstack");
  10289b:	c7 04 24 e9 68 10 00 	movl   $0x1068e9,(%esp)
  1028a2:	e8 69 e0 ff ff       	call   100910 <panic>
  1028a7:	90                   	nop
{
  char *kstack, *top;
  
  kstack = kalloc();
  if(kstack == 0)
    panic("jmpkstack kalloc");
  1028a8:	c7 04 24 d8 68 10 00 	movl   $0x1068d8,(%esp)
  1028af:	e8 5c e0 ff ff       	call   100910 <panic>
  1028b4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  1028ba:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

001028c0 <main>:
// Bootstrap processor starts running C code here.
// Allocate a real stack and switch to it, first
// doing some setup required for memory allocator to work.
int
main(void)
{
  1028c0:	55                   	push   %ebp
  1028c1:	89 e5                	mov    %esp,%ebp
  1028c3:	83 e4 f0             	and    $0xfffffff0,%esp
  1028c6:	83 ec 10             	sub    $0x10,%esp
  mpinit();        // collect info about this machine
  1028c9:	e8 b2 00 00 00       	call   102980 <mpinit>
  lapicinit(mpbcpu());
  1028ce:	e8 1d 00 00 00       	call   1028f0 <mpbcpu>
  1028d3:	89 04 24             	mov    %eax,(%esp)
  1028d6:	e8 b5 fc ff ff       	call   102590 <lapicinit>
  seginit();       // set up segments
  1028db:	e8 70 3a 00 00       	call   106350 <seginit>
  kinit();         // initialize memory allocator
  1028e0:	e8 2b fa ff ff       	call   102310 <kinit>
  jmpkstack();       // call mainc() on a properly-allocated stack 
  1028e5:	e8 96 ff ff ff       	call   102880 <jmpkstack>
  1028ea:	90                   	nop
  1028eb:	90                   	nop
  1028ec:	90                   	nop
  1028ed:	90                   	nop
  1028ee:	90                   	nop
  1028ef:	90                   	nop

001028f0 <mpbcpu>:
int ncpu;
uchar ioapicid;

int
mpbcpu(void)
{
  1028f0:	a1 c4 78 10 00       	mov    0x1078c4,%eax
  1028f5:	55                   	push   %ebp
  1028f6:	89 e5                	mov    %esp,%ebp
  return bcpu-cpus;
}
  1028f8:	5d                   	pop    %ebp
int ncpu;
uchar ioapicid;

int
mpbcpu(void)
{
  1028f9:	2d 20 bb 10 00       	sub    $0x10bb20,%eax
  1028fe:	c1 f8 02             	sar    $0x2,%eax
  102901:	69 c0 cf 46 7d 67    	imul   $0x677d46cf,%eax,%eax
  return bcpu-cpus;
}
  102907:	c3                   	ret    
  102908:	90                   	nop
  102909:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00102910 <mpsearch1>:
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uchar *addr, int len)
{
  102910:	55                   	push   %ebp
  102911:	89 e5                	mov    %esp,%ebp
  102913:	56                   	push   %esi
  102914:	53                   	push   %ebx
  uchar *e, *p;

  e = addr+len;
  102915:	8d 34 10             	lea    (%eax,%edx,1),%esi
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uchar *addr, int len)
{
  102918:	83 ec 10             	sub    $0x10,%esp
  uchar *e, *p;

  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
  10291b:	39 f0                	cmp    %esi,%eax
  10291d:	73 42                	jae    102961 <mpsearch1+0x51>
  10291f:	89 c3                	mov    %eax,%ebx
  102921:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
  102928:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
  10292f:	00 
  102930:	c7 44 24 04 f3 68 10 	movl   $0x1068f3,0x4(%esp)
  102937:	00 
  102938:	89 1c 24             	mov    %ebx,(%esp)
  10293b:	e8 b0 12 00 00       	call   103bf0 <memcmp>
  102940:	85 c0                	test   %eax,%eax
  102942:	75 16                	jne    10295a <mpsearch1+0x4a>
  102944:	31 d2                	xor    %edx,%edx
  102946:	66 90                	xchg   %ax,%ax
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
    sum += addr[i];
  102948:	0f b6 0c 03          	movzbl (%ebx,%eax,1),%ecx
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
  10294c:	83 c0 01             	add    $0x1,%eax
    sum += addr[i];
  10294f:	01 ca                	add    %ecx,%edx
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
  102951:	83 f8 10             	cmp    $0x10,%eax
  102954:	75 f2                	jne    102948 <mpsearch1+0x38>
{
  uchar *e, *p;

  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
  102956:	84 d2                	test   %dl,%dl
  102958:	74 10                	je     10296a <mpsearch1+0x5a>
mpsearch1(uchar *addr, int len)
{
  uchar *e, *p;

  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
  10295a:	83 c3 10             	add    $0x10,%ebx
  10295d:	39 de                	cmp    %ebx,%esi
  10295f:	77 c7                	ja     102928 <mpsearch1+0x18>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
}
  102961:	83 c4 10             	add    $0x10,%esp
mpsearch1(uchar *addr, int len)
{
  uchar *e, *p;

  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
  102964:	31 c0                	xor    %eax,%eax
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
}
  102966:	5b                   	pop    %ebx
  102967:	5e                   	pop    %esi
  102968:	5d                   	pop    %ebp
  102969:	c3                   	ret    
  10296a:	83 c4 10             	add    $0x10,%esp
  uchar *e, *p;

  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  10296d:	89 d8                	mov    %ebx,%eax
  return 0;
}
  10296f:	5b                   	pop    %ebx
  102970:	5e                   	pop    %esi
  102971:	5d                   	pop    %ebp
  102972:	c3                   	ret    
  102973:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  102979:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00102980 <mpinit>:
  return conf;
}

void
mpinit(void)
{
  102980:	55                   	push   %ebp
  102981:	89 e5                	mov    %esp,%ebp
  102983:	57                   	push   %edi
  102984:	56                   	push   %esi
  102985:	53                   	push   %ebx
  102986:	83 ec 1c             	sub    $0x1c,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar*)0x400;
  if((p = ((bda[0x0F]<<8)|bda[0x0E]) << 4)){
  102989:	0f b6 05 0f 04 00 00 	movzbl 0x40f,%eax
  102990:	0f b6 15 0e 04 00 00 	movzbl 0x40e,%edx
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
  102997:	c7 05 c4 78 10 00 20 	movl   $0x10bb20,0x1078c4
  10299e:	bb 10 00 
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar*)0x400;
  if((p = ((bda[0x0F]<<8)|bda[0x0E]) << 4)){
  1029a1:	c1 e0 08             	shl    $0x8,%eax
  1029a4:	09 d0                	or     %edx,%eax
  1029a6:	c1 e0 04             	shl    $0x4,%eax
  1029a9:	85 c0                	test   %eax,%eax
  1029ab:	75 1b                	jne    1029c8 <mpinit+0x48>
    if((mp = mpsearch1((uchar*)p, 1024)))
      return mp;
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
    if((mp = mpsearch1((uchar*)p-1024, 1024)))
  1029ad:	0f b6 05 14 04 00 00 	movzbl 0x414,%eax
  1029b4:	0f b6 15 13 04 00 00 	movzbl 0x413,%edx
  1029bb:	c1 e0 08             	shl    $0x8,%eax
  1029be:	09 d0                	or     %edx,%eax
  1029c0:	c1 e0 0a             	shl    $0xa,%eax
  1029c3:	2d 00 04 00 00       	sub    $0x400,%eax
  1029c8:	ba 00 04 00 00       	mov    $0x400,%edx
  1029cd:	e8 3e ff ff ff       	call   102910 <mpsearch1>
  1029d2:	85 c0                	test   %eax,%eax
  1029d4:	89 c6                	mov    %eax,%esi
  1029d6:	0f 84 94 01 00 00    	je     102b70 <mpinit+0x1f0>
mpconfig(struct mp **pmp)
{
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
  1029dc:	8b 5e 04             	mov    0x4(%esi),%ebx
  1029df:	85 db                	test   %ebx,%ebx
  1029e1:	74 1c                	je     1029ff <mpinit+0x7f>
    return 0;
  conf = (struct mpconf*)mp->physaddr;
  if(memcmp(conf, "PCMP", 4) != 0)
  1029e3:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
  1029ea:	00 
  1029eb:	c7 44 24 04 f8 68 10 	movl   $0x1068f8,0x4(%esp)
  1029f2:	00 
  1029f3:	89 1c 24             	mov    %ebx,(%esp)
  1029f6:	e8 f5 11 00 00       	call   103bf0 <memcmp>
  1029fb:	85 c0                	test   %eax,%eax
  1029fd:	74 09                	je     102a08 <mpinit+0x88>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
  1029ff:	83 c4 1c             	add    $0x1c,%esp
  102a02:	5b                   	pop    %ebx
  102a03:	5e                   	pop    %esi
  102a04:	5f                   	pop    %edi
  102a05:	5d                   	pop    %ebp
  102a06:	c3                   	ret    
  102a07:	90                   	nop
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
    return 0;
  conf = (struct mpconf*)mp->physaddr;
  if(memcmp(conf, "PCMP", 4) != 0)
    return 0;
  if(conf->version != 1 && conf->version != 4)
  102a08:	0f b6 43 06          	movzbl 0x6(%ebx),%eax
  102a0c:	3c 04                	cmp    $0x4,%al
  102a0e:	74 04                	je     102a14 <mpinit+0x94>
  102a10:	3c 01                	cmp    $0x1,%al
  102a12:	75 eb                	jne    1029ff <mpinit+0x7f>
    return 0;
  if(sum((uchar*)conf, conf->length) != 0)
  102a14:	0f b7 7b 04          	movzwl 0x4(%ebx),%edi
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
  102a18:	85 ff                	test   %edi,%edi
  102a1a:	74 15                	je     102a31 <mpinit+0xb1>
  102a1c:	31 d2                	xor    %edx,%edx
  102a1e:	31 c0                	xor    %eax,%eax
    sum += addr[i];
  102a20:	0f b6 0c 03          	movzbl (%ebx,%eax,1),%ecx
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
  102a24:	83 c0 01             	add    $0x1,%eax
    sum += addr[i];
  102a27:	01 ca                	add    %ecx,%edx
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
  102a29:	39 c7                	cmp    %eax,%edi
  102a2b:	7f f3                	jg     102a20 <mpinit+0xa0>
  conf = (struct mpconf*)mp->physaddr;
  if(memcmp(conf, "PCMP", 4) != 0)
    return 0;
  if(conf->version != 1 && conf->version != 4)
    return 0;
  if(sum((uchar*)conf, conf->length) != 0)
  102a2d:	84 d2                	test   %dl,%dl
  102a2f:	75 ce                	jne    1029ff <mpinit+0x7f>
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  102a31:	c7 05 04 bb 10 00 01 	movl   $0x1,0x10bb04
  102a38:	00 00 00 
  lapic = (uint*)conf->lapicaddr;
  102a3b:	8b 43 24             	mov    0x24(%ebx),%eax
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
  102a3e:	8d 7b 2c             	lea    0x2c(%ebx),%edi

  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  102a41:	a3 f8 ba 10 00       	mov    %eax,0x10baf8
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
  102a46:	0f b7 43 04          	movzwl 0x4(%ebx),%eax
  102a4a:	01 c3                	add    %eax,%ebx
  102a4c:	39 df                	cmp    %ebx,%edi
  102a4e:	72 29                	jb     102a79 <mpinit+0xf9>
  102a50:	eb 52                	jmp    102aa4 <mpinit+0x124>
  102a52:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    case MPIOINTR:
    case MPLINTR:
      p += 8;
      continue;
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
  102a58:	0f b6 c0             	movzbl %al,%eax
  102a5b:	89 44 24 04          	mov    %eax,0x4(%esp)
  102a5f:	c7 04 24 18 69 10 00 	movl   $0x106918,(%esp)
  102a66:	e8 c5 da ff ff       	call   100530 <cprintf>
      ismp = 0;
  102a6b:	c7 05 04 bb 10 00 00 	movl   $0x0,0x10bb04
  102a72:	00 00 00 
  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
  102a75:	39 fb                	cmp    %edi,%ebx
  102a77:	76 1e                	jbe    102a97 <mpinit+0x117>
    switch(*p){
  102a79:	0f b6 07             	movzbl (%edi),%eax
  102a7c:	3c 04                	cmp    $0x4,%al
  102a7e:	77 d8                	ja     102a58 <mpinit+0xd8>
  102a80:	0f b6 c0             	movzbl %al,%eax
  102a83:	ff 24 85 38 69 10 00 	jmp    *0x106938(,%eax,4)
  102a8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      p += sizeof(struct mpioapic);
      continue;
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
  102a90:	83 c7 08             	add    $0x8,%edi
  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
  102a93:	39 fb                	cmp    %edi,%ebx
  102a95:	77 e2                	ja     102a79 <mpinit+0xf9>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
      ismp = 0;
    }
  }
  if(!ismp){
  102a97:	a1 04 bb 10 00       	mov    0x10bb04,%eax
  102a9c:	85 c0                	test   %eax,%eax
  102a9e:	0f 84 a4 00 00 00    	je     102b48 <mpinit+0x1c8>
    lapic = 0;
    ioapicid = 0;
    return;
  }

  if(mp->imcrp){
  102aa4:	80 7e 0c 00          	cmpb   $0x0,0xc(%esi)
  102aa8:	0f 84 51 ff ff ff    	je     1029ff <mpinit+0x7f>
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  102aae:	ba 22 00 00 00       	mov    $0x22,%edx
  102ab3:	b8 70 00 00 00       	mov    $0x70,%eax
  102ab8:	ee                   	out    %al,(%dx)
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  102ab9:	b2 23                	mov    $0x23,%dl
  102abb:	ec                   	in     (%dx),%al
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  102abc:	83 c8 01             	or     $0x1,%eax
  102abf:	ee                   	out    %al,(%dx)
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
  102ac0:	83 c4 1c             	add    $0x1c,%esp
  102ac3:	5b                   	pop    %ebx
  102ac4:	5e                   	pop    %esi
  102ac5:	5f                   	pop    %edi
  102ac6:	5d                   	pop    %ebp
  102ac7:	c3                   	ret    
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
    switch(*p){
    case MPPROC:
      proc = (struct mpproc*)p;
      if(ncpu != proc->apicid){
  102ac8:	0f b6 57 01          	movzbl 0x1(%edi),%edx
  102acc:	a1 00 c1 10 00       	mov    0x10c100,%eax
  102ad1:	39 c2                	cmp    %eax,%edx
  102ad3:	74 23                	je     102af8 <mpinit+0x178>
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
  102ad5:	89 44 24 04          	mov    %eax,0x4(%esp)
  102ad9:	89 54 24 08          	mov    %edx,0x8(%esp)
  102add:	c7 04 24 fd 68 10 00 	movl   $0x1068fd,(%esp)
  102ae4:	e8 47 da ff ff       	call   100530 <cprintf>
        ismp = 0;
  102ae9:	a1 00 c1 10 00       	mov    0x10c100,%eax
  102aee:	c7 05 04 bb 10 00 00 	movl   $0x0,0x10bb04
  102af5:	00 00 00 
      }
      if(proc->flags & MPBOOT)
  102af8:	f6 47 03 02          	testb  $0x2,0x3(%edi)
  102afc:	74 12                	je     102b10 <mpinit+0x190>
        bcpu = &cpus[ncpu];
  102afe:	69 d0 bc 00 00 00    	imul   $0xbc,%eax,%edx
  102b04:	81 c2 20 bb 10 00    	add    $0x10bb20,%edx
  102b0a:	89 15 c4 78 10 00    	mov    %edx,0x1078c4
      cpus[ncpu].id = ncpu;
  102b10:	69 d0 bc 00 00 00    	imul   $0xbc,%eax,%edx
      ncpu++;
      p += sizeof(struct mpproc);
  102b16:	83 c7 14             	add    $0x14,%edi
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
        ismp = 0;
      }
      if(proc->flags & MPBOOT)
        bcpu = &cpus[ncpu];
      cpus[ncpu].id = ncpu;
  102b19:	88 82 20 bb 10 00    	mov    %al,0x10bb20(%edx)
      ncpu++;
  102b1f:	83 c0 01             	add    $0x1,%eax
  102b22:	a3 00 c1 10 00       	mov    %eax,0x10c100
      p += sizeof(struct mpproc);
      continue;
  102b27:	e9 49 ff ff ff       	jmp    102a75 <mpinit+0xf5>
  102b2c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    case MPIOAPIC:
      ioapic = (struct mpioapic*)p;
      ioapicid = ioapic->apicno;
  102b30:	0f b6 47 01          	movzbl 0x1(%edi),%eax
      p += sizeof(struct mpioapic);
  102b34:	83 c7 08             	add    $0x8,%edi
      ncpu++;
      p += sizeof(struct mpproc);
      continue;
    case MPIOAPIC:
      ioapic = (struct mpioapic*)p;
      ioapicid = ioapic->apicno;
  102b37:	a2 00 bb 10 00       	mov    %al,0x10bb00
      p += sizeof(struct mpioapic);
      continue;
  102b3c:	e9 34 ff ff ff       	jmp    102a75 <mpinit+0xf5>
  102b41:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      ismp = 0;
    }
  }
  if(!ismp){
    // Didn't like what we found; fall back to no MP.
    ncpu = 1;
  102b48:	c7 05 00 c1 10 00 01 	movl   $0x1,0x10c100
  102b4f:	00 00 00 
    lapic = 0;
  102b52:	c7 05 f8 ba 10 00 00 	movl   $0x0,0x10baf8
  102b59:	00 00 00 
    ioapicid = 0;
  102b5c:	c6 05 00 bb 10 00 00 	movb   $0x0,0x10bb00
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
  102b63:	83 c4 1c             	add    $0x1c,%esp
  102b66:	5b                   	pop    %ebx
  102b67:	5e                   	pop    %esi
  102b68:	5f                   	pop    %edi
  102b69:	5d                   	pop    %ebp
  102b6a:	c3                   	ret    
  102b6b:	90                   	nop
  102b6c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
    if((mp = mpsearch1((uchar*)p-1024, 1024)))
      return mp;
  }
  return mpsearch1((uchar*)0xF0000, 0x10000);
  102b70:	ba 00 00 01 00       	mov    $0x10000,%edx
  102b75:	b8 00 00 0f 00       	mov    $0xf0000,%eax
  102b7a:	e8 91 fd ff ff       	call   102910 <mpsearch1>
mpconfig(struct mp **pmp)
{
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
  102b7f:	85 c0                	test   %eax,%eax
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
    if((mp = mpsearch1((uchar*)p-1024, 1024)))
      return mp;
  }
  return mpsearch1((uchar*)0xF0000, 0x10000);
  102b81:	89 c6                	mov    %eax,%esi
mpconfig(struct mp **pmp)
{
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
  102b83:	0f 85 53 fe ff ff    	jne    1029dc <mpinit+0x5c>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
  102b89:	83 c4 1c             	add    $0x1c,%esp
  102b8c:	5b                   	pop    %ebx
  102b8d:	5e                   	pop    %esi
  102b8e:	5f                   	pop    %edi
  102b8f:	5d                   	pop    %ebp
  102b90:	c3                   	ret    
  102b91:	90                   	nop
  102b92:	90                   	nop
  102b93:	90                   	nop
  102b94:	90                   	nop
  102b95:	90                   	nop
  102b96:	90                   	nop
  102b97:	90                   	nop
  102b98:	90                   	nop
  102b99:	90                   	nop
  102b9a:	90                   	nop
  102b9b:	90                   	nop
  102b9c:	90                   	nop
  102b9d:	90                   	nop
  102b9e:	90                   	nop
  102b9f:	90                   	nop

00102ba0 <picenable>:
  outb(IO_PIC2+1, mask >> 8);
}

void
picenable(int irq)
{
  102ba0:	55                   	push   %ebp
  picsetmask(irqmask & ~(1<<irq));
  102ba1:	b8 fe ff ff ff       	mov    $0xfffffffe,%eax
  outb(IO_PIC2+1, mask >> 8);
}

void
picenable(int irq)
{
  102ba6:	89 e5                	mov    %esp,%ebp
  102ba8:	ba 21 00 00 00       	mov    $0x21,%edx
  picsetmask(irqmask & ~(1<<irq));
  102bad:	8b 4d 08             	mov    0x8(%ebp),%ecx
  102bb0:	d3 c0                	rol    %cl,%eax
  102bb2:	66 23 05 20 73 10 00 	and    0x107320,%ax
static ushort irqmask = 0xFFFF & ~(1<<IRQ_SLAVE);

static void
picsetmask(ushort mask)
{
  irqmask = mask;
  102bb9:	66 a3 20 73 10 00    	mov    %ax,0x107320
  102bbf:	ee                   	out    %al,(%dx)
  102bc0:	66 c1 e8 08          	shr    $0x8,%ax
  102bc4:	b2 a1                	mov    $0xa1,%dl
  102bc6:	ee                   	out    %al,(%dx)

void
picenable(int irq)
{
  picsetmask(irqmask & ~(1<<irq));
}
  102bc7:	5d                   	pop    %ebp
  102bc8:	c3                   	ret    
  102bc9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00102bd0 <picinit>:

// Initialize the 8259A interrupt controllers.
void
picinit(void)
{
  102bd0:	55                   	push   %ebp
  102bd1:	b9 21 00 00 00       	mov    $0x21,%ecx
  102bd6:	89 e5                	mov    %esp,%ebp
  102bd8:	83 ec 0c             	sub    $0xc,%esp
  102bdb:	89 1c 24             	mov    %ebx,(%esp)
  102bde:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  102be3:	89 ca                	mov    %ecx,%edx
  102be5:	89 74 24 04          	mov    %esi,0x4(%esp)
  102be9:	89 7c 24 08          	mov    %edi,0x8(%esp)
  102bed:	ee                   	out    %al,(%dx)
  102bee:	bb a1 00 00 00       	mov    $0xa1,%ebx
  102bf3:	89 da                	mov    %ebx,%edx
  102bf5:	ee                   	out    %al,(%dx)
  102bf6:	be 11 00 00 00       	mov    $0x11,%esi
  102bfb:	b2 20                	mov    $0x20,%dl
  102bfd:	89 f0                	mov    %esi,%eax
  102bff:	ee                   	out    %al,(%dx)
  102c00:	b8 20 00 00 00       	mov    $0x20,%eax
  102c05:	89 ca                	mov    %ecx,%edx
  102c07:	ee                   	out    %al,(%dx)
  102c08:	b8 04 00 00 00       	mov    $0x4,%eax
  102c0d:	ee                   	out    %al,(%dx)
  102c0e:	bf 03 00 00 00       	mov    $0x3,%edi
  102c13:	89 f8                	mov    %edi,%eax
  102c15:	ee                   	out    %al,(%dx)
  102c16:	b1 a0                	mov    $0xa0,%cl
  102c18:	89 f0                	mov    %esi,%eax
  102c1a:	89 ca                	mov    %ecx,%edx
  102c1c:	ee                   	out    %al,(%dx)
  102c1d:	b8 28 00 00 00       	mov    $0x28,%eax
  102c22:	89 da                	mov    %ebx,%edx
  102c24:	ee                   	out    %al,(%dx)
  102c25:	b8 02 00 00 00       	mov    $0x2,%eax
  102c2a:	ee                   	out    %al,(%dx)
  102c2b:	89 f8                	mov    %edi,%eax
  102c2d:	ee                   	out    %al,(%dx)
  102c2e:	be 68 00 00 00       	mov    $0x68,%esi
  102c33:	b2 20                	mov    $0x20,%dl
  102c35:	89 f0                	mov    %esi,%eax
  102c37:	ee                   	out    %al,(%dx)
  102c38:	bb 0a 00 00 00       	mov    $0xa,%ebx
  102c3d:	89 d8                	mov    %ebx,%eax
  102c3f:	ee                   	out    %al,(%dx)
  102c40:	89 f0                	mov    %esi,%eax
  102c42:	89 ca                	mov    %ecx,%edx
  102c44:	ee                   	out    %al,(%dx)
  102c45:	89 d8                	mov    %ebx,%eax
  102c47:	ee                   	out    %al,(%dx)
  outb(IO_PIC1, 0x0a);             // read IRR by default

  outb(IO_PIC2, 0x68);             // OCW3
  outb(IO_PIC2, 0x0a);             // OCW3

  if(irqmask != 0xFFFF)
  102c48:	0f b7 05 20 73 10 00 	movzwl 0x107320,%eax
  102c4f:	66 83 f8 ff          	cmp    $0xffffffff,%ax
  102c53:	74 0a                	je     102c5f <picinit+0x8f>
  102c55:	b2 21                	mov    $0x21,%dl
  102c57:	ee                   	out    %al,(%dx)
  102c58:	66 c1 e8 08          	shr    $0x8,%ax
  102c5c:	b2 a1                	mov    $0xa1,%dl
  102c5e:	ee                   	out    %al,(%dx)
    picsetmask(irqmask);
}
  102c5f:	8b 1c 24             	mov    (%esp),%ebx
  102c62:	8b 74 24 04          	mov    0x4(%esp),%esi
  102c66:	8b 7c 24 08          	mov    0x8(%esp),%edi
  102c6a:	89 ec                	mov    %ebp,%esp
  102c6c:	5d                   	pop    %ebp
  102c6d:	c3                   	ret    
  102c6e:	90                   	nop
  102c6f:	90                   	nop

00102c70 <piperead>:
  return n;
}

int
piperead(struct pipe *p, char *addr, int n)
{
  102c70:	55                   	push   %ebp
  102c71:	89 e5                	mov    %esp,%ebp
  102c73:	57                   	push   %edi
  102c74:	56                   	push   %esi
  102c75:	53                   	push   %ebx
  102c76:	83 ec 1c             	sub    $0x1c,%esp
  102c79:	8b 5d 08             	mov    0x8(%ebp),%ebx
  102c7c:	8b 7d 10             	mov    0x10(%ebp),%edi
  int i;

  acquire(&p->lock);
  102c7f:	89 1c 24             	mov    %ebx,(%esp)
  102c82:	e8 a9 0e 00 00       	call   103b30 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
  102c87:	8b 93 34 02 00 00    	mov    0x234(%ebx),%edx
  102c8d:	3b 93 38 02 00 00    	cmp    0x238(%ebx),%edx
  102c93:	75 58                	jne    102ced <piperead+0x7d>
  102c95:	8b b3 40 02 00 00    	mov    0x240(%ebx),%esi
  102c9b:	85 f6                	test   %esi,%esi
  102c9d:	74 4e                	je     102ced <piperead+0x7d>
    if(proc->killed){
  102c9f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  102ca5:	8d b3 34 02 00 00    	lea    0x234(%ebx),%esi
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
    if(proc->killed){
  102cab:	8b 48 24             	mov    0x24(%eax),%ecx
  102cae:	85 c9                	test   %ecx,%ecx
  102cb0:	74 21                	je     102cd3 <piperead+0x63>
  102cb2:	e9 99 00 00 00       	jmp    102d50 <piperead+0xe0>
  102cb7:	90                   	nop
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
  102cb8:	8b 83 40 02 00 00    	mov    0x240(%ebx),%eax
  102cbe:	85 c0                	test   %eax,%eax
  102cc0:	74 2b                	je     102ced <piperead+0x7d>
    if(proc->killed){
  102cc2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  102cc8:	8b 50 24             	mov    0x24(%eax),%edx
  102ccb:	85 d2                	test   %edx,%edx
  102ccd:	0f 85 7d 00 00 00    	jne    102d50 <piperead+0xe0>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  102cd3:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  102cd7:	89 34 24             	mov    %esi,(%esp)
  102cda:	e8 81 05 00 00       	call   103260 <sleep>
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
  102cdf:	8b 93 34 02 00 00    	mov    0x234(%ebx),%edx
  102ce5:	3b 93 38 02 00 00    	cmp    0x238(%ebx),%edx
  102ceb:	74 cb                	je     102cb8 <piperead+0x48>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
  102ced:	85 ff                	test   %edi,%edi
  102cef:	7e 76                	jle    102d67 <piperead+0xf7>
    if(p->nread == p->nwrite)
  102cf1:	31 f6                	xor    %esi,%esi
  102cf3:	3b 93 38 02 00 00    	cmp    0x238(%ebx),%edx
  102cf9:	75 0d                	jne    102d08 <piperead+0x98>
  102cfb:	eb 6a                	jmp    102d67 <piperead+0xf7>
  102cfd:	8d 76 00             	lea    0x0(%esi),%esi
  102d00:	39 93 38 02 00 00    	cmp    %edx,0x238(%ebx)
  102d06:	74 22                	je     102d2a <piperead+0xba>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
  102d08:	89 d0                	mov    %edx,%eax
  102d0a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  102d0d:	83 c2 01             	add    $0x1,%edx
  102d10:	25 ff 01 00 00       	and    $0x1ff,%eax
  102d15:	0f b6 44 03 34       	movzbl 0x34(%ebx,%eax,1),%eax
  102d1a:	88 04 31             	mov    %al,(%ecx,%esi,1)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
  102d1d:	83 c6 01             	add    $0x1,%esi
  102d20:	39 f7                	cmp    %esi,%edi
    if(p->nread == p->nwrite)
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
  102d22:	89 93 34 02 00 00    	mov    %edx,0x234(%ebx)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
  102d28:	7f d6                	jg     102d00 <piperead+0x90>
    if(p->nread == p->nwrite)
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
  102d2a:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
  102d30:	89 04 24             	mov    %eax,(%esp)
  102d33:	e8 08 04 00 00       	call   103140 <wakeup>
  release(&p->lock);
  102d38:	89 1c 24             	mov    %ebx,(%esp)
  102d3b:	e8 a0 0d 00 00       	call   103ae0 <release>
  return i;
}
  102d40:	83 c4 1c             	add    $0x1c,%esp
  102d43:	89 f0                	mov    %esi,%eax
  102d45:	5b                   	pop    %ebx
  102d46:	5e                   	pop    %esi
  102d47:	5f                   	pop    %edi
  102d48:	5d                   	pop    %ebp
  102d49:	c3                   	ret    
  102d4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
    if(proc->killed){
      release(&p->lock);
  102d50:	be ff ff ff ff       	mov    $0xffffffff,%esi
  102d55:	89 1c 24             	mov    %ebx,(%esp)
  102d58:	e8 83 0d 00 00       	call   103ae0 <release>
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
  release(&p->lock);
  return i;
}
  102d5d:	83 c4 1c             	add    $0x1c,%esp
  102d60:	89 f0                	mov    %esi,%eax
  102d62:	5b                   	pop    %ebx
  102d63:	5e                   	pop    %esi
  102d64:	5f                   	pop    %edi
  102d65:	5d                   	pop    %ebp
  102d66:	c3                   	ret    
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
  102d67:	31 f6                	xor    %esi,%esi
  102d69:	eb bf                	jmp    102d2a <piperead+0xba>
  102d6b:	90                   	nop
  102d6c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00102d70 <pipewrite>:
    release(&p->lock);
}

int
pipewrite(struct pipe *p, char *addr, int n)
{
  102d70:	55                   	push   %ebp
  102d71:	89 e5                	mov    %esp,%ebp
  102d73:	57                   	push   %edi
  102d74:	56                   	push   %esi
  102d75:	53                   	push   %ebx
  102d76:	83 ec 3c             	sub    $0x3c,%esp
  102d79:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
  102d7c:	89 1c 24             	mov    %ebx,(%esp)
  102d7f:	8d b3 34 02 00 00    	lea    0x234(%ebx),%esi
  102d85:	e8 a6 0d 00 00       	call   103b30 <acquire>
  for(i = 0; i < n; i++){
  102d8a:	8b 4d 10             	mov    0x10(%ebp),%ecx
  102d8d:	85 c9                	test   %ecx,%ecx
  102d8f:	0f 8e 8d 00 00 00    	jle    102e22 <pipewrite+0xb2>
  102d95:	8b 83 38 02 00 00    	mov    0x238(%ebx),%eax
      if(p->readopen == 0 || proc->killed){
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
  102d9b:	8d bb 38 02 00 00    	lea    0x238(%ebx),%edi
  102da1:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  102da8:	eb 37                	jmp    102de1 <pipewrite+0x71>
  102daa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
  102db0:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
  102db6:	85 c0                	test   %eax,%eax
  102db8:	74 7e                	je     102e38 <pipewrite+0xc8>
  102dba:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  102dc0:	8b 50 24             	mov    0x24(%eax),%edx
  102dc3:	85 d2                	test   %edx,%edx
  102dc5:	75 71                	jne    102e38 <pipewrite+0xc8>
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
  102dc7:	89 34 24             	mov    %esi,(%esp)
  102dca:	e8 71 03 00 00       	call   103140 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
  102dcf:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  102dd3:	89 3c 24             	mov    %edi,(%esp)
  102dd6:	e8 85 04 00 00       	call   103260 <sleep>
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
  102ddb:	8b 83 38 02 00 00    	mov    0x238(%ebx),%eax
  102de1:	8b 93 34 02 00 00    	mov    0x234(%ebx),%edx
  102de7:	81 c2 00 02 00 00    	add    $0x200,%edx
  102ded:	39 d0                	cmp    %edx,%eax
  102def:	74 bf                	je     102db0 <pipewrite+0x40>
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  102df1:	89 c2                	mov    %eax,%edx
  102df3:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  102df6:	83 c0 01             	add    $0x1,%eax
  102df9:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
  102dff:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  102e02:	8b 55 0c             	mov    0xc(%ebp),%edx
  102e05:	0f b6 0c 0a          	movzbl (%edx,%ecx,1),%ecx
  102e09:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  102e0c:	88 4c 13 34          	mov    %cl,0x34(%ebx,%edx,1)
  102e10:	89 83 38 02 00 00    	mov    %eax,0x238(%ebx)
pipewrite(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
  102e16:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
  102e1a:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  102e1d:	39 4d 10             	cmp    %ecx,0x10(%ebp)
  102e20:	7f bf                	jg     102de1 <pipewrite+0x71>
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  102e22:	89 34 24             	mov    %esi,(%esp)
  102e25:	e8 16 03 00 00       	call   103140 <wakeup>
  release(&p->lock);
  102e2a:	89 1c 24             	mov    %ebx,(%esp)
  102e2d:	e8 ae 0c 00 00       	call   103ae0 <release>
  return n;
  102e32:	eb 13                	jmp    102e47 <pipewrite+0xd7>
  102e34:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
        release(&p->lock);
  102e38:	89 1c 24             	mov    %ebx,(%esp)
  102e3b:	e8 a0 0c 00 00       	call   103ae0 <release>
  102e40:	c7 45 10 ff ff ff ff 	movl   $0xffffffff,0x10(%ebp)
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
  102e47:	8b 45 10             	mov    0x10(%ebp),%eax
  102e4a:	83 c4 3c             	add    $0x3c,%esp
  102e4d:	5b                   	pop    %ebx
  102e4e:	5e                   	pop    %esi
  102e4f:	5f                   	pop    %edi
  102e50:	5d                   	pop    %ebp
  102e51:	c3                   	ret    
  102e52:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  102e59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00102e60 <pipeclose>:
  return -1;
}

void
pipeclose(struct pipe *p, int writable)
{
  102e60:	55                   	push   %ebp
  102e61:	89 e5                	mov    %esp,%ebp
  102e63:	83 ec 18             	sub    $0x18,%esp
  102e66:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  102e69:	8b 5d 08             	mov    0x8(%ebp),%ebx
  102e6c:	89 75 fc             	mov    %esi,-0x4(%ebp)
  102e6f:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&p->lock);
  102e72:	89 1c 24             	mov    %ebx,(%esp)
  102e75:	e8 b6 0c 00 00       	call   103b30 <acquire>
  if(writable){
  102e7a:	85 f6                	test   %esi,%esi
  102e7c:	74 42                	je     102ec0 <pipeclose+0x60>
    p->writeopen = 0;
  102e7e:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
  102e85:	00 00 00 
    wakeup(&p->nread);
  102e88:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
  102e8e:	89 04 24             	mov    %eax,(%esp)
  102e91:	e8 aa 02 00 00       	call   103140 <wakeup>
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
  102e96:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
  102e9c:	85 c0                	test   %eax,%eax
  102e9e:	75 0a                	jne    102eaa <pipeclose+0x4a>
  102ea0:	8b b3 40 02 00 00    	mov    0x240(%ebx),%esi
  102ea6:	85 f6                	test   %esi,%esi
  102ea8:	74 36                	je     102ee0 <pipeclose+0x80>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
  102eaa:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
  102ead:	8b 75 fc             	mov    -0x4(%ebp),%esi
  102eb0:	8b 5d f8             	mov    -0x8(%ebp),%ebx
  102eb3:	89 ec                	mov    %ebp,%esp
  102eb5:	5d                   	pop    %ebp
  }
  if(p->readopen == 0 && p->writeopen == 0){
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
  102eb6:	e9 25 0c 00 00       	jmp    103ae0 <release>
  102ebb:	90                   	nop
  102ebc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  acquire(&p->lock);
  if(writable){
    p->writeopen = 0;
    wakeup(&p->nread);
  } else {
    p->readopen = 0;
  102ec0:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
  102ec7:	00 00 00 
    wakeup(&p->nwrite);
  102eca:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
  102ed0:	89 04 24             	mov    %eax,(%esp)
  102ed3:	e8 68 02 00 00       	call   103140 <wakeup>
  102ed8:	eb bc                	jmp    102e96 <pipeclose+0x36>
  102eda:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  }
  if(p->readopen == 0 && p->writeopen == 0){
    release(&p->lock);
  102ee0:	89 1c 24             	mov    %ebx,(%esp)
  102ee3:	e8 f8 0b 00 00       	call   103ae0 <release>
    kfree((char*)p);
  } else
    release(&p->lock);
}
  102ee8:	8b 75 fc             	mov    -0x4(%ebp),%esi
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
    release(&p->lock);
    kfree((char*)p);
  102eeb:	89 5d 08             	mov    %ebx,0x8(%ebp)
  } else
    release(&p->lock);
}
  102eee:	8b 5d f8             	mov    -0x8(%ebp),%ebx
  102ef1:	89 ec                	mov    %ebp,%esp
  102ef3:	5d                   	pop    %ebp
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
    release(&p->lock);
    kfree((char*)p);
  102ef4:	e9 a7 f3 ff ff       	jmp    1022a0 <kfree>
  102ef9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00102f00 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
  102f00:	55                   	push   %ebp
  102f01:	89 e5                	mov    %esp,%ebp
  102f03:	57                   	push   %edi
  102f04:	56                   	push   %esi
  102f05:	53                   	push   %ebx
  102f06:	83 ec 1c             	sub    $0x1c,%esp
  102f09:	8b 75 08             	mov    0x8(%ebp),%esi
  102f0c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
  102f0f:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  102f15:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
  102f1b:	e8 d0 df ff ff       	call   100ef0 <filealloc>
  102f20:	85 c0                	test   %eax,%eax
  102f22:	89 06                	mov    %eax,(%esi)
  102f24:	0f 84 9c 00 00 00    	je     102fc6 <pipealloc+0xc6>
  102f2a:	e8 c1 df ff ff       	call   100ef0 <filealloc>
  102f2f:	85 c0                	test   %eax,%eax
  102f31:	89 03                	mov    %eax,(%ebx)
  102f33:	0f 84 7f 00 00 00    	je     102fb8 <pipealloc+0xb8>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
  102f39:	e8 22 f3 ff ff       	call   102260 <kalloc>
  102f3e:	85 c0                	test   %eax,%eax
  102f40:	89 c7                	mov    %eax,%edi
  102f42:	74 74                	je     102fb8 <pipealloc+0xb8>
    goto bad;
  p->readopen = 1;
  102f44:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
  102f4b:	00 00 00 
  p->writeopen = 1;
  102f4e:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
  102f55:	00 00 00 
  p->nwrite = 0;
  102f58:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
  102f5f:	00 00 00 
  p->nread = 0;
  102f62:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
  102f69:	00 00 00 
  initlock(&p->lock, "pipe");
  102f6c:	89 04 24             	mov    %eax,(%esp)
  102f6f:	c7 44 24 04 4c 69 10 	movl   $0x10694c,0x4(%esp)
  102f76:	00 
  102f77:	e8 24 0a 00 00       	call   1039a0 <initlock>
  (*f0)->type = FD_PIPE;
  102f7c:	8b 06                	mov    (%esi),%eax
  102f7e:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
  102f84:	8b 06                	mov    (%esi),%eax
  102f86:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
  102f8a:	8b 06                	mov    (%esi),%eax
  102f8c:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
  102f90:	8b 06                	mov    (%esi),%eax
  102f92:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
  102f95:	8b 03                	mov    (%ebx),%eax
  102f97:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
  102f9d:	8b 03                	mov    (%ebx),%eax
  102f9f:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
  102fa3:	8b 03                	mov    (%ebx),%eax
  102fa5:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
  102fa9:	8b 03                	mov    (%ebx),%eax
  102fab:	89 78 0c             	mov    %edi,0xc(%eax)
  102fae:	31 c0                	xor    %eax,%eax
  if(*f0)
    fileclose(*f0);
  if(*f1)
    fileclose(*f1);
  return -1;
}
  102fb0:	83 c4 1c             	add    $0x1c,%esp
  102fb3:	5b                   	pop    %ebx
  102fb4:	5e                   	pop    %esi
  102fb5:	5f                   	pop    %edi
  102fb6:	5d                   	pop    %ebp
  102fb7:	c3                   	ret    
  return 0;

 bad:
  if(p)
    kfree((char*)p);
  if(*f0)
  102fb8:	8b 06                	mov    (%esi),%eax
  102fba:	85 c0                	test   %eax,%eax
  102fbc:	74 08                	je     102fc6 <pipealloc+0xc6>
    fileclose(*f0);
  102fbe:	89 04 24             	mov    %eax,(%esp)
  102fc1:	e8 aa df ff ff       	call   100f70 <fileclose>
  if(*f1)
  102fc6:	8b 13                	mov    (%ebx),%edx
  102fc8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  102fcd:	85 d2                	test   %edx,%edx
  102fcf:	74 df                	je     102fb0 <pipealloc+0xb0>
    fileclose(*f1);
  102fd1:	89 14 24             	mov    %edx,(%esp)
  102fd4:	e8 97 df ff ff       	call   100f70 <fileclose>
  102fd9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  102fde:	eb d0                	jmp    102fb0 <pipealloc+0xb0>

00102fe0 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  102fe0:	55                   	push   %ebp
  102fe1:	89 e5                	mov    %esp,%ebp
  102fe3:	57                   	push   %edi
  102fe4:	56                   	push   %esi
  102fe5:	53                   	push   %ebx

// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
  102fe6:	bb 54 c1 10 00       	mov    $0x10c154,%ebx
{
  102feb:	83 ec 4c             	sub    $0x4c,%esp
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
  102fee:	8d 7d c0             	lea    -0x40(%ebp),%edi
  102ff1:	eb 4b                	jmp    10303e <procdump+0x5e>
  102ff3:	90                   	nop
  102ff4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
  102ff8:	8b 04 85 24 6a 10 00 	mov    0x106a24(,%eax,4),%eax
  102fff:	85 c0                	test   %eax,%eax
  103001:	74 47                	je     10304a <procdump+0x6a>
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
  103003:	8b 53 10             	mov    0x10(%ebx),%edx
  103006:	8d 4b 6c             	lea    0x6c(%ebx),%ecx
  103009:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  10300d:	89 44 24 08          	mov    %eax,0x8(%esp)
  103011:	c7 04 24 55 69 10 00 	movl   $0x106955,(%esp)
  103018:	89 54 24 04          	mov    %edx,0x4(%esp)
  10301c:	e8 0f d5 ff ff       	call   100530 <cprintf>
    if(p->state == SLEEPING){
  103021:	83 7b 0c 02          	cmpl   $0x2,0xc(%ebx)
  103025:	74 31                	je     103058 <procdump+0x78>
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  103027:	c7 04 24 d6 68 10 00 	movl   $0x1068d6,(%esp)
  10302e:	e8 fd d4 ff ff       	call   100530 <cprintf>
  int i;
  struct proc *p;
  char *state;
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  103033:	83 c3 7c             	add    $0x7c,%ebx
  103036:	81 fb 54 e0 10 00    	cmp    $0x10e054,%ebx
  10303c:	74 5a                	je     103098 <procdump+0xb8>
    if(p->state == UNUSED)
  10303e:	8b 43 0c             	mov    0xc(%ebx),%eax
  103041:	85 c0                	test   %eax,%eax
  103043:	74 ee                	je     103033 <procdump+0x53>
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
  103045:	83 f8 05             	cmp    $0x5,%eax
  103048:	76 ae                	jbe    102ff8 <procdump+0x18>
  10304a:	b8 51 69 10 00       	mov    $0x106951,%eax
  10304f:	eb b2                	jmp    103003 <procdump+0x23>
  103051:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
  103058:	8b 43 1c             	mov    0x1c(%ebx),%eax
  10305b:	31 f6                	xor    %esi,%esi
  10305d:	89 7c 24 04          	mov    %edi,0x4(%esp)
  103061:	8b 40 0c             	mov    0xc(%eax),%eax
  103064:	83 c0 08             	add    $0x8,%eax
  103067:	89 04 24             	mov    %eax,(%esp)
  10306a:	e8 51 09 00 00       	call   1039c0 <getcallerpcs>
  10306f:	90                   	nop
      for(i=0; i<10 && pc[i] != 0; i++)
  103070:	8b 04 b7             	mov    (%edi,%esi,4),%eax
  103073:	85 c0                	test   %eax,%eax
  103075:	74 b0                	je     103027 <procdump+0x47>
  103077:	83 c6 01             	add    $0x1,%esi
        cprintf(" %p", pc[i]);
  10307a:	89 44 24 04          	mov    %eax,0x4(%esp)
  10307e:	c7 04 24 ca 64 10 00 	movl   $0x1064ca,(%esp)
  103085:	e8 a6 d4 ff ff       	call   100530 <cprintf>
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
  10308a:	83 fe 0a             	cmp    $0xa,%esi
  10308d:	75 e1                	jne    103070 <procdump+0x90>
  10308f:	eb 96                	jmp    103027 <procdump+0x47>
  103091:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}
  103098:	83 c4 4c             	add    $0x4c,%esp
  10309b:	5b                   	pop    %ebx
  10309c:	5e                   	pop    %esi
  10309d:	5f                   	pop    %edi
  10309e:	5d                   	pop    %ebp
  10309f:	90                   	nop
  1030a0:	c3                   	ret    
  1030a1:	eb 0d                	jmp    1030b0 <kill>
  1030a3:	90                   	nop
  1030a4:	90                   	nop
  1030a5:	90                   	nop
  1030a6:	90                   	nop
  1030a7:	90                   	nop
  1030a8:	90                   	nop
  1030a9:	90                   	nop
  1030aa:	90                   	nop
  1030ab:	90                   	nop
  1030ac:	90                   	nop
  1030ad:	90                   	nop
  1030ae:	90                   	nop
  1030af:	90                   	nop

001030b0 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
  1030b0:	55                   	push   %ebp
  1030b1:	89 e5                	mov    %esp,%ebp
  1030b3:	53                   	push   %ebx
  1030b4:	83 ec 14             	sub    $0x14,%esp
  1030b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
  1030ba:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1030c1:	e8 6a 0a 00 00       	call   103b30 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
  1030c6:	8b 15 64 c1 10 00    	mov    0x10c164,%edx

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
  1030cc:	b8 d0 c1 10 00       	mov    $0x10c1d0,%eax
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
  1030d1:	39 da                	cmp    %ebx,%edx
  1030d3:	75 0d                	jne    1030e2 <kill+0x32>
  1030d5:	eb 60                	jmp    103137 <kill+0x87>
  1030d7:	90                   	nop
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  1030d8:	83 c0 7c             	add    $0x7c,%eax
  1030db:	3d 54 e0 10 00       	cmp    $0x10e054,%eax
  1030e0:	74 3e                	je     103120 <kill+0x70>
    if(p->pid == pid){
  1030e2:	8b 50 10             	mov    0x10(%eax),%edx
  1030e5:	39 da                	cmp    %ebx,%edx
  1030e7:	75 ef                	jne    1030d8 <kill+0x28>
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
  1030e9:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
  1030ed:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
  1030f4:	74 1a                	je     103110 <kill+0x60>
        p->state = RUNNABLE;
      release(&ptable.lock);
  1030f6:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1030fd:	e8 de 09 00 00       	call   103ae0 <release>
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
  103102:	83 c4 14             	add    $0x14,%esp
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
  103105:	31 c0                	xor    %eax,%eax
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
  103107:	5b                   	pop    %ebx
  103108:	5d                   	pop    %ebp
  103109:	c3                   	ret    
  10310a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
  103110:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  103117:	eb dd                	jmp    1030f6 <kill+0x46>
  103119:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  103120:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103127:	e8 b4 09 00 00       	call   103ae0 <release>
  return -1;
}
  10312c:	83 c4 14             	add    $0x14,%esp
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  10312f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return -1;
}
  103134:	5b                   	pop    %ebx
  103135:	5d                   	pop    %ebp
  103136:	c3                   	ret    
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
  103137:	b8 54 c1 10 00       	mov    $0x10c154,%eax
  10313c:	eb ab                	jmp    1030e9 <kill+0x39>
  10313e:	66 90                	xchg   %ax,%ax

00103140 <wakeup>:
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  103140:	55                   	push   %ebp
  103141:	89 e5                	mov    %esp,%ebp
  103143:	53                   	push   %ebx
  103144:	83 ec 14             	sub    $0x14,%esp
  103147:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ptable.lock);
  10314a:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103151:	e8 da 09 00 00       	call   103b30 <acquire>
      p->state = RUNNABLE;
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
  103156:	b8 54 c1 10 00       	mov    $0x10c154,%eax
  10315b:	eb 0d                	jmp    10316a <wakeup+0x2a>
  10315d:	8d 76 00             	lea    0x0(%esi),%esi
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  103160:	83 c0 7c             	add    $0x7c,%eax
  103163:	3d 54 e0 10 00       	cmp    $0x10e054,%eax
  103168:	74 1e                	je     103188 <wakeup+0x48>
    if(p->state == SLEEPING && p->chan == chan)
  10316a:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
  10316e:	75 f0                	jne    103160 <wakeup+0x20>
  103170:	3b 58 20             	cmp    0x20(%eax),%ebx
  103173:	75 eb                	jne    103160 <wakeup+0x20>
      p->state = RUNNABLE;
  103175:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  10317c:	83 c0 7c             	add    $0x7c,%eax
  10317f:	3d 54 e0 10 00       	cmp    $0x10e054,%eax
  103184:	75 e4                	jne    10316a <wakeup+0x2a>
  103186:	66 90                	xchg   %ax,%ax
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
  103188:	c7 45 08 20 c1 10 00 	movl   $0x10c120,0x8(%ebp)
}
  10318f:	83 c4 14             	add    $0x14,%esp
  103192:	5b                   	pop    %ebx
  103193:	5d                   	pop    %ebp
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
  103194:	e9 47 09 00 00       	jmp    103ae0 <release>
  103199:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

001031a0 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  1031a0:	55                   	push   %ebp
  1031a1:	89 e5                	mov    %esp,%ebp
  1031a3:	83 ec 18             	sub    $0x18,%esp
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
  1031a6:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1031ad:	e8 2e 09 00 00       	call   103ae0 <release>
  
  // Return to "caller", actually trapret (see allocproc).
}
  1031b2:	c9                   	leave  
  1031b3:	c3                   	ret    
  1031b4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  1031ba:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

001031c0 <sched>:

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
  1031c0:	55                   	push   %ebp
  1031c1:	89 e5                	mov    %esp,%ebp
  1031c3:	53                   	push   %ebx
  1031c4:	83 ec 14             	sub    $0x14,%esp
  int intena;

  if(!holding(&ptable.lock))
  1031c7:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1031ce:	e8 4d 08 00 00       	call   103a20 <holding>
  1031d3:	85 c0                	test   %eax,%eax
  1031d5:	74 4d                	je     103224 <sched+0x64>
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
  1031d7:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  1031dd:	83 b8 ac 00 00 00 01 	cmpl   $0x1,0xac(%eax)
  1031e4:	75 62                	jne    103248 <sched+0x88>
    panic("sched locks");
  if(proc->state == RUNNING)
  1031e6:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  1031ed:	83 7a 0c 04          	cmpl   $0x4,0xc(%edx)
  1031f1:	74 49                	je     10323c <sched+0x7c>

static inline uint
readeflags(void)
{
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
  1031f3:	9c                   	pushf  
  1031f4:	59                   	pop    %ecx
    panic("sched running");
  if(readeflags()&FL_IF)
  1031f5:	80 e5 02             	and    $0x2,%ch
  1031f8:	75 36                	jne    103230 <sched+0x70>
    panic("sched interruptible");
  intena = cpu->intena;
  1031fa:	8b 98 b0 00 00 00    	mov    0xb0(%eax),%ebx
  swtch(&proc->context, cpu->scheduler);
  103200:	83 c2 1c             	add    $0x1c,%edx
  103203:	8b 40 04             	mov    0x4(%eax),%eax
  103206:	89 14 24             	mov    %edx,(%esp)
  103209:	89 44 24 04          	mov    %eax,0x4(%esp)
  10320d:	e8 ba 0b 00 00       	call   103dcc <swtch>
  cpu->intena = intena;
  103212:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  103218:	89 98 b0 00 00 00    	mov    %ebx,0xb0(%eax)
}
  10321e:	83 c4 14             	add    $0x14,%esp
  103221:	5b                   	pop    %ebx
  103222:	5d                   	pop    %ebp
  103223:	c3                   	ret    
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  103224:	c7 04 24 5e 69 10 00 	movl   $0x10695e,(%esp)
  10322b:	e8 e0 d6 ff ff       	call   100910 <panic>
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  103230:	c7 04 24 8a 69 10 00 	movl   $0x10698a,(%esp)
  103237:	e8 d4 d6 ff ff       	call   100910 <panic>
  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  10323c:	c7 04 24 7c 69 10 00 	movl   $0x10697c,(%esp)
  103243:	e8 c8 d6 ff ff       	call   100910 <panic>
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  103248:	c7 04 24 70 69 10 00 	movl   $0x106970,(%esp)
  10324f:	e8 bc d6 ff ff       	call   100910 <panic>
  103254:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  10325a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00103260 <sleep>:

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  103260:	55                   	push   %ebp
  103261:	89 e5                	mov    %esp,%ebp
  103263:	56                   	push   %esi
  103264:	53                   	push   %ebx
  103265:	83 ec 10             	sub    $0x10,%esp
  if(proc == 0)
  103268:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  10326e:	8b 75 08             	mov    0x8(%ebp),%esi
  103271:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  if(proc == 0)
  103274:	85 c0                	test   %eax,%eax
  103276:	0f 84 a1 00 00 00    	je     10331d <sleep+0xbd>
    panic("sleep");

  if(lk == 0)
  10327c:	85 db                	test   %ebx,%ebx
  10327e:	0f 84 8d 00 00 00    	je     103311 <sleep+0xb1>
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
  103284:	81 fb 20 c1 10 00    	cmp    $0x10c120,%ebx
  10328a:	74 5c                	je     1032e8 <sleep+0x88>
    acquire(&ptable.lock);  //DOC: sleeplock1
  10328c:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103293:	e8 98 08 00 00       	call   103b30 <acquire>
    release(lk);
  103298:	89 1c 24             	mov    %ebx,(%esp)
  10329b:	e8 40 08 00 00       	call   103ae0 <release>
  }

  // Go to sleep.
  proc->chan = chan;
  1032a0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  1032a6:	89 70 20             	mov    %esi,0x20(%eax)
  proc->state = SLEEPING;
  1032a9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  1032af:	c7 40 0c 02 00 00 00 	movl   $0x2,0xc(%eax)
  sched();
  1032b6:	e8 05 ff ff ff       	call   1031c0 <sched>

  // Tidy up.
  proc->chan = 0;
  1032bb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  1032c1:	c7 40 20 00 00 00 00 	movl   $0x0,0x20(%eax)

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
  1032c8:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1032cf:	e8 0c 08 00 00       	call   103ae0 <release>
    acquire(lk);
  1032d4:	89 5d 08             	mov    %ebx,0x8(%ebp)
  }
}
  1032d7:	83 c4 10             	add    $0x10,%esp
  1032da:	5b                   	pop    %ebx
  1032db:	5e                   	pop    %esi
  1032dc:	5d                   	pop    %ebp
  proc->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
    acquire(lk);
  1032dd:	e9 4e 08 00 00       	jmp    103b30 <acquire>
  1032e2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    acquire(&ptable.lock);  //DOC: sleeplock1
    release(lk);
  }

  // Go to sleep.
  proc->chan = chan;
  1032e8:	89 70 20             	mov    %esi,0x20(%eax)
  proc->state = SLEEPING;
  1032eb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  1032f1:	c7 40 0c 02 00 00 00 	movl   $0x2,0xc(%eax)
  sched();
  1032f8:	e8 c3 fe ff ff       	call   1031c0 <sched>

  // Tidy up.
  proc->chan = 0;
  1032fd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103303:	c7 40 20 00 00 00 00 	movl   $0x0,0x20(%eax)
  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
    acquire(lk);
  }
}
  10330a:	83 c4 10             	add    $0x10,%esp
  10330d:	5b                   	pop    %ebx
  10330e:	5e                   	pop    %esi
  10330f:	5d                   	pop    %ebp
  103310:	c3                   	ret    
{
  if(proc == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");
  103311:	c7 04 24 a4 69 10 00 	movl   $0x1069a4,(%esp)
  103318:	e8 f3 d5 ff ff       	call   100910 <panic>
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");
  10331d:	c7 04 24 9e 69 10 00 	movl   $0x10699e,(%esp)
  103324:	e8 e7 d5 ff ff       	call   100910 <panic>
  103329:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00103330 <yield>:
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
  103330:	55                   	push   %ebp
  103331:	89 e5                	mov    %esp,%ebp
  103333:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
  103336:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  10333d:	e8 ee 07 00 00       	call   103b30 <acquire>
  proc->state = RUNNABLE;
  103342:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103348:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  sched();
  10334f:	e8 6c fe ff ff       	call   1031c0 <sched>
  release(&ptable.lock);
  103354:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  10335b:	e8 80 07 00 00       	call   103ae0 <release>
}
  103360:	c9                   	leave  
  103361:	c3                   	ret    
  103362:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  103369:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103370 <scheduler>:
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
  103370:	55                   	push   %ebp
  103371:	89 e5                	mov    %esp,%ebp
  103373:	53                   	push   %ebx
  103374:	83 ec 14             	sub    $0x14,%esp
  103377:	90                   	nop
}

static inline void
sti(void)
{
  asm volatile("sti");
  103378:	fb                   	sti    
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
  103379:	bb 54 c1 10 00       	mov    $0x10c154,%ebx
  for(;;){
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
  10337e:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103385:	e8 a6 07 00 00       	call   103b30 <acquire>
  10338a:	eb 0f                	jmp    10339b <scheduler+0x2b>
  10338c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  103390:	83 c3 7c             	add    $0x7c,%ebx
  103393:	81 fb 54 e0 10 00    	cmp    $0x10e054,%ebx
  103399:	74 55                	je     1033f0 <scheduler+0x80>
      if(p->state != RUNNABLE)
  10339b:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
  10339f:	75 ef                	jne    103390 <scheduler+0x20>
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      proc = p;
  1033a1:	65 89 1d 04 00 00 00 	mov    %ebx,%gs:0x4
      switchuvm(p);
  1033a8:	89 1c 24             	mov    %ebx,(%esp)
  1033ab:	e8 f0 2e 00 00       	call   1062a0 <switchuvm>
      p->state = RUNNING;
      swtch(&cpu->scheduler, proc->context);
  1033b0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
  1033b6:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  1033bd:	83 c3 7c             	add    $0x7c,%ebx
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
      swtch(&cpu->scheduler, proc->context);
  1033c0:	8b 40 1c             	mov    0x1c(%eax),%eax
  1033c3:	89 44 24 04          	mov    %eax,0x4(%esp)
  1033c7:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  1033cd:	83 c0 04             	add    $0x4,%eax
  1033d0:	89 04 24             	mov    %eax,(%esp)
  1033d3:	e8 f4 09 00 00       	call   103dcc <swtch>
      switchkvm();
  1033d8:	e8 53 28 00 00       	call   105c30 <switchkvm>
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  1033dd:	81 fb 54 e0 10 00    	cmp    $0x10e054,%ebx
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
  1033e3:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
  1033ea:	00 00 00 00 
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  1033ee:	75 ab                	jne    10339b <scheduler+0x2b>

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
  1033f0:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1033f7:	e8 e4 06 00 00       	call   103ae0 <release>

  }
  1033fc:	e9 77 ff ff ff       	jmp    103378 <scheduler+0x8>
  103401:	eb 0d                	jmp    103410 <wait>
  103403:	90                   	nop
  103404:	90                   	nop
  103405:	90                   	nop
  103406:	90                   	nop
  103407:	90                   	nop
  103408:	90                   	nop
  103409:	90                   	nop
  10340a:	90                   	nop
  10340b:	90                   	nop
  10340c:	90                   	nop
  10340d:	90                   	nop
  10340e:	90                   	nop
  10340f:	90                   	nop

00103410 <wait>:

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  103410:	55                   	push   %ebp
  103411:	89 e5                	mov    %esp,%ebp
  103413:	53                   	push   %ebx
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  103414:	bb 54 c1 10 00       	mov    $0x10c154,%ebx

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  103419:	83 ec 24             	sub    $0x24,%esp
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  10341c:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103423:	e8 08 07 00 00       	call   103b30 <acquire>
  103428:	31 c0                	xor    %eax,%eax
  10342a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  103430:	81 fb 54 e0 10 00    	cmp    $0x10e054,%ebx
  103436:	72 30                	jb     103468 <wait+0x58>
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
  103438:	85 c0                	test   %eax,%eax
  10343a:	74 54                	je     103490 <wait+0x80>
  10343c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103442:	8b 50 24             	mov    0x24(%eax),%edx
  103445:	85 d2                	test   %edx,%edx
  103447:	75 47                	jne    103490 <wait+0x80>
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  103449:	bb 54 c1 10 00       	mov    $0x10c154,%ebx
  10344e:	89 04 24             	mov    %eax,(%esp)
  103451:	c7 44 24 04 20 c1 10 	movl   $0x10c120,0x4(%esp)
  103458:	00 
  103459:	e8 02 fe ff ff       	call   103260 <sleep>
  10345e:	31 c0                	xor    %eax,%eax

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  103460:	81 fb 54 e0 10 00    	cmp    $0x10e054,%ebx
  103466:	73 d0                	jae    103438 <wait+0x28>
      if(p->parent != proc)
  103468:	8b 53 14             	mov    0x14(%ebx),%edx
  10346b:	65 3b 15 04 00 00 00 	cmp    %gs:0x4,%edx
  103472:	74 0c                	je     103480 <wait+0x70>

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  103474:	83 c3 7c             	add    $0x7c,%ebx
  103477:	eb b7                	jmp    103430 <wait+0x20>
  103479:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
  103480:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
  103484:	74 21                	je     1034a7 <wait+0x97>
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
  103486:	b8 01 00 00 00       	mov    $0x1,%eax
  10348b:	eb e7                	jmp    103474 <wait+0x64>
  10348d:	8d 76 00             	lea    0x0(%esi),%esi
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
  103490:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103497:	e8 44 06 00 00       	call   103ae0 <release>
  10349c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
  1034a1:	83 c4 24             	add    $0x24,%esp
  1034a4:	5b                   	pop    %ebx
  1034a5:	5d                   	pop    %ebp
  1034a6:	c3                   	ret    
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
  1034a7:	8b 43 10             	mov    0x10(%ebx),%eax
        kfree(p->kstack);
  1034aa:	8b 53 08             	mov    0x8(%ebx),%edx
  1034ad:	89 45 f4             	mov    %eax,-0xc(%ebp)
  1034b0:	89 14 24             	mov    %edx,(%esp)
  1034b3:	e8 e8 ed ff ff       	call   1022a0 <kfree>
        p->kstack = 0;
        freevm(p->pgdir);
  1034b8:	8b 53 04             	mov    0x4(%ebx),%edx
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
  1034bb:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
  1034c2:	89 14 24             	mov    %edx,(%esp)
  1034c5:	e8 06 2b 00 00       	call   105fd0 <freevm>
        p->state = UNUSED;
  1034ca:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        p->pid = 0;
  1034d1:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
  1034d8:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->name[0] = 0;
  1034df:	c6 43 6c 00          	movb   $0x0,0x6c(%ebx)
        p->killed = 0;
  1034e3:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
        release(&ptable.lock);
  1034ea:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1034f1:	e8 ea 05 00 00       	call   103ae0 <release>
        return pid;
  1034f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1034f9:	eb a6                	jmp    1034a1 <wait+0x91>
  1034fb:	90                   	nop
  1034fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00103500 <exit>:
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  103500:	55                   	push   %ebp
  103501:	89 e5                	mov    %esp,%ebp
  103503:	56                   	push   %esi
  103504:	53                   	push   %ebx
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");
  103505:	31 db                	xor    %ebx,%ebx
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  103507:	83 ec 10             	sub    $0x10,%esp
  struct proc *p;
  int fd;

  if(proc == initproc)
  10350a:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  103511:	3b 15 c8 78 10 00    	cmp    0x1078c8,%edx
  103517:	0f 84 fe 00 00 00    	je     10361b <exit+0x11b>
  10351d:	8d 76 00             	lea    0x0(%esi),%esi
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
  103520:	8d 73 08             	lea    0x8(%ebx),%esi
  103523:	8b 44 b2 08          	mov    0x8(%edx,%esi,4),%eax
  103527:	85 c0                	test   %eax,%eax
  103529:	74 1d                	je     103548 <exit+0x48>
      fileclose(proc->ofile[fd]);
  10352b:	89 04 24             	mov    %eax,(%esp)
  10352e:	e8 3d da ff ff       	call   100f70 <fileclose>
      proc->ofile[fd] = 0;
  103533:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103539:	c7 44 b0 08 00 00 00 	movl   $0x0,0x8(%eax,%esi,4)
  103540:	00 
  103541:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
  103548:	83 c3 01             	add    $0x1,%ebx
  10354b:	83 fb 10             	cmp    $0x10,%ebx
  10354e:	75 d0                	jne    103520 <exit+0x20>
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  iput(proc->cwd);
  103550:	8b 42 68             	mov    0x68(%edx),%eax
  103553:	89 04 24             	mov    %eax,(%esp)
  103556:	e8 35 e3 ff ff       	call   101890 <iput>
  proc->cwd = 0;
  10355b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103561:	c7 40 68 00 00 00 00 	movl   $0x0,0x68(%eax)

  acquire(&ptable.lock);
  103568:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  10356f:	e8 bc 05 00 00       	call   103b30 <acquire>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
  103574:	65 8b 1d 04 00 00 00 	mov    %gs:0x4,%ebx

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
  10357b:	b9 54 e0 10 00       	mov    $0x10e054,%ecx
  103580:	b8 54 c1 10 00       	mov    $0x10c154,%eax
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
  103585:	8b 53 14             	mov    0x14(%ebx),%edx
  103588:	eb 10                	jmp    10359a <exit+0x9a>
  10358a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  103590:	83 c0 7c             	add    $0x7c,%eax
  103593:	3d 54 e0 10 00       	cmp    $0x10e054,%eax
  103598:	74 1c                	je     1035b6 <exit+0xb6>
    if(p->state == SLEEPING && p->chan == chan)
  10359a:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
  10359e:	75 f0                	jne    103590 <exit+0x90>
  1035a0:	3b 50 20             	cmp    0x20(%eax),%edx
  1035a3:	75 eb                	jne    103590 <exit+0x90>
      p->state = RUNNABLE;
  1035a5:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  1035ac:	83 c0 7c             	add    $0x7c,%eax
  1035af:	3d 54 e0 10 00       	cmp    $0x10e054,%eax
  1035b4:	75 e4                	jne    10359a <exit+0x9a>
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
  1035b6:	8b 35 c8 78 10 00    	mov    0x1078c8,%esi
  1035bc:	ba 54 c1 10 00       	mov    $0x10c154,%edx
  1035c1:	eb 0c                	jmp    1035cf <exit+0xcf>
  1035c3:	90                   	nop
  1035c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  1035c8:	83 c2 7c             	add    $0x7c,%edx
  1035cb:	39 d1                	cmp    %edx,%ecx
  1035cd:	74 34                	je     103603 <exit+0x103>
    if(p->parent == proc){
  1035cf:	3b 5a 14             	cmp    0x14(%edx),%ebx
  1035d2:	75 f4                	jne    1035c8 <exit+0xc8>
      p->parent = initproc;
      if(p->state == ZOMBIE)
  1035d4:	83 7a 0c 05          	cmpl   $0x5,0xc(%edx)
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
  1035d8:	89 72 14             	mov    %esi,0x14(%edx)
      if(p->state == ZOMBIE)
  1035db:	75 eb                	jne    1035c8 <exit+0xc8>
  1035dd:	b8 54 c1 10 00       	mov    $0x10c154,%eax
  1035e2:	eb 0b                	jmp    1035ef <exit+0xef>
  1035e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  1035e8:	83 c0 7c             	add    $0x7c,%eax
  1035eb:	39 c1                	cmp    %eax,%ecx
  1035ed:	74 d9                	je     1035c8 <exit+0xc8>
    if(p->state == SLEEPING && p->chan == chan)
  1035ef:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
  1035f3:	75 f3                	jne    1035e8 <exit+0xe8>
  1035f5:	3b 70 20             	cmp    0x20(%eax),%esi
  1035f8:	75 ee                	jne    1035e8 <exit+0xe8>
      p->state = RUNNABLE;
  1035fa:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  103601:	eb e5                	jmp    1035e8 <exit+0xe8>
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
  103603:	c7 43 0c 05 00 00 00 	movl   $0x5,0xc(%ebx)
  sched();
  10360a:	e8 b1 fb ff ff       	call   1031c0 <sched>
  panic("zombie exit");
  10360f:	c7 04 24 c2 69 10 00 	movl   $0x1069c2,(%esp)
  103616:	e8 f5 d2 ff ff       	call   100910 <panic>
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");
  10361b:	c7 04 24 b5 69 10 00 	movl   $0x1069b5,(%esp)
  103622:	e8 e9 d2 ff ff       	call   100910 <panic>
  103627:	89 f6                	mov    %esi,%esi
  103629:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103630 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  103630:	55                   	push   %ebp
  103631:	89 e5                	mov    %esp,%ebp
  103633:	53                   	push   %ebx
  103634:	83 ec 14             	sub    $0x14,%esp
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  103637:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  10363e:	e8 ed 04 00 00       	call   103b30 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
  103643:	8b 1d 60 c1 10 00    	mov    0x10c160,%ebx
  103649:	85 db                	test   %ebx,%ebx
  10364b:	0f 84 a5 00 00 00    	je     1036f6 <allocproc+0xc6>
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
  103651:	bb d0 c1 10 00       	mov    $0x10c1d0,%ebx
  103656:	eb 0b                	jmp    103663 <allocproc+0x33>
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  103658:	83 c3 7c             	add    $0x7c,%ebx
  10365b:	81 fb 54 e0 10 00    	cmp    $0x10e054,%ebx
  103661:	74 7d                	je     1036e0 <allocproc+0xb0>
    if(p->state == UNUSED)
  103663:	8b 4b 0c             	mov    0xc(%ebx),%ecx
  103666:	85 c9                	test   %ecx,%ecx
  103668:	75 ee                	jne    103658 <allocproc+0x28>
      goto found;
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  10366a:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)
  p->pid = nextpid++;
  103671:	a1 24 73 10 00       	mov    0x107324,%eax
  103676:	89 43 10             	mov    %eax,0x10(%ebx)
  103679:	83 c0 01             	add    $0x1,%eax
  10367c:	a3 24 73 10 00       	mov    %eax,0x107324
  release(&ptable.lock);
  103681:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103688:	e8 53 04 00 00       	call   103ae0 <release>

  // Allocate kernel stack if possible.
  if((p->kstack = kalloc()) == 0){
  10368d:	e8 ce eb ff ff       	call   102260 <kalloc>
  103692:	85 c0                	test   %eax,%eax
  103694:	89 43 08             	mov    %eax,0x8(%ebx)
  103697:	74 67                	je     103700 <allocproc+0xd0>
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  103699:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  p->tf = (struct trapframe*)sp;
  10369f:	89 53 18             	mov    %edx,0x18(%ebx)
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;
  1036a2:	c7 80 b0 0f 00 00 20 	movl   $0x104d20,0xfb0(%eax)
  1036a9:	4d 10 00 

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  1036ac:	05 9c 0f 00 00       	add    $0xf9c,%eax
  1036b1:	89 43 1c             	mov    %eax,0x1c(%ebx)
  memset(p->context, 0, sizeof *p->context);
  1036b4:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
  1036bb:	00 
  1036bc:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1036c3:	00 
  1036c4:	89 04 24             	mov    %eax,(%esp)
  1036c7:	e8 04 05 00 00       	call   103bd0 <memset>
  p->context->eip = (uint)forkret;
  1036cc:	8b 43 1c             	mov    0x1c(%ebx),%eax
  1036cf:	c7 40 10 a0 31 10 00 	movl   $0x1031a0,0x10(%eax)

  return p;
}
  1036d6:	89 d8                	mov    %ebx,%eax
  1036d8:	83 c4 14             	add    $0x14,%esp
  1036db:	5b                   	pop    %ebx
  1036dc:	5d                   	pop    %ebp
  1036dd:	c3                   	ret    
  1036de:	66 90                	xchg   %ax,%ax

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
  1036e0:	31 db                	xor    %ebx,%ebx
  1036e2:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  1036e9:	e8 f2 03 00 00       	call   103ae0 <release>
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  return p;
}
  1036ee:	89 d8                	mov    %ebx,%eax
  1036f0:	83 c4 14             	add    $0x14,%esp
  1036f3:	5b                   	pop    %ebx
  1036f4:	5d                   	pop    %ebp
  1036f5:	c3                   	ret    
  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
  return 0;
  1036f6:	bb 54 c1 10 00       	mov    $0x10c154,%ebx
  1036fb:	e9 6a ff ff ff       	jmp    10366a <allocproc+0x3a>
  p->pid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack if possible.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
  103700:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  103707:	31 db                	xor    %ebx,%ebx
    return 0;
  103709:	eb cb                	jmp    1036d6 <allocproc+0xa6>
  10370b:	90                   	nop
  10370c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00103710 <fork>:
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  103710:	55                   	push   %ebp
  103711:	89 e5                	mov    %esp,%ebp
  103713:	57                   	push   %edi
  103714:	56                   	push   %esi
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
  103715:	be ff ff ff ff       	mov    $0xffffffff,%esi
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  10371a:	53                   	push   %ebx
  10371b:	83 ec 1c             	sub    $0x1c,%esp
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
  10371e:	e8 0d ff ff ff       	call   103630 <allocproc>
  103723:	85 c0                	test   %eax,%eax
  103725:	89 c3                	mov    %eax,%ebx
  103727:	0f 84 be 00 00 00    	je     1037eb <fork+0xdb>
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
  10372d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103733:	8b 10                	mov    (%eax),%edx
  103735:	89 54 24 04          	mov    %edx,0x4(%esp)
  103739:	8b 40 04             	mov    0x4(%eax),%eax
  10373c:	89 04 24             	mov    %eax,(%esp)
  10373f:	e8 0c 29 00 00       	call   106050 <copyuvm>
  103744:	85 c0                	test   %eax,%eax
  103746:	89 43 04             	mov    %eax,0x4(%ebx)
  103749:	0f 84 a6 00 00 00    	je     1037f5 <fork+0xe5>
    kfree(np->kstack);
    np->kstack = 0;
    np->state = UNUSED;
    return -1;
  }
  np->sz = proc->sz;
  10374f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  np->parent = proc;
  *np->tf = *proc->tf;
  103755:	b9 13 00 00 00       	mov    $0x13,%ecx
    kfree(np->kstack);
    np->kstack = 0;
    np->state = UNUSED;
    return -1;
  }
  np->sz = proc->sz;
  10375a:	8b 00                	mov    (%eax),%eax
  10375c:	89 03                	mov    %eax,(%ebx)
  np->parent = proc;
  10375e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103764:	89 43 14             	mov    %eax,0x14(%ebx)
  *np->tf = *proc->tf;
  103767:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  10376e:	8b 43 18             	mov    0x18(%ebx),%eax
  103771:	8b 72 18             	mov    0x18(%edx),%esi
  103774:	89 c7                	mov    %eax,%edi
  103776:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
  103778:	31 f6                	xor    %esi,%esi
  10377a:	8b 43 18             	mov    0x18(%ebx),%eax
  10377d:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
  103784:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  10378b:	90                   	nop
  10378c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
  103790:	8b 44 b2 28          	mov    0x28(%edx,%esi,4),%eax
  103794:	85 c0                	test   %eax,%eax
  103796:	74 13                	je     1037ab <fork+0x9b>
      np->ofile[i] = filedup(proc->ofile[i]);
  103798:	89 04 24             	mov    %eax,(%esp)
  10379b:	e8 00 d7 ff ff       	call   100ea0 <filedup>
  1037a0:	89 44 b3 28          	mov    %eax,0x28(%ebx,%esi,4)
  1037a4:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
  1037ab:	83 c6 01             	add    $0x1,%esi
  1037ae:	83 fe 10             	cmp    $0x10,%esi
  1037b1:	75 dd                	jne    103790 <fork+0x80>
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
  1037b3:	8b 42 68             	mov    0x68(%edx),%eax
  1037b6:	89 04 24             	mov    %eax,(%esp)
  1037b9:	e8 e2 d8 ff ff       	call   1010a0 <idup>
 
  pid = np->pid;
  1037be:	8b 73 10             	mov    0x10(%ebx),%esi
  np->state = RUNNABLE;
  1037c1:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
  1037c8:	89 43 68             	mov    %eax,0x68(%ebx)
 
  pid = np->pid;
  np->state = RUNNABLE;
  safestrcpy(np->name, proc->name, sizeof(proc->name));
  1037cb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  1037d1:	83 c3 6c             	add    $0x6c,%ebx
  1037d4:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
  1037db:	00 
  1037dc:	89 1c 24             	mov    %ebx,(%esp)
  1037df:	83 c0 6c             	add    $0x6c,%eax
  1037e2:	89 44 24 04          	mov    %eax,0x4(%esp)
  1037e6:	e8 85 05 00 00       	call   103d70 <safestrcpy>
  return pid;
}
  1037eb:	83 c4 1c             	add    $0x1c,%esp
  1037ee:	89 f0                	mov    %esi,%eax
  1037f0:	5b                   	pop    %ebx
  1037f1:	5e                   	pop    %esi
  1037f2:	5f                   	pop    %edi
  1037f3:	5d                   	pop    %ebp
  1037f4:	c3                   	ret    
  if((np = allocproc()) == 0)
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(np->kstack);
  1037f5:	8b 43 08             	mov    0x8(%ebx),%eax
  1037f8:	89 04 24             	mov    %eax,(%esp)
  1037fb:	e8 a0 ea ff ff       	call   1022a0 <kfree>
    np->kstack = 0;
  103800:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    np->state = UNUSED;
  103807:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
  10380e:	eb db                	jmp    1037eb <fork+0xdb>

00103810 <growproc>:

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  103810:	55                   	push   %ebp
  103811:	89 e5                	mov    %esp,%ebp
  103813:	83 ec 18             	sub    $0x18,%esp
  uint sz;
  
  sz = proc->sz;
  103816:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  10381d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  uint sz;
  
  sz = proc->sz;
  103820:	8b 02                	mov    (%edx),%eax
  if(n > 0){
  103822:	83 f9 00             	cmp    $0x0,%ecx
  103825:	7f 19                	jg     103840 <growproc+0x30>
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
  103827:	75 39                	jne    103862 <growproc+0x52>
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->sz = sz;
  103829:	89 02                	mov    %eax,(%edx)
  switchuvm(proc);
  10382b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103831:	89 04 24             	mov    %eax,(%esp)
  103834:	e8 67 2a 00 00       	call   1062a0 <switchuvm>
  103839:	31 c0                	xor    %eax,%eax
  return 0;
}
  10383b:	c9                   	leave  
  10383c:	c3                   	ret    
  10383d:	8d 76 00             	lea    0x0(%esi),%esi
{
  uint sz;
  
  sz = proc->sz;
  if(n > 0){
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
  103840:	01 c1                	add    %eax,%ecx
  103842:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  103846:	89 44 24 04          	mov    %eax,0x4(%esp)
  10384a:	8b 42 04             	mov    0x4(%edx),%eax
  10384d:	89 04 24             	mov    %eax,(%esp)
  103850:	e8 bb 28 00 00       	call   106110 <allocuvm>
  103855:	85 c0                	test   %eax,%eax
  103857:	74 27                	je     103880 <growproc+0x70>
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
  103859:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  103860:	eb c7                	jmp    103829 <growproc+0x19>
  103862:	01 c1                	add    %eax,%ecx
  103864:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  103868:	89 44 24 04          	mov    %eax,0x4(%esp)
  10386c:	8b 42 04             	mov    0x4(%edx),%eax
  10386f:	89 04 24             	mov    %eax,(%esp)
  103872:	e8 c9 26 00 00       	call   105f40 <deallocuvm>
  103877:	85 c0                	test   %eax,%eax
  103879:	75 de                	jne    103859 <growproc+0x49>
  10387b:	90                   	nop
  10387c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      return -1;
  }
  proc->sz = sz;
  switchuvm(proc);
  return 0;
  103880:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  103885:	c9                   	leave  
  103886:	c3                   	ret    
  103887:	89 f6                	mov    %esi,%esi
  103889:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103890 <userinit>:
}

// Set up first user process.
void
userinit(void)
{
  103890:	55                   	push   %ebp
  103891:	89 e5                	mov    %esp,%ebp
  103893:	53                   	push   %ebx
  103894:	83 ec 14             	sub    $0x14,%esp
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
  103897:	e8 94 fd ff ff       	call   103630 <allocproc>
  10389c:	89 c3                	mov    %eax,%ebx
  initproc = p;
  10389e:	a3 c8 78 10 00       	mov    %eax,0x1078c8
  if((p->pgdir = setupkvm()) == 0)
  1038a3:	e8 68 25 00 00       	call   105e10 <setupkvm>
  1038a8:	85 c0                	test   %eax,%eax
  1038aa:	89 43 04             	mov    %eax,0x4(%ebx)
  1038ad:	0f 84 b6 00 00 00    	je     103969 <userinit+0xd9>
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  1038b3:	89 04 24             	mov    %eax,(%esp)
  1038b6:	c7 44 24 08 2c 00 00 	movl   $0x2c,0x8(%esp)
  1038bd:	00 
  1038be:	c7 44 24 04 70 77 10 	movl   $0x107770,0x4(%esp)
  1038c5:	00 
  1038c6:	e8 e5 25 00 00       	call   105eb0 <inituvm>
  p->sz = PGSIZE;
  1038cb:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
  1038d1:	c7 44 24 08 4c 00 00 	movl   $0x4c,0x8(%esp)
  1038d8:	00 
  1038d9:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1038e0:	00 
  1038e1:	8b 43 18             	mov    0x18(%ebx),%eax
  1038e4:	89 04 24             	mov    %eax,(%esp)
  1038e7:	e8 e4 02 00 00       	call   103bd0 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  1038ec:	8b 43 18             	mov    0x18(%ebx),%eax
  1038ef:	66 c7 40 3c 23 00    	movw   $0x23,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  1038f5:	8b 43 18             	mov    0x18(%ebx),%eax
  1038f8:	66 c7 40 2c 2b 00    	movw   $0x2b,0x2c(%eax)
  p->tf->es = p->tf->ds;
  1038fe:	8b 43 18             	mov    0x18(%ebx),%eax
  103901:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
  103905:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
  103909:	8b 43 18             	mov    0x18(%ebx),%eax
  10390c:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
  103910:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
  103914:	8b 43 18             	mov    0x18(%ebx),%eax
  103917:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
  10391e:	8b 43 18             	mov    0x18(%ebx),%eax
  103921:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
  103928:	8b 43 18             	mov    0x18(%ebx),%eax
  10392b:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)

  safestrcpy(p->name, "initcode", sizeof(p->name));
  103932:	8d 43 6c             	lea    0x6c(%ebx),%eax
  103935:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
  10393c:	00 
  10393d:	c7 44 24 04 e7 69 10 	movl   $0x1069e7,0x4(%esp)
  103944:	00 
  103945:	89 04 24             	mov    %eax,(%esp)
  103948:	e8 23 04 00 00       	call   103d70 <safestrcpy>
  p->cwd = namei("/");
  10394d:	c7 04 24 f0 69 10 00 	movl   $0x1069f0,(%esp)
  103954:	e8 07 e5 ff ff       	call   101e60 <namei>

  p->state = RUNNABLE;
  103959:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");
  103960:	89 43 68             	mov    %eax,0x68(%ebx)

  p->state = RUNNABLE;
}
  103963:	83 c4 14             	add    $0x14,%esp
  103966:	5b                   	pop    %ebx
  103967:	5d                   	pop    %ebp
  103968:	c3                   	ret    
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  103969:	c7 04 24 ce 69 10 00 	movl   $0x1069ce,(%esp)
  103970:	e8 9b cf ff ff       	call   100910 <panic>
  103975:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  103979:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103980 <pinit>:

static void wakeup1(void *chan);

void
pinit(void)
{
  103980:	55                   	push   %ebp
  103981:	89 e5                	mov    %esp,%ebp
  103983:	83 ec 18             	sub    $0x18,%esp
  initlock(&ptable.lock, "ptable");
  103986:	c7 44 24 04 f2 69 10 	movl   $0x1069f2,0x4(%esp)
  10398d:	00 
  10398e:	c7 04 24 20 c1 10 00 	movl   $0x10c120,(%esp)
  103995:	e8 06 00 00 00       	call   1039a0 <initlock>
}
  10399a:	c9                   	leave  
  10399b:	c3                   	ret    
  10399c:	90                   	nop
  10399d:	90                   	nop
  10399e:	90                   	nop
  10399f:	90                   	nop

001039a0 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
  1039a0:	55                   	push   %ebp
  1039a1:	89 e5                	mov    %esp,%ebp
  1039a3:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
  1039a6:	8b 55 0c             	mov    0xc(%ebp),%edx
  lk->locked = 0;
  1039a9:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
  lk->name = name;
  1039af:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
  lk->cpu = 0;
  1039b2:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
  1039b9:	5d                   	pop    %ebp
  1039ba:	c3                   	ret    
  1039bb:	90                   	nop
  1039bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

001039c0 <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
  1039c0:	55                   	push   %ebp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  1039c1:	31 c0                	xor    %eax,%eax
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
  1039c3:	89 e5                	mov    %esp,%ebp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  1039c5:	8b 55 08             	mov    0x8(%ebp),%edx
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
  1039c8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  1039cb:	53                   	push   %ebx
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  1039cc:	83 ea 08             	sub    $0x8,%edx
  1039cf:	90                   	nop
  for(i = 0; i < 10; i++){
    if(ebp == 0 || ebp < (uint*)0x100000 || ebp == (uint*)0xffffffff)
  1039d0:	8d 9a 00 00 f0 ff    	lea    -0x100000(%edx),%ebx
  1039d6:	81 fb fe ff ef ff    	cmp    $0xffeffffe,%ebx
  1039dc:	77 1a                	ja     1039f8 <getcallerpcs+0x38>
      break;
    pcs[i] = ebp[1];     // saved %eip
  1039de:	8b 5a 04             	mov    0x4(%edx),%ebx
  1039e1:	89 1c 81             	mov    %ebx,(%ecx,%eax,4)
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
  1039e4:	83 c0 01             	add    $0x1,%eax
    if(ebp == 0 || ebp < (uint*)0x100000 || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  1039e7:	8b 12                	mov    (%edx),%edx
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
  1039e9:	83 f8 0a             	cmp    $0xa,%eax
  1039ec:	75 e2                	jne    1039d0 <getcallerpcs+0x10>
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
    pcs[i] = 0;
}
  1039ee:	5b                   	pop    %ebx
  1039ef:	5d                   	pop    %ebp
  1039f0:	c3                   	ret    
  1039f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ebp == 0 || ebp < (uint*)0x100000 || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
  1039f8:	83 f8 09             	cmp    $0x9,%eax
  1039fb:	7f f1                	jg     1039ee <getcallerpcs+0x2e>
  1039fd:	8d 76 00             	lea    0x0(%esi),%esi
    pcs[i] = 0;
  103a00:	c7 04 81 00 00 00 00 	movl   $0x0,(%ecx,%eax,4)
    if(ebp == 0 || ebp < (uint*)0x100000 || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
  103a07:	83 c0 01             	add    $0x1,%eax
  103a0a:	83 f8 0a             	cmp    $0xa,%eax
  103a0d:	75 f1                	jne    103a00 <getcallerpcs+0x40>
    pcs[i] = 0;
}
  103a0f:	5b                   	pop    %ebx
  103a10:	5d                   	pop    %ebp
  103a11:	c3                   	ret    
  103a12:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  103a19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103a20 <holding>:

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
  103a20:	55                   	push   %ebp
  return lock->locked && lock->cpu == cpu;
  103a21:	31 c0                	xor    %eax,%eax
}

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
  103a23:	89 e5                	mov    %esp,%ebp
  103a25:	8b 55 08             	mov    0x8(%ebp),%edx
  return lock->locked && lock->cpu == cpu;
  103a28:	8b 0a                	mov    (%edx),%ecx
  103a2a:	85 c9                	test   %ecx,%ecx
  103a2c:	74 10                	je     103a3e <holding+0x1e>
  103a2e:	8b 42 08             	mov    0x8(%edx),%eax
  103a31:	65 3b 05 00 00 00 00 	cmp    %gs:0x0,%eax
  103a38:	0f 94 c0             	sete   %al
  103a3b:	0f b6 c0             	movzbl %al,%eax
}
  103a3e:	5d                   	pop    %ebp
  103a3f:	c3                   	ret    

00103a40 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
  103a40:	55                   	push   %ebp
  103a41:	89 e5                	mov    %esp,%ebp
  103a43:	53                   	push   %ebx

static inline uint
readeflags(void)
{
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
  103a44:	9c                   	pushf  
  103a45:	5b                   	pop    %ebx
}

static inline void
cli(void)
{
  asm volatile("cli");
  103a46:	fa                   	cli    
  int eflags;
  
  eflags = readeflags();
  cli();
  if(cpu->ncli++ == 0)
  103a47:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
  103a4e:	8b 82 ac 00 00 00    	mov    0xac(%edx),%eax
  103a54:	8d 48 01             	lea    0x1(%eax),%ecx
  103a57:	85 c0                	test   %eax,%eax
  103a59:	89 8a ac 00 00 00    	mov    %ecx,0xac(%edx)
  103a5f:	75 12                	jne    103a73 <pushcli+0x33>
    cpu->intena = eflags & FL_IF;
  103a61:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  103a67:	81 e3 00 02 00 00    	and    $0x200,%ebx
  103a6d:	89 98 b0 00 00 00    	mov    %ebx,0xb0(%eax)
}
  103a73:	5b                   	pop    %ebx
  103a74:	5d                   	pop    %ebp
  103a75:	c3                   	ret    
  103a76:	8d 76 00             	lea    0x0(%esi),%esi
  103a79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103a80 <popcli>:

void
popcli(void)
{
  103a80:	55                   	push   %ebp
  103a81:	89 e5                	mov    %esp,%ebp
  103a83:	83 ec 18             	sub    $0x18,%esp

static inline uint
readeflags(void)
{
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
  103a86:	9c                   	pushf  
  103a87:	58                   	pop    %eax
  if(readeflags()&FL_IF)
  103a88:	f6 c4 02             	test   $0x2,%ah
  103a8b:	75 43                	jne    103ad0 <popcli+0x50>
    panic("popcli - interruptible");
  if(--cpu->ncli < 0)
  103a8d:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
  103a94:	8b 82 ac 00 00 00    	mov    0xac(%edx),%eax
  103a9a:	83 e8 01             	sub    $0x1,%eax
  103a9d:	85 c0                	test   %eax,%eax
  103a9f:	89 82 ac 00 00 00    	mov    %eax,0xac(%edx)
  103aa5:	78 1d                	js     103ac4 <popcli+0x44>
    panic("popcli");
  if(cpu->ncli == 0 && cpu->intena)
  103aa7:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  103aad:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
  103ab3:	85 d2                	test   %edx,%edx
  103ab5:	75 0b                	jne    103ac2 <popcli+0x42>
  103ab7:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
  103abd:	85 c0                	test   %eax,%eax
  103abf:	74 01                	je     103ac2 <popcli+0x42>
}

static inline void
sti(void)
{
  asm volatile("sti");
  103ac1:	fb                   	sti    
    sti();
}
  103ac2:	c9                   	leave  
  103ac3:	c3                   	ret    
popcli(void)
{
  if(readeflags()&FL_IF)
    panic("popcli - interruptible");
  if(--cpu->ncli < 0)
    panic("popcli");
  103ac4:	c7 04 24 53 6a 10 00 	movl   $0x106a53,(%esp)
  103acb:	e8 40 ce ff ff       	call   100910 <panic>

void
popcli(void)
{
  if(readeflags()&FL_IF)
    panic("popcli - interruptible");
  103ad0:	c7 04 24 3c 6a 10 00 	movl   $0x106a3c,(%esp)
  103ad7:	e8 34 ce ff ff       	call   100910 <panic>
  103adc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00103ae0 <release>:
}

// Release the lock.
void
release(struct spinlock *lk)
{
  103ae0:	55                   	push   %ebp
  103ae1:	89 e5                	mov    %esp,%ebp
  103ae3:	83 ec 18             	sub    $0x18,%esp
  103ae6:	8b 55 08             	mov    0x8(%ebp),%edx

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
  return lock->locked && lock->cpu == cpu;
  103ae9:	8b 0a                	mov    (%edx),%ecx
  103aeb:	85 c9                	test   %ecx,%ecx
  103aed:	74 0c                	je     103afb <release+0x1b>
  103aef:	8b 42 08             	mov    0x8(%edx),%eax
  103af2:	65 3b 05 00 00 00 00 	cmp    %gs:0x0,%eax
  103af9:	74 0d                	je     103b08 <release+0x28>
// Release the lock.
void
release(struct spinlock *lk)
{
  if(!holding(lk))
    panic("release");
  103afb:	c7 04 24 5a 6a 10 00 	movl   $0x106a5a,(%esp)
  103b02:	e8 09 ce ff ff       	call   100910 <panic>
  103b07:	90                   	nop

  lk->pcs[0] = 0;
  103b08:	c7 42 0c 00 00 00 00 	movl   $0x0,0xc(%edx)
xchg(volatile uint *addr, uint newval)
{
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
  103b0f:	31 c0                	xor    %eax,%eax
  lk->cpu = 0;
  103b11:	c7 42 08 00 00 00 00 	movl   $0x0,0x8(%edx)
  103b18:	f0 87 02             	lock xchg %eax,(%edx)
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);

  popcli();
}
  103b1b:	c9                   	leave  
  // after a store. So lock->locked = 0 would work here.
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);

  popcli();
  103b1c:	e9 5f ff ff ff       	jmp    103a80 <popcli>
  103b21:	eb 0d                	jmp    103b30 <acquire>
  103b23:	90                   	nop
  103b24:	90                   	nop
  103b25:	90                   	nop
  103b26:	90                   	nop
  103b27:	90                   	nop
  103b28:	90                   	nop
  103b29:	90                   	nop
  103b2a:	90                   	nop
  103b2b:	90                   	nop
  103b2c:	90                   	nop
  103b2d:	90                   	nop
  103b2e:	90                   	nop
  103b2f:	90                   	nop

00103b30 <acquire>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
  103b30:	55                   	push   %ebp
  103b31:	89 e5                	mov    %esp,%ebp
  103b33:	53                   	push   %ebx
  103b34:	83 ec 14             	sub    $0x14,%esp

static inline uint
readeflags(void)
{
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
  103b37:	9c                   	pushf  
  103b38:	5b                   	pop    %ebx
}

static inline void
cli(void)
{
  asm volatile("cli");
  103b39:	fa                   	cli    
{
  int eflags;
  
  eflags = readeflags();
  cli();
  if(cpu->ncli++ == 0)
  103b3a:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
  103b41:	8b 82 ac 00 00 00    	mov    0xac(%edx),%eax
  103b47:	8d 48 01             	lea    0x1(%eax),%ecx
  103b4a:	85 c0                	test   %eax,%eax
  103b4c:	89 8a ac 00 00 00    	mov    %ecx,0xac(%edx)
  103b52:	75 12                	jne    103b66 <acquire+0x36>
    cpu->intena = eflags & FL_IF;
  103b54:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  103b5a:	81 e3 00 02 00 00    	and    $0x200,%ebx
  103b60:	89 98 b0 00 00 00    	mov    %ebx,0xb0(%eax)
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
  pushcli(); // disable interrupts to avoid deadlock.
  if(holding(lk))
  103b66:	8b 55 08             	mov    0x8(%ebp),%edx

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
  return lock->locked && lock->cpu == cpu;
  103b69:	8b 1a                	mov    (%edx),%ebx
  103b6b:	85 db                	test   %ebx,%ebx
  103b6d:	74 0c                	je     103b7b <acquire+0x4b>
  103b6f:	8b 42 08             	mov    0x8(%edx),%eax
  103b72:	65 3b 05 00 00 00 00 	cmp    %gs:0x0,%eax
  103b79:	74 45                	je     103bc0 <acquire+0x90>
xchg(volatile uint *addr, uint newval)
{
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
  103b7b:	b9 01 00 00 00       	mov    $0x1,%ecx
  103b80:	eb 09                	jmp    103b8b <acquire+0x5b>
  103b82:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    panic("acquire");

  // The xchg is atomic.
  // It also serializes, so that reads after acquire are not
  // reordered before it. 
  while(xchg(&lk->locked, 1) != 0)
  103b88:	8b 55 08             	mov    0x8(%ebp),%edx
  103b8b:	89 c8                	mov    %ecx,%eax
  103b8d:	f0 87 02             	lock xchg %eax,(%edx)
  103b90:	85 c0                	test   %eax,%eax
  103b92:	75 f4                	jne    103b88 <acquire+0x58>
    ;

  // Record info about lock acquisition for debugging.
  lk->cpu = cpu;
  103b94:	8b 45 08             	mov    0x8(%ebp),%eax
  103b97:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
  103b9e:	89 50 08             	mov    %edx,0x8(%eax)
  getcallerpcs(&lk, lk->pcs);
  103ba1:	8b 45 08             	mov    0x8(%ebp),%eax
  103ba4:	83 c0 0c             	add    $0xc,%eax
  103ba7:	89 44 24 04          	mov    %eax,0x4(%esp)
  103bab:	8d 45 08             	lea    0x8(%ebp),%eax
  103bae:	89 04 24             	mov    %eax,(%esp)
  103bb1:	e8 0a fe ff ff       	call   1039c0 <getcallerpcs>
}
  103bb6:	83 c4 14             	add    $0x14,%esp
  103bb9:	5b                   	pop    %ebx
  103bba:	5d                   	pop    %ebp
  103bbb:	c3                   	ret    
  103bbc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
void
acquire(struct spinlock *lk)
{
  pushcli(); // disable interrupts to avoid deadlock.
  if(holding(lk))
    panic("acquire");
  103bc0:	c7 04 24 62 6a 10 00 	movl   $0x106a62,(%esp)
  103bc7:	e8 44 cd ff ff       	call   100910 <panic>
  103bcc:	90                   	nop
  103bcd:	90                   	nop
  103bce:	90                   	nop
  103bcf:	90                   	nop

00103bd0 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
  103bd0:	55                   	push   %ebp
  103bd1:	89 e5                	mov    %esp,%ebp
  103bd3:	8b 55 08             	mov    0x8(%ebp),%edx
  103bd6:	57                   	push   %edi
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
  103bd7:	8b 4d 10             	mov    0x10(%ebp),%ecx
  103bda:	8b 45 0c             	mov    0xc(%ebp),%eax
  103bdd:	89 d7                	mov    %edx,%edi
  103bdf:	fc                   	cld    
  103be0:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
  103be2:	89 d0                	mov    %edx,%eax
  103be4:	5f                   	pop    %edi
  103be5:	5d                   	pop    %ebp
  103be6:	c3                   	ret    
  103be7:	89 f6                	mov    %esi,%esi
  103be9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103bf0 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
  103bf0:	55                   	push   %ebp
  103bf1:	89 e5                	mov    %esp,%ebp
  103bf3:	8b 55 10             	mov    0x10(%ebp),%edx
  103bf6:	57                   	push   %edi
  103bf7:	8b 7d 0c             	mov    0xc(%ebp),%edi
  103bfa:	56                   	push   %esi
  103bfb:	8b 75 08             	mov    0x8(%ebp),%esi
  103bfe:	53                   	push   %ebx
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
  103bff:	85 d2                	test   %edx,%edx
  103c01:	74 2d                	je     103c30 <memcmp+0x40>
    if(*s1 != *s2)
  103c03:	0f b6 1e             	movzbl (%esi),%ebx
  103c06:	0f b6 0f             	movzbl (%edi),%ecx
  103c09:	38 cb                	cmp    %cl,%bl
  103c0b:	75 2b                	jne    103c38 <memcmp+0x48>
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
  103c0d:	83 ea 01             	sub    $0x1,%edx
  103c10:	31 c0                	xor    %eax,%eax
  103c12:	eb 18                	jmp    103c2c <memcmp+0x3c>
  103c14:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(*s1 != *s2)
  103c18:	0f b6 5c 06 01       	movzbl 0x1(%esi,%eax,1),%ebx
  103c1d:	83 ea 01             	sub    $0x1,%edx
  103c20:	0f b6 4c 07 01       	movzbl 0x1(%edi,%eax,1),%ecx
  103c25:	83 c0 01             	add    $0x1,%eax
  103c28:	38 cb                	cmp    %cl,%bl
  103c2a:	75 0c                	jne    103c38 <memcmp+0x48>
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
  103c2c:	85 d2                	test   %edx,%edx
  103c2e:	75 e8                	jne    103c18 <memcmp+0x28>
  103c30:	31 c0                	xor    %eax,%eax
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
}
  103c32:	5b                   	pop    %ebx
  103c33:	5e                   	pop    %esi
  103c34:	5f                   	pop    %edi
  103c35:	5d                   	pop    %ebp
  103c36:	c3                   	ret    
  103c37:	90                   	nop
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
    if(*s1 != *s2)
      return *s1 - *s2;
  103c38:	0f b6 c3             	movzbl %bl,%eax
  103c3b:	0f b6 c9             	movzbl %cl,%ecx
  103c3e:	29 c8                	sub    %ecx,%eax
    s1++, s2++;
  }

  return 0;
}
  103c40:	5b                   	pop    %ebx
  103c41:	5e                   	pop    %esi
  103c42:	5f                   	pop    %edi
  103c43:	5d                   	pop    %ebp
  103c44:	c3                   	ret    
  103c45:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  103c49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103c50 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
  103c50:	55                   	push   %ebp
  103c51:	89 e5                	mov    %esp,%ebp
  103c53:	57                   	push   %edi
  103c54:	8b 45 08             	mov    0x8(%ebp),%eax
  103c57:	56                   	push   %esi
  103c58:	8b 75 0c             	mov    0xc(%ebp),%esi
  103c5b:	53                   	push   %ebx
  103c5c:	8b 5d 10             	mov    0x10(%ebp),%ebx
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
  103c5f:	39 c6                	cmp    %eax,%esi
  103c61:	73 2d                	jae    103c90 <memmove+0x40>
  103c63:	8d 3c 1e             	lea    (%esi,%ebx,1),%edi
  103c66:	39 f8                	cmp    %edi,%eax
  103c68:	73 26                	jae    103c90 <memmove+0x40>
    s += n;
    d += n;
    while(n-- > 0)
  103c6a:	85 db                	test   %ebx,%ebx
  103c6c:	74 1d                	je     103c8b <memmove+0x3b>

  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
  103c6e:	8d 34 18             	lea    (%eax,%ebx,1),%esi
  103c71:	31 d2                	xor    %edx,%edx
  103c73:	90                   	nop
  103c74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    while(n-- > 0)
      *--d = *--s;
  103c78:	0f b6 4c 17 ff       	movzbl -0x1(%edi,%edx,1),%ecx
  103c7d:	88 4c 16 ff          	mov    %cl,-0x1(%esi,%edx,1)
  103c81:	83 ea 01             	sub    $0x1,%edx
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
  103c84:	8d 0c 1a             	lea    (%edx,%ebx,1),%ecx
  103c87:	85 c9                	test   %ecx,%ecx
  103c89:	75 ed                	jne    103c78 <memmove+0x28>
  } else
    while(n-- > 0)
      *d++ = *s++;

  return dst;
}
  103c8b:	5b                   	pop    %ebx
  103c8c:	5e                   	pop    %esi
  103c8d:	5f                   	pop    %edi
  103c8e:	5d                   	pop    %ebp
  103c8f:	c3                   	ret    
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
  103c90:	31 d2                	xor    %edx,%edx
      *--d = *--s;
  } else
    while(n-- > 0)
  103c92:	85 db                	test   %ebx,%ebx
  103c94:	74 f5                	je     103c8b <memmove+0x3b>
  103c96:	66 90                	xchg   %ax,%ax
      *d++ = *s++;
  103c98:	0f b6 0c 16          	movzbl (%esi,%edx,1),%ecx
  103c9c:	88 0c 10             	mov    %cl,(%eax,%edx,1)
  103c9f:	83 c2 01             	add    $0x1,%edx
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
  103ca2:	39 d3                	cmp    %edx,%ebx
  103ca4:	75 f2                	jne    103c98 <memmove+0x48>
      *d++ = *s++;

  return dst;
}
  103ca6:	5b                   	pop    %ebx
  103ca7:	5e                   	pop    %esi
  103ca8:	5f                   	pop    %edi
  103ca9:	5d                   	pop    %ebp
  103caa:	c3                   	ret    
  103cab:	90                   	nop
  103cac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00103cb0 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
  103cb0:	55                   	push   %ebp
  103cb1:	89 e5                	mov    %esp,%ebp
  return memmove(dst, src, n);
}
  103cb3:	5d                   	pop    %ebp

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
  return memmove(dst, src, n);
  103cb4:	e9 97 ff ff ff       	jmp    103c50 <memmove>
  103cb9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00103cc0 <strncmp>:
}

int
strncmp(const char *p, const char *q, uint n)
{
  103cc0:	55                   	push   %ebp
  103cc1:	89 e5                	mov    %esp,%ebp
  103cc3:	57                   	push   %edi
  103cc4:	8b 7d 10             	mov    0x10(%ebp),%edi
  103cc7:	56                   	push   %esi
  103cc8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  103ccb:	53                   	push   %ebx
  103ccc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while(n > 0 && *p && *p == *q)
  103ccf:	85 ff                	test   %edi,%edi
  103cd1:	74 3d                	je     103d10 <strncmp+0x50>
  103cd3:	0f b6 01             	movzbl (%ecx),%eax
  103cd6:	84 c0                	test   %al,%al
  103cd8:	75 18                	jne    103cf2 <strncmp+0x32>
  103cda:	eb 3c                	jmp    103d18 <strncmp+0x58>
  103cdc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  103ce0:	83 ef 01             	sub    $0x1,%edi
  103ce3:	74 2b                	je     103d10 <strncmp+0x50>
    n--, p++, q++;
  103ce5:	83 c1 01             	add    $0x1,%ecx
  103ce8:	83 c3 01             	add    $0x1,%ebx
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
  103ceb:	0f b6 01             	movzbl (%ecx),%eax
  103cee:	84 c0                	test   %al,%al
  103cf0:	74 26                	je     103d18 <strncmp+0x58>
  103cf2:	0f b6 33             	movzbl (%ebx),%esi
  103cf5:	89 f2                	mov    %esi,%edx
  103cf7:	38 d0                	cmp    %dl,%al
  103cf9:	74 e5                	je     103ce0 <strncmp+0x20>
    n--, p++, q++;
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
  103cfb:	81 e6 ff 00 00 00    	and    $0xff,%esi
  103d01:	0f b6 c0             	movzbl %al,%eax
  103d04:	29 f0                	sub    %esi,%eax
}
  103d06:	5b                   	pop    %ebx
  103d07:	5e                   	pop    %esi
  103d08:	5f                   	pop    %edi
  103d09:	5d                   	pop    %ebp
  103d0a:	c3                   	ret    
  103d0b:	90                   	nop
  103d0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
  103d10:	31 c0                	xor    %eax,%eax
    n--, p++, q++;
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
}
  103d12:	5b                   	pop    %ebx
  103d13:	5e                   	pop    %esi
  103d14:	5f                   	pop    %edi
  103d15:	5d                   	pop    %ebp
  103d16:	c3                   	ret    
  103d17:	90                   	nop
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
  103d18:	0f b6 33             	movzbl (%ebx),%esi
  103d1b:	eb de                	jmp    103cfb <strncmp+0x3b>
  103d1d:	8d 76 00             	lea    0x0(%esi),%esi

00103d20 <strncpy>:
  return (uchar)*p - (uchar)*q;
}

char*
strncpy(char *s, const char *t, int n)
{
  103d20:	55                   	push   %ebp
  103d21:	89 e5                	mov    %esp,%ebp
  103d23:	8b 45 08             	mov    0x8(%ebp),%eax
  103d26:	56                   	push   %esi
  103d27:	8b 4d 10             	mov    0x10(%ebp),%ecx
  103d2a:	53                   	push   %ebx
  103d2b:	8b 75 0c             	mov    0xc(%ebp),%esi
  103d2e:	89 c3                	mov    %eax,%ebx
  103d30:	eb 09                	jmp    103d3b <strncpy+0x1b>
  103d32:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
  103d38:	83 c6 01             	add    $0x1,%esi
  103d3b:	83 e9 01             	sub    $0x1,%ecx
    return 0;
  return (uchar)*p - (uchar)*q;
}

char*
strncpy(char *s, const char *t, int n)
  103d3e:	8d 51 01             	lea    0x1(%ecx),%edx
{
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
  103d41:	85 d2                	test   %edx,%edx
  103d43:	7e 0c                	jle    103d51 <strncpy+0x31>
  103d45:	0f b6 16             	movzbl (%esi),%edx
  103d48:	88 13                	mov    %dl,(%ebx)
  103d4a:	83 c3 01             	add    $0x1,%ebx
  103d4d:	84 d2                	test   %dl,%dl
  103d4f:	75 e7                	jne    103d38 <strncpy+0x18>
    return 0;
  return (uchar)*p - (uchar)*q;
}

char*
strncpy(char *s, const char *t, int n)
  103d51:	31 d2                	xor    %edx,%edx
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
  103d53:	85 c9                	test   %ecx,%ecx
  103d55:	7e 0c                	jle    103d63 <strncpy+0x43>
  103d57:	90                   	nop
    *s++ = 0;
  103d58:	c6 04 13 00          	movb   $0x0,(%ebx,%edx,1)
  103d5c:	83 c2 01             	add    $0x1,%edx
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
  103d5f:	39 ca                	cmp    %ecx,%edx
  103d61:	75 f5                	jne    103d58 <strncpy+0x38>
    *s++ = 0;
  return os;
}
  103d63:	5b                   	pop    %ebx
  103d64:	5e                   	pop    %esi
  103d65:	5d                   	pop    %ebp
  103d66:	c3                   	ret    
  103d67:	89 f6                	mov    %esi,%esi
  103d69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103d70 <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
  103d70:	55                   	push   %ebp
  103d71:	89 e5                	mov    %esp,%ebp
  103d73:	8b 55 10             	mov    0x10(%ebp),%edx
  103d76:	56                   	push   %esi
  103d77:	8b 45 08             	mov    0x8(%ebp),%eax
  103d7a:	53                   	push   %ebx
  103d7b:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *os;
  
  os = s;
  if(n <= 0)
  103d7e:	85 d2                	test   %edx,%edx
  103d80:	7e 1f                	jle    103da1 <safestrcpy+0x31>
  103d82:	89 c1                	mov    %eax,%ecx
  103d84:	eb 05                	jmp    103d8b <safestrcpy+0x1b>
  103d86:	66 90                	xchg   %ax,%ax
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
  103d88:	83 c6 01             	add    $0x1,%esi
  103d8b:	83 ea 01             	sub    $0x1,%edx
  103d8e:	85 d2                	test   %edx,%edx
  103d90:	7e 0c                	jle    103d9e <safestrcpy+0x2e>
  103d92:	0f b6 1e             	movzbl (%esi),%ebx
  103d95:	88 19                	mov    %bl,(%ecx)
  103d97:	83 c1 01             	add    $0x1,%ecx
  103d9a:	84 db                	test   %bl,%bl
  103d9c:	75 ea                	jne    103d88 <safestrcpy+0x18>
    ;
  *s = 0;
  103d9e:	c6 01 00             	movb   $0x0,(%ecx)
  return os;
}
  103da1:	5b                   	pop    %ebx
  103da2:	5e                   	pop    %esi
  103da3:	5d                   	pop    %ebp
  103da4:	c3                   	ret    
  103da5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  103da9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103db0 <strlen>:

int
strlen(const char *s)
{
  103db0:	55                   	push   %ebp
  int n;

  for(n = 0; s[n]; n++)
  103db1:	31 c0                	xor    %eax,%eax
  return os;
}

int
strlen(const char *s)
{
  103db3:	89 e5                	mov    %esp,%ebp
  103db5:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
  103db8:	80 3a 00             	cmpb   $0x0,(%edx)
  103dbb:	74 0c                	je     103dc9 <strlen+0x19>
  103dbd:	8d 76 00             	lea    0x0(%esi),%esi
  103dc0:	83 c0 01             	add    $0x1,%eax
  103dc3:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
  103dc7:	75 f7                	jne    103dc0 <strlen+0x10>
    ;
  return n;
}
  103dc9:	5d                   	pop    %ebp
  103dca:	c3                   	ret    
  103dcb:	90                   	nop

00103dcc <swtch>:
# Save current register context in old
# and then load register context from new.

.globl swtch
swtch:
  movl 4(%esp), %eax
  103dcc:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
  103dd0:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-save registers
  pushl %ebp
  103dd4:	55                   	push   %ebp
  pushl %ebx
  103dd5:	53                   	push   %ebx
  pushl %esi
  103dd6:	56                   	push   %esi
  pushl %edi
  103dd7:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
  103dd8:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
  103dda:	89 d4                	mov    %edx,%esp

  # Load new callee-save registers
  popl %edi
  103ddc:	5f                   	pop    %edi
  popl %esi
  103ddd:	5e                   	pop    %esi
  popl %ebx
  103dde:	5b                   	pop    %ebx
  popl %ebp
  103ddf:	5d                   	pop    %ebp
  ret
  103de0:	c3                   	ret    
  103de1:	90                   	nop
  103de2:	90                   	nop
  103de3:	90                   	nop
  103de4:	90                   	nop
  103de5:	90                   	nop
  103de6:	90                   	nop
  103de7:	90                   	nop
  103de8:	90                   	nop
  103de9:	90                   	nop
  103dea:	90                   	nop
  103deb:	90                   	nop
  103dec:	90                   	nop
  103ded:	90                   	nop
  103dee:	90                   	nop
  103def:	90                   	nop

00103df0 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  103df0:	55                   	push   %ebp
  103df1:	89 e5                	mov    %esp,%ebp
  if(addr >= p->sz || addr+4 > p->sz)
  103df3:	8b 55 08             	mov    0x8(%ebp),%edx
// to a saved program counter, and then the first argument.

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  103df6:	8b 45 0c             	mov    0xc(%ebp),%eax
  if(addr >= p->sz || addr+4 > p->sz)
  103df9:	8b 12                	mov    (%edx),%edx
  103dfb:	39 c2                	cmp    %eax,%edx
  103dfd:	77 09                	ja     103e08 <fetchint+0x18>
    return -1;
  *ip = *(int*)(addr);
  return 0;
  103dff:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  103e04:	5d                   	pop    %ebp
  103e05:	c3                   	ret    
  103e06:	66 90                	xchg   %ax,%ax

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
  103e08:	8d 48 04             	lea    0x4(%eax),%ecx
  103e0b:	39 ca                	cmp    %ecx,%edx
  103e0d:	72 f0                	jb     103dff <fetchint+0xf>
    return -1;
  *ip = *(int*)(addr);
  103e0f:	8b 10                	mov    (%eax),%edx
  103e11:	8b 45 10             	mov    0x10(%ebp),%eax
  103e14:	89 10                	mov    %edx,(%eax)
  103e16:	31 c0                	xor    %eax,%eax
  return 0;
}
  103e18:	5d                   	pop    %ebp
  103e19:	c3                   	ret    
  103e1a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00103e20 <fetchstr>:
// Fetch the nul-terminated string at addr from process p.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(struct proc *p, uint addr, char **pp)
{
  103e20:	55                   	push   %ebp
  103e21:	89 e5                	mov    %esp,%ebp
  103e23:	8b 45 08             	mov    0x8(%ebp),%eax
  103e26:	8b 55 0c             	mov    0xc(%ebp),%edx
  103e29:	53                   	push   %ebx
  char *s, *ep;

  if(addr >= p->sz)
  103e2a:	39 10                	cmp    %edx,(%eax)
  103e2c:	77 0a                	ja     103e38 <fetchstr+0x18>
    return -1;
  *pp = (char*)addr;
  ep = (char*)p->sz;
  for(s = *pp; s < ep; s++)
  103e2e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    if(*s == 0)
      return s - *pp;
  return -1;
}
  103e33:	5b                   	pop    %ebx
  103e34:	5d                   	pop    %ebp
  103e35:	c3                   	ret    
  103e36:	66 90                	xchg   %ax,%ax
{
  char *s, *ep;

  if(addr >= p->sz)
    return -1;
  *pp = (char*)addr;
  103e38:	8b 4d 10             	mov    0x10(%ebp),%ecx
  103e3b:	89 11                	mov    %edx,(%ecx)
  ep = (char*)p->sz;
  103e3d:	8b 18                	mov    (%eax),%ebx
  for(s = *pp; s < ep; s++)
  103e3f:	39 da                	cmp    %ebx,%edx
  103e41:	73 eb                	jae    103e2e <fetchstr+0xe>
    if(*s == 0)
  103e43:	31 c0                	xor    %eax,%eax
  103e45:	89 d1                	mov    %edx,%ecx
  103e47:	80 3a 00             	cmpb   $0x0,(%edx)
  103e4a:	74 e7                	je     103e33 <fetchstr+0x13>
  103e4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

  if(addr >= p->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)p->sz;
  for(s = *pp; s < ep; s++)
  103e50:	83 c1 01             	add    $0x1,%ecx
  103e53:	39 cb                	cmp    %ecx,%ebx
  103e55:	76 d7                	jbe    103e2e <fetchstr+0xe>
    if(*s == 0)
  103e57:	80 39 00             	cmpb   $0x0,(%ecx)
  103e5a:	75 f4                	jne    103e50 <fetchstr+0x30>
  103e5c:	89 c8                	mov    %ecx,%eax
  103e5e:	29 d0                	sub    %edx,%eax
  103e60:	eb d1                	jmp    103e33 <fetchstr+0x13>
  103e62:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  103e69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103e70 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103e70:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
}

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  103e76:	55                   	push   %ebp
  103e77:	89 e5                	mov    %esp,%ebp
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103e79:	8b 4d 08             	mov    0x8(%ebp),%ecx
  103e7c:	8b 50 18             	mov    0x18(%eax),%edx

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
  103e7f:	8b 00                	mov    (%eax),%eax

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103e81:	8b 52 44             	mov    0x44(%edx),%edx
  103e84:	8d 54 8a 04          	lea    0x4(%edx,%ecx,4),%edx

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
  103e88:	39 c2                	cmp    %eax,%edx
  103e8a:	72 0c                	jb     103e98 <argint+0x28>
    return -1;
  *ip = *(int*)(addr);
  103e8c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
}
  103e91:	5d                   	pop    %ebp
  103e92:	c3                   	ret    
  103e93:	90                   	nop
  103e94:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
  103e98:	8d 4a 04             	lea    0x4(%edx),%ecx
  103e9b:	39 c8                	cmp    %ecx,%eax
  103e9d:	72 ed                	jb     103e8c <argint+0x1c>
    return -1;
  *ip = *(int*)(addr);
  103e9f:	8b 45 0c             	mov    0xc(%ebp),%eax
  103ea2:	8b 12                	mov    (%edx),%edx
  103ea4:	89 10                	mov    %edx,(%eax)
  103ea6:	31 c0                	xor    %eax,%eax
// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
}
  103ea8:	5d                   	pop    %ebp
  103ea9:	c3                   	ret    
  103eaa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00103eb0 <argptr>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103eb0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size n bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
  103eb6:	55                   	push   %ebp
  103eb7:	89 e5                	mov    %esp,%ebp

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103eb9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  103ebc:	8b 50 18             	mov    0x18(%eax),%edx

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
  103ebf:	8b 00                	mov    (%eax),%eax

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103ec1:	8b 52 44             	mov    0x44(%edx),%edx
  103ec4:	8d 54 8a 04          	lea    0x4(%edx,%ecx,4),%edx

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
  103ec8:	39 c2                	cmp    %eax,%edx
  103eca:	73 07                	jae    103ed3 <argptr+0x23>
  103ecc:	8d 4a 04             	lea    0x4(%edx),%ecx
  103ecf:	39 c8                	cmp    %ecx,%eax
  103ed1:	73 0d                	jae    103ee0 <argptr+0x30>
  if(argint(n, &i) < 0)
    return -1;
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
    return -1;
  *pp = (char*)i;
  return 0;
  103ed3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  103ed8:	5d                   	pop    %ebp
  103ed9:	c3                   	ret    
  103eda:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
    return -1;
  *ip = *(int*)(addr);
  103ee0:	8b 12                	mov    (%edx),%edx
{
  int i;
  
  if(argint(n, &i) < 0)
    return -1;
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
  103ee2:	39 c2                	cmp    %eax,%edx
  103ee4:	73 ed                	jae    103ed3 <argptr+0x23>
  103ee6:	8b 4d 10             	mov    0x10(%ebp),%ecx
  103ee9:	01 d1                	add    %edx,%ecx
  103eeb:	39 c1                	cmp    %eax,%ecx
  103eed:	77 e4                	ja     103ed3 <argptr+0x23>
    return -1;
  *pp = (char*)i;
  103eef:	8b 45 0c             	mov    0xc(%ebp),%eax
  103ef2:	89 10                	mov    %edx,(%eax)
  103ef4:	31 c0                	xor    %eax,%eax
  return 0;
}
  103ef6:	5d                   	pop    %ebp
  103ef7:	c3                   	ret    
  103ef8:	90                   	nop
  103ef9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00103f00 <argstr>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103f00:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
  103f07:	55                   	push   %ebp
  103f08:	89 e5                	mov    %esp,%ebp
  103f0a:	53                   	push   %ebx

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
  103f0b:	8b 4d 08             	mov    0x8(%ebp),%ecx
  103f0e:	8b 42 18             	mov    0x18(%edx),%eax
  103f11:	8b 40 44             	mov    0x44(%eax),%eax
  103f14:	8d 44 88 04          	lea    0x4(%eax,%ecx,4),%eax

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
  103f18:	8b 0a                	mov    (%edx),%ecx
  103f1a:	39 c8                	cmp    %ecx,%eax
  103f1c:	73 07                	jae    103f25 <argstr+0x25>
  103f1e:	8d 58 04             	lea    0x4(%eax),%ebx
  103f21:	39 d9                	cmp    %ebx,%ecx
  103f23:	73 0b                	jae    103f30 <argstr+0x30>

  if(addr >= p->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)p->sz;
  for(s = *pp; s < ep; s++)
  103f25:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
{
  int addr;
  if(argint(n, &addr) < 0)
    return -1;
  return fetchstr(proc, addr, pp);
}
  103f2a:	5b                   	pop    %ebx
  103f2b:	5d                   	pop    %ebp
  103f2c:	c3                   	ret    
  103f2d:	8d 76 00             	lea    0x0(%esi),%esi
int
fetchint(struct proc *p, uint addr, int *ip)
{
  if(addr >= p->sz || addr+4 > p->sz)
    return -1;
  *ip = *(int*)(addr);
  103f30:	8b 18                	mov    (%eax),%ebx
int
fetchstr(struct proc *p, uint addr, char **pp)
{
  char *s, *ep;

  if(addr >= p->sz)
  103f32:	39 cb                	cmp    %ecx,%ebx
  103f34:	73 ef                	jae    103f25 <argstr+0x25>
    return -1;
  *pp = (char*)addr;
  103f36:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  103f39:	89 d8                	mov    %ebx,%eax
  103f3b:	89 19                	mov    %ebx,(%ecx)
  ep = (char*)p->sz;
  103f3d:	8b 12                	mov    (%edx),%edx
  for(s = *pp; s < ep; s++)
  103f3f:	39 d3                	cmp    %edx,%ebx
  103f41:	73 e2                	jae    103f25 <argstr+0x25>
    if(*s == 0)
  103f43:	80 3b 00             	cmpb   $0x0,(%ebx)
  103f46:	75 0d                	jne    103f55 <argstr+0x55>
  103f48:	eb 16                	jmp    103f60 <argstr+0x60>
  103f4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  103f50:	80 38 00             	cmpb   $0x0,(%eax)
  103f53:	74 0b                	je     103f60 <argstr+0x60>

  if(addr >= p->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)p->sz;
  for(s = *pp; s < ep; s++)
  103f55:	83 c0 01             	add    $0x1,%eax
  103f58:	39 c2                	cmp    %eax,%edx
  103f5a:	77 f4                	ja     103f50 <argstr+0x50>
  103f5c:	eb c7                	jmp    103f25 <argstr+0x25>
  103f5e:	66 90                	xchg   %ax,%ax
    if(*s == 0)
      return s - *pp;
  103f60:	29 d8                	sub    %ebx,%eax
{
  int addr;
  if(argint(n, &addr) < 0)
    return -1;
  return fetchstr(proc, addr, pp);
}
  103f62:	5b                   	pop    %ebx
  103f63:	5d                   	pop    %ebp
  103f64:	c3                   	ret    
  103f65:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  103f69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00103f70 <syscall>:
[SYS_uptime]  sys_uptime,
};

void
syscall(void)
{
  103f70:	55                   	push   %ebp
  103f71:	89 e5                	mov    %esp,%ebp
  103f73:	53                   	push   %ebx
  103f74:	83 ec 14             	sub    $0x14,%esp
  int num;
  
  num = proc->tf->eax;
  103f77:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  103f7e:	8b 5a 18             	mov    0x18(%edx),%ebx
  103f81:	8b 43 1c             	mov    0x1c(%ebx),%eax
  if(num >= 0 && num < NELEM(syscalls) && syscalls[num])
  103f84:	83 f8 15             	cmp    $0x15,%eax
  103f87:	77 17                	ja     103fa0 <syscall+0x30>
  103f89:	8b 0c 85 a0 6a 10 00 	mov    0x106aa0(,%eax,4),%ecx
  103f90:	85 c9                	test   %ecx,%ecx
  103f92:	74 0c                	je     103fa0 <syscall+0x30>
    proc->tf->eax = syscalls[num]();
  103f94:	ff d1                	call   *%ecx
  103f96:	89 43 1c             	mov    %eax,0x1c(%ebx)
  else {
    cprintf("%d %s: unknown sys call %d\n",
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
  }
}
  103f99:	83 c4 14             	add    $0x14,%esp
  103f9c:	5b                   	pop    %ebx
  103f9d:	5d                   	pop    %ebp
  103f9e:	c3                   	ret    
  103f9f:	90                   	nop
  
  num = proc->tf->eax;
  if(num >= 0 && num < NELEM(syscalls) && syscalls[num])
    proc->tf->eax = syscalls[num]();
  else {
    cprintf("%d %s: unknown sys call %d\n",
  103fa0:	8b 4a 10             	mov    0x10(%edx),%ecx
  103fa3:	83 c2 6c             	add    $0x6c,%edx
  103fa6:	89 44 24 0c          	mov    %eax,0xc(%esp)
  103faa:	89 54 24 08          	mov    %edx,0x8(%esp)
  103fae:	c7 04 24 6a 6a 10 00 	movl   $0x106a6a,(%esp)
  103fb5:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  103fb9:	e8 72 c5 ff ff       	call   100530 <cprintf>
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
  103fbe:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  103fc4:	8b 40 18             	mov    0x18(%eax),%eax
  103fc7:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
  }
}
  103fce:	83 c4 14             	add    $0x14,%esp
  103fd1:	5b                   	pop    %ebx
  103fd2:	5d                   	pop    %ebp
  103fd3:	c3                   	ret    
  103fd4:	90                   	nop
  103fd5:	90                   	nop
  103fd6:	90                   	nop
  103fd7:	90                   	nop
  103fd8:	90                   	nop
  103fd9:	90                   	nop
  103fda:	90                   	nop
  103fdb:	90                   	nop
  103fdc:	90                   	nop
  103fdd:	90                   	nop
  103fde:	90                   	nop
  103fdf:	90                   	nop

00103fe0 <sys_pipe>:
  return exec(path, argv);
}

int
sys_pipe(void)
{
  103fe0:	55                   	push   %ebp
  103fe1:	89 e5                	mov    %esp,%ebp
  103fe3:	83 ec 28             	sub    $0x28,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
  103fe6:	8d 45 f4             	lea    -0xc(%ebp),%eax
  return exec(path, argv);
}

int
sys_pipe(void)
{
  103fe9:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  103fec:	89 75 fc             	mov    %esi,-0x4(%ebp)
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
  103fef:	c7 44 24 08 08 00 00 	movl   $0x8,0x8(%esp)
  103ff6:	00 
  103ff7:	89 44 24 04          	mov    %eax,0x4(%esp)
  103ffb:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104002:	e8 a9 fe ff ff       	call   103eb0 <argptr>
  104007:	85 c0                	test   %eax,%eax
  104009:	79 15                	jns    104020 <sys_pipe+0x40>
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
    if(fd0 >= 0)
      proc->ofile[fd0] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  10400b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  fd[0] = fd0;
  fd[1] = fd1;
  return 0;
}
  104010:	8b 5d f8             	mov    -0x8(%ebp),%ebx
  104013:	8b 75 fc             	mov    -0x4(%ebp),%esi
  104016:	89 ec                	mov    %ebp,%esp
  104018:	5d                   	pop    %ebp
  104019:	c3                   	ret    
  10401a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
    return -1;
  if(pipealloc(&rf, &wf) < 0)
  104020:	8d 45 ec             	lea    -0x14(%ebp),%eax
  104023:	89 44 24 04          	mov    %eax,0x4(%esp)
  104027:	8d 45 f0             	lea    -0x10(%ebp),%eax
  10402a:	89 04 24             	mov    %eax,(%esp)
  10402d:	e8 ce ee ff ff       	call   102f00 <pipealloc>
  104032:	85 c0                	test   %eax,%eax
  104034:	78 d5                	js     10400b <sys_pipe+0x2b>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
  104036:	8b 4d f0             	mov    -0x10(%ebp),%ecx
  104039:	31 c0                	xor    %eax,%eax
  10403b:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  104042:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd] == 0){
  104048:	8b 5c 82 28          	mov    0x28(%edx,%eax,4),%ebx
  10404c:	85 db                	test   %ebx,%ebx
  10404e:	74 28                	je     104078 <sys_pipe+0x98>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
  104050:	83 c0 01             	add    $0x1,%eax
  104053:	83 f8 10             	cmp    $0x10,%eax
  104056:	75 f0                	jne    104048 <sys_pipe+0x68>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
    if(fd0 >= 0)
      proc->ofile[fd0] = 0;
    fileclose(rf);
  104058:	89 0c 24             	mov    %ecx,(%esp)
  10405b:	e8 10 cf ff ff       	call   100f70 <fileclose>
    fileclose(wf);
  104060:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104063:	89 04 24             	mov    %eax,(%esp)
  104066:	e8 05 cf ff ff       	call   100f70 <fileclose>
  10406b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    return -1;
  104070:	eb 9e                	jmp    104010 <sys_pipe+0x30>
  104072:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
  104078:	8d 58 08             	lea    0x8(%eax),%ebx
  10407b:	89 4c 9a 08          	mov    %ecx,0x8(%edx,%ebx,4)
  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
    return -1;
  if(pipealloc(&rf, &wf) < 0)
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
  10407f:	8b 75 ec             	mov    -0x14(%ebp),%esi
  104082:	31 d2                	xor    %edx,%edx
  104084:	65 8b 0d 04 00 00 00 	mov    %gs:0x4,%ecx
  10408b:	90                   	nop
  10408c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd] == 0){
  104090:	83 7c 91 28 00       	cmpl   $0x0,0x28(%ecx,%edx,4)
  104095:	74 19                	je     1040b0 <sys_pipe+0xd0>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
  104097:	83 c2 01             	add    $0x1,%edx
  10409a:	83 fa 10             	cmp    $0x10,%edx
  10409d:	75 f1                	jne    104090 <sys_pipe+0xb0>
  if(pipealloc(&rf, &wf) < 0)
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
    if(fd0 >= 0)
      proc->ofile[fd0] = 0;
  10409f:	c7 44 99 08 00 00 00 	movl   $0x0,0x8(%ecx,%ebx,4)
  1040a6:	00 
  1040a7:	8b 4d f0             	mov    -0x10(%ebp),%ecx
  1040aa:	eb ac                	jmp    104058 <sys_pipe+0x78>
  1040ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
  1040b0:	89 74 91 28          	mov    %esi,0x28(%ecx,%edx,4)
      proc->ofile[fd0] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  fd[0] = fd0;
  1040b4:	8b 4d f4             	mov    -0xc(%ebp),%ecx
  1040b7:	89 01                	mov    %eax,(%ecx)
  fd[1] = fd1;
  1040b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1040bc:	89 50 04             	mov    %edx,0x4(%eax)
  1040bf:	31 c0                	xor    %eax,%eax
  return 0;
  1040c1:	e9 4a ff ff ff       	jmp    104010 <sys_pipe+0x30>
  1040c6:	8d 76 00             	lea    0x0(%esi),%esi
  1040c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

001040d0 <sys_exec>:
  return 0;
}

int
sys_exec(void)
{
  1040d0:	55                   	push   %ebp
  1040d1:	89 e5                	mov    %esp,%ebp
  1040d3:	81 ec b8 00 00 00    	sub    $0xb8,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
  1040d9:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  return 0;
}

int
sys_exec(void)
{
  1040dc:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  1040df:	89 75 f8             	mov    %esi,-0x8(%ebp)
  1040e2:	89 7d fc             	mov    %edi,-0x4(%ebp)
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
  1040e5:	89 44 24 04          	mov    %eax,0x4(%esp)
  1040e9:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1040f0:	e8 0b fe ff ff       	call   103f00 <argstr>
  1040f5:	85 c0                	test   %eax,%eax
  1040f7:	79 17                	jns    104110 <sys_exec+0x40>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
    if(i >= NELEM(argv))
  1040f9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    }
    if(fetchstr(proc, uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
}
  1040fe:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  104101:	8b 75 f8             	mov    -0x8(%ebp),%esi
  104104:	8b 7d fc             	mov    -0x4(%ebp),%edi
  104107:	89 ec                	mov    %ebp,%esp
  104109:	5d                   	pop    %ebp
  10410a:	c3                   	ret    
  10410b:	90                   	nop
  10410c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
  104110:	8d 45 e0             	lea    -0x20(%ebp),%eax
  104113:	89 44 24 04          	mov    %eax,0x4(%esp)
  104117:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  10411e:	e8 4d fd ff ff       	call   103e70 <argint>
  104123:	85 c0                	test   %eax,%eax
  104125:	78 d2                	js     1040f9 <sys_exec+0x29>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  104127:	8d bd 5c ff ff ff    	lea    -0xa4(%ebp),%edi
  10412d:	31 f6                	xor    %esi,%esi
  10412f:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
  104136:	00 
  104137:	31 db                	xor    %ebx,%ebx
  104139:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  104140:	00 
  104141:	89 3c 24             	mov    %edi,(%esp)
  104144:	e8 87 fa ff ff       	call   103bd0 <memset>
  104149:	eb 2c                	jmp    104177 <sys_exec+0xa7>
  10414b:	90                   	nop
  10414c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(proc, uarg, &argv[i]) < 0)
  104150:	89 44 24 04          	mov    %eax,0x4(%esp)
  104154:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  10415a:	8d 14 b7             	lea    (%edi,%esi,4),%edx
  10415d:	89 54 24 08          	mov    %edx,0x8(%esp)
  104161:	89 04 24             	mov    %eax,(%esp)
  104164:	e8 b7 fc ff ff       	call   103e20 <fetchstr>
  104169:	85 c0                	test   %eax,%eax
  10416b:	78 8c                	js     1040f9 <sys_exec+0x29>

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
  10416d:	83 c3 01             	add    $0x1,%ebx
    if(i >= NELEM(argv))
  104170:	83 fb 20             	cmp    $0x20,%ebx

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
  104173:	89 de                	mov    %ebx,%esi
    if(i >= NELEM(argv))
  104175:	74 82                	je     1040f9 <sys_exec+0x29>
      return -1;
    if(fetchint(proc, uargv+4*i, (int*)&uarg) < 0)
  104177:	8d 45 dc             	lea    -0x24(%ebp),%eax
  10417a:	89 44 24 08          	mov    %eax,0x8(%esp)
  10417e:	8d 04 9d 00 00 00 00 	lea    0x0(,%ebx,4),%eax
  104185:	03 45 e0             	add    -0x20(%ebp),%eax
  104188:	89 44 24 04          	mov    %eax,0x4(%esp)
  10418c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104192:	89 04 24             	mov    %eax,(%esp)
  104195:	e8 56 fc ff ff       	call   103df0 <fetchint>
  10419a:	85 c0                	test   %eax,%eax
  10419c:	0f 88 57 ff ff ff    	js     1040f9 <sys_exec+0x29>
      return -1;
    if(uarg == 0){
  1041a2:	8b 45 dc             	mov    -0x24(%ebp),%eax
  1041a5:	85 c0                	test   %eax,%eax
  1041a7:	75 a7                	jne    104150 <sys_exec+0x80>
      break;
    }
    if(fetchstr(proc, uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
  1041a9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(proc, uargv+4*i, (int*)&uarg) < 0)
      return -1;
    if(uarg == 0){
      argv[i] = 0;
  1041ac:	c7 84 9d 5c ff ff ff 	movl   $0x0,-0xa4(%ebp,%ebx,4)
  1041b3:	00 00 00 00 
      break;
    }
    if(fetchstr(proc, uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
  1041b7:	89 7c 24 04          	mov    %edi,0x4(%esp)
  1041bb:	89 04 24             	mov    %eax,(%esp)
  1041be:	e8 cd c7 ff ff       	call   100990 <exec>
  1041c3:	e9 36 ff ff ff       	jmp    1040fe <sys_exec+0x2e>
  1041c8:	90                   	nop
  1041c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

001041d0 <sys_chdir>:
  return 0;
}

int
sys_chdir(void)
{
  1041d0:	55                   	push   %ebp
  1041d1:	89 e5                	mov    %esp,%ebp
  1041d3:	53                   	push   %ebx
  1041d4:	83 ec 24             	sub    $0x24,%esp
  char *path;
  struct inode *ip;

  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0)
  1041d7:	8d 45 f4             	lea    -0xc(%ebp),%eax
  1041da:	89 44 24 04          	mov    %eax,0x4(%esp)
  1041de:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1041e5:	e8 16 fd ff ff       	call   103f00 <argstr>
  1041ea:	85 c0                	test   %eax,%eax
  1041ec:	79 12                	jns    104200 <sys_chdir+0x30>
    return -1;
  }
  iunlock(ip);
  iput(proc->cwd);
  proc->cwd = ip;
  return 0;
  1041ee:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  1041f3:	83 c4 24             	add    $0x24,%esp
  1041f6:	5b                   	pop    %ebx
  1041f7:	5d                   	pop    %ebp
  1041f8:	c3                   	ret    
  1041f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
sys_chdir(void)
{
  char *path;
  struct inode *ip;

  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0)
  104200:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104203:	89 04 24             	mov    %eax,(%esp)
  104206:	e8 55 dc ff ff       	call   101e60 <namei>
  10420b:	85 c0                	test   %eax,%eax
  10420d:	89 c3                	mov    %eax,%ebx
  10420f:	74 dd                	je     1041ee <sys_chdir+0x1e>
    return -1;
  ilock(ip);
  104211:	89 04 24             	mov    %eax,(%esp)
  104214:	e8 a7 d9 ff ff       	call   101bc0 <ilock>
  if(ip->type != T_DIR){
  104219:	66 83 7b 10 01       	cmpw   $0x1,0x10(%ebx)
  10421e:	75 26                	jne    104246 <sys_chdir+0x76>
    iunlockput(ip);
    return -1;
  }
  iunlock(ip);
  104220:	89 1c 24             	mov    %ebx,(%esp)
  104223:	e8 58 d5 ff ff       	call   101780 <iunlock>
  iput(proc->cwd);
  104228:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  10422e:	8b 40 68             	mov    0x68(%eax),%eax
  104231:	89 04 24             	mov    %eax,(%esp)
  104234:	e8 57 d6 ff ff       	call   101890 <iput>
  proc->cwd = ip;
  104239:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  10423f:	89 58 68             	mov    %ebx,0x68(%eax)
  104242:	31 c0                	xor    %eax,%eax
  return 0;
  104244:	eb ad                	jmp    1041f3 <sys_chdir+0x23>

  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0)
    return -1;
  ilock(ip);
  if(ip->type != T_DIR){
    iunlockput(ip);
  104246:	89 1c 24             	mov    %ebx,(%esp)
  104249:	e8 82 d8 ff ff       	call   101ad0 <iunlockput>
  10424e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    return -1;
  104253:	eb 9e                	jmp    1041f3 <sys_chdir+0x23>
  104255:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  104259:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00104260 <create>:
  return 0;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
  104260:	55                   	push   %ebp
  104261:	89 e5                	mov    %esp,%ebp
  104263:	83 ec 58             	sub    $0x58,%esp
  104266:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  104269:	8b 4d 08             	mov    0x8(%ebp),%ecx
  10426c:	89 75 f8             	mov    %esi,-0x8(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
  10426f:	8d 75 d6             	lea    -0x2a(%ebp),%esi
  return 0;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
  104272:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
  104275:	31 db                	xor    %ebx,%ebx
  return 0;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
  104277:	89 7d fc             	mov    %edi,-0x4(%ebp)
  10427a:	89 d7                	mov    %edx,%edi
  10427c:	89 4d c0             	mov    %ecx,-0x40(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
  10427f:	89 74 24 04          	mov    %esi,0x4(%esp)
  104283:	89 04 24             	mov    %eax,(%esp)
  104286:	e8 b5 db ff ff       	call   101e40 <nameiparent>
  10428b:	85 c0                	test   %eax,%eax
  10428d:	74 47                	je     1042d6 <create+0x76>
    return 0;
  ilock(dp);
  10428f:	89 04 24             	mov    %eax,(%esp)
  104292:	89 45 bc             	mov    %eax,-0x44(%ebp)
  104295:	e8 26 d9 ff ff       	call   101bc0 <ilock>

  if((ip = dirlookup(dp, name, &off)) != 0){
  10429a:	8b 55 bc             	mov    -0x44(%ebp),%edx
  10429d:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  1042a0:	89 44 24 08          	mov    %eax,0x8(%esp)
  1042a4:	89 74 24 04          	mov    %esi,0x4(%esp)
  1042a8:	89 14 24             	mov    %edx,(%esp)
  1042ab:	e8 d0 d3 ff ff       	call   101680 <dirlookup>
  1042b0:	8b 55 bc             	mov    -0x44(%ebp),%edx
  1042b3:	85 c0                	test   %eax,%eax
  1042b5:	89 c3                	mov    %eax,%ebx
  1042b7:	74 4f                	je     104308 <create+0xa8>
    iunlockput(dp);
  1042b9:	89 14 24             	mov    %edx,(%esp)
  1042bc:	e8 0f d8 ff ff       	call   101ad0 <iunlockput>
    ilock(ip);
  1042c1:	89 1c 24             	mov    %ebx,(%esp)
  1042c4:	e8 f7 d8 ff ff       	call   101bc0 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
  1042c9:	66 83 ff 02          	cmp    $0x2,%di
  1042cd:	75 19                	jne    1042e8 <create+0x88>
  1042cf:	66 83 7b 10 02       	cmpw   $0x2,0x10(%ebx)
  1042d4:	75 12                	jne    1042e8 <create+0x88>
  if(dirlink(dp, name, ip->inum) < 0)
    panic("create: dirlink");

  iunlockput(dp);
  return ip;
}
  1042d6:	89 d8                	mov    %ebx,%eax
  1042d8:	8b 75 f8             	mov    -0x8(%ebp),%esi
  1042db:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  1042de:	8b 7d fc             	mov    -0x4(%ebp),%edi
  1042e1:	89 ec                	mov    %ebp,%esp
  1042e3:	5d                   	pop    %ebp
  1042e4:	c3                   	ret    
  1042e5:	8d 76 00             	lea    0x0(%esi),%esi
  if((ip = dirlookup(dp, name, &off)) != 0){
    iunlockput(dp);
    ilock(ip);
    if(type == T_FILE && ip->type == T_FILE)
      return ip;
    iunlockput(ip);
  1042e8:	89 1c 24             	mov    %ebx,(%esp)
  1042eb:	31 db                	xor    %ebx,%ebx
  1042ed:	e8 de d7 ff ff       	call   101ad0 <iunlockput>
  if(dirlink(dp, name, ip->inum) < 0)
    panic("create: dirlink");

  iunlockput(dp);
  return ip;
}
  1042f2:	89 d8                	mov    %ebx,%eax
  1042f4:	8b 75 f8             	mov    -0x8(%ebp),%esi
  1042f7:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  1042fa:	8b 7d fc             	mov    -0x4(%ebp),%edi
  1042fd:	89 ec                	mov    %ebp,%esp
  1042ff:	5d                   	pop    %ebp
  104300:	c3                   	ret    
  104301:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      return ip;
    iunlockput(ip);
    return 0;
  }

  if((ip = ialloc(dp->dev, type)) == 0)
  104308:	0f bf c7             	movswl %di,%eax
  10430b:	89 44 24 04          	mov    %eax,0x4(%esp)
  10430f:	8b 02                	mov    (%edx),%eax
  104311:	89 55 bc             	mov    %edx,-0x44(%ebp)
  104314:	89 04 24             	mov    %eax,(%esp)
  104317:	e8 d4 d7 ff ff       	call   101af0 <ialloc>
  10431c:	8b 55 bc             	mov    -0x44(%ebp),%edx
  10431f:	85 c0                	test   %eax,%eax
  104321:	89 c3                	mov    %eax,%ebx
  104323:	0f 84 cb 00 00 00    	je     1043f4 <create+0x194>
    panic("create: ialloc");

  ilock(ip);
  104329:	89 55 bc             	mov    %edx,-0x44(%ebp)
  10432c:	89 04 24             	mov    %eax,(%esp)
  10432f:	e8 8c d8 ff ff       	call   101bc0 <ilock>
  ip->major = major;
  104334:	0f b7 45 c4          	movzwl -0x3c(%ebp),%eax
  104338:	66 89 43 12          	mov    %ax,0x12(%ebx)
  ip->minor = minor;
  10433c:	0f b7 4d c0          	movzwl -0x40(%ebp),%ecx
  ip->nlink = 1;
  104340:	66 c7 43 16 01 00    	movw   $0x1,0x16(%ebx)
  if((ip = ialloc(dp->dev, type)) == 0)
    panic("create: ialloc");

  ilock(ip);
  ip->major = major;
  ip->minor = minor;
  104346:	66 89 4b 14          	mov    %cx,0x14(%ebx)
  ip->nlink = 1;
  iupdate(ip);
  10434a:	89 1c 24             	mov    %ebx,(%esp)
  10434d:	e8 2e d1 ff ff       	call   101480 <iupdate>

  if(type == T_DIR){  // Create . and .. entries.
  104352:	66 83 ff 01          	cmp    $0x1,%di
  104356:	8b 55 bc             	mov    -0x44(%ebp),%edx
  104359:	74 3d                	je     104398 <create+0x138>
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
      panic("create dots");
  }

  if(dirlink(dp, name, ip->inum) < 0)
  10435b:	8b 43 04             	mov    0x4(%ebx),%eax
  10435e:	89 14 24             	mov    %edx,(%esp)
  104361:	89 55 bc             	mov    %edx,-0x44(%ebp)
  104364:	89 74 24 04          	mov    %esi,0x4(%esp)
  104368:	89 44 24 08          	mov    %eax,0x8(%esp)
  10436c:	e8 6f d6 ff ff       	call   1019e0 <dirlink>
  104371:	8b 55 bc             	mov    -0x44(%ebp),%edx
  104374:	85 c0                	test   %eax,%eax
  104376:	0f 88 84 00 00 00    	js     104400 <create+0x1a0>
    panic("create: dirlink");

  iunlockput(dp);
  10437c:	89 14 24             	mov    %edx,(%esp)
  10437f:	e8 4c d7 ff ff       	call   101ad0 <iunlockput>
  return ip;
}
  104384:	89 d8                	mov    %ebx,%eax
  104386:	8b 75 f8             	mov    -0x8(%ebp),%esi
  104389:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  10438c:	8b 7d fc             	mov    -0x4(%ebp),%edi
  10438f:	89 ec                	mov    %ebp,%esp
  104391:	5d                   	pop    %ebp
  104392:	c3                   	ret    
  104393:	90                   	nop
  104394:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  ip->minor = minor;
  ip->nlink = 1;
  iupdate(ip);

  if(type == T_DIR){  // Create . and .. entries.
    dp->nlink++;  // for ".."
  104398:	66 83 42 16 01       	addw   $0x1,0x16(%edx)
    iupdate(dp);
  10439d:	89 14 24             	mov    %edx,(%esp)
  1043a0:	89 55 bc             	mov    %edx,-0x44(%ebp)
  1043a3:	e8 d8 d0 ff ff       	call   101480 <iupdate>
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
  1043a8:	8b 43 04             	mov    0x4(%ebx),%eax
  1043ab:	c7 44 24 04 08 6b 10 	movl   $0x106b08,0x4(%esp)
  1043b2:	00 
  1043b3:	89 1c 24             	mov    %ebx,(%esp)
  1043b6:	89 44 24 08          	mov    %eax,0x8(%esp)
  1043ba:	e8 21 d6 ff ff       	call   1019e0 <dirlink>
  1043bf:	8b 55 bc             	mov    -0x44(%ebp),%edx
  1043c2:	85 c0                	test   %eax,%eax
  1043c4:	78 22                	js     1043e8 <create+0x188>
  1043c6:	8b 42 04             	mov    0x4(%edx),%eax
  1043c9:	c7 44 24 04 07 6b 10 	movl   $0x106b07,0x4(%esp)
  1043d0:	00 
  1043d1:	89 1c 24             	mov    %ebx,(%esp)
  1043d4:	89 44 24 08          	mov    %eax,0x8(%esp)
  1043d8:	e8 03 d6 ff ff       	call   1019e0 <dirlink>
  1043dd:	8b 55 bc             	mov    -0x44(%ebp),%edx
  1043e0:	85 c0                	test   %eax,%eax
  1043e2:	0f 89 73 ff ff ff    	jns    10435b <create+0xfb>
      panic("create dots");
  1043e8:	c7 04 24 0a 6b 10 00 	movl   $0x106b0a,(%esp)
  1043ef:	e8 1c c5 ff ff       	call   100910 <panic>
    iunlockput(ip);
    return 0;
  }

  if((ip = ialloc(dp->dev, type)) == 0)
    panic("create: ialloc");
  1043f4:	c7 04 24 f8 6a 10 00 	movl   $0x106af8,(%esp)
  1043fb:	e8 10 c5 ff ff       	call   100910 <panic>
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
      panic("create dots");
  }

  if(dirlink(dp, name, ip->inum) < 0)
    panic("create: dirlink");
  104400:	c7 04 24 16 6b 10 00 	movl   $0x106b16,(%esp)
  104407:	e8 04 c5 ff ff       	call   100910 <panic>
  10440c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00104410 <sys_mknod>:
  return 0;
}

int
sys_mknod(void)
{
  104410:	55                   	push   %ebp
  104411:	89 e5                	mov    %esp,%ebp
  104413:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  
  if((len=argstr(0, &path)) < 0 ||
  104416:	8d 45 f4             	lea    -0xc(%ebp),%eax
  104419:	89 44 24 04          	mov    %eax,0x4(%esp)
  10441d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104424:	e8 d7 fa ff ff       	call   103f00 <argstr>
  104429:	85 c0                	test   %eax,%eax
  10442b:	79 0b                	jns    104438 <sys_mknod+0x28>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0)
    return -1;
  iunlockput(ip);
  return 0;
  10442d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  104432:	c9                   	leave  
  104433:	c3                   	ret    
  104434:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  char *path;
  int len;
  int major, minor;
  
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
  104438:	8d 45 f0             	lea    -0x10(%ebp),%eax
  10443b:	89 44 24 04          	mov    %eax,0x4(%esp)
  10443f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104446:	e8 25 fa ff ff       	call   103e70 <argint>
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  
  if((len=argstr(0, &path)) < 0 ||
  10444b:	85 c0                	test   %eax,%eax
  10444d:	78 de                	js     10442d <sys_mknod+0x1d>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
  10444f:	8d 45 ec             	lea    -0x14(%ebp),%eax
  104452:	89 44 24 04          	mov    %eax,0x4(%esp)
  104456:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  10445d:	e8 0e fa ff ff       	call   103e70 <argint>
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  
  if((len=argstr(0, &path)) < 0 ||
  104462:	85 c0                	test   %eax,%eax
  104464:	78 c7                	js     10442d <sys_mknod+0x1d>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0)
  104466:	0f bf 45 ec          	movswl -0x14(%ebp),%eax
  10446a:	ba 03 00 00 00       	mov    $0x3,%edx
  10446f:	0f bf 4d f0          	movswl -0x10(%ebp),%ecx
  104473:	89 04 24             	mov    %eax,(%esp)
  104476:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104479:	e8 e2 fd ff ff       	call   104260 <create>
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  
  if((len=argstr(0, &path)) < 0 ||
  10447e:	85 c0                	test   %eax,%eax
  104480:	74 ab                	je     10442d <sys_mknod+0x1d>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0)
    return -1;
  iunlockput(ip);
  104482:	89 04 24             	mov    %eax,(%esp)
  104485:	e8 46 d6 ff ff       	call   101ad0 <iunlockput>
  10448a:	31 c0                	xor    %eax,%eax
  return 0;
}
  10448c:	c9                   	leave  
  10448d:	c3                   	ret    
  10448e:	66 90                	xchg   %ax,%ax

00104490 <sys_mkdir>:
  return fd;
}

int
sys_mkdir(void)
{
  104490:	55                   	push   %ebp
  104491:	89 e5                	mov    %esp,%ebp
  104493:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0)
  104496:	8d 45 f4             	lea    -0xc(%ebp),%eax
  104499:	89 44 24 04          	mov    %eax,0x4(%esp)
  10449d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1044a4:	e8 57 fa ff ff       	call   103f00 <argstr>
  1044a9:	85 c0                	test   %eax,%eax
  1044ab:	79 0b                	jns    1044b8 <sys_mkdir+0x28>
    return -1;
  iunlockput(ip);
  return 0;
  1044ad:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  1044b2:	c9                   	leave  
  1044b3:	c3                   	ret    
  1044b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
sys_mkdir(void)
{
  char *path;
  struct inode *ip;

  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0)
  1044b8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1044bb:	31 c9                	xor    %ecx,%ecx
  1044bd:	ba 01 00 00 00       	mov    $0x1,%edx
  1044c2:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1044c9:	e8 92 fd ff ff       	call   104260 <create>
  1044ce:	85 c0                	test   %eax,%eax
  1044d0:	74 db                	je     1044ad <sys_mkdir+0x1d>
    return -1;
  iunlockput(ip);
  1044d2:	89 04 24             	mov    %eax,(%esp)
  1044d5:	e8 f6 d5 ff ff       	call   101ad0 <iunlockput>
  1044da:	31 c0                	xor    %eax,%eax
  return 0;
}
  1044dc:	c9                   	leave  
  1044dd:	c3                   	ret    
  1044de:	66 90                	xchg   %ax,%ax

001044e0 <sys_link>:
}

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
  1044e0:	55                   	push   %ebp
  1044e1:	89 e5                	mov    %esp,%ebp
  1044e3:	83 ec 48             	sub    $0x48,%esp
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
  1044e6:	8d 45 e0             	lea    -0x20(%ebp),%eax
}

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
  1044e9:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  1044ec:	89 75 f8             	mov    %esi,-0x8(%ebp)
  1044ef:	89 7d fc             	mov    %edi,-0x4(%ebp)
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
  1044f2:	89 44 24 04          	mov    %eax,0x4(%esp)
  1044f6:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1044fd:	e8 fe f9 ff ff       	call   103f00 <argstr>
  104502:	85 c0                	test   %eax,%eax
  104504:	79 12                	jns    104518 <sys_link+0x38>
bad:
  ilock(ip);
  ip->nlink--;
  iupdate(ip);
  iunlockput(ip);
  return -1;
  104506:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  10450b:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  10450e:	8b 75 f8             	mov    -0x8(%ebp),%esi
  104511:	8b 7d fc             	mov    -0x4(%ebp),%edi
  104514:	89 ec                	mov    %ebp,%esp
  104516:	5d                   	pop    %ebp
  104517:	c3                   	ret    
sys_link(void)
{
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
  104518:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  10451b:	89 44 24 04          	mov    %eax,0x4(%esp)
  10451f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104526:	e8 d5 f9 ff ff       	call   103f00 <argstr>
  10452b:	85 c0                	test   %eax,%eax
  10452d:	78 d7                	js     104506 <sys_link+0x26>
    return -1;
  if((ip = namei(old)) == 0)
  10452f:	8b 45 e0             	mov    -0x20(%ebp),%eax
  104532:	89 04 24             	mov    %eax,(%esp)
  104535:	e8 26 d9 ff ff       	call   101e60 <namei>
  10453a:	85 c0                	test   %eax,%eax
  10453c:	89 c3                	mov    %eax,%ebx
  10453e:	74 c6                	je     104506 <sys_link+0x26>
    return -1;
  ilock(ip);
  104540:	89 04 24             	mov    %eax,(%esp)
  104543:	e8 78 d6 ff ff       	call   101bc0 <ilock>
  if(ip->type == T_DIR){
  104548:	66 83 7b 10 01       	cmpw   $0x1,0x10(%ebx)
  10454d:	0f 84 86 00 00 00    	je     1045d9 <sys_link+0xf9>
    iunlockput(ip);
    return -1;
  }
  ip->nlink++;
  104553:	66 83 43 16 01       	addw   $0x1,0x16(%ebx)
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
  104558:	8d 7d d2             	lea    -0x2e(%ebp),%edi
  if(ip->type == T_DIR){
    iunlockput(ip);
    return -1;
  }
  ip->nlink++;
  iupdate(ip);
  10455b:	89 1c 24             	mov    %ebx,(%esp)
  10455e:	e8 1d cf ff ff       	call   101480 <iupdate>
  iunlock(ip);
  104563:	89 1c 24             	mov    %ebx,(%esp)
  104566:	e8 15 d2 ff ff       	call   101780 <iunlock>

  if((dp = nameiparent(new, name)) == 0)
  10456b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  10456e:	89 7c 24 04          	mov    %edi,0x4(%esp)
  104572:	89 04 24             	mov    %eax,(%esp)
  104575:	e8 c6 d8 ff ff       	call   101e40 <nameiparent>
  10457a:	85 c0                	test   %eax,%eax
  10457c:	89 c6                	mov    %eax,%esi
  10457e:	74 44                	je     1045c4 <sys_link+0xe4>
    goto bad;
  ilock(dp);
  104580:	89 04 24             	mov    %eax,(%esp)
  104583:	e8 38 d6 ff ff       	call   101bc0 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
  104588:	8b 06                	mov    (%esi),%eax
  10458a:	3b 03                	cmp    (%ebx),%eax
  10458c:	75 2e                	jne    1045bc <sys_link+0xdc>
  10458e:	8b 43 04             	mov    0x4(%ebx),%eax
  104591:	89 7c 24 04          	mov    %edi,0x4(%esp)
  104595:	89 34 24             	mov    %esi,(%esp)
  104598:	89 44 24 08          	mov    %eax,0x8(%esp)
  10459c:	e8 3f d4 ff ff       	call   1019e0 <dirlink>
  1045a1:	85 c0                	test   %eax,%eax
  1045a3:	78 17                	js     1045bc <sys_link+0xdc>
    iunlockput(dp);
    goto bad;
  }
  iunlockput(dp);
  1045a5:	89 34 24             	mov    %esi,(%esp)
  1045a8:	e8 23 d5 ff ff       	call   101ad0 <iunlockput>
  iput(ip);
  1045ad:	89 1c 24             	mov    %ebx,(%esp)
  1045b0:	e8 db d2 ff ff       	call   101890 <iput>
  1045b5:	31 c0                	xor    %eax,%eax
  return 0;
  1045b7:	e9 4f ff ff ff       	jmp    10450b <sys_link+0x2b>

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
  ilock(dp);
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
    iunlockput(dp);
  1045bc:	89 34 24             	mov    %esi,(%esp)
  1045bf:	e8 0c d5 ff ff       	call   101ad0 <iunlockput>
  iunlockput(dp);
  iput(ip);
  return 0;

bad:
  ilock(ip);
  1045c4:	89 1c 24             	mov    %ebx,(%esp)
  1045c7:	e8 f4 d5 ff ff       	call   101bc0 <ilock>
  ip->nlink--;
  1045cc:	66 83 6b 16 01       	subw   $0x1,0x16(%ebx)
  iupdate(ip);
  1045d1:	89 1c 24             	mov    %ebx,(%esp)
  1045d4:	e8 a7 ce ff ff       	call   101480 <iupdate>
  iunlockput(ip);
  1045d9:	89 1c 24             	mov    %ebx,(%esp)
  1045dc:	e8 ef d4 ff ff       	call   101ad0 <iunlockput>
  1045e1:	83 c8 ff             	or     $0xffffffff,%eax
  return -1;
  1045e4:	e9 22 ff ff ff       	jmp    10450b <sys_link+0x2b>
  1045e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

001045f0 <sys_open>:
  return ip;
}

int
sys_open(void)
{
  1045f0:	55                   	push   %ebp
  1045f1:	89 e5                	mov    %esp,%ebp
  1045f3:	83 ec 38             	sub    $0x38,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
  1045f6:	8d 45 f4             	lea    -0xc(%ebp),%eax
  return ip;
}

int
sys_open(void)
{
  1045f9:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  1045fc:	89 75 fc             	mov    %esi,-0x4(%ebp)
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
  1045ff:	89 44 24 04          	mov    %eax,0x4(%esp)
  104603:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  10460a:	e8 f1 f8 ff ff       	call   103f00 <argstr>
  10460f:	85 c0                	test   %eax,%eax
  104611:	79 15                	jns    104628 <sys_open+0x38>

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
    if(f)
      fileclose(f);
    iunlockput(ip);
    return -1;
  104613:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
  return fd;
}
  104618:	8b 5d f8             	mov    -0x8(%ebp),%ebx
  10461b:	8b 75 fc             	mov    -0x4(%ebp),%esi
  10461e:	89 ec                	mov    %ebp,%esp
  104620:	5d                   	pop    %ebp
  104621:	c3                   	ret    
  104622:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
  104628:	8d 45 f0             	lea    -0x10(%ebp),%eax
  10462b:	89 44 24 04          	mov    %eax,0x4(%esp)
  10462f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104636:	e8 35 f8 ff ff       	call   103e70 <argint>
  10463b:	85 c0                	test   %eax,%eax
  10463d:	78 d4                	js     104613 <sys_open+0x23>
    return -1;
  if(omode & O_CREATE){
  10463f:	f6 45 f1 02          	testb  $0x2,-0xf(%ebp)
  104643:	74 63                	je     1046a8 <sys_open+0xb8>
    if((ip = create(path, T_FILE, 0, 0)) == 0)
  104645:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104648:	31 c9                	xor    %ecx,%ecx
  10464a:	ba 02 00 00 00       	mov    $0x2,%edx
  10464f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104656:	e8 05 fc ff ff       	call   104260 <create>
  10465b:	85 c0                	test   %eax,%eax
  10465d:	89 c3                	mov    %eax,%ebx
  10465f:	74 b2                	je     104613 <sys_open+0x23>
      iunlockput(ip);
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
  104661:	e8 8a c8 ff ff       	call   100ef0 <filealloc>
  104666:	85 c0                	test   %eax,%eax
  104668:	89 c6                	mov    %eax,%esi
  10466a:	74 24                	je     104690 <sys_open+0xa0>
  10466c:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  104673:	31 c0                	xor    %eax,%eax
  104675:	8d 76 00             	lea    0x0(%esi),%esi
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd] == 0){
  104678:	8b 4c 82 28          	mov    0x28(%edx,%eax,4),%ecx
  10467c:	85 c9                	test   %ecx,%ecx
  10467e:	74 58                	je     1046d8 <sys_open+0xe8>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
  104680:	83 c0 01             	add    $0x1,%eax
  104683:	83 f8 10             	cmp    $0x10,%eax
  104686:	75 f0                	jne    104678 <sys_open+0x88>
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
    if(f)
      fileclose(f);
  104688:	89 34 24             	mov    %esi,(%esp)
  10468b:	e8 e0 c8 ff ff       	call   100f70 <fileclose>
    iunlockput(ip);
  104690:	89 1c 24             	mov    %ebx,(%esp)
  104693:	e8 38 d4 ff ff       	call   101ad0 <iunlockput>
  104698:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    return -1;
  10469d:	e9 76 ff ff ff       	jmp    104618 <sys_open+0x28>
  1046a2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
  if(omode & O_CREATE){
    if((ip = create(path, T_FILE, 0, 0)) == 0)
      return -1;
  } else {
    if((ip = namei(path)) == 0)
  1046a8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1046ab:	89 04 24             	mov    %eax,(%esp)
  1046ae:	e8 ad d7 ff ff       	call   101e60 <namei>
  1046b3:	85 c0                	test   %eax,%eax
  1046b5:	89 c3                	mov    %eax,%ebx
  1046b7:	0f 84 56 ff ff ff    	je     104613 <sys_open+0x23>
      return -1;
    ilock(ip);
  1046bd:	89 04 24             	mov    %eax,(%esp)
  1046c0:	e8 fb d4 ff ff       	call   101bc0 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
  1046c5:	66 83 7b 10 01       	cmpw   $0x1,0x10(%ebx)
  1046ca:	75 95                	jne    104661 <sys_open+0x71>
  1046cc:	8b 75 f0             	mov    -0x10(%ebp),%esi
  1046cf:	85 f6                	test   %esi,%esi
  1046d1:	74 8e                	je     104661 <sys_open+0x71>
  1046d3:	eb bb                	jmp    104690 <sys_open+0xa0>
  1046d5:	8d 76 00             	lea    0x0(%esi),%esi
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
  1046d8:	89 74 82 28          	mov    %esi,0x28(%edx,%eax,4)
    if(f)
      fileclose(f);
    iunlockput(ip);
    return -1;
  }
  iunlock(ip);
  1046dc:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  1046df:	89 1c 24             	mov    %ebx,(%esp)
  1046e2:	e8 99 d0 ff ff       	call   101780 <iunlock>

  f->type = FD_INODE;
  1046e7:	c7 06 02 00 00 00    	movl   $0x2,(%esi)
  f->ip = ip;
  1046ed:	89 5e 10             	mov    %ebx,0x10(%esi)
  f->off = 0;
  1046f0:	c7 46 14 00 00 00 00 	movl   $0x0,0x14(%esi)
  f->readable = !(omode & O_WRONLY);
  1046f7:	8b 55 f0             	mov    -0x10(%ebp),%edx
  1046fa:	83 f2 01             	xor    $0x1,%edx
  1046fd:	83 e2 01             	and    $0x1,%edx
  104700:	88 56 08             	mov    %dl,0x8(%esi)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
  104703:	f6 45 f0 03          	testb  $0x3,-0x10(%ebp)
  104707:	0f 95 46 09          	setne  0x9(%esi)
  return fd;
  10470b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  10470e:	e9 05 ff ff ff       	jmp    104618 <sys_open+0x28>
  104713:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  104719:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00104720 <sys_unlink>:
  return 1;
}

int
sys_unlink(void)
{
  104720:	55                   	push   %ebp
  104721:	89 e5                	mov    %esp,%ebp
  104723:	83 ec 78             	sub    $0x78,%esp
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
  104726:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  return 1;
}

int
sys_unlink(void)
{
  104729:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  10472c:	89 75 f8             	mov    %esi,-0x8(%ebp)
  10472f:	89 7d fc             	mov    %edi,-0x4(%ebp)
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
  104732:	89 44 24 04          	mov    %eax,0x4(%esp)
  104736:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  10473d:	e8 be f7 ff ff       	call   103f00 <argstr>
  104742:	85 c0                	test   %eax,%eax
  104744:	79 12                	jns    104758 <sys_unlink+0x38>
  iunlockput(dp);

  ip->nlink--;
  iupdate(ip);
  iunlockput(ip);
  return 0;
  104746:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  10474b:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  10474e:	8b 75 f8             	mov    -0x8(%ebp),%esi
  104751:	8b 7d fc             	mov    -0x4(%ebp),%edi
  104754:	89 ec                	mov    %ebp,%esp
  104756:	5d                   	pop    %ebp
  104757:	c3                   	ret    
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
    return -1;
  if((dp = nameiparent(path, name)) == 0)
  104758:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  10475b:	8d 5d d2             	lea    -0x2e(%ebp),%ebx
  10475e:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  104762:	89 04 24             	mov    %eax,(%esp)
  104765:	e8 d6 d6 ff ff       	call   101e40 <nameiparent>
  10476a:	85 c0                	test   %eax,%eax
  10476c:	89 45 a4             	mov    %eax,-0x5c(%ebp)
  10476f:	74 d5                	je     104746 <sys_unlink+0x26>
    return -1;
  ilock(dp);
  104771:	89 04 24             	mov    %eax,(%esp)
  104774:	e8 47 d4 ff ff       	call   101bc0 <ilock>

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0){
  104779:	c7 44 24 04 08 6b 10 	movl   $0x106b08,0x4(%esp)
  104780:	00 
  104781:	89 1c 24             	mov    %ebx,(%esp)
  104784:	e8 c7 ce ff ff       	call   101650 <namecmp>
  104789:	85 c0                	test   %eax,%eax
  10478b:	0f 84 97 00 00 00    	je     104828 <sys_unlink+0x108>
  104791:	c7 44 24 04 07 6b 10 	movl   $0x106b07,0x4(%esp)
  104798:	00 
  104799:	89 1c 24             	mov    %ebx,(%esp)
  10479c:	e8 af ce ff ff       	call   101650 <namecmp>
  1047a1:	85 c0                	test   %eax,%eax
  1047a3:	0f 84 7f 00 00 00    	je     104828 <sys_unlink+0x108>
    iunlockput(dp);
    return -1;
  }

  if((ip = dirlookup(dp, name, &off)) == 0){
  1047a9:	8d 45 e0             	lea    -0x20(%ebp),%eax
  1047ac:	89 44 24 08          	mov    %eax,0x8(%esp)
  1047b0:	8b 45 a4             	mov    -0x5c(%ebp),%eax
  1047b3:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  1047b7:	89 04 24             	mov    %eax,(%esp)
  1047ba:	e8 c1 ce ff ff       	call   101680 <dirlookup>
  1047bf:	85 c0                	test   %eax,%eax
  1047c1:	89 c6                	mov    %eax,%esi
  1047c3:	74 63                	je     104828 <sys_unlink+0x108>
    iunlockput(dp);
    return -1;
  }
  ilock(ip);
  1047c5:	89 04 24             	mov    %eax,(%esp)
  1047c8:	e8 f3 d3 ff ff       	call   101bc0 <ilock>

  if(ip->nlink < 1)
  1047cd:	66 83 7e 16 00       	cmpw   $0x0,0x16(%esi)
  1047d2:	0f 8e fe 00 00 00    	jle    1048d6 <sys_unlink+0x1b6>
    panic("unlink: nlink < 1");
  if(ip->type == T_DIR && !isdirempty(ip)){
  1047d8:	66 83 7e 10 01       	cmpw   $0x1,0x10(%esi)
  1047dd:	75 61                	jne    104840 <sys_unlink+0x120>
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
  1047df:	83 7e 18 20          	cmpl   $0x20,0x18(%esi)
  1047e3:	76 5b                	jbe    104840 <sys_unlink+0x120>
  1047e5:	8d 7d b2             	lea    -0x4e(%ebp),%edi
  1047e8:	bb 20 00 00 00       	mov    $0x20,%ebx
  1047ed:	eb 09                	jmp    1047f8 <sys_unlink+0xd8>
  1047ef:	90                   	nop
  1047f0:	83 c3 10             	add    $0x10,%ebx
  1047f3:	3b 5e 18             	cmp    0x18(%esi),%ebx
  1047f6:	73 48                	jae    104840 <sys_unlink+0x120>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
  1047f8:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
  1047ff:	00 
  104800:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  104804:	89 7c 24 04          	mov    %edi,0x4(%esp)
  104808:	89 34 24             	mov    %esi,(%esp)
  10480b:	e8 60 cb ff ff       	call   101370 <readi>
  104810:	83 f8 10             	cmp    $0x10,%eax
  104813:	0f 85 a5 00 00 00    	jne    1048be <sys_unlink+0x19e>
      panic("isdirempty: readi");
    if(de.inum != 0)
  104819:	66 83 7d b2 00       	cmpw   $0x0,-0x4e(%ebp)
  10481e:	74 d0                	je     1047f0 <sys_unlink+0xd0>
  ilock(ip);

  if(ip->nlink < 1)
    panic("unlink: nlink < 1");
  if(ip->type == T_DIR && !isdirempty(ip)){
    iunlockput(ip);
  104820:	89 34 24             	mov    %esi,(%esp)
  104823:	e8 a8 d2 ff ff       	call   101ad0 <iunlockput>
    iunlockput(dp);
  104828:	8b 45 a4             	mov    -0x5c(%ebp),%eax
  10482b:	89 04 24             	mov    %eax,(%esp)
  10482e:	e8 9d d2 ff ff       	call   101ad0 <iunlockput>
  104833:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    return -1;
  104838:	e9 0e ff ff ff       	jmp    10474b <sys_unlink+0x2b>
  10483d:	8d 76 00             	lea    0x0(%esi),%esi
  }

  memset(&de, 0, sizeof(de));
  104840:	8d 5d c2             	lea    -0x3e(%ebp),%ebx
  104843:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
  10484a:	00 
  10484b:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  104852:	00 
  104853:	89 1c 24             	mov    %ebx,(%esp)
  104856:	e8 75 f3 ff ff       	call   103bd0 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
  10485b:	8b 45 e0             	mov    -0x20(%ebp),%eax
  10485e:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
  104865:	00 
  104866:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  10486a:	89 44 24 08          	mov    %eax,0x8(%esp)
  10486e:	8b 45 a4             	mov    -0x5c(%ebp),%eax
  104871:	89 04 24             	mov    %eax,(%esp)
  104874:	e8 97 cc ff ff       	call   101510 <writei>
  104879:	83 f8 10             	cmp    $0x10,%eax
  10487c:	75 4c                	jne    1048ca <sys_unlink+0x1aa>
    panic("unlink: writei");
  if(ip->type == T_DIR){
  10487e:	66 83 7e 10 01       	cmpw   $0x1,0x10(%esi)
  104883:	74 27                	je     1048ac <sys_unlink+0x18c>
    dp->nlink--;
    iupdate(dp);
  }
  iunlockput(dp);
  104885:	8b 45 a4             	mov    -0x5c(%ebp),%eax
  104888:	89 04 24             	mov    %eax,(%esp)
  10488b:	e8 40 d2 ff ff       	call   101ad0 <iunlockput>

  ip->nlink--;
  104890:	66 83 6e 16 01       	subw   $0x1,0x16(%esi)
  iupdate(ip);
  104895:	89 34 24             	mov    %esi,(%esp)
  104898:	e8 e3 cb ff ff       	call   101480 <iupdate>
  iunlockput(ip);
  10489d:	89 34 24             	mov    %esi,(%esp)
  1048a0:	e8 2b d2 ff ff       	call   101ad0 <iunlockput>
  1048a5:	31 c0                	xor    %eax,%eax
  return 0;
  1048a7:	e9 9f fe ff ff       	jmp    10474b <sys_unlink+0x2b>

  memset(&de, 0, sizeof(de));
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
    panic("unlink: writei");
  if(ip->type == T_DIR){
    dp->nlink--;
  1048ac:	8b 45 a4             	mov    -0x5c(%ebp),%eax
  1048af:	66 83 68 16 01       	subw   $0x1,0x16(%eax)
    iupdate(dp);
  1048b4:	89 04 24             	mov    %eax,(%esp)
  1048b7:	e8 c4 cb ff ff       	call   101480 <iupdate>
  1048bc:	eb c7                	jmp    104885 <sys_unlink+0x165>
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
  1048be:	c7 04 24 38 6b 10 00 	movl   $0x106b38,(%esp)
  1048c5:	e8 46 c0 ff ff       	call   100910 <panic>
    return -1;
  }

  memset(&de, 0, sizeof(de));
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
    panic("unlink: writei");
  1048ca:	c7 04 24 4a 6b 10 00 	movl   $0x106b4a,(%esp)
  1048d1:	e8 3a c0 ff ff       	call   100910 <panic>
    return -1;
  }
  ilock(ip);

  if(ip->nlink < 1)
    panic("unlink: nlink < 1");
  1048d6:	c7 04 24 26 6b 10 00 	movl   $0x106b26,(%esp)
  1048dd:	e8 2e c0 ff ff       	call   100910 <panic>
  1048e2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  1048e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

001048f0 <argfd.clone.0>:
#include "fcntl.h"

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
  1048f0:	55                   	push   %ebp
  1048f1:	89 e5                	mov    %esp,%ebp
  1048f3:	83 ec 28             	sub    $0x28,%esp
  1048f6:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  1048f9:	89 c3                	mov    %eax,%ebx
{
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
  1048fb:	8d 45 f4             	lea    -0xc(%ebp),%eax
#include "fcntl.h"

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
  1048fe:	89 75 fc             	mov    %esi,-0x4(%ebp)
  104901:	89 d6                	mov    %edx,%esi
{
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
  104903:	89 44 24 04          	mov    %eax,0x4(%esp)
  104907:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  10490e:	e8 5d f5 ff ff       	call   103e70 <argint>
  104913:	85 c0                	test   %eax,%eax
  104915:	79 11                	jns    104928 <argfd.clone.0+0x38>
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
    return -1;
  if(pfd)
    *pfd = fd;
  if(pf)
    *pf = f;
  104917:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return 0;
}
  10491c:	8b 5d f8             	mov    -0x8(%ebp),%ebx
  10491f:	8b 75 fc             	mov    -0x4(%ebp),%esi
  104922:	89 ec                	mov    %ebp,%esp
  104924:	5d                   	pop    %ebp
  104925:	c3                   	ret    
  104926:	66 90                	xchg   %ax,%ax
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
    return -1;
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
  104928:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10492b:	83 f8 0f             	cmp    $0xf,%eax
  10492e:	77 e7                	ja     104917 <argfd.clone.0+0x27>
  104930:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  104937:	8b 54 82 28          	mov    0x28(%edx,%eax,4),%edx
  10493b:	85 d2                	test   %edx,%edx
  10493d:	74 d8                	je     104917 <argfd.clone.0+0x27>
    return -1;
  if(pfd)
  10493f:	85 db                	test   %ebx,%ebx
  104941:	74 02                	je     104945 <argfd.clone.0+0x55>
    *pfd = fd;
  104943:	89 03                	mov    %eax,(%ebx)
  if(pf)
  104945:	31 c0                	xor    %eax,%eax
  104947:	85 f6                	test   %esi,%esi
  104949:	74 d1                	je     10491c <argfd.clone.0+0x2c>
    *pf = f;
  10494b:	89 16                	mov    %edx,(%esi)
  10494d:	eb cd                	jmp    10491c <argfd.clone.0+0x2c>
  10494f:	90                   	nop

00104950 <sys_dup>:
  return -1;
}

int
sys_dup(void)
{
  104950:	55                   	push   %ebp
  struct file *f;
  int fd;
  
  if(argfd(0, 0, &f) < 0)
  104951:	31 c0                	xor    %eax,%eax
  return -1;
}

int
sys_dup(void)
{
  104953:	89 e5                	mov    %esp,%ebp
  104955:	53                   	push   %ebx
  104956:	83 ec 24             	sub    $0x24,%esp
  struct file *f;
  int fd;
  
  if(argfd(0, 0, &f) < 0)
  104959:	8d 55 f4             	lea    -0xc(%ebp),%edx
  10495c:	e8 8f ff ff ff       	call   1048f0 <argfd.clone.0>
  104961:	85 c0                	test   %eax,%eax
  104963:	79 13                	jns    104978 <sys_dup+0x28>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
  104965:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
    return -1;
  if((fd=fdalloc(f)) < 0)
    return -1;
  filedup(f);
  return fd;
}
  10496a:	89 d8                	mov    %ebx,%eax
  10496c:	83 c4 24             	add    $0x24,%esp
  10496f:	5b                   	pop    %ebx
  104970:	5d                   	pop    %ebp
  104971:	c3                   	ret    
  104972:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  struct file *f;
  int fd;
  
  if(argfd(0, 0, &f) < 0)
    return -1;
  if((fd=fdalloc(f)) < 0)
  104978:	8b 55 f4             	mov    -0xc(%ebp),%edx
  10497b:	31 db                	xor    %ebx,%ebx
  10497d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104983:	eb 0b                	jmp    104990 <sys_dup+0x40>
  104985:	8d 76 00             	lea    0x0(%esi),%esi
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
  104988:	83 c3 01             	add    $0x1,%ebx
  10498b:	83 fb 10             	cmp    $0x10,%ebx
  10498e:	74 d5                	je     104965 <sys_dup+0x15>
    if(proc->ofile[fd] == 0){
  104990:	8b 4c 98 28          	mov    0x28(%eax,%ebx,4),%ecx
  104994:	85 c9                	test   %ecx,%ecx
  104996:	75 f0                	jne    104988 <sys_dup+0x38>
      proc->ofile[fd] = f;
  104998:	89 54 98 28          	mov    %edx,0x28(%eax,%ebx,4)
  
  if(argfd(0, 0, &f) < 0)
    return -1;
  if((fd=fdalloc(f)) < 0)
    return -1;
  filedup(f);
  10499c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10499f:	89 04 24             	mov    %eax,(%esp)
  1049a2:	e8 f9 c4 ff ff       	call   100ea0 <filedup>
  return fd;
  1049a7:	eb c1                	jmp    10496a <sys_dup+0x1a>
  1049a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

001049b0 <sys_read>:
}

int
sys_read(void)
{
  1049b0:	55                   	push   %ebp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
  1049b1:	31 c0                	xor    %eax,%eax
  return fd;
}

int
sys_read(void)
{
  1049b3:	89 e5                	mov    %esp,%ebp
  1049b5:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
  1049b8:	8d 55 f4             	lea    -0xc(%ebp),%edx
  1049bb:	e8 30 ff ff ff       	call   1048f0 <argfd.clone.0>
  1049c0:	85 c0                	test   %eax,%eax
  1049c2:	79 0c                	jns    1049d0 <sys_read+0x20>
    return -1;
  return fileread(f, p, n);
  1049c4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  1049c9:	c9                   	leave  
  1049ca:	c3                   	ret    
  1049cb:	90                   	nop
  1049cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
  1049d0:	8d 45 f0             	lea    -0x10(%ebp),%eax
  1049d3:	89 44 24 04          	mov    %eax,0x4(%esp)
  1049d7:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  1049de:	e8 8d f4 ff ff       	call   103e70 <argint>
  1049e3:	85 c0                	test   %eax,%eax
  1049e5:	78 dd                	js     1049c4 <sys_read+0x14>
  1049e7:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1049ea:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  1049f1:	89 44 24 08          	mov    %eax,0x8(%esp)
  1049f5:	8d 45 ec             	lea    -0x14(%ebp),%eax
  1049f8:	89 44 24 04          	mov    %eax,0x4(%esp)
  1049fc:	e8 af f4 ff ff       	call   103eb0 <argptr>
  104a01:	85 c0                	test   %eax,%eax
  104a03:	78 bf                	js     1049c4 <sys_read+0x14>
    return -1;
  return fileread(f, p, n);
  104a05:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104a08:	89 44 24 08          	mov    %eax,0x8(%esp)
  104a0c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104a0f:	89 44 24 04          	mov    %eax,0x4(%esp)
  104a13:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104a16:	89 04 24             	mov    %eax,(%esp)
  104a19:	e8 72 c3 ff ff       	call   100d90 <fileread>
}
  104a1e:	c9                   	leave  
  104a1f:	c3                   	ret    

00104a20 <sys_write>:

int
sys_write(void)
{
  104a20:	55                   	push   %ebp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
  104a21:	31 c0                	xor    %eax,%eax
  return fileread(f, p, n);
}

int
sys_write(void)
{
  104a23:	89 e5                	mov    %esp,%ebp
  104a25:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
  104a28:	8d 55 f4             	lea    -0xc(%ebp),%edx
  104a2b:	e8 c0 fe ff ff       	call   1048f0 <argfd.clone.0>
  104a30:	85 c0                	test   %eax,%eax
  104a32:	79 0c                	jns    104a40 <sys_write+0x20>
    return -1;
  return filewrite(f, p, n);
  104a34:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  104a39:	c9                   	leave  
  104a3a:	c3                   	ret    
  104a3b:	90                   	nop
  104a3c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
  104a40:	8d 45 f0             	lea    -0x10(%ebp),%eax
  104a43:	89 44 24 04          	mov    %eax,0x4(%esp)
  104a47:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  104a4e:	e8 1d f4 ff ff       	call   103e70 <argint>
  104a53:	85 c0                	test   %eax,%eax
  104a55:	78 dd                	js     104a34 <sys_write+0x14>
  104a57:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104a5a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104a61:	89 44 24 08          	mov    %eax,0x8(%esp)
  104a65:	8d 45 ec             	lea    -0x14(%ebp),%eax
  104a68:	89 44 24 04          	mov    %eax,0x4(%esp)
  104a6c:	e8 3f f4 ff ff       	call   103eb0 <argptr>
  104a71:	85 c0                	test   %eax,%eax
  104a73:	78 bf                	js     104a34 <sys_write+0x14>
    return -1;
  return filewrite(f, p, n);
  104a75:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104a78:	89 44 24 08          	mov    %eax,0x8(%esp)
  104a7c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104a7f:	89 44 24 04          	mov    %eax,0x4(%esp)
  104a83:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104a86:	89 04 24             	mov    %eax,(%esp)
  104a89:	e8 42 c2 ff ff       	call   100cd0 <filewrite>
}
  104a8e:	c9                   	leave  
  104a8f:	c3                   	ret    

00104a90 <sys_fstat>:
  return 0;
}

int
sys_fstat(void)
{
  104a90:	55                   	push   %ebp
  struct file *f;
  struct stat *st;
  
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
  104a91:	31 c0                	xor    %eax,%eax
  return 0;
}

int
sys_fstat(void)
{
  104a93:	89 e5                	mov    %esp,%ebp
  104a95:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  struct stat *st;
  
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
  104a98:	8d 55 f4             	lea    -0xc(%ebp),%edx
  104a9b:	e8 50 fe ff ff       	call   1048f0 <argfd.clone.0>
  104aa0:	85 c0                	test   %eax,%eax
  104aa2:	79 0c                	jns    104ab0 <sys_fstat+0x20>
    return -1;
  return filestat(f, st);
  104aa4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  104aa9:	c9                   	leave  
  104aaa:	c3                   	ret    
  104aab:	90                   	nop
  104aac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
sys_fstat(void)
{
  struct file *f;
  struct stat *st;
  
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
  104ab0:	8d 45 f0             	lea    -0x10(%ebp),%eax
  104ab3:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
  104aba:	00 
  104abb:	89 44 24 04          	mov    %eax,0x4(%esp)
  104abf:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104ac6:	e8 e5 f3 ff ff       	call   103eb0 <argptr>
  104acb:	85 c0                	test   %eax,%eax
  104acd:	78 d5                	js     104aa4 <sys_fstat+0x14>
    return -1;
  return filestat(f, st);
  104acf:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104ad2:	89 44 24 04          	mov    %eax,0x4(%esp)
  104ad6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104ad9:	89 04 24             	mov    %eax,(%esp)
  104adc:	e8 6f c3 ff ff       	call   100e50 <filestat>
}
  104ae1:	c9                   	leave  
  104ae2:	c3                   	ret    
  104ae3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  104ae9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00104af0 <sys_close>:
  return filewrite(f, p, n);
}

int
sys_close(void)
{
  104af0:	55                   	push   %ebp
  104af1:	89 e5                	mov    %esp,%ebp
  104af3:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;
  
  if(argfd(0, &fd, &f) < 0)
  104af6:	8d 55 f0             	lea    -0x10(%ebp),%edx
  104af9:	8d 45 f4             	lea    -0xc(%ebp),%eax
  104afc:	e8 ef fd ff ff       	call   1048f0 <argfd.clone.0>
  104b01:	89 c2                	mov    %eax,%edx
  104b03:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  104b08:	85 d2                	test   %edx,%edx
  104b0a:	78 1e                	js     104b2a <sys_close+0x3a>
    return -1;
  proc->ofile[fd] = 0;
  104b0c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104b12:	8b 55 f4             	mov    -0xc(%ebp),%edx
  104b15:	c7 44 90 28 00 00 00 	movl   $0x0,0x28(%eax,%edx,4)
  104b1c:	00 
  fileclose(f);
  104b1d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104b20:	89 04 24             	mov    %eax,(%esp)
  104b23:	e8 48 c4 ff ff       	call   100f70 <fileclose>
  104b28:	31 c0                	xor    %eax,%eax
  return 0;
}
  104b2a:	c9                   	leave  
  104b2b:	c3                   	ret    
  104b2c:	90                   	nop
  104b2d:	90                   	nop
  104b2e:	90                   	nop
  104b2f:	90                   	nop

00104b30 <sys_getpid>:
}

int
sys_getpid(void)
{
  return proc->pid;
  104b30:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  return kill(pid);
}

int
sys_getpid(void)
{
  104b36:	55                   	push   %ebp
  104b37:	89 e5                	mov    %esp,%ebp
  return proc->pid;
}
  104b39:	5d                   	pop    %ebp
}

int
sys_getpid(void)
{
  return proc->pid;
  104b3a:	8b 40 10             	mov    0x10(%eax),%eax
}
  104b3d:	c3                   	ret    
  104b3e:	66 90                	xchg   %ax,%ax

00104b40 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since boot.
int
sys_uptime(void)
{
  104b40:	55                   	push   %ebp
  104b41:	89 e5                	mov    %esp,%ebp
  104b43:	53                   	push   %ebx
  104b44:	83 ec 14             	sub    $0x14,%esp
  uint xticks;
  
  acquire(&tickslock);
  104b47:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
  104b4e:	e8 dd ef ff ff       	call   103b30 <acquire>
  xticks = ticks;
  104b53:	8b 1d a0 e8 10 00    	mov    0x10e8a0,%ebx
  release(&tickslock);
  104b59:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
  104b60:	e8 7b ef ff ff       	call   103ae0 <release>
  return xticks;
}
  104b65:	83 c4 14             	add    $0x14,%esp
  104b68:	89 d8                	mov    %ebx,%eax
  104b6a:	5b                   	pop    %ebx
  104b6b:	5d                   	pop    %ebp
  104b6c:	c3                   	ret    
  104b6d:	8d 76 00             	lea    0x0(%esi),%esi

00104b70 <sys_sleep>:
  return addr;
}

int
sys_sleep(void)
{
  104b70:	55                   	push   %ebp
  104b71:	89 e5                	mov    %esp,%ebp
  104b73:	53                   	push   %ebx
  104b74:	83 ec 24             	sub    $0x24,%esp
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
  104b77:	8d 45 f4             	lea    -0xc(%ebp),%eax
  104b7a:	89 44 24 04          	mov    %eax,0x4(%esp)
  104b7e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104b85:	e8 e6 f2 ff ff       	call   103e70 <argint>
  104b8a:	89 c2                	mov    %eax,%edx
  104b8c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  104b91:	85 d2                	test   %edx,%edx
  104b93:	78 59                	js     104bee <sys_sleep+0x7e>
    return -1;
  acquire(&tickslock);
  104b95:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
  104b9c:	e8 8f ef ff ff       	call   103b30 <acquire>
  ticks0 = ticks;
  while(ticks - ticks0 < n){
  104ba1:	8b 55 f4             	mov    -0xc(%ebp),%edx
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  104ba4:	8b 1d a0 e8 10 00    	mov    0x10e8a0,%ebx
  while(ticks - ticks0 < n){
  104baa:	85 d2                	test   %edx,%edx
  104bac:	75 22                	jne    104bd0 <sys_sleep+0x60>
  104bae:	eb 48                	jmp    104bf8 <sys_sleep+0x88>
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  104bb0:	c7 44 24 04 60 e0 10 	movl   $0x10e060,0x4(%esp)
  104bb7:	00 
  104bb8:	c7 04 24 a0 e8 10 00 	movl   $0x10e8a0,(%esp)
  104bbf:	e8 9c e6 ff ff       	call   103260 <sleep>
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
  104bc4:	a1 a0 e8 10 00       	mov    0x10e8a0,%eax
  104bc9:	29 d8                	sub    %ebx,%eax
  104bcb:	3b 45 f4             	cmp    -0xc(%ebp),%eax
  104bce:	73 28                	jae    104bf8 <sys_sleep+0x88>
    if(proc->killed){
  104bd0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104bd6:	8b 40 24             	mov    0x24(%eax),%eax
  104bd9:	85 c0                	test   %eax,%eax
  104bdb:	74 d3                	je     104bb0 <sys_sleep+0x40>
      release(&tickslock);
  104bdd:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
  104be4:	e8 f7 ee ff ff       	call   103ae0 <release>
  104be9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}
  104bee:	83 c4 24             	add    $0x24,%esp
  104bf1:	5b                   	pop    %ebx
  104bf2:	5d                   	pop    %ebp
  104bf3:	c3                   	ret    
  104bf4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  104bf8:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
  104bff:	e8 dc ee ff ff       	call   103ae0 <release>
  return 0;
}
  104c04:	83 c4 24             	add    $0x24,%esp
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  104c07:	31 c0                	xor    %eax,%eax
  return 0;
}
  104c09:	5b                   	pop    %ebx
  104c0a:	5d                   	pop    %ebp
  104c0b:	c3                   	ret    
  104c0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00104c10 <sys_sbrk>:
  return proc->pid;
}

int
sys_sbrk(void)
{
  104c10:	55                   	push   %ebp
  104c11:	89 e5                	mov    %esp,%ebp
  104c13:	53                   	push   %ebx
  104c14:	83 ec 24             	sub    $0x24,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
  104c17:	8d 45 f4             	lea    -0xc(%ebp),%eax
  104c1a:	89 44 24 04          	mov    %eax,0x4(%esp)
  104c1e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104c25:	e8 46 f2 ff ff       	call   103e70 <argint>
  104c2a:	85 c0                	test   %eax,%eax
  104c2c:	79 12                	jns    104c40 <sys_sbrk+0x30>
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
  104c2e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  104c33:	83 c4 24             	add    $0x24,%esp
  104c36:	5b                   	pop    %ebx
  104c37:	5d                   	pop    %ebp
  104c38:	c3                   	ret    
  104c39:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  104c40:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104c46:	8b 18                	mov    (%eax),%ebx
  if(growproc(n) < 0)
  104c48:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104c4b:	89 04 24             	mov    %eax,(%esp)
  104c4e:	e8 bd eb ff ff       	call   103810 <growproc>
  104c53:	89 c2                	mov    %eax,%edx
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  104c55:	89 d8                	mov    %ebx,%eax
  if(growproc(n) < 0)
  104c57:	85 d2                	test   %edx,%edx
  104c59:	79 d8                	jns    104c33 <sys_sbrk+0x23>
  104c5b:	eb d1                	jmp    104c2e <sys_sbrk+0x1e>
  104c5d:	8d 76 00             	lea    0x0(%esi),%esi

00104c60 <sys_kill>:
  return wait();
}

int
sys_kill(void)
{
  104c60:	55                   	push   %ebp
  104c61:	89 e5                	mov    %esp,%ebp
  104c63:	83 ec 28             	sub    $0x28,%esp
  int pid;

  if(argint(0, &pid) < 0)
  104c66:	8d 45 f4             	lea    -0xc(%ebp),%eax
  104c69:	89 44 24 04          	mov    %eax,0x4(%esp)
  104c6d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104c74:	e8 f7 f1 ff ff       	call   103e70 <argint>
  104c79:	89 c2                	mov    %eax,%edx
  104c7b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  104c80:	85 d2                	test   %edx,%edx
  104c82:	78 0b                	js     104c8f <sys_kill+0x2f>
    return -1;
  return kill(pid);
  104c84:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104c87:	89 04 24             	mov    %eax,(%esp)
  104c8a:	e8 21 e4 ff ff       	call   1030b0 <kill>
}
  104c8f:	c9                   	leave  
  104c90:	c3                   	ret    
  104c91:	eb 0d                	jmp    104ca0 <sys_wait>
  104c93:	90                   	nop
  104c94:	90                   	nop
  104c95:	90                   	nop
  104c96:	90                   	nop
  104c97:	90                   	nop
  104c98:	90                   	nop
  104c99:	90                   	nop
  104c9a:	90                   	nop
  104c9b:	90                   	nop
  104c9c:	90                   	nop
  104c9d:	90                   	nop
  104c9e:	90                   	nop
  104c9f:	90                   	nop

00104ca0 <sys_wait>:
  return 0;  // not reached
}

int
sys_wait(void)
{
  104ca0:	55                   	push   %ebp
  104ca1:	89 e5                	mov    %esp,%ebp
  104ca3:	83 ec 08             	sub    $0x8,%esp
  return wait();
}
  104ca6:	c9                   	leave  
}

int
sys_wait(void)
{
  return wait();
  104ca7:	e9 64 e7 ff ff       	jmp    103410 <wait>
  104cac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00104cb0 <sys_exit>:
  return fork();
}

int
sys_exit(void)
{
  104cb0:	55                   	push   %ebp
  104cb1:	89 e5                	mov    %esp,%ebp
  104cb3:	83 ec 08             	sub    $0x8,%esp
  exit();
  104cb6:	e8 45 e8 ff ff       	call   103500 <exit>
  return 0;  // not reached
}
  104cbb:	31 c0                	xor    %eax,%eax
  104cbd:	c9                   	leave  
  104cbe:	c3                   	ret    
  104cbf:	90                   	nop

00104cc0 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  104cc0:	55                   	push   %ebp
  104cc1:	89 e5                	mov    %esp,%ebp
  104cc3:	83 ec 08             	sub    $0x8,%esp
  return fork();
}
  104cc6:	c9                   	leave  
#include "proc.h"

int
sys_fork(void)
{
  return fork();
  104cc7:	e9 44 ea ff ff       	jmp    103710 <fork>
  104ccc:	90                   	nop
  104ccd:	90                   	nop
  104cce:	90                   	nop
  104ccf:	90                   	nop

00104cd0 <timerinit>:
#define TIMER_RATEGEN   0x04    // mode 2, rate generator
#define TIMER_16BIT     0x30    // r/w counter 16 bits, LSB first

void
timerinit(void)
{
  104cd0:	55                   	push   %ebp
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  104cd1:	ba 43 00 00 00       	mov    $0x43,%edx
  104cd6:	89 e5                	mov    %esp,%ebp
  104cd8:	83 ec 18             	sub    $0x18,%esp
  104cdb:	b8 34 00 00 00       	mov    $0x34,%eax
  104ce0:	ee                   	out    %al,(%dx)
  104ce1:	b8 9c ff ff ff       	mov    $0xffffff9c,%eax
  104ce6:	b2 40                	mov    $0x40,%dl
  104ce8:	ee                   	out    %al,(%dx)
  104ce9:	b8 2e 00 00 00       	mov    $0x2e,%eax
  104cee:	ee                   	out    %al,(%dx)
  // Interrupt 100 times/sec.
  outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
  outb(IO_TIMER1, TIMER_DIV(100) % 256);
  outb(IO_TIMER1, TIMER_DIV(100) / 256);
  picenable(IRQ_TIMER);
  104cef:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104cf6:	e8 a5 de ff ff       	call   102ba0 <picenable>
}
  104cfb:	c9                   	leave  
  104cfc:	c3                   	ret    
  104cfd:	90                   	nop
  104cfe:	90                   	nop
  104cff:	90                   	nop

00104d00 <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
  104d00:	1e                   	push   %ds
  pushl %es
  104d01:	06                   	push   %es
  pushl %fs
  104d02:	0f a0                	push   %fs
  pushl %gs
  104d04:	0f a8                	push   %gs
  pushal
  104d06:	60                   	pusha  
  
  # Set up data and per-cpu segments.
  movw $(SEG_KDATA<<3), %ax
  104d07:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
  104d0b:	8e d8                	mov    %eax,%ds
  movw %ax, %es
  104d0d:	8e c0                	mov    %eax,%es
  movw $(SEG_KCPU<<3), %ax
  104d0f:	66 b8 18 00          	mov    $0x18,%ax
  movw %ax, %fs
  104d13:	8e e0                	mov    %eax,%fs
  movw %ax, %gs
  104d15:	8e e8                	mov    %eax,%gs

  # Call trap(tf), where tf=%esp
  pushl %esp
  104d17:	54                   	push   %esp
  call trap
  104d18:	e8 43 00 00 00       	call   104d60 <trap>
  addl $4, %esp
  104d1d:	83 c4 04             	add    $0x4,%esp

00104d20 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
  104d20:	61                   	popa   
  popl %gs
  104d21:	0f a9                	pop    %gs
  popl %fs
  104d23:	0f a1                	pop    %fs
  popl %es
  104d25:	07                   	pop    %es
  popl %ds
  104d26:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
  104d27:	83 c4 08             	add    $0x8,%esp
  iret
  104d2a:	cf                   	iret   
  104d2b:	90                   	nop
  104d2c:	90                   	nop
  104d2d:	90                   	nop
  104d2e:	90                   	nop
  104d2f:	90                   	nop

00104d30 <idtinit>:
  initlock(&tickslock, "time");
}

void
idtinit(void)
{
  104d30:	55                   	push   %ebp
lidt(struct gatedesc *p, int size)
{
  volatile ushort pd[3];

  pd[0] = size-1;
  pd[1] = (uint)p;
  104d31:	b8 a0 e0 10 00       	mov    $0x10e0a0,%eax
  104d36:	89 e5                	mov    %esp,%ebp
  104d38:	83 ec 10             	sub    $0x10,%esp
static inline void
lidt(struct gatedesc *p, int size)
{
  volatile ushort pd[3];

  pd[0] = size-1;
  104d3b:	66 c7 45 fa ff 07    	movw   $0x7ff,-0x6(%ebp)
  pd[1] = (uint)p;
  104d41:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
  104d45:	c1 e8 10             	shr    $0x10,%eax
  104d48:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lidt (%0)" : : "r" (pd));
  104d4c:	8d 45 fa             	lea    -0x6(%ebp),%eax
  104d4f:	0f 01 18             	lidtl  (%eax)
  lidt(idt, sizeof(idt));
}
  104d52:	c9                   	leave  
  104d53:	c3                   	ret    
  104d54:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  104d5a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00104d60 <trap>:

void
trap(struct trapframe *tf)
{
  104d60:	55                   	push   %ebp
  104d61:	89 e5                	mov    %esp,%ebp
  104d63:	56                   	push   %esi
  104d64:	53                   	push   %ebx
  104d65:	83 ec 20             	sub    $0x20,%esp
  104d68:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(tf->trapno == T_SYSCALL){
  104d6b:	8b 43 30             	mov    0x30(%ebx),%eax
  104d6e:	83 f8 40             	cmp    $0x40,%eax
  104d71:	0f 84 c9 00 00 00    	je     104e40 <trap+0xe0>
    if(proc->killed)
      exit();
    return;
  }

  switch(tf->trapno){
  104d77:	8d 50 e0             	lea    -0x20(%eax),%edx
  104d7a:	83 fa 1f             	cmp    $0x1f,%edx
  104d7d:	0f 86 b5 00 00 00    	jbe    104e38 <trap+0xd8>
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
    break;
   
  default:
    if(proc == 0 || (tf->cs&3) == 0){
  104d83:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
  104d8a:	85 d2                	test   %edx,%edx
  104d8c:	0f 84 de 01 00 00    	je     104f70 <trap+0x210>
  104d92:	f6 43 3c 03          	testb  $0x3,0x3c(%ebx)
  104d96:	0f 84 d4 01 00 00    	je     104f70 <trap+0x210>

static inline uint
rcr2(void)
{
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
  104d9c:	0f 20 d6             	mov    %cr2,%esi
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
  104d9f:	8b 4a 10             	mov    0x10(%edx),%ecx
  104da2:	83 c2 6c             	add    $0x6c,%edx
  104da5:	89 74 24 1c          	mov    %esi,0x1c(%esp)
  104da9:	8b 73 38             	mov    0x38(%ebx),%esi
  104dac:	89 74 24 18          	mov    %esi,0x18(%esp)
  104db0:	65 8b 35 00 00 00 00 	mov    %gs:0x0,%esi
  104db7:	0f b6 36             	movzbl (%esi),%esi
  104dba:	89 74 24 14          	mov    %esi,0x14(%esp)
  104dbe:	8b 73 34             	mov    0x34(%ebx),%esi
  104dc1:	89 44 24 0c          	mov    %eax,0xc(%esp)
  104dc5:	89 54 24 08          	mov    %edx,0x8(%esp)
  104dc9:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  104dcd:	89 74 24 10          	mov    %esi,0x10(%esp)
  104dd1:	c7 04 24 b4 6b 10 00 	movl   $0x106bb4,(%esp)
  104dd8:	e8 53 b7 ff ff       	call   100530 <cprintf>
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
            rcr2());
    proc->killed = 1;
  104ddd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104de3:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
  104dea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running 
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
  104df0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104df6:	85 c0                	test   %eax,%eax
  104df8:	74 34                	je     104e2e <trap+0xce>
  104dfa:	8b 50 24             	mov    0x24(%eax),%edx
  104dfd:	85 d2                	test   %edx,%edx
  104dff:	74 10                	je     104e11 <trap+0xb1>
  104e01:	0f b7 53 3c          	movzwl 0x3c(%ebx),%edx
  104e05:	83 e2 03             	and    $0x3,%edx
  104e08:	83 fa 03             	cmp    $0x3,%edx
  104e0b:	0f 84 47 01 00 00    	je     104f58 <trap+0x1f8>
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
  104e11:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
  104e15:	0f 84 15 01 00 00    	je     104f30 <trap+0x1d0>
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
  104e1b:	8b 40 24             	mov    0x24(%eax),%eax
  104e1e:	85 c0                	test   %eax,%eax
  104e20:	74 0c                	je     104e2e <trap+0xce>
  104e22:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
  104e26:	83 e0 03             	and    $0x3,%eax
  104e29:	83 f8 03             	cmp    $0x3,%eax
  104e2c:	74 34                	je     104e62 <trap+0x102>
    exit();
}
  104e2e:	83 c4 20             	add    $0x20,%esp
  104e31:	5b                   	pop    %ebx
  104e32:	5e                   	pop    %esi
  104e33:	5d                   	pop    %ebp
  104e34:	c3                   	ret    
  104e35:	8d 76 00             	lea    0x0(%esi),%esi
    if(proc->killed)
      exit();
    return;
  }

  switch(tf->trapno){
  104e38:	ff 24 95 04 6c 10 00 	jmp    *0x106c04(,%edx,4)
  104e3f:	90                   	nop

void
trap(struct trapframe *tf)
{
  if(tf->trapno == T_SYSCALL){
    if(proc->killed)
  104e40:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104e46:	8b 70 24             	mov    0x24(%eax),%esi
  104e49:	85 f6                	test   %esi,%esi
  104e4b:	75 23                	jne    104e70 <trap+0x110>
      exit();
    proc->tf = tf;
  104e4d:	89 58 18             	mov    %ebx,0x18(%eax)
    syscall();
  104e50:	e8 1b f1 ff ff       	call   103f70 <syscall>
    if(proc->killed)
  104e55:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104e5b:	8b 48 24             	mov    0x24(%eax),%ecx
  104e5e:	85 c9                	test   %ecx,%ecx
  104e60:	74 cc                	je     104e2e <trap+0xce>
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    exit();
}
  104e62:	83 c4 20             	add    $0x20,%esp
  104e65:	5b                   	pop    %ebx
  104e66:	5e                   	pop    %esi
  104e67:	5d                   	pop    %ebp
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    exit();
  104e68:	e9 93 e6 ff ff       	jmp    103500 <exit>
  104e6d:	8d 76 00             	lea    0x0(%esi),%esi
void
trap(struct trapframe *tf)
{
  if(tf->trapno == T_SYSCALL){
    if(proc->killed)
      exit();
  104e70:	e8 8b e6 ff ff       	call   103500 <exit>
  104e75:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104e7b:	eb d0                	jmp    104e4d <trap+0xed>
  104e7d:	8d 76 00             	lea    0x0(%esi),%esi
      release(&tickslock);
    }
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE:
    ideintr();
  104e80:	e8 9b d1 ff ff       	call   102020 <ideintr>
    lapiceoi();
  104e85:	e8 d6 d5 ff ff       	call   102460 <lapiceoi>
    break;
  104e8a:	e9 61 ff ff ff       	jmp    104df0 <trap+0x90>
  104e8f:	90                   	nop
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
  104e90:	8b 43 38             	mov    0x38(%ebx),%eax
  104e93:	89 44 24 0c          	mov    %eax,0xc(%esp)
  104e97:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
  104e9b:	89 44 24 08          	mov    %eax,0x8(%esp)
  104e9f:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  104ea5:	0f b6 00             	movzbl (%eax),%eax
  104ea8:	c7 04 24 5c 6b 10 00 	movl   $0x106b5c,(%esp)
  104eaf:	89 44 24 04          	mov    %eax,0x4(%esp)
  104eb3:	e8 78 b6 ff ff       	call   100530 <cprintf>
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
  104eb8:	e8 a3 d5 ff ff       	call   102460 <lapiceoi>
    break;
  104ebd:	e9 2e ff ff ff       	jmp    104df0 <trap+0x90>
  104ec2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  104ec8:	90                   	nop
  104ec9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_COM1:
    uartintr();
  104ed0:	e8 9b 01 00 00       	call   105070 <uartintr>
    lapiceoi();
  104ed5:	e8 86 d5 ff ff       	call   102460 <lapiceoi>
    break;
  104eda:	e9 11 ff ff ff       	jmp    104df0 <trap+0x90>
  104edf:	90                   	nop
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
  104ee0:	e8 5b d5 ff ff       	call   102440 <kbdintr>
    lapiceoi();
  104ee5:	e8 76 d5 ff ff       	call   102460 <lapiceoi>
    break;
  104eea:	e9 01 ff ff ff       	jmp    104df0 <trap+0x90>
  104eef:	90                   	nop
    return;
  }

  switch(tf->trapno){
  case T_IRQ0 + IRQ_TIMER:
    if(cpu->id == 0){
  104ef0:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  104ef6:	80 38 00             	cmpb   $0x0,(%eax)
  104ef9:	75 8a                	jne    104e85 <trap+0x125>
      acquire(&tickslock);
  104efb:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
  104f02:	e8 29 ec ff ff       	call   103b30 <acquire>
      ticks++;
  104f07:	83 05 a0 e8 10 00 01 	addl   $0x1,0x10e8a0
      wakeup(&ticks);
  104f0e:	c7 04 24 a0 e8 10 00 	movl   $0x10e8a0,(%esp)
  104f15:	e8 26 e2 ff ff       	call   103140 <wakeup>
      release(&tickslock);
  104f1a:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
  104f21:	e8 ba eb ff ff       	call   103ae0 <release>
  104f26:	e9 5a ff ff ff       	jmp    104e85 <trap+0x125>
  104f2b:	90                   	nop
  104f2c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
  104f30:	83 7b 30 20          	cmpl   $0x20,0x30(%ebx)
  104f34:	0f 85 e1 fe ff ff    	jne    104e1b <trap+0xbb>
  104f3a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    yield();
  104f40:	e8 eb e3 ff ff       	call   103330 <yield>

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
  104f45:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104f4b:	85 c0                	test   %eax,%eax
  104f4d:	0f 85 c8 fe ff ff    	jne    104e1b <trap+0xbb>
  104f53:	e9 d6 fe ff ff       	jmp    104e2e <trap+0xce>

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running 
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    exit();
  104f58:	e8 a3 e5 ff ff       	call   103500 <exit>

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
  104f5d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  104f63:	85 c0                	test   %eax,%eax
  104f65:	0f 85 a6 fe ff ff    	jne    104e11 <trap+0xb1>
  104f6b:	e9 be fe ff ff       	jmp    104e2e <trap+0xce>
  104f70:	0f 20 d2             	mov    %cr2,%edx
    break;
   
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
  104f73:	89 54 24 10          	mov    %edx,0x10(%esp)
  104f77:	8b 53 38             	mov    0x38(%ebx),%edx
  104f7a:	89 54 24 0c          	mov    %edx,0xc(%esp)
  104f7e:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
  104f85:	0f b6 12             	movzbl (%edx),%edx
  104f88:	89 44 24 04          	mov    %eax,0x4(%esp)
  104f8c:	c7 04 24 80 6b 10 00 	movl   $0x106b80,(%esp)
  104f93:	89 54 24 08          	mov    %edx,0x8(%esp)
  104f97:	e8 94 b5 ff ff       	call   100530 <cprintf>
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
  104f9c:	c7 04 24 f7 6b 10 00 	movl   $0x106bf7,(%esp)
  104fa3:	e8 68 b9 ff ff       	call   100910 <panic>
  104fa8:	90                   	nop
  104fa9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00104fb0 <tvinit>:
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
  104fb0:	55                   	push   %ebp
  104fb1:	31 c0                	xor    %eax,%eax
  104fb3:	89 e5                	mov    %esp,%ebp
  104fb5:	ba a0 e0 10 00       	mov    $0x10e0a0,%edx
  104fba:	83 ec 18             	sub    $0x18,%esp
  104fbd:	8d 76 00             	lea    0x0(%esi),%esi
  int i;

  for(i = 0; i < 256; i++)
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  104fc0:	8b 0c 85 28 73 10 00 	mov    0x107328(,%eax,4),%ecx
  104fc7:	66 89 0c c5 a0 e0 10 	mov    %cx,0x10e0a0(,%eax,8)
  104fce:	00 
  104fcf:	c1 e9 10             	shr    $0x10,%ecx
  104fd2:	66 c7 44 c2 02 08 00 	movw   $0x8,0x2(%edx,%eax,8)
  104fd9:	c6 44 c2 04 00       	movb   $0x0,0x4(%edx,%eax,8)
  104fde:	c6 44 c2 05 8e       	movb   $0x8e,0x5(%edx,%eax,8)
  104fe3:	66 89 4c c2 06       	mov    %cx,0x6(%edx,%eax,8)
void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
  104fe8:	83 c0 01             	add    $0x1,%eax
  104feb:	3d 00 01 00 00       	cmp    $0x100,%eax
  104ff0:	75 ce                	jne    104fc0 <tvinit+0x10>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
  104ff2:	a1 28 74 10 00       	mov    0x107428,%eax
  
  initlock(&tickslock, "time");
  104ff7:	c7 44 24 04 fc 6b 10 	movl   $0x106bfc,0x4(%esp)
  104ffe:	00 
  104fff:	c7 04 24 60 e0 10 00 	movl   $0x10e060,(%esp)
{
  int i;

  for(i = 0; i < 256; i++)
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
  105006:	66 c7 05 a2 e2 10 00 	movw   $0x8,0x10e2a2
  10500d:	08 00 
  10500f:	66 a3 a0 e2 10 00    	mov    %ax,0x10e2a0
  105015:	c1 e8 10             	shr    $0x10,%eax
  105018:	c6 05 a4 e2 10 00 00 	movb   $0x0,0x10e2a4
  10501f:	c6 05 a5 e2 10 00 ef 	movb   $0xef,0x10e2a5
  105026:	66 a3 a6 e2 10 00    	mov    %ax,0x10e2a6
  
  initlock(&tickslock, "time");
  10502c:	e8 6f e9 ff ff       	call   1039a0 <initlock>
}
  105031:	c9                   	leave  
  105032:	c3                   	ret    
  105033:	90                   	nop
  105034:	90                   	nop
  105035:	90                   	nop
  105036:	90                   	nop
  105037:	90                   	nop
  105038:	90                   	nop
  105039:	90                   	nop
  10503a:	90                   	nop
  10503b:	90                   	nop
  10503c:	90                   	nop
  10503d:	90                   	nop
  10503e:	90                   	nop
  10503f:	90                   	nop

00105040 <uartgetc>:
}

static int
uartgetc(void)
{
  if(!uart)
  105040:	a1 cc 78 10 00       	mov    0x1078cc,%eax
  outb(COM1+0, c);
}

static int
uartgetc(void)
{
  105045:	55                   	push   %ebp
  105046:	89 e5                	mov    %esp,%ebp
  if(!uart)
  105048:	85 c0                	test   %eax,%eax
  10504a:	75 0c                	jne    105058 <uartgetc+0x18>
    return -1;
  if(!(inb(COM1+5) & 0x01))
    return -1;
  return inb(COM1+0);
  10504c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
  105051:	5d                   	pop    %ebp
  105052:	c3                   	ret    
  105053:	90                   	nop
  105054:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  105058:	ba fd 03 00 00       	mov    $0x3fd,%edx
  10505d:	ec                   	in     (%dx),%al
static int
uartgetc(void)
{
  if(!uart)
    return -1;
  if(!(inb(COM1+5) & 0x01))
  10505e:	a8 01                	test   $0x1,%al
  105060:	74 ea                	je     10504c <uartgetc+0xc>
  105062:	b2 f8                	mov    $0xf8,%dl
  105064:	ec                   	in     (%dx),%al
    return -1;
  return inb(COM1+0);
  105065:	0f b6 c0             	movzbl %al,%eax
}
  105068:	5d                   	pop    %ebp
  105069:	c3                   	ret    
  10506a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00105070 <uartintr>:

void
uartintr(void)
{
  105070:	55                   	push   %ebp
  105071:	89 e5                	mov    %esp,%ebp
  105073:	83 ec 18             	sub    $0x18,%esp
  consoleintr(uartgetc);
  105076:	c7 04 24 40 50 10 00 	movl   $0x105040,(%esp)
  10507d:	e8 0e b7 ff ff       	call   100790 <consoleintr>
}
  105082:	c9                   	leave  
  105083:	c3                   	ret    
  105084:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  10508a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00105090 <uartputc>:
    uartputc(*p);
}

void
uartputc(int c)
{
  105090:	55                   	push   %ebp
  105091:	89 e5                	mov    %esp,%ebp
  105093:	56                   	push   %esi
  105094:	be fd 03 00 00       	mov    $0x3fd,%esi
  105099:	53                   	push   %ebx
  int i;

  if(!uart)
  10509a:	31 db                	xor    %ebx,%ebx
    uartputc(*p);
}

void
uartputc(int c)
{
  10509c:	83 ec 10             	sub    $0x10,%esp
  int i;

  if(!uart)
  10509f:	8b 15 cc 78 10 00    	mov    0x1078cc,%edx
  1050a5:	85 d2                	test   %edx,%edx
  1050a7:	75 1e                	jne    1050c7 <uartputc+0x37>
  1050a9:	eb 2c                	jmp    1050d7 <uartputc+0x47>
  1050ab:	90                   	nop
  1050ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
  1050b0:	83 c3 01             	add    $0x1,%ebx
    microdelay(10);
  1050b3:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  1050ba:	e8 c1 d3 ff ff       	call   102480 <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
  1050bf:	81 fb 80 00 00 00    	cmp    $0x80,%ebx
  1050c5:	74 07                	je     1050ce <uartputc+0x3e>
  1050c7:	89 f2                	mov    %esi,%edx
  1050c9:	ec                   	in     (%dx),%al
  1050ca:	a8 20                	test   $0x20,%al
  1050cc:	74 e2                	je     1050b0 <uartputc+0x20>
}

static inline void
outb(ushort port, uchar data)
{
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
  1050ce:	ba f8 03 00 00       	mov    $0x3f8,%edx
  1050d3:	8b 45 08             	mov    0x8(%ebp),%eax
  1050d6:	ee                   	out    %al,(%dx)
    microdelay(10);
  outb(COM1+0, c);
}
  1050d7:	83 c4 10             	add    $0x10,%esp
  1050da:	5b                   	pop    %ebx
  1050db:	5e                   	pop    %esi
  1050dc:	5d                   	pop    %ebp
  1050dd:	c3                   	ret    
  1050de:	66 90                	xchg   %ax,%ax

001050e0 <uartinit>:

static int uart;    // is there a uart?

void
uartinit(void)
{
  1050e0:	55                   	push   %ebp
  1050e1:	31 c9                	xor    %ecx,%ecx
  1050e3:	89 e5                	mov    %esp,%ebp
  1050e5:	89 c8                	mov    %ecx,%eax
  1050e7:	57                   	push   %edi
  1050e8:	bf fa 03 00 00       	mov    $0x3fa,%edi
  1050ed:	56                   	push   %esi
  1050ee:	89 fa                	mov    %edi,%edx
  1050f0:	53                   	push   %ebx
  1050f1:	83 ec 1c             	sub    $0x1c,%esp
  1050f4:	ee                   	out    %al,(%dx)
  1050f5:	bb fb 03 00 00       	mov    $0x3fb,%ebx
  1050fa:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
  1050ff:	89 da                	mov    %ebx,%edx
  105101:	ee                   	out    %al,(%dx)
  105102:	b8 0c 00 00 00       	mov    $0xc,%eax
  105107:	b2 f8                	mov    $0xf8,%dl
  105109:	ee                   	out    %al,(%dx)
  10510a:	be f9 03 00 00       	mov    $0x3f9,%esi
  10510f:	89 c8                	mov    %ecx,%eax
  105111:	89 f2                	mov    %esi,%edx
  105113:	ee                   	out    %al,(%dx)
  105114:	b8 03 00 00 00       	mov    $0x3,%eax
  105119:	89 da                	mov    %ebx,%edx
  10511b:	ee                   	out    %al,(%dx)
  10511c:	b2 fc                	mov    $0xfc,%dl
  10511e:	89 c8                	mov    %ecx,%eax
  105120:	ee                   	out    %al,(%dx)
  105121:	b8 01 00 00 00       	mov    $0x1,%eax
  105126:	89 f2                	mov    %esi,%edx
  105128:	ee                   	out    %al,(%dx)
static inline uchar
inb(ushort port)
{
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
  105129:	b2 fd                	mov    $0xfd,%dl
  10512b:	ec                   	in     (%dx),%al
  outb(COM1+3, 0x03);    // Lock divisor, 8 data bits.
  outb(COM1+4, 0);
  outb(COM1+1, 0x01);    // Enable receive interrupts.

  // If status is 0xFF, no serial port.
  if(inb(COM1+5) == 0xFF)
  10512c:	3c ff                	cmp    $0xff,%al
  10512e:	74 55                	je     105185 <uartinit+0xa5>
    return;
  uart = 1;
  105130:	c7 05 cc 78 10 00 01 	movl   $0x1,0x1078cc
  105137:	00 00 00 
  10513a:	89 fa                	mov    %edi,%edx
  10513c:	ec                   	in     (%dx),%al
  10513d:	b2 f8                	mov    $0xf8,%dl
  10513f:	ec                   	in     (%dx),%al
  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+2);
  inb(COM1+0);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);
  105140:	bb 84 6c 10 00       	mov    $0x106c84,%ebx

  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+2);
  inb(COM1+0);
  picenable(IRQ_COM1);
  105145:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  10514c:	e8 4f da ff ff       	call   102ba0 <picenable>
  ioapicenable(IRQ_COM1, 0);
  105151:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  105158:	00 
  105159:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  105160:	e8 eb cf ff ff       	call   102150 <ioapicenable>
  105165:	b8 78 00 00 00       	mov    $0x78,%eax
  10516a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
    uartputc(*p);
  105170:	0f be c0             	movsbl %al,%eax
  inb(COM1+0);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
  105173:	83 c3 01             	add    $0x1,%ebx
    uartputc(*p);
  105176:	89 04 24             	mov    %eax,(%esp)
  105179:	e8 12 ff ff ff       	call   105090 <uartputc>
  inb(COM1+0);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
  10517e:	0f b6 03             	movzbl (%ebx),%eax
  105181:	84 c0                	test   %al,%al
  105183:	75 eb                	jne    105170 <uartinit+0x90>
    uartputc(*p);
}
  105185:	83 c4 1c             	add    $0x1c,%esp
  105188:	5b                   	pop    %ebx
  105189:	5e                   	pop    %esi
  10518a:	5f                   	pop    %edi
  10518b:	5d                   	pop    %ebp
  10518c:	c3                   	ret    
  10518d:	90                   	nop
  10518e:	90                   	nop
  10518f:	90                   	nop

00105190 <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
  105190:	6a 00                	push   $0x0
  pushl $0
  105192:	6a 00                	push   $0x0
  jmp alltraps
  105194:	e9 67 fb ff ff       	jmp    104d00 <alltraps>

00105199 <vector1>:
.globl vector1
vector1:
  pushl $0
  105199:	6a 00                	push   $0x0
  pushl $1
  10519b:	6a 01                	push   $0x1
  jmp alltraps
  10519d:	e9 5e fb ff ff       	jmp    104d00 <alltraps>

001051a2 <vector2>:
.globl vector2
vector2:
  pushl $0
  1051a2:	6a 00                	push   $0x0
  pushl $2
  1051a4:	6a 02                	push   $0x2
  jmp alltraps
  1051a6:	e9 55 fb ff ff       	jmp    104d00 <alltraps>

001051ab <vector3>:
.globl vector3
vector3:
  pushl $0
  1051ab:	6a 00                	push   $0x0
  pushl $3
  1051ad:	6a 03                	push   $0x3
  jmp alltraps
  1051af:	e9 4c fb ff ff       	jmp    104d00 <alltraps>

001051b4 <vector4>:
.globl vector4
vector4:
  pushl $0
  1051b4:	6a 00                	push   $0x0
  pushl $4
  1051b6:	6a 04                	push   $0x4
  jmp alltraps
  1051b8:	e9 43 fb ff ff       	jmp    104d00 <alltraps>

001051bd <vector5>:
.globl vector5
vector5:
  pushl $0
  1051bd:	6a 00                	push   $0x0
  pushl $5
  1051bf:	6a 05                	push   $0x5
  jmp alltraps
  1051c1:	e9 3a fb ff ff       	jmp    104d00 <alltraps>

001051c6 <vector6>:
.globl vector6
vector6:
  pushl $0
  1051c6:	6a 00                	push   $0x0
  pushl $6
  1051c8:	6a 06                	push   $0x6
  jmp alltraps
  1051ca:	e9 31 fb ff ff       	jmp    104d00 <alltraps>

001051cf <vector7>:
.globl vector7
vector7:
  pushl $0
  1051cf:	6a 00                	push   $0x0
  pushl $7
  1051d1:	6a 07                	push   $0x7
  jmp alltraps
  1051d3:	e9 28 fb ff ff       	jmp    104d00 <alltraps>

001051d8 <vector8>:
.globl vector8
vector8:
  pushl $8
  1051d8:	6a 08                	push   $0x8
  jmp alltraps
  1051da:	e9 21 fb ff ff       	jmp    104d00 <alltraps>

001051df <vector9>:
.globl vector9
vector9:
  pushl $0
  1051df:	6a 00                	push   $0x0
  pushl $9
  1051e1:	6a 09                	push   $0x9
  jmp alltraps
  1051e3:	e9 18 fb ff ff       	jmp    104d00 <alltraps>

001051e8 <vector10>:
.globl vector10
vector10:
  pushl $10
  1051e8:	6a 0a                	push   $0xa
  jmp alltraps
  1051ea:	e9 11 fb ff ff       	jmp    104d00 <alltraps>

001051ef <vector11>:
.globl vector11
vector11:
  pushl $11
  1051ef:	6a 0b                	push   $0xb
  jmp alltraps
  1051f1:	e9 0a fb ff ff       	jmp    104d00 <alltraps>

001051f6 <vector12>:
.globl vector12
vector12:
  pushl $12
  1051f6:	6a 0c                	push   $0xc
  jmp alltraps
  1051f8:	e9 03 fb ff ff       	jmp    104d00 <alltraps>

001051fd <vector13>:
.globl vector13
vector13:
  pushl $13
  1051fd:	6a 0d                	push   $0xd
  jmp alltraps
  1051ff:	e9 fc fa ff ff       	jmp    104d00 <alltraps>

00105204 <vector14>:
.globl vector14
vector14:
  pushl $14
  105204:	6a 0e                	push   $0xe
  jmp alltraps
  105206:	e9 f5 fa ff ff       	jmp    104d00 <alltraps>

0010520b <vector15>:
.globl vector15
vector15:
  pushl $0
  10520b:	6a 00                	push   $0x0
  pushl $15
  10520d:	6a 0f                	push   $0xf
  jmp alltraps
  10520f:	e9 ec fa ff ff       	jmp    104d00 <alltraps>

00105214 <vector16>:
.globl vector16
vector16:
  pushl $0
  105214:	6a 00                	push   $0x0
  pushl $16
  105216:	6a 10                	push   $0x10
  jmp alltraps
  105218:	e9 e3 fa ff ff       	jmp    104d00 <alltraps>

0010521d <vector17>:
.globl vector17
vector17:
  pushl $17
  10521d:	6a 11                	push   $0x11
  jmp alltraps
  10521f:	e9 dc fa ff ff       	jmp    104d00 <alltraps>

00105224 <vector18>:
.globl vector18
vector18:
  pushl $0
  105224:	6a 00                	push   $0x0
  pushl $18
  105226:	6a 12                	push   $0x12
  jmp alltraps
  105228:	e9 d3 fa ff ff       	jmp    104d00 <alltraps>

0010522d <vector19>:
.globl vector19
vector19:
  pushl $0
  10522d:	6a 00                	push   $0x0
  pushl $19
  10522f:	6a 13                	push   $0x13
  jmp alltraps
  105231:	e9 ca fa ff ff       	jmp    104d00 <alltraps>

00105236 <vector20>:
.globl vector20
vector20:
  pushl $0
  105236:	6a 00                	push   $0x0
  pushl $20
  105238:	6a 14                	push   $0x14
  jmp alltraps
  10523a:	e9 c1 fa ff ff       	jmp    104d00 <alltraps>

0010523f <vector21>:
.globl vector21
vector21:
  pushl $0
  10523f:	6a 00                	push   $0x0
  pushl $21
  105241:	6a 15                	push   $0x15
  jmp alltraps
  105243:	e9 b8 fa ff ff       	jmp    104d00 <alltraps>

00105248 <vector22>:
.globl vector22
vector22:
  pushl $0
  105248:	6a 00                	push   $0x0
  pushl $22
  10524a:	6a 16                	push   $0x16
  jmp alltraps
  10524c:	e9 af fa ff ff       	jmp    104d00 <alltraps>

00105251 <vector23>:
.globl vector23
vector23:
  pushl $0
  105251:	6a 00                	push   $0x0
  pushl $23
  105253:	6a 17                	push   $0x17
  jmp alltraps
  105255:	e9 a6 fa ff ff       	jmp    104d00 <alltraps>

0010525a <vector24>:
.globl vector24
vector24:
  pushl $0
  10525a:	6a 00                	push   $0x0
  pushl $24
  10525c:	6a 18                	push   $0x18
  jmp alltraps
  10525e:	e9 9d fa ff ff       	jmp    104d00 <alltraps>

00105263 <vector25>:
.globl vector25
vector25:
  pushl $0
  105263:	6a 00                	push   $0x0
  pushl $25
  105265:	6a 19                	push   $0x19
  jmp alltraps
  105267:	e9 94 fa ff ff       	jmp    104d00 <alltraps>

0010526c <vector26>:
.globl vector26
vector26:
  pushl $0
  10526c:	6a 00                	push   $0x0
  pushl $26
  10526e:	6a 1a                	push   $0x1a
  jmp alltraps
  105270:	e9 8b fa ff ff       	jmp    104d00 <alltraps>

00105275 <vector27>:
.globl vector27
vector27:
  pushl $0
  105275:	6a 00                	push   $0x0
  pushl $27
  105277:	6a 1b                	push   $0x1b
  jmp alltraps
  105279:	e9 82 fa ff ff       	jmp    104d00 <alltraps>

0010527e <vector28>:
.globl vector28
vector28:
  pushl $0
  10527e:	6a 00                	push   $0x0
  pushl $28
  105280:	6a 1c                	push   $0x1c
  jmp alltraps
  105282:	e9 79 fa ff ff       	jmp    104d00 <alltraps>

00105287 <vector29>:
.globl vector29
vector29:
  pushl $0
  105287:	6a 00                	push   $0x0
  pushl $29
  105289:	6a 1d                	push   $0x1d
  jmp alltraps
  10528b:	e9 70 fa ff ff       	jmp    104d00 <alltraps>

00105290 <vector30>:
.globl vector30
vector30:
  pushl $0
  105290:	6a 00                	push   $0x0
  pushl $30
  105292:	6a 1e                	push   $0x1e
  jmp alltraps
  105294:	e9 67 fa ff ff       	jmp    104d00 <alltraps>

00105299 <vector31>:
.globl vector31
vector31:
  pushl $0
  105299:	6a 00                	push   $0x0
  pushl $31
  10529b:	6a 1f                	push   $0x1f
  jmp alltraps
  10529d:	e9 5e fa ff ff       	jmp    104d00 <alltraps>

001052a2 <vector32>:
.globl vector32
vector32:
  pushl $0
  1052a2:	6a 00                	push   $0x0
  pushl $32
  1052a4:	6a 20                	push   $0x20
  jmp alltraps
  1052a6:	e9 55 fa ff ff       	jmp    104d00 <alltraps>

001052ab <vector33>:
.globl vector33
vector33:
  pushl $0
  1052ab:	6a 00                	push   $0x0
  pushl $33
  1052ad:	6a 21                	push   $0x21
  jmp alltraps
  1052af:	e9 4c fa ff ff       	jmp    104d00 <alltraps>

001052b4 <vector34>:
.globl vector34
vector34:
  pushl $0
  1052b4:	6a 00                	push   $0x0
  pushl $34
  1052b6:	6a 22                	push   $0x22
  jmp alltraps
  1052b8:	e9 43 fa ff ff       	jmp    104d00 <alltraps>

001052bd <vector35>:
.globl vector35
vector35:
  pushl $0
  1052bd:	6a 00                	push   $0x0
  pushl $35
  1052bf:	6a 23                	push   $0x23
  jmp alltraps
  1052c1:	e9 3a fa ff ff       	jmp    104d00 <alltraps>

001052c6 <vector36>:
.globl vector36
vector36:
  pushl $0
  1052c6:	6a 00                	push   $0x0
  pushl $36
  1052c8:	6a 24                	push   $0x24
  jmp alltraps
  1052ca:	e9 31 fa ff ff       	jmp    104d00 <alltraps>

001052cf <vector37>:
.globl vector37
vector37:
  pushl $0
  1052cf:	6a 00                	push   $0x0
  pushl $37
  1052d1:	6a 25                	push   $0x25
  jmp alltraps
  1052d3:	e9 28 fa ff ff       	jmp    104d00 <alltraps>

001052d8 <vector38>:
.globl vector38
vector38:
  pushl $0
  1052d8:	6a 00                	push   $0x0
  pushl $38
  1052da:	6a 26                	push   $0x26
  jmp alltraps
  1052dc:	e9 1f fa ff ff       	jmp    104d00 <alltraps>

001052e1 <vector39>:
.globl vector39
vector39:
  pushl $0
  1052e1:	6a 00                	push   $0x0
  pushl $39
  1052e3:	6a 27                	push   $0x27
  jmp alltraps
  1052e5:	e9 16 fa ff ff       	jmp    104d00 <alltraps>

001052ea <vector40>:
.globl vector40
vector40:
  pushl $0
  1052ea:	6a 00                	push   $0x0
  pushl $40
  1052ec:	6a 28                	push   $0x28
  jmp alltraps
  1052ee:	e9 0d fa ff ff       	jmp    104d00 <alltraps>

001052f3 <vector41>:
.globl vector41
vector41:
  pushl $0
  1052f3:	6a 00                	push   $0x0
  pushl $41
  1052f5:	6a 29                	push   $0x29
  jmp alltraps
  1052f7:	e9 04 fa ff ff       	jmp    104d00 <alltraps>

001052fc <vector42>:
.globl vector42
vector42:
  pushl $0
  1052fc:	6a 00                	push   $0x0
  pushl $42
  1052fe:	6a 2a                	push   $0x2a
  jmp alltraps
  105300:	e9 fb f9 ff ff       	jmp    104d00 <alltraps>

00105305 <vector43>:
.globl vector43
vector43:
  pushl $0
  105305:	6a 00                	push   $0x0
  pushl $43
  105307:	6a 2b                	push   $0x2b
  jmp alltraps
  105309:	e9 f2 f9 ff ff       	jmp    104d00 <alltraps>

0010530e <vector44>:
.globl vector44
vector44:
  pushl $0
  10530e:	6a 00                	push   $0x0
  pushl $44
  105310:	6a 2c                	push   $0x2c
  jmp alltraps
  105312:	e9 e9 f9 ff ff       	jmp    104d00 <alltraps>

00105317 <vector45>:
.globl vector45
vector45:
  pushl $0
  105317:	6a 00                	push   $0x0
  pushl $45
  105319:	6a 2d                	push   $0x2d
  jmp alltraps
  10531b:	e9 e0 f9 ff ff       	jmp    104d00 <alltraps>

00105320 <vector46>:
.globl vector46
vector46:
  pushl $0
  105320:	6a 00                	push   $0x0
  pushl $46
  105322:	6a 2e                	push   $0x2e
  jmp alltraps
  105324:	e9 d7 f9 ff ff       	jmp    104d00 <alltraps>

00105329 <vector47>:
.globl vector47
vector47:
  pushl $0
  105329:	6a 00                	push   $0x0
  pushl $47
  10532b:	6a 2f                	push   $0x2f
  jmp alltraps
  10532d:	e9 ce f9 ff ff       	jmp    104d00 <alltraps>

00105332 <vector48>:
.globl vector48
vector48:
  pushl $0
  105332:	6a 00                	push   $0x0
  pushl $48
  105334:	6a 30                	push   $0x30
  jmp alltraps
  105336:	e9 c5 f9 ff ff       	jmp    104d00 <alltraps>

0010533b <vector49>:
.globl vector49
vector49:
  pushl $0
  10533b:	6a 00                	push   $0x0
  pushl $49
  10533d:	6a 31                	push   $0x31
  jmp alltraps
  10533f:	e9 bc f9 ff ff       	jmp    104d00 <alltraps>

00105344 <vector50>:
.globl vector50
vector50:
  pushl $0
  105344:	6a 00                	push   $0x0
  pushl $50
  105346:	6a 32                	push   $0x32
  jmp alltraps
  105348:	e9 b3 f9 ff ff       	jmp    104d00 <alltraps>

0010534d <vector51>:
.globl vector51
vector51:
  pushl $0
  10534d:	6a 00                	push   $0x0
  pushl $51
  10534f:	6a 33                	push   $0x33
  jmp alltraps
  105351:	e9 aa f9 ff ff       	jmp    104d00 <alltraps>

00105356 <vector52>:
.globl vector52
vector52:
  pushl $0
  105356:	6a 00                	push   $0x0
  pushl $52
  105358:	6a 34                	push   $0x34
  jmp alltraps
  10535a:	e9 a1 f9 ff ff       	jmp    104d00 <alltraps>

0010535f <vector53>:
.globl vector53
vector53:
  pushl $0
  10535f:	6a 00                	push   $0x0
  pushl $53
  105361:	6a 35                	push   $0x35
  jmp alltraps
  105363:	e9 98 f9 ff ff       	jmp    104d00 <alltraps>

00105368 <vector54>:
.globl vector54
vector54:
  pushl $0
  105368:	6a 00                	push   $0x0
  pushl $54
  10536a:	6a 36                	push   $0x36
  jmp alltraps
  10536c:	e9 8f f9 ff ff       	jmp    104d00 <alltraps>

00105371 <vector55>:
.globl vector55
vector55:
  pushl $0
  105371:	6a 00                	push   $0x0
  pushl $55
  105373:	6a 37                	push   $0x37
  jmp alltraps
  105375:	e9 86 f9 ff ff       	jmp    104d00 <alltraps>

0010537a <vector56>:
.globl vector56
vector56:
  pushl $0
  10537a:	6a 00                	push   $0x0
  pushl $56
  10537c:	6a 38                	push   $0x38
  jmp alltraps
  10537e:	e9 7d f9 ff ff       	jmp    104d00 <alltraps>

00105383 <vector57>:
.globl vector57
vector57:
  pushl $0
  105383:	6a 00                	push   $0x0
  pushl $57
  105385:	6a 39                	push   $0x39
  jmp alltraps
  105387:	e9 74 f9 ff ff       	jmp    104d00 <alltraps>

0010538c <vector58>:
.globl vector58
vector58:
  pushl $0
  10538c:	6a 00                	push   $0x0
  pushl $58
  10538e:	6a 3a                	push   $0x3a
  jmp alltraps
  105390:	e9 6b f9 ff ff       	jmp    104d00 <alltraps>

00105395 <vector59>:
.globl vector59
vector59:
  pushl $0
  105395:	6a 00                	push   $0x0
  pushl $59
  105397:	6a 3b                	push   $0x3b
  jmp alltraps
  105399:	e9 62 f9 ff ff       	jmp    104d00 <alltraps>

0010539e <vector60>:
.globl vector60
vector60:
  pushl $0
  10539e:	6a 00                	push   $0x0
  pushl $60
  1053a0:	6a 3c                	push   $0x3c
  jmp alltraps
  1053a2:	e9 59 f9 ff ff       	jmp    104d00 <alltraps>

001053a7 <vector61>:
.globl vector61
vector61:
  pushl $0
  1053a7:	6a 00                	push   $0x0
  pushl $61
  1053a9:	6a 3d                	push   $0x3d
  jmp alltraps
  1053ab:	e9 50 f9 ff ff       	jmp    104d00 <alltraps>

001053b0 <vector62>:
.globl vector62
vector62:
  pushl $0
  1053b0:	6a 00                	push   $0x0
  pushl $62
  1053b2:	6a 3e                	push   $0x3e
  jmp alltraps
  1053b4:	e9 47 f9 ff ff       	jmp    104d00 <alltraps>

001053b9 <vector63>:
.globl vector63
vector63:
  pushl $0
  1053b9:	6a 00                	push   $0x0
  pushl $63
  1053bb:	6a 3f                	push   $0x3f
  jmp alltraps
  1053bd:	e9 3e f9 ff ff       	jmp    104d00 <alltraps>

001053c2 <vector64>:
.globl vector64
vector64:
  pushl $0
  1053c2:	6a 00                	push   $0x0
  pushl $64
  1053c4:	6a 40                	push   $0x40
  jmp alltraps
  1053c6:	e9 35 f9 ff ff       	jmp    104d00 <alltraps>

001053cb <vector65>:
.globl vector65
vector65:
  pushl $0
  1053cb:	6a 00                	push   $0x0
  pushl $65
  1053cd:	6a 41                	push   $0x41
  jmp alltraps
  1053cf:	e9 2c f9 ff ff       	jmp    104d00 <alltraps>

001053d4 <vector66>:
.globl vector66
vector66:
  pushl $0
  1053d4:	6a 00                	push   $0x0
  pushl $66
  1053d6:	6a 42                	push   $0x42
  jmp alltraps
  1053d8:	e9 23 f9 ff ff       	jmp    104d00 <alltraps>

001053dd <vector67>:
.globl vector67
vector67:
  pushl $0
  1053dd:	6a 00                	push   $0x0
  pushl $67
  1053df:	6a 43                	push   $0x43
  jmp alltraps
  1053e1:	e9 1a f9 ff ff       	jmp    104d00 <alltraps>

001053e6 <vector68>:
.globl vector68
vector68:
  pushl $0
  1053e6:	6a 00                	push   $0x0
  pushl $68
  1053e8:	6a 44                	push   $0x44
  jmp alltraps
  1053ea:	e9 11 f9 ff ff       	jmp    104d00 <alltraps>

001053ef <vector69>:
.globl vector69
vector69:
  pushl $0
  1053ef:	6a 00                	push   $0x0
  pushl $69
  1053f1:	6a 45                	push   $0x45
  jmp alltraps
  1053f3:	e9 08 f9 ff ff       	jmp    104d00 <alltraps>

001053f8 <vector70>:
.globl vector70
vector70:
  pushl $0
  1053f8:	6a 00                	push   $0x0
  pushl $70
  1053fa:	6a 46                	push   $0x46
  jmp alltraps
  1053fc:	e9 ff f8 ff ff       	jmp    104d00 <alltraps>

00105401 <vector71>:
.globl vector71
vector71:
  pushl $0
  105401:	6a 00                	push   $0x0
  pushl $71
  105403:	6a 47                	push   $0x47
  jmp alltraps
  105405:	e9 f6 f8 ff ff       	jmp    104d00 <alltraps>

0010540a <vector72>:
.globl vector72
vector72:
  pushl $0
  10540a:	6a 00                	push   $0x0
  pushl $72
  10540c:	6a 48                	push   $0x48
  jmp alltraps
  10540e:	e9 ed f8 ff ff       	jmp    104d00 <alltraps>

00105413 <vector73>:
.globl vector73
vector73:
  pushl $0
  105413:	6a 00                	push   $0x0
  pushl $73
  105415:	6a 49                	push   $0x49
  jmp alltraps
  105417:	e9 e4 f8 ff ff       	jmp    104d00 <alltraps>

0010541c <vector74>:
.globl vector74
vector74:
  pushl $0
  10541c:	6a 00                	push   $0x0
  pushl $74
  10541e:	6a 4a                	push   $0x4a
  jmp alltraps
  105420:	e9 db f8 ff ff       	jmp    104d00 <alltraps>

00105425 <vector75>:
.globl vector75
vector75:
  pushl $0
  105425:	6a 00                	push   $0x0
  pushl $75
  105427:	6a 4b                	push   $0x4b
  jmp alltraps
  105429:	e9 d2 f8 ff ff       	jmp    104d00 <alltraps>

0010542e <vector76>:
.globl vector76
vector76:
  pushl $0
  10542e:	6a 00                	push   $0x0
  pushl $76
  105430:	6a 4c                	push   $0x4c
  jmp alltraps
  105432:	e9 c9 f8 ff ff       	jmp    104d00 <alltraps>

00105437 <vector77>:
.globl vector77
vector77:
  pushl $0
  105437:	6a 00                	push   $0x0
  pushl $77
  105439:	6a 4d                	push   $0x4d
  jmp alltraps
  10543b:	e9 c0 f8 ff ff       	jmp    104d00 <alltraps>

00105440 <vector78>:
.globl vector78
vector78:
  pushl $0
  105440:	6a 00                	push   $0x0
  pushl $78
  105442:	6a 4e                	push   $0x4e
  jmp alltraps
  105444:	e9 b7 f8 ff ff       	jmp    104d00 <alltraps>

00105449 <vector79>:
.globl vector79
vector79:
  pushl $0
  105449:	6a 00                	push   $0x0
  pushl $79
  10544b:	6a 4f                	push   $0x4f
  jmp alltraps
  10544d:	e9 ae f8 ff ff       	jmp    104d00 <alltraps>

00105452 <vector80>:
.globl vector80
vector80:
  pushl $0
  105452:	6a 00                	push   $0x0
  pushl $80
  105454:	6a 50                	push   $0x50
  jmp alltraps
  105456:	e9 a5 f8 ff ff       	jmp    104d00 <alltraps>

0010545b <vector81>:
.globl vector81
vector81:
  pushl $0
  10545b:	6a 00                	push   $0x0
  pushl $81
  10545d:	6a 51                	push   $0x51
  jmp alltraps
  10545f:	e9 9c f8 ff ff       	jmp    104d00 <alltraps>

00105464 <vector82>:
.globl vector82
vector82:
  pushl $0
  105464:	6a 00                	push   $0x0
  pushl $82
  105466:	6a 52                	push   $0x52
  jmp alltraps
  105468:	e9 93 f8 ff ff       	jmp    104d00 <alltraps>

0010546d <vector83>:
.globl vector83
vector83:
  pushl $0
  10546d:	6a 00                	push   $0x0
  pushl $83
  10546f:	6a 53                	push   $0x53
  jmp alltraps
  105471:	e9 8a f8 ff ff       	jmp    104d00 <alltraps>

00105476 <vector84>:
.globl vector84
vector84:
  pushl $0
  105476:	6a 00                	push   $0x0
  pushl $84
  105478:	6a 54                	push   $0x54
  jmp alltraps
  10547a:	e9 81 f8 ff ff       	jmp    104d00 <alltraps>

0010547f <vector85>:
.globl vector85
vector85:
  pushl $0
  10547f:	6a 00                	push   $0x0
  pushl $85
  105481:	6a 55                	push   $0x55
  jmp alltraps
  105483:	e9 78 f8 ff ff       	jmp    104d00 <alltraps>

00105488 <vector86>:
.globl vector86
vector86:
  pushl $0
  105488:	6a 00                	push   $0x0
  pushl $86
  10548a:	6a 56                	push   $0x56
  jmp alltraps
  10548c:	e9 6f f8 ff ff       	jmp    104d00 <alltraps>

00105491 <vector87>:
.globl vector87
vector87:
  pushl $0
  105491:	6a 00                	push   $0x0
  pushl $87
  105493:	6a 57                	push   $0x57
  jmp alltraps
  105495:	e9 66 f8 ff ff       	jmp    104d00 <alltraps>

0010549a <vector88>:
.globl vector88
vector88:
  pushl $0
  10549a:	6a 00                	push   $0x0
  pushl $88
  10549c:	6a 58                	push   $0x58
  jmp alltraps
  10549e:	e9 5d f8 ff ff       	jmp    104d00 <alltraps>

001054a3 <vector89>:
.globl vector89
vector89:
  pushl $0
  1054a3:	6a 00                	push   $0x0
  pushl $89
  1054a5:	6a 59                	push   $0x59
  jmp alltraps
  1054a7:	e9 54 f8 ff ff       	jmp    104d00 <alltraps>

001054ac <vector90>:
.globl vector90
vector90:
  pushl $0
  1054ac:	6a 00                	push   $0x0
  pushl $90
  1054ae:	6a 5a                	push   $0x5a
  jmp alltraps
  1054b0:	e9 4b f8 ff ff       	jmp    104d00 <alltraps>

001054b5 <vector91>:
.globl vector91
vector91:
  pushl $0
  1054b5:	6a 00                	push   $0x0
  pushl $91
  1054b7:	6a 5b                	push   $0x5b
  jmp alltraps
  1054b9:	e9 42 f8 ff ff       	jmp    104d00 <alltraps>

001054be <vector92>:
.globl vector92
vector92:
  pushl $0
  1054be:	6a 00                	push   $0x0
  pushl $92
  1054c0:	6a 5c                	push   $0x5c
  jmp alltraps
  1054c2:	e9 39 f8 ff ff       	jmp    104d00 <alltraps>

001054c7 <vector93>:
.globl vector93
vector93:
  pushl $0
  1054c7:	6a 00                	push   $0x0
  pushl $93
  1054c9:	6a 5d                	push   $0x5d
  jmp alltraps
  1054cb:	e9 30 f8 ff ff       	jmp    104d00 <alltraps>

001054d0 <vector94>:
.globl vector94
vector94:
  pushl $0
  1054d0:	6a 00                	push   $0x0
  pushl $94
  1054d2:	6a 5e                	push   $0x5e
  jmp alltraps
  1054d4:	e9 27 f8 ff ff       	jmp    104d00 <alltraps>

001054d9 <vector95>:
.globl vector95
vector95:
  pushl $0
  1054d9:	6a 00                	push   $0x0
  pushl $95
  1054db:	6a 5f                	push   $0x5f
  jmp alltraps
  1054dd:	e9 1e f8 ff ff       	jmp    104d00 <alltraps>

001054e2 <vector96>:
.globl vector96
vector96:
  pushl $0
  1054e2:	6a 00                	push   $0x0
  pushl $96
  1054e4:	6a 60                	push   $0x60
  jmp alltraps
  1054e6:	e9 15 f8 ff ff       	jmp    104d00 <alltraps>

001054eb <vector97>:
.globl vector97
vector97:
  pushl $0
  1054eb:	6a 00                	push   $0x0
  pushl $97
  1054ed:	6a 61                	push   $0x61
  jmp alltraps
  1054ef:	e9 0c f8 ff ff       	jmp    104d00 <alltraps>

001054f4 <vector98>:
.globl vector98
vector98:
  pushl $0
  1054f4:	6a 00                	push   $0x0
  pushl $98
  1054f6:	6a 62                	push   $0x62
  jmp alltraps
  1054f8:	e9 03 f8 ff ff       	jmp    104d00 <alltraps>

001054fd <vector99>:
.globl vector99
vector99:
  pushl $0
  1054fd:	6a 00                	push   $0x0
  pushl $99
  1054ff:	6a 63                	push   $0x63
  jmp alltraps
  105501:	e9 fa f7 ff ff       	jmp    104d00 <alltraps>

00105506 <vector100>:
.globl vector100
vector100:
  pushl $0
  105506:	6a 00                	push   $0x0
  pushl $100
  105508:	6a 64                	push   $0x64
  jmp alltraps
  10550a:	e9 f1 f7 ff ff       	jmp    104d00 <alltraps>

0010550f <vector101>:
.globl vector101
vector101:
  pushl $0
  10550f:	6a 00                	push   $0x0
  pushl $101
  105511:	6a 65                	push   $0x65
  jmp alltraps
  105513:	e9 e8 f7 ff ff       	jmp    104d00 <alltraps>

00105518 <vector102>:
.globl vector102
vector102:
  pushl $0
  105518:	6a 00                	push   $0x0
  pushl $102
  10551a:	6a 66                	push   $0x66
  jmp alltraps
  10551c:	e9 df f7 ff ff       	jmp    104d00 <alltraps>

00105521 <vector103>:
.globl vector103
vector103:
  pushl $0
  105521:	6a 00                	push   $0x0
  pushl $103
  105523:	6a 67                	push   $0x67
  jmp alltraps
  105525:	e9 d6 f7 ff ff       	jmp    104d00 <alltraps>

0010552a <vector104>:
.globl vector104
vector104:
  pushl $0
  10552a:	6a 00                	push   $0x0
  pushl $104
  10552c:	6a 68                	push   $0x68
  jmp alltraps
  10552e:	e9 cd f7 ff ff       	jmp    104d00 <alltraps>

00105533 <vector105>:
.globl vector105
vector105:
  pushl $0
  105533:	6a 00                	push   $0x0
  pushl $105
  105535:	6a 69                	push   $0x69
  jmp alltraps
  105537:	e9 c4 f7 ff ff       	jmp    104d00 <alltraps>

0010553c <vector106>:
.globl vector106
vector106:
  pushl $0
  10553c:	6a 00                	push   $0x0
  pushl $106
  10553e:	6a 6a                	push   $0x6a
  jmp alltraps
  105540:	e9 bb f7 ff ff       	jmp    104d00 <alltraps>

00105545 <vector107>:
.globl vector107
vector107:
  pushl $0
  105545:	6a 00                	push   $0x0
  pushl $107
  105547:	6a 6b                	push   $0x6b
  jmp alltraps
  105549:	e9 b2 f7 ff ff       	jmp    104d00 <alltraps>

0010554e <vector108>:
.globl vector108
vector108:
  pushl $0
  10554e:	6a 00                	push   $0x0
  pushl $108
  105550:	6a 6c                	push   $0x6c
  jmp alltraps
  105552:	e9 a9 f7 ff ff       	jmp    104d00 <alltraps>

00105557 <vector109>:
.globl vector109
vector109:
  pushl $0
  105557:	6a 00                	push   $0x0
  pushl $109
  105559:	6a 6d                	push   $0x6d
  jmp alltraps
  10555b:	e9 a0 f7 ff ff       	jmp    104d00 <alltraps>

00105560 <vector110>:
.globl vector110
vector110:
  pushl $0
  105560:	6a 00                	push   $0x0
  pushl $110
  105562:	6a 6e                	push   $0x6e
  jmp alltraps
  105564:	e9 97 f7 ff ff       	jmp    104d00 <alltraps>

00105569 <vector111>:
.globl vector111
vector111:
  pushl $0
  105569:	6a 00                	push   $0x0
  pushl $111
  10556b:	6a 6f                	push   $0x6f
  jmp alltraps
  10556d:	e9 8e f7 ff ff       	jmp    104d00 <alltraps>

00105572 <vector112>:
.globl vector112
vector112:
  pushl $0
  105572:	6a 00                	push   $0x0
  pushl $112
  105574:	6a 70                	push   $0x70
  jmp alltraps
  105576:	e9 85 f7 ff ff       	jmp    104d00 <alltraps>

0010557b <vector113>:
.globl vector113
vector113:
  pushl $0
  10557b:	6a 00                	push   $0x0
  pushl $113
  10557d:	6a 71                	push   $0x71
  jmp alltraps
  10557f:	e9 7c f7 ff ff       	jmp    104d00 <alltraps>

00105584 <vector114>:
.globl vector114
vector114:
  pushl $0
  105584:	6a 00                	push   $0x0
  pushl $114
  105586:	6a 72                	push   $0x72
  jmp alltraps
  105588:	e9 73 f7 ff ff       	jmp    104d00 <alltraps>

0010558d <vector115>:
.globl vector115
vector115:
  pushl $0
  10558d:	6a 00                	push   $0x0
  pushl $115
  10558f:	6a 73                	push   $0x73
  jmp alltraps
  105591:	e9 6a f7 ff ff       	jmp    104d00 <alltraps>

00105596 <vector116>:
.globl vector116
vector116:
  pushl $0
  105596:	6a 00                	push   $0x0
  pushl $116
  105598:	6a 74                	push   $0x74
  jmp alltraps
  10559a:	e9 61 f7 ff ff       	jmp    104d00 <alltraps>

0010559f <vector117>:
.globl vector117
vector117:
  pushl $0
  10559f:	6a 00                	push   $0x0
  pushl $117
  1055a1:	6a 75                	push   $0x75
  jmp alltraps
  1055a3:	e9 58 f7 ff ff       	jmp    104d00 <alltraps>

001055a8 <vector118>:
.globl vector118
vector118:
  pushl $0
  1055a8:	6a 00                	push   $0x0
  pushl $118
  1055aa:	6a 76                	push   $0x76
  jmp alltraps
  1055ac:	e9 4f f7 ff ff       	jmp    104d00 <alltraps>

001055b1 <vector119>:
.globl vector119
vector119:
  pushl $0
  1055b1:	6a 00                	push   $0x0
  pushl $119
  1055b3:	6a 77                	push   $0x77
  jmp alltraps
  1055b5:	e9 46 f7 ff ff       	jmp    104d00 <alltraps>

001055ba <vector120>:
.globl vector120
vector120:
  pushl $0
  1055ba:	6a 00                	push   $0x0
  pushl $120
  1055bc:	6a 78                	push   $0x78
  jmp alltraps
  1055be:	e9 3d f7 ff ff       	jmp    104d00 <alltraps>

001055c3 <vector121>:
.globl vector121
vector121:
  pushl $0
  1055c3:	6a 00                	push   $0x0
  pushl $121
  1055c5:	6a 79                	push   $0x79
  jmp alltraps
  1055c7:	e9 34 f7 ff ff       	jmp    104d00 <alltraps>

001055cc <vector122>:
.globl vector122
vector122:
  pushl $0
  1055cc:	6a 00                	push   $0x0
  pushl $122
  1055ce:	6a 7a                	push   $0x7a
  jmp alltraps
  1055d0:	e9 2b f7 ff ff       	jmp    104d00 <alltraps>

001055d5 <vector123>:
.globl vector123
vector123:
  pushl $0
  1055d5:	6a 00                	push   $0x0
  pushl $123
  1055d7:	6a 7b                	push   $0x7b
  jmp alltraps
  1055d9:	e9 22 f7 ff ff       	jmp    104d00 <alltraps>

001055de <vector124>:
.globl vector124
vector124:
  pushl $0
  1055de:	6a 00                	push   $0x0
  pushl $124
  1055e0:	6a 7c                	push   $0x7c
  jmp alltraps
  1055e2:	e9 19 f7 ff ff       	jmp    104d00 <alltraps>

001055e7 <vector125>:
.globl vector125
vector125:
  pushl $0
  1055e7:	6a 00                	push   $0x0
  pushl $125
  1055e9:	6a 7d                	push   $0x7d
  jmp alltraps
  1055eb:	e9 10 f7 ff ff       	jmp    104d00 <alltraps>

001055f0 <vector126>:
.globl vector126
vector126:
  pushl $0
  1055f0:	6a 00                	push   $0x0
  pushl $126
  1055f2:	6a 7e                	push   $0x7e
  jmp alltraps
  1055f4:	e9 07 f7 ff ff       	jmp    104d00 <alltraps>

001055f9 <vector127>:
.globl vector127
vector127:
  pushl $0
  1055f9:	6a 00                	push   $0x0
  pushl $127
  1055fb:	6a 7f                	push   $0x7f
  jmp alltraps
  1055fd:	e9 fe f6 ff ff       	jmp    104d00 <alltraps>

00105602 <vector128>:
.globl vector128
vector128:
  pushl $0
  105602:	6a 00                	push   $0x0
  pushl $128
  105604:	68 80 00 00 00       	push   $0x80
  jmp alltraps
  105609:	e9 f2 f6 ff ff       	jmp    104d00 <alltraps>

0010560e <vector129>:
.globl vector129
vector129:
  pushl $0
  10560e:	6a 00                	push   $0x0
  pushl $129
  105610:	68 81 00 00 00       	push   $0x81
  jmp alltraps
  105615:	e9 e6 f6 ff ff       	jmp    104d00 <alltraps>

0010561a <vector130>:
.globl vector130
vector130:
  pushl $0
  10561a:	6a 00                	push   $0x0
  pushl $130
  10561c:	68 82 00 00 00       	push   $0x82
  jmp alltraps
  105621:	e9 da f6 ff ff       	jmp    104d00 <alltraps>

00105626 <vector131>:
.globl vector131
vector131:
  pushl $0
  105626:	6a 00                	push   $0x0
  pushl $131
  105628:	68 83 00 00 00       	push   $0x83
  jmp alltraps
  10562d:	e9 ce f6 ff ff       	jmp    104d00 <alltraps>

00105632 <vector132>:
.globl vector132
vector132:
  pushl $0
  105632:	6a 00                	push   $0x0
  pushl $132
  105634:	68 84 00 00 00       	push   $0x84
  jmp alltraps
  105639:	e9 c2 f6 ff ff       	jmp    104d00 <alltraps>

0010563e <vector133>:
.globl vector133
vector133:
  pushl $0
  10563e:	6a 00                	push   $0x0
  pushl $133
  105640:	68 85 00 00 00       	push   $0x85
  jmp alltraps
  105645:	e9 b6 f6 ff ff       	jmp    104d00 <alltraps>

0010564a <vector134>:
.globl vector134
vector134:
  pushl $0
  10564a:	6a 00                	push   $0x0
  pushl $134
  10564c:	68 86 00 00 00       	push   $0x86
  jmp alltraps
  105651:	e9 aa f6 ff ff       	jmp    104d00 <alltraps>

00105656 <vector135>:
.globl vector135
vector135:
  pushl $0
  105656:	6a 00                	push   $0x0
  pushl $135
  105658:	68 87 00 00 00       	push   $0x87
  jmp alltraps
  10565d:	e9 9e f6 ff ff       	jmp    104d00 <alltraps>

00105662 <vector136>:
.globl vector136
vector136:
  pushl $0
  105662:	6a 00                	push   $0x0
  pushl $136
  105664:	68 88 00 00 00       	push   $0x88
  jmp alltraps
  105669:	e9 92 f6 ff ff       	jmp    104d00 <alltraps>

0010566e <vector137>:
.globl vector137
vector137:
  pushl $0
  10566e:	6a 00                	push   $0x0
  pushl $137
  105670:	68 89 00 00 00       	push   $0x89
  jmp alltraps
  105675:	e9 86 f6 ff ff       	jmp    104d00 <alltraps>

0010567a <vector138>:
.globl vector138
vector138:
  pushl $0
  10567a:	6a 00                	push   $0x0
  pushl $138
  10567c:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
  105681:	e9 7a f6 ff ff       	jmp    104d00 <alltraps>

00105686 <vector139>:
.globl vector139
vector139:
  pushl $0
  105686:	6a 00                	push   $0x0
  pushl $139
  105688:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
  10568d:	e9 6e f6 ff ff       	jmp    104d00 <alltraps>

00105692 <vector140>:
.globl vector140
vector140:
  pushl $0
  105692:	6a 00                	push   $0x0
  pushl $140
  105694:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
  105699:	e9 62 f6 ff ff       	jmp    104d00 <alltraps>

0010569e <vector141>:
.globl vector141
vector141:
  pushl $0
  10569e:	6a 00                	push   $0x0
  pushl $141
  1056a0:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
  1056a5:	e9 56 f6 ff ff       	jmp    104d00 <alltraps>

001056aa <vector142>:
.globl vector142
vector142:
  pushl $0
  1056aa:	6a 00                	push   $0x0
  pushl $142
  1056ac:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
  1056b1:	e9 4a f6 ff ff       	jmp    104d00 <alltraps>

001056b6 <vector143>:
.globl vector143
vector143:
  pushl $0
  1056b6:	6a 00                	push   $0x0
  pushl $143
  1056b8:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
  1056bd:	e9 3e f6 ff ff       	jmp    104d00 <alltraps>

001056c2 <vector144>:
.globl vector144
vector144:
  pushl $0
  1056c2:	6a 00                	push   $0x0
  pushl $144
  1056c4:	68 90 00 00 00       	push   $0x90
  jmp alltraps
  1056c9:	e9 32 f6 ff ff       	jmp    104d00 <alltraps>

001056ce <vector145>:
.globl vector145
vector145:
  pushl $0
  1056ce:	6a 00                	push   $0x0
  pushl $145
  1056d0:	68 91 00 00 00       	push   $0x91
  jmp alltraps
  1056d5:	e9 26 f6 ff ff       	jmp    104d00 <alltraps>

001056da <vector146>:
.globl vector146
vector146:
  pushl $0
  1056da:	6a 00                	push   $0x0
  pushl $146
  1056dc:	68 92 00 00 00       	push   $0x92
  jmp alltraps
  1056e1:	e9 1a f6 ff ff       	jmp    104d00 <alltraps>

001056e6 <vector147>:
.globl vector147
vector147:
  pushl $0
  1056e6:	6a 00                	push   $0x0
  pushl $147
  1056e8:	68 93 00 00 00       	push   $0x93
  jmp alltraps
  1056ed:	e9 0e f6 ff ff       	jmp    104d00 <alltraps>

001056f2 <vector148>:
.globl vector148
vector148:
  pushl $0
  1056f2:	6a 00                	push   $0x0
  pushl $148
  1056f4:	68 94 00 00 00       	push   $0x94
  jmp alltraps
  1056f9:	e9 02 f6 ff ff       	jmp    104d00 <alltraps>

001056fe <vector149>:
.globl vector149
vector149:
  pushl $0
  1056fe:	6a 00                	push   $0x0
  pushl $149
  105700:	68 95 00 00 00       	push   $0x95
  jmp alltraps
  105705:	e9 f6 f5 ff ff       	jmp    104d00 <alltraps>

0010570a <vector150>:
.globl vector150
vector150:
  pushl $0
  10570a:	6a 00                	push   $0x0
  pushl $150
  10570c:	68 96 00 00 00       	push   $0x96
  jmp alltraps
  105711:	e9 ea f5 ff ff       	jmp    104d00 <alltraps>

00105716 <vector151>:
.globl vector151
vector151:
  pushl $0
  105716:	6a 00                	push   $0x0
  pushl $151
  105718:	68 97 00 00 00       	push   $0x97
  jmp alltraps
  10571d:	e9 de f5 ff ff       	jmp    104d00 <alltraps>

00105722 <vector152>:
.globl vector152
vector152:
  pushl $0
  105722:	6a 00                	push   $0x0
  pushl $152
  105724:	68 98 00 00 00       	push   $0x98
  jmp alltraps
  105729:	e9 d2 f5 ff ff       	jmp    104d00 <alltraps>

0010572e <vector153>:
.globl vector153
vector153:
  pushl $0
  10572e:	6a 00                	push   $0x0
  pushl $153
  105730:	68 99 00 00 00       	push   $0x99
  jmp alltraps
  105735:	e9 c6 f5 ff ff       	jmp    104d00 <alltraps>

0010573a <vector154>:
.globl vector154
vector154:
  pushl $0
  10573a:	6a 00                	push   $0x0
  pushl $154
  10573c:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
  105741:	e9 ba f5 ff ff       	jmp    104d00 <alltraps>

00105746 <vector155>:
.globl vector155
vector155:
  pushl $0
  105746:	6a 00                	push   $0x0
  pushl $155
  105748:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
  10574d:	e9 ae f5 ff ff       	jmp    104d00 <alltraps>

00105752 <vector156>:
.globl vector156
vector156:
  pushl $0
  105752:	6a 00                	push   $0x0
  pushl $156
  105754:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
  105759:	e9 a2 f5 ff ff       	jmp    104d00 <alltraps>

0010575e <vector157>:
.globl vector157
vector157:
  pushl $0
  10575e:	6a 00                	push   $0x0
  pushl $157
  105760:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
  105765:	e9 96 f5 ff ff       	jmp    104d00 <alltraps>

0010576a <vector158>:
.globl vector158
vector158:
  pushl $0
  10576a:	6a 00                	push   $0x0
  pushl $158
  10576c:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
  105771:	e9 8a f5 ff ff       	jmp    104d00 <alltraps>

00105776 <vector159>:
.globl vector159
vector159:
  pushl $0
  105776:	6a 00                	push   $0x0
  pushl $159
  105778:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
  10577d:	e9 7e f5 ff ff       	jmp    104d00 <alltraps>

00105782 <vector160>:
.globl vector160
vector160:
  pushl $0
  105782:	6a 00                	push   $0x0
  pushl $160
  105784:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
  105789:	e9 72 f5 ff ff       	jmp    104d00 <alltraps>

0010578e <vector161>:
.globl vector161
vector161:
  pushl $0
  10578e:	6a 00                	push   $0x0
  pushl $161
  105790:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
  105795:	e9 66 f5 ff ff       	jmp    104d00 <alltraps>

0010579a <vector162>:
.globl vector162
vector162:
  pushl $0
  10579a:	6a 00                	push   $0x0
  pushl $162
  10579c:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
  1057a1:	e9 5a f5 ff ff       	jmp    104d00 <alltraps>

001057a6 <vector163>:
.globl vector163
vector163:
  pushl $0
  1057a6:	6a 00                	push   $0x0
  pushl $163
  1057a8:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
  1057ad:	e9 4e f5 ff ff       	jmp    104d00 <alltraps>

001057b2 <vector164>:
.globl vector164
vector164:
  pushl $0
  1057b2:	6a 00                	push   $0x0
  pushl $164
  1057b4:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
  1057b9:	e9 42 f5 ff ff       	jmp    104d00 <alltraps>

001057be <vector165>:
.globl vector165
vector165:
  pushl $0
  1057be:	6a 00                	push   $0x0
  pushl $165
  1057c0:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
  1057c5:	e9 36 f5 ff ff       	jmp    104d00 <alltraps>

001057ca <vector166>:
.globl vector166
vector166:
  pushl $0
  1057ca:	6a 00                	push   $0x0
  pushl $166
  1057cc:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
  1057d1:	e9 2a f5 ff ff       	jmp    104d00 <alltraps>

001057d6 <vector167>:
.globl vector167
vector167:
  pushl $0
  1057d6:	6a 00                	push   $0x0
  pushl $167
  1057d8:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
  1057dd:	e9 1e f5 ff ff       	jmp    104d00 <alltraps>

001057e2 <vector168>:
.globl vector168
vector168:
  pushl $0
  1057e2:	6a 00                	push   $0x0
  pushl $168
  1057e4:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
  1057e9:	e9 12 f5 ff ff       	jmp    104d00 <alltraps>

001057ee <vector169>:
.globl vector169
vector169:
  pushl $0
  1057ee:	6a 00                	push   $0x0
  pushl $169
  1057f0:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
  1057f5:	e9 06 f5 ff ff       	jmp    104d00 <alltraps>

001057fa <vector170>:
.globl vector170
vector170:
  pushl $0
  1057fa:	6a 00                	push   $0x0
  pushl $170
  1057fc:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
  105801:	e9 fa f4 ff ff       	jmp    104d00 <alltraps>

00105806 <vector171>:
.globl vector171
vector171:
  pushl $0
  105806:	6a 00                	push   $0x0
  pushl $171
  105808:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
  10580d:	e9 ee f4 ff ff       	jmp    104d00 <alltraps>

00105812 <vector172>:
.globl vector172
vector172:
  pushl $0
  105812:	6a 00                	push   $0x0
  pushl $172
  105814:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
  105819:	e9 e2 f4 ff ff       	jmp    104d00 <alltraps>

0010581e <vector173>:
.globl vector173
vector173:
  pushl $0
  10581e:	6a 00                	push   $0x0
  pushl $173
  105820:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
  105825:	e9 d6 f4 ff ff       	jmp    104d00 <alltraps>

0010582a <vector174>:
.globl vector174
vector174:
  pushl $0
  10582a:	6a 00                	push   $0x0
  pushl $174
  10582c:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
  105831:	e9 ca f4 ff ff       	jmp    104d00 <alltraps>

00105836 <vector175>:
.globl vector175
vector175:
  pushl $0
  105836:	6a 00                	push   $0x0
  pushl $175
  105838:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
  10583d:	e9 be f4 ff ff       	jmp    104d00 <alltraps>

00105842 <vector176>:
.globl vector176
vector176:
  pushl $0
  105842:	6a 00                	push   $0x0
  pushl $176
  105844:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
  105849:	e9 b2 f4 ff ff       	jmp    104d00 <alltraps>

0010584e <vector177>:
.globl vector177
vector177:
  pushl $0
  10584e:	6a 00                	push   $0x0
  pushl $177
  105850:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
  105855:	e9 a6 f4 ff ff       	jmp    104d00 <alltraps>

0010585a <vector178>:
.globl vector178
vector178:
  pushl $0
  10585a:	6a 00                	push   $0x0
  pushl $178
  10585c:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
  105861:	e9 9a f4 ff ff       	jmp    104d00 <alltraps>

00105866 <vector179>:
.globl vector179
vector179:
  pushl $0
  105866:	6a 00                	push   $0x0
  pushl $179
  105868:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
  10586d:	e9 8e f4 ff ff       	jmp    104d00 <alltraps>

00105872 <vector180>:
.globl vector180
vector180:
  pushl $0
  105872:	6a 00                	push   $0x0
  pushl $180
  105874:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
  105879:	e9 82 f4 ff ff       	jmp    104d00 <alltraps>

0010587e <vector181>:
.globl vector181
vector181:
  pushl $0
  10587e:	6a 00                	push   $0x0
  pushl $181
  105880:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
  105885:	e9 76 f4 ff ff       	jmp    104d00 <alltraps>

0010588a <vector182>:
.globl vector182
vector182:
  pushl $0
  10588a:	6a 00                	push   $0x0
  pushl $182
  10588c:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
  105891:	e9 6a f4 ff ff       	jmp    104d00 <alltraps>

00105896 <vector183>:
.globl vector183
vector183:
  pushl $0
  105896:	6a 00                	push   $0x0
  pushl $183
  105898:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
  10589d:	e9 5e f4 ff ff       	jmp    104d00 <alltraps>

001058a2 <vector184>:
.globl vector184
vector184:
  pushl $0
  1058a2:	6a 00                	push   $0x0
  pushl $184
  1058a4:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
  1058a9:	e9 52 f4 ff ff       	jmp    104d00 <alltraps>

001058ae <vector185>:
.globl vector185
vector185:
  pushl $0
  1058ae:	6a 00                	push   $0x0
  pushl $185
  1058b0:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
  1058b5:	e9 46 f4 ff ff       	jmp    104d00 <alltraps>

001058ba <vector186>:
.globl vector186
vector186:
  pushl $0
  1058ba:	6a 00                	push   $0x0
  pushl $186
  1058bc:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
  1058c1:	e9 3a f4 ff ff       	jmp    104d00 <alltraps>

001058c6 <vector187>:
.globl vector187
vector187:
  pushl $0
  1058c6:	6a 00                	push   $0x0
  pushl $187
  1058c8:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
  1058cd:	e9 2e f4 ff ff       	jmp    104d00 <alltraps>

001058d2 <vector188>:
.globl vector188
vector188:
  pushl $0
  1058d2:	6a 00                	push   $0x0
  pushl $188
  1058d4:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
  1058d9:	e9 22 f4 ff ff       	jmp    104d00 <alltraps>

001058de <vector189>:
.globl vector189
vector189:
  pushl $0
  1058de:	6a 00                	push   $0x0
  pushl $189
  1058e0:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
  1058e5:	e9 16 f4 ff ff       	jmp    104d00 <alltraps>

001058ea <vector190>:
.globl vector190
vector190:
  pushl $0
  1058ea:	6a 00                	push   $0x0
  pushl $190
  1058ec:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
  1058f1:	e9 0a f4 ff ff       	jmp    104d00 <alltraps>

001058f6 <vector191>:
.globl vector191
vector191:
  pushl $0
  1058f6:	6a 00                	push   $0x0
  pushl $191
  1058f8:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
  1058fd:	e9 fe f3 ff ff       	jmp    104d00 <alltraps>

00105902 <vector192>:
.globl vector192
vector192:
  pushl $0
  105902:	6a 00                	push   $0x0
  pushl $192
  105904:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
  105909:	e9 f2 f3 ff ff       	jmp    104d00 <alltraps>

0010590e <vector193>:
.globl vector193
vector193:
  pushl $0
  10590e:	6a 00                	push   $0x0
  pushl $193
  105910:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
  105915:	e9 e6 f3 ff ff       	jmp    104d00 <alltraps>

0010591a <vector194>:
.globl vector194
vector194:
  pushl $0
  10591a:	6a 00                	push   $0x0
  pushl $194
  10591c:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
  105921:	e9 da f3 ff ff       	jmp    104d00 <alltraps>

00105926 <vector195>:
.globl vector195
vector195:
  pushl $0
  105926:	6a 00                	push   $0x0
  pushl $195
  105928:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
  10592d:	e9 ce f3 ff ff       	jmp    104d00 <alltraps>

00105932 <vector196>:
.globl vector196
vector196:
  pushl $0
  105932:	6a 00                	push   $0x0
  pushl $196
  105934:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
  105939:	e9 c2 f3 ff ff       	jmp    104d00 <alltraps>

0010593e <vector197>:
.globl vector197
vector197:
  pushl $0
  10593e:	6a 00                	push   $0x0
  pushl $197
  105940:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
  105945:	e9 b6 f3 ff ff       	jmp    104d00 <alltraps>

0010594a <vector198>:
.globl vector198
vector198:
  pushl $0
  10594a:	6a 00                	push   $0x0
  pushl $198
  10594c:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
  105951:	e9 aa f3 ff ff       	jmp    104d00 <alltraps>

00105956 <vector199>:
.globl vector199
vector199:
  pushl $0
  105956:	6a 00                	push   $0x0
  pushl $199
  105958:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
  10595d:	e9 9e f3 ff ff       	jmp    104d00 <alltraps>

00105962 <vector200>:
.globl vector200
vector200:
  pushl $0
  105962:	6a 00                	push   $0x0
  pushl $200
  105964:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
  105969:	e9 92 f3 ff ff       	jmp    104d00 <alltraps>

0010596e <vector201>:
.globl vector201
vector201:
  pushl $0
  10596e:	6a 00                	push   $0x0
  pushl $201
  105970:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
  105975:	e9 86 f3 ff ff       	jmp    104d00 <alltraps>

0010597a <vector202>:
.globl vector202
vector202:
  pushl $0
  10597a:	6a 00                	push   $0x0
  pushl $202
  10597c:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
  105981:	e9 7a f3 ff ff       	jmp    104d00 <alltraps>

00105986 <vector203>:
.globl vector203
vector203:
  pushl $0
  105986:	6a 00                	push   $0x0
  pushl $203
  105988:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
  10598d:	e9 6e f3 ff ff       	jmp    104d00 <alltraps>

00105992 <vector204>:
.globl vector204
vector204:
  pushl $0
  105992:	6a 00                	push   $0x0
  pushl $204
  105994:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
  105999:	e9 62 f3 ff ff       	jmp    104d00 <alltraps>

0010599e <vector205>:
.globl vector205
vector205:
  pushl $0
  10599e:	6a 00                	push   $0x0
  pushl $205
  1059a0:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
  1059a5:	e9 56 f3 ff ff       	jmp    104d00 <alltraps>

001059aa <vector206>:
.globl vector206
vector206:
  pushl $0
  1059aa:	6a 00                	push   $0x0
  pushl $206
  1059ac:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
  1059b1:	e9 4a f3 ff ff       	jmp    104d00 <alltraps>

001059b6 <vector207>:
.globl vector207
vector207:
  pushl $0
  1059b6:	6a 00                	push   $0x0
  pushl $207
  1059b8:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
  1059bd:	e9 3e f3 ff ff       	jmp    104d00 <alltraps>

001059c2 <vector208>:
.globl vector208
vector208:
  pushl $0
  1059c2:	6a 00                	push   $0x0
  pushl $208
  1059c4:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
  1059c9:	e9 32 f3 ff ff       	jmp    104d00 <alltraps>

001059ce <vector209>:
.globl vector209
vector209:
  pushl $0
  1059ce:	6a 00                	push   $0x0
  pushl $209
  1059d0:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
  1059d5:	e9 26 f3 ff ff       	jmp    104d00 <alltraps>

001059da <vector210>:
.globl vector210
vector210:
  pushl $0
  1059da:	6a 00                	push   $0x0
  pushl $210
  1059dc:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
  1059e1:	e9 1a f3 ff ff       	jmp    104d00 <alltraps>

001059e6 <vector211>:
.globl vector211
vector211:
  pushl $0
  1059e6:	6a 00                	push   $0x0
  pushl $211
  1059e8:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
  1059ed:	e9 0e f3 ff ff       	jmp    104d00 <alltraps>

001059f2 <vector212>:
.globl vector212
vector212:
  pushl $0
  1059f2:	6a 00                	push   $0x0
  pushl $212
  1059f4:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
  1059f9:	e9 02 f3 ff ff       	jmp    104d00 <alltraps>

001059fe <vector213>:
.globl vector213
vector213:
  pushl $0
  1059fe:	6a 00                	push   $0x0
  pushl $213
  105a00:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
  105a05:	e9 f6 f2 ff ff       	jmp    104d00 <alltraps>

00105a0a <vector214>:
.globl vector214
vector214:
  pushl $0
  105a0a:	6a 00                	push   $0x0
  pushl $214
  105a0c:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
  105a11:	e9 ea f2 ff ff       	jmp    104d00 <alltraps>

00105a16 <vector215>:
.globl vector215
vector215:
  pushl $0
  105a16:	6a 00                	push   $0x0
  pushl $215
  105a18:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
  105a1d:	e9 de f2 ff ff       	jmp    104d00 <alltraps>

00105a22 <vector216>:
.globl vector216
vector216:
  pushl $0
  105a22:	6a 00                	push   $0x0
  pushl $216
  105a24:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
  105a29:	e9 d2 f2 ff ff       	jmp    104d00 <alltraps>

00105a2e <vector217>:
.globl vector217
vector217:
  pushl $0
  105a2e:	6a 00                	push   $0x0
  pushl $217
  105a30:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
  105a35:	e9 c6 f2 ff ff       	jmp    104d00 <alltraps>

00105a3a <vector218>:
.globl vector218
vector218:
  pushl $0
  105a3a:	6a 00                	push   $0x0
  pushl $218
  105a3c:	68 da 00 00 00       	push   $0xda
  jmp alltraps
  105a41:	e9 ba f2 ff ff       	jmp    104d00 <alltraps>

00105a46 <vector219>:
.globl vector219
vector219:
  pushl $0
  105a46:	6a 00                	push   $0x0
  pushl $219
  105a48:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
  105a4d:	e9 ae f2 ff ff       	jmp    104d00 <alltraps>

00105a52 <vector220>:
.globl vector220
vector220:
  pushl $0
  105a52:	6a 00                	push   $0x0
  pushl $220
  105a54:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
  105a59:	e9 a2 f2 ff ff       	jmp    104d00 <alltraps>

00105a5e <vector221>:
.globl vector221
vector221:
  pushl $0
  105a5e:	6a 00                	push   $0x0
  pushl $221
  105a60:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
  105a65:	e9 96 f2 ff ff       	jmp    104d00 <alltraps>

00105a6a <vector222>:
.globl vector222
vector222:
  pushl $0
  105a6a:	6a 00                	push   $0x0
  pushl $222
  105a6c:	68 de 00 00 00       	push   $0xde
  jmp alltraps
  105a71:	e9 8a f2 ff ff       	jmp    104d00 <alltraps>

00105a76 <vector223>:
.globl vector223
vector223:
  pushl $0
  105a76:	6a 00                	push   $0x0
  pushl $223
  105a78:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
  105a7d:	e9 7e f2 ff ff       	jmp    104d00 <alltraps>

00105a82 <vector224>:
.globl vector224
vector224:
  pushl $0
  105a82:	6a 00                	push   $0x0
  pushl $224
  105a84:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
  105a89:	e9 72 f2 ff ff       	jmp    104d00 <alltraps>

00105a8e <vector225>:
.globl vector225
vector225:
  pushl $0
  105a8e:	6a 00                	push   $0x0
  pushl $225
  105a90:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
  105a95:	e9 66 f2 ff ff       	jmp    104d00 <alltraps>

00105a9a <vector226>:
.globl vector226
vector226:
  pushl $0
  105a9a:	6a 00                	push   $0x0
  pushl $226
  105a9c:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
  105aa1:	e9 5a f2 ff ff       	jmp    104d00 <alltraps>

00105aa6 <vector227>:
.globl vector227
vector227:
  pushl $0
  105aa6:	6a 00                	push   $0x0
  pushl $227
  105aa8:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
  105aad:	e9 4e f2 ff ff       	jmp    104d00 <alltraps>

00105ab2 <vector228>:
.globl vector228
vector228:
  pushl $0
  105ab2:	6a 00                	push   $0x0
  pushl $228
  105ab4:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
  105ab9:	e9 42 f2 ff ff       	jmp    104d00 <alltraps>

00105abe <vector229>:
.globl vector229
vector229:
  pushl $0
  105abe:	6a 00                	push   $0x0
  pushl $229
  105ac0:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
  105ac5:	e9 36 f2 ff ff       	jmp    104d00 <alltraps>

00105aca <vector230>:
.globl vector230
vector230:
  pushl $0
  105aca:	6a 00                	push   $0x0
  pushl $230
  105acc:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
  105ad1:	e9 2a f2 ff ff       	jmp    104d00 <alltraps>

00105ad6 <vector231>:
.globl vector231
vector231:
  pushl $0
  105ad6:	6a 00                	push   $0x0
  pushl $231
  105ad8:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
  105add:	e9 1e f2 ff ff       	jmp    104d00 <alltraps>

00105ae2 <vector232>:
.globl vector232
vector232:
  pushl $0
  105ae2:	6a 00                	push   $0x0
  pushl $232
  105ae4:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
  105ae9:	e9 12 f2 ff ff       	jmp    104d00 <alltraps>

00105aee <vector233>:
.globl vector233
vector233:
  pushl $0
  105aee:	6a 00                	push   $0x0
  pushl $233
  105af0:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
  105af5:	e9 06 f2 ff ff       	jmp    104d00 <alltraps>

00105afa <vector234>:
.globl vector234
vector234:
  pushl $0
  105afa:	6a 00                	push   $0x0
  pushl $234
  105afc:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
  105b01:	e9 fa f1 ff ff       	jmp    104d00 <alltraps>

00105b06 <vector235>:
.globl vector235
vector235:
  pushl $0
  105b06:	6a 00                	push   $0x0
  pushl $235
  105b08:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
  105b0d:	e9 ee f1 ff ff       	jmp    104d00 <alltraps>

00105b12 <vector236>:
.globl vector236
vector236:
  pushl $0
  105b12:	6a 00                	push   $0x0
  pushl $236
  105b14:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
  105b19:	e9 e2 f1 ff ff       	jmp    104d00 <alltraps>

00105b1e <vector237>:
.globl vector237
vector237:
  pushl $0
  105b1e:	6a 00                	push   $0x0
  pushl $237
  105b20:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
  105b25:	e9 d6 f1 ff ff       	jmp    104d00 <alltraps>

00105b2a <vector238>:
.globl vector238
vector238:
  pushl $0
  105b2a:	6a 00                	push   $0x0
  pushl $238
  105b2c:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
  105b31:	e9 ca f1 ff ff       	jmp    104d00 <alltraps>

00105b36 <vector239>:
.globl vector239
vector239:
  pushl $0
  105b36:	6a 00                	push   $0x0
  pushl $239
  105b38:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
  105b3d:	e9 be f1 ff ff       	jmp    104d00 <alltraps>

00105b42 <vector240>:
.globl vector240
vector240:
  pushl $0
  105b42:	6a 00                	push   $0x0
  pushl $240
  105b44:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
  105b49:	e9 b2 f1 ff ff       	jmp    104d00 <alltraps>

00105b4e <vector241>:
.globl vector241
vector241:
  pushl $0
  105b4e:	6a 00                	push   $0x0
  pushl $241
  105b50:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
  105b55:	e9 a6 f1 ff ff       	jmp    104d00 <alltraps>

00105b5a <vector242>:
.globl vector242
vector242:
  pushl $0
  105b5a:	6a 00                	push   $0x0
  pushl $242
  105b5c:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
  105b61:	e9 9a f1 ff ff       	jmp    104d00 <alltraps>

00105b66 <vector243>:
.globl vector243
vector243:
  pushl $0
  105b66:	6a 00                	push   $0x0
  pushl $243
  105b68:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
  105b6d:	e9 8e f1 ff ff       	jmp    104d00 <alltraps>

00105b72 <vector244>:
.globl vector244
vector244:
  pushl $0
  105b72:	6a 00                	push   $0x0
  pushl $244
  105b74:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
  105b79:	e9 82 f1 ff ff       	jmp    104d00 <alltraps>

00105b7e <vector245>:
.globl vector245
vector245:
  pushl $0
  105b7e:	6a 00                	push   $0x0
  pushl $245
  105b80:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
  105b85:	e9 76 f1 ff ff       	jmp    104d00 <alltraps>

00105b8a <vector246>:
.globl vector246
vector246:
  pushl $0
  105b8a:	6a 00                	push   $0x0
  pushl $246
  105b8c:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
  105b91:	e9 6a f1 ff ff       	jmp    104d00 <alltraps>

00105b96 <vector247>:
.globl vector247
vector247:
  pushl $0
  105b96:	6a 00                	push   $0x0
  pushl $247
  105b98:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
  105b9d:	e9 5e f1 ff ff       	jmp    104d00 <alltraps>

00105ba2 <vector248>:
.globl vector248
vector248:
  pushl $0
  105ba2:	6a 00                	push   $0x0
  pushl $248
  105ba4:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
  105ba9:	e9 52 f1 ff ff       	jmp    104d00 <alltraps>

00105bae <vector249>:
.globl vector249
vector249:
  pushl $0
  105bae:	6a 00                	push   $0x0
  pushl $249
  105bb0:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
  105bb5:	e9 46 f1 ff ff       	jmp    104d00 <alltraps>

00105bba <vector250>:
.globl vector250
vector250:
  pushl $0
  105bba:	6a 00                	push   $0x0
  pushl $250
  105bbc:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
  105bc1:	e9 3a f1 ff ff       	jmp    104d00 <alltraps>

00105bc6 <vector251>:
.globl vector251
vector251:
  pushl $0
  105bc6:	6a 00                	push   $0x0
  pushl $251
  105bc8:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
  105bcd:	e9 2e f1 ff ff       	jmp    104d00 <alltraps>

00105bd2 <vector252>:
.globl vector252
vector252:
  pushl $0
  105bd2:	6a 00                	push   $0x0
  pushl $252
  105bd4:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
  105bd9:	e9 22 f1 ff ff       	jmp    104d00 <alltraps>

00105bde <vector253>:
.globl vector253
vector253:
  pushl $0
  105bde:	6a 00                	push   $0x0
  pushl $253
  105be0:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
  105be5:	e9 16 f1 ff ff       	jmp    104d00 <alltraps>

00105bea <vector254>:
.globl vector254
vector254:
  pushl $0
  105bea:	6a 00                	push   $0x0
  pushl $254
  105bec:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
  105bf1:	e9 0a f1 ff ff       	jmp    104d00 <alltraps>

00105bf6 <vector255>:
.globl vector255
vector255:
  pushl $0
  105bf6:	6a 00                	push   $0x0
  pushl $255
  105bf8:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
  105bfd:	e9 fe f0 ff ff       	jmp    104d00 <alltraps>
  105c02:	90                   	nop
  105c03:	90                   	nop
  105c04:	90                   	nop
  105c05:	90                   	nop
  105c06:	90                   	nop
  105c07:	90                   	nop
  105c08:	90                   	nop
  105c09:	90                   	nop
  105c0a:	90                   	nop
  105c0b:	90                   	nop
  105c0c:	90                   	nop
  105c0d:	90                   	nop
  105c0e:	90                   	nop
  105c0f:	90                   	nop

00105c10 <vmenable>:
}

// Turn on paging.
void
vmenable(void)
{
  105c10:	55                   	push   %ebp
}

static inline void
lcr3(uint val) 
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
  105c11:	a1 d0 78 10 00       	mov    0x1078d0,%eax
  105c16:	89 e5                	mov    %esp,%ebp
  105c18:	0f 22 d8             	mov    %eax,%cr3

static inline uint
rcr0(void)
{
  uint val;
  asm volatile("movl %%cr0,%0" : "=r" (val));
  105c1b:	0f 20 c0             	mov    %cr0,%eax
}

static inline void
lcr0(uint val)
{
  asm volatile("movl %0,%%cr0" : : "r" (val));
  105c1e:	0d 00 00 00 80       	or     $0x80000000,%eax
  105c23:	0f 22 c0             	mov    %eax,%cr0

  switchkvm(); // load kpgdir into cr3
  cr0 = rcr0();
  cr0 |= CR0_PG;
  lcr0(cr0);
}
  105c26:	5d                   	pop    %ebp
  105c27:	c3                   	ret    
  105c28:	90                   	nop
  105c29:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00105c30 <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
  105c30:	55                   	push   %ebp
}

static inline void
lcr3(uint val) 
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
  105c31:	a1 d0 78 10 00       	mov    0x1078d0,%eax
  105c36:	89 e5                	mov    %esp,%ebp
  105c38:	0f 22 d8             	mov    %eax,%cr3
  lcr3(PADDR(kpgdir));   // switch to the kernel page table
}
  105c3b:	5d                   	pop    %ebp
  105c3c:	c3                   	ret    
  105c3d:	8d 76 00             	lea    0x0(%esi),%esi

00105c40 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to linear address va.  If create!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int create)
{
  105c40:	55                   	push   %ebp
  105c41:	89 e5                	mov    %esp,%ebp
  105c43:	83 ec 28             	sub    $0x28,%esp
  105c46:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
  105c49:	89 d3                	mov    %edx,%ebx
  105c4b:	c1 eb 16             	shr    $0x16,%ebx
  105c4e:	8d 1c 98             	lea    (%eax,%ebx,4),%ebx
// Return the address of the PTE in page table pgdir
// that corresponds to linear address va.  If create!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int create)
{
  105c51:	89 75 fc             	mov    %esi,-0x4(%ebp)
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
  if(*pde & PTE_P){
  105c54:	8b 33                	mov    (%ebx),%esi
  105c56:	f7 c6 01 00 00 00    	test   $0x1,%esi
  105c5c:	74 22                	je     105c80 <walkpgdir+0x40>
    pgtab = (pte_t*)PTE_ADDR(*pde);
  105c5e:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table 
    // entries, if necessary.
    *pde = PADDR(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
  105c64:	c1 ea 0a             	shr    $0xa,%edx
  105c67:	81 e2 fc 0f 00 00    	and    $0xffc,%edx
  105c6d:	8d 04 16             	lea    (%esi,%edx,1),%eax
}
  105c70:	8b 5d f8             	mov    -0x8(%ebp),%ebx
  105c73:	8b 75 fc             	mov    -0x4(%ebp),%esi
  105c76:	89 ec                	mov    %ebp,%esp
  105c78:	5d                   	pop    %ebp
  105c79:	c3                   	ret    
  105c7a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

  pde = &pgdir[PDX(va)];
  if(*pde & PTE_P){
    pgtab = (pte_t*)PTE_ADDR(*pde);
  } else {
    if(!create || (pgtab = (pte_t*)kalloc()) == 0)
  105c80:	85 c9                	test   %ecx,%ecx
  105c82:	75 04                	jne    105c88 <walkpgdir+0x48>
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table 
    // entries, if necessary.
    *pde = PADDR(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
  105c84:	31 c0                	xor    %eax,%eax
  105c86:	eb e8                	jmp    105c70 <walkpgdir+0x30>

  pde = &pgdir[PDX(va)];
  if(*pde & PTE_P){
    pgtab = (pte_t*)PTE_ADDR(*pde);
  } else {
    if(!create || (pgtab = (pte_t*)kalloc()) == 0)
  105c88:	89 55 f4             	mov    %edx,-0xc(%ebp)
  105c8b:	e8 d0 c5 ff ff       	call   102260 <kalloc>
  105c90:	85 c0                	test   %eax,%eax
  105c92:	89 c6                	mov    %eax,%esi
  105c94:	74 ee                	je     105c84 <walkpgdir+0x44>
      return 0;
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
  105c96:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  105c9d:	00 
  105c9e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  105ca5:	00 
  105ca6:	89 04 24             	mov    %eax,(%esp)
  105ca9:	e8 22 df ff ff       	call   103bd0 <memset>
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table 
    // entries, if necessary.
    *pde = PADDR(pgtab) | PTE_P | PTE_W | PTE_U;
  105cae:	89 f0                	mov    %esi,%eax
  105cb0:	83 c8 07             	or     $0x7,%eax
  105cb3:	89 03                	mov    %eax,(%ebx)
  105cb5:	8b 55 f4             	mov    -0xc(%ebp),%edx
  105cb8:	eb aa                	jmp    105c64 <walkpgdir+0x24>
  105cba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00105cc0 <uva2ka>:
}

// Map user virtual address to kernel physical address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
  105cc0:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  105cc1:	31 c9                	xor    %ecx,%ecx
}

// Map user virtual address to kernel physical address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
  105cc3:	89 e5                	mov    %esp,%ebp
  105cc5:	83 ec 08             	sub    $0x8,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  105cc8:	8b 55 0c             	mov    0xc(%ebp),%edx
  105ccb:	8b 45 08             	mov    0x8(%ebp),%eax
  105cce:	e8 6d ff ff ff       	call   105c40 <walkpgdir>
  if((*pte & PTE_P) == 0)
  105cd3:	8b 00                	mov    (%eax),%eax
  105cd5:	a8 01                	test   $0x1,%al
  105cd7:	75 07                	jne    105ce0 <uva2ka+0x20>
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)PTE_ADDR(*pte);
  105cd9:	31 c0                	xor    %eax,%eax
}
  105cdb:	c9                   	leave  
  105cdc:	c3                   	ret    
  105cdd:	8d 76 00             	lea    0x0(%esi),%esi
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if((*pte & PTE_P) == 0)
    return 0;
  if((*pte & PTE_U) == 0)
  105ce0:	a8 04                	test   $0x4,%al
  105ce2:	74 f5                	je     105cd9 <uva2ka+0x19>
    return 0;
  return (char*)PTE_ADDR(*pte);
  105ce4:	25 00 f0 ff ff       	and    $0xfffff000,%eax
}
  105ce9:	c9                   	leave  
  105cea:	c3                   	ret    
  105ceb:	90                   	nop
  105cec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00105cf0 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
  105cf0:	55                   	push   %ebp
  105cf1:	89 e5                	mov    %esp,%ebp
  105cf3:	57                   	push   %edi
  105cf4:	56                   	push   %esi
  105cf5:	53                   	push   %ebx
  105cf6:	83 ec 2c             	sub    $0x2c,%esp
  105cf9:	8b 5d 14             	mov    0x14(%ebp),%ebx
  105cfc:	8b 55 0c             	mov    0xc(%ebp),%edx
  char *buf, *pa0;
  uint n, va0;
  
  buf = (char*)p;
  while(len > 0){
  105cff:	85 db                	test   %ebx,%ebx
  105d01:	74 75                	je     105d78 <copyout+0x88>
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
  char *buf, *pa0;
  uint n, va0;
  
  buf = (char*)p;
  105d03:	8b 45 10             	mov    0x10(%ebp),%eax
  105d06:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  105d09:	eb 39                	jmp    105d44 <copyout+0x54>
  105d0b:	90                   	nop
  105d0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
  105d10:	89 f7                	mov    %esi,%edi
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
  105d12:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
  105d15:	29 d7                	sub    %edx,%edi
  105d17:	81 c7 00 10 00 00    	add    $0x1000,%edi
  105d1d:	39 df                	cmp    %ebx,%edi
  105d1f:	0f 47 fb             	cmova  %ebx,%edi
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
  105d22:	29 f2                	sub    %esi,%edx
  105d24:	8d 14 10             	lea    (%eax,%edx,1),%edx
  105d27:	89 7c 24 08          	mov    %edi,0x8(%esp)
  105d2b:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  105d2f:	89 14 24             	mov    %edx,(%esp)
  105d32:	e8 19 df ff ff       	call   103c50 <memmove>
{
  char *buf, *pa0;
  uint n, va0;
  
  buf = (char*)p;
  while(len > 0){
  105d37:	29 fb                	sub    %edi,%ebx
  105d39:	74 3d                	je     105d78 <copyout+0x88>
    n = PGSIZE - (va - va0);
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
  105d3b:	01 7d e4             	add    %edi,-0x1c(%ebp)
    va = va0 + PGSIZE;
  105d3e:	8d 96 00 10 00 00    	lea    0x1000(%esi),%edx
  uint n, va0;
  
  buf = (char*)p;
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
  105d44:	8b 4d 08             	mov    0x8(%ebp),%ecx
  char *buf, *pa0;
  uint n, va0;
  
  buf = (char*)p;
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
  105d47:	89 d6                	mov    %edx,%esi
  105d49:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
    pa0 = uva2ka(pgdir, (char*)va0);
  105d4f:	89 55 e0             	mov    %edx,-0x20(%ebp)
  105d52:	89 74 24 04          	mov    %esi,0x4(%esp)
  105d56:	89 0c 24             	mov    %ecx,(%esp)
  105d59:	e8 62 ff ff ff       	call   105cc0 <uva2ka>
    if(pa0 == 0)
  105d5e:	8b 55 e0             	mov    -0x20(%ebp),%edx
  105d61:	85 c0                	test   %eax,%eax
  105d63:	75 ab                	jne    105d10 <copyout+0x20>
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
}
  105d65:	83 c4 2c             	add    $0x2c,%esp
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  105d68:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  return 0;
}
  105d6d:	5b                   	pop    %ebx
  105d6e:	5e                   	pop    %esi
  105d6f:	5f                   	pop    %edi
  105d70:	5d                   	pop    %ebp
  105d71:	c3                   	ret    
  105d72:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  105d78:	83 c4 2c             	add    $0x2c,%esp
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  105d7b:	31 c0                	xor    %eax,%eax
  }
  return 0;
}
  105d7d:	5b                   	pop    %ebx
  105d7e:	5e                   	pop    %esi
  105d7f:	5f                   	pop    %edi
  105d80:	5d                   	pop    %ebp
  105d81:	c3                   	ret    
  105d82:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  105d89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00105d90 <mappages>:
// Create PTEs for linear addresses starting at la that refer to
// physical addresses starting at pa. la and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *la, uint size, uint pa, int perm)
{
  105d90:	55                   	push   %ebp
  105d91:	89 e5                	mov    %esp,%ebp
  105d93:	57                   	push   %edi
  105d94:	56                   	push   %esi
  105d95:	53                   	push   %ebx
  char *a, *last;
  pte_t *pte;
  
  a = PGROUNDDOWN(la);
  105d96:	89 d3                	mov    %edx,%ebx
  last = PGROUNDDOWN(la + size - 1);
  105d98:	8d 7c 0a ff          	lea    -0x1(%edx,%ecx,1),%edi
// Create PTEs for linear addresses starting at la that refer to
// physical addresses starting at pa. la and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *la, uint size, uint pa, int perm)
{
  105d9c:	83 ec 2c             	sub    $0x2c,%esp
  105d9f:	8b 75 08             	mov    0x8(%ebp),%esi
  105da2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  char *a, *last;
  pte_t *pte;
  
  a = PGROUNDDOWN(la);
  105da5:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  last = PGROUNDDOWN(la + size - 1);
  105dab:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
    pte = walkpgdir(pgdir, a, 1);
    if(pte == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
  105db1:	83 4d 0c 01          	orl    $0x1,0xc(%ebp)
  105db5:	eb 1d                	jmp    105dd4 <mappages+0x44>
  105db7:	90                   	nop
  last = PGROUNDDOWN(la + size - 1);
  for(;;){
    pte = walkpgdir(pgdir, a, 1);
    if(pte == 0)
      return -1;
    if(*pte & PTE_P)
  105db8:	f6 00 01             	testb  $0x1,(%eax)
  105dbb:	75 45                	jne    105e02 <mappages+0x72>
      panic("remap");
    *pte = pa | perm | PTE_P;
  105dbd:	8b 55 0c             	mov    0xc(%ebp),%edx
  105dc0:	09 f2                	or     %esi,%edx
    if(a == last)
  105dc2:	39 fb                	cmp    %edi,%ebx
    pte = walkpgdir(pgdir, a, 1);
    if(pte == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
  105dc4:	89 10                	mov    %edx,(%eax)
    if(a == last)
  105dc6:	74 30                	je     105df8 <mappages+0x68>
      break;
    a += PGSIZE;
  105dc8:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    pa += PGSIZE;
  105dce:	81 c6 00 10 00 00    	add    $0x1000,%esi
  pte_t *pte;
  
  a = PGROUNDDOWN(la);
  last = PGROUNDDOWN(la + size - 1);
  for(;;){
    pte = walkpgdir(pgdir, a, 1);
  105dd4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  105dd7:	b9 01 00 00 00       	mov    $0x1,%ecx
  105ddc:	89 da                	mov    %ebx,%edx
  105dde:	e8 5d fe ff ff       	call   105c40 <walkpgdir>
    if(pte == 0)
  105de3:	85 c0                	test   %eax,%eax
  105de5:	75 d1                	jne    105db8 <mappages+0x28>
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}
  105de7:	83 c4 2c             	add    $0x2c,%esp
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  105dea:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return 0;
}
  105def:	5b                   	pop    %ebx
  105df0:	5e                   	pop    %esi
  105df1:	5f                   	pop    %edi
  105df2:	5d                   	pop    %ebp
  105df3:	c3                   	ret    
  105df4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  105df8:	83 c4 2c             	add    $0x2c,%esp
    if(pte == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
  105dfb:	31 c0                	xor    %eax,%eax
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}
  105dfd:	5b                   	pop    %ebx
  105dfe:	5e                   	pop    %esi
  105dff:	5f                   	pop    %edi
  105e00:	5d                   	pop    %ebp
  105e01:	c3                   	ret    
  for(;;){
    pte = walkpgdir(pgdir, a, 1);
    if(pte == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
  105e02:	c7 04 24 8c 6c 10 00 	movl   $0x106c8c,(%esp)
  105e09:	e8 02 ab ff ff       	call   100910 <panic>
  105e0e:	66 90                	xchg   %ax,%ax

00105e10 <setupkvm>:
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
  105e10:	55                   	push   %ebp
  105e11:	89 e5                	mov    %esp,%ebp
  105e13:	56                   	push   %esi
  105e14:	53                   	push   %ebx
  105e15:	83 ec 10             	sub    $0x10,%esp
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
  105e18:	e8 43 c4 ff ff       	call   102260 <kalloc>
  105e1d:	85 c0                	test   %eax,%eax
  105e1f:	89 c6                	mov    %eax,%esi
  105e21:	74 50                	je     105e73 <setupkvm+0x63>
    return 0;
  memset(pgdir, 0, PGSIZE);
  105e23:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  105e2a:	00 
  105e2b:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  105e32:	00 
  105e33:	89 04 24             	mov    %eax,(%esp)
  105e36:	e8 95 dd ff ff       	call   103bd0 <memset>
  k = kmap;
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
  105e3b:	b8 70 77 10 00       	mov    $0x107770,%eax
  105e40:	3d 40 77 10 00       	cmp    $0x107740,%eax
  105e45:	76 2c                	jbe    105e73 <setupkvm+0x63>
  {(void*)0xFE000000, 0,               PTE_W},  // device mappings
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
  105e47:	bb 40 77 10 00       	mov    $0x107740,%ebx
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  k = kmap;
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->p, k->e - k->p, (uint)k->p, k->perm) < 0)
  105e4c:	8b 13                	mov    (%ebx),%edx
  105e4e:	8b 43 08             	mov    0x8(%ebx),%eax
  105e51:	8b 4b 04             	mov    0x4(%ebx),%ecx
  105e54:	89 14 24             	mov    %edx,(%esp)
  105e57:	89 44 24 04          	mov    %eax,0x4(%esp)
  105e5b:	89 f0                	mov    %esi,%eax
  105e5d:	29 d1                	sub    %edx,%ecx
  105e5f:	e8 2c ff ff ff       	call   105d90 <mappages>
  105e64:	85 c0                	test   %eax,%eax
  105e66:	78 18                	js     105e80 <setupkvm+0x70>

  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  k = kmap;
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
  105e68:	83 c3 0c             	add    $0xc,%ebx
  105e6b:	81 fb 70 77 10 00    	cmp    $0x107770,%ebx
  105e71:	75 d9                	jne    105e4c <setupkvm+0x3c>
    if(mappages(pgdir, k->p, k->e - k->p, (uint)k->p, k->perm) < 0)
      return 0;

  return pgdir;
}
  105e73:	83 c4 10             	add    $0x10,%esp
  105e76:	89 f0                	mov    %esi,%eax
  105e78:	5b                   	pop    %ebx
  105e79:	5e                   	pop    %esi
  105e7a:	5d                   	pop    %ebp
  105e7b:	c3                   	ret    
  105e7c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  k = kmap;
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->p, k->e - k->p, (uint)k->p, k->perm) < 0)
  105e80:	31 f6                	xor    %esi,%esi
      return 0;

  return pgdir;
}
  105e82:	83 c4 10             	add    $0x10,%esp
  105e85:	89 f0                	mov    %esi,%eax
  105e87:	5b                   	pop    %ebx
  105e88:	5e                   	pop    %esi
  105e89:	5d                   	pop    %ebp
  105e8a:	c3                   	ret    
  105e8b:	90                   	nop
  105e8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00105e90 <kvmalloc>:

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
  105e90:	55                   	push   %ebp
  105e91:	89 e5                	mov    %esp,%ebp
  105e93:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
  105e96:	e8 75 ff ff ff       	call   105e10 <setupkvm>
  105e9b:	a3 d0 78 10 00       	mov    %eax,0x1078d0
}
  105ea0:	c9                   	leave  
  105ea1:	c3                   	ret    
  105ea2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  105ea9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00105eb0 <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
  105eb0:	55                   	push   %ebp
  105eb1:	89 e5                	mov    %esp,%ebp
  105eb3:	83 ec 38             	sub    $0x38,%esp
  105eb6:	89 75 f8             	mov    %esi,-0x8(%ebp)
  105eb9:	8b 75 10             	mov    0x10(%ebp),%esi
  105ebc:	8b 45 08             	mov    0x8(%ebp),%eax
  105ebf:	89 7d fc             	mov    %edi,-0x4(%ebp)
  105ec2:	8b 7d 0c             	mov    0xc(%ebp),%edi
  105ec5:	89 5d f4             	mov    %ebx,-0xc(%ebp)
  char *mem;
  
  if(sz >= PGSIZE)
  105ec8:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
  105ece:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  char *mem;
  
  if(sz >= PGSIZE)
  105ed1:	77 53                	ja     105f26 <inituvm+0x76>
    panic("inituvm: more than a page");
  mem = kalloc();
  105ed3:	e8 88 c3 ff ff       	call   102260 <kalloc>
  memset(mem, 0, PGSIZE);
  105ed8:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  105edf:	00 
  105ee0:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  105ee7:	00 
{
  char *mem;
  
  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  105ee8:	89 c3                	mov    %eax,%ebx
  memset(mem, 0, PGSIZE);
  105eea:	89 04 24             	mov    %eax,(%esp)
  105eed:	e8 de dc ff ff       	call   103bd0 <memset>
  mappages(pgdir, 0, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  105ef2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  105ef5:	b9 00 10 00 00       	mov    $0x1000,%ecx
  105efa:	31 d2                	xor    %edx,%edx
  105efc:	89 1c 24             	mov    %ebx,(%esp)
  105eff:	c7 44 24 04 06 00 00 	movl   $0x6,0x4(%esp)
  105f06:	00 
  105f07:	e8 84 fe ff ff       	call   105d90 <mappages>
  memmove(mem, init, sz);
  105f0c:	89 75 10             	mov    %esi,0x10(%ebp)
}
  105f0f:	8b 75 f8             	mov    -0x8(%ebp),%esi
  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pgdir, 0, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  memmove(mem, init, sz);
  105f12:	89 7d 0c             	mov    %edi,0xc(%ebp)
}
  105f15:	8b 7d fc             	mov    -0x4(%ebp),%edi
  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pgdir, 0, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  memmove(mem, init, sz);
  105f18:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
  105f1b:	8b 5d f4             	mov    -0xc(%ebp),%ebx
  105f1e:	89 ec                	mov    %ebp,%esp
  105f20:	5d                   	pop    %ebp
  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pgdir, 0, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  memmove(mem, init, sz);
  105f21:	e9 2a dd ff ff       	jmp    103c50 <memmove>
inituvm(pde_t *pgdir, char *init, uint sz)
{
  char *mem;
  
  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  105f26:	c7 04 24 92 6c 10 00 	movl   $0x106c92,(%esp)
  105f2d:	e8 de a9 ff ff       	call   100910 <panic>
  105f32:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  105f39:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00105f40 <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  105f40:	55                   	push   %ebp
  105f41:	89 e5                	mov    %esp,%ebp
  105f43:	57                   	push   %edi
  105f44:	56                   	push   %esi
  105f45:	53                   	push   %ebx
  105f46:	83 ec 2c             	sub    $0x2c,%esp
  105f49:	8b 75 0c             	mov    0xc(%ebp),%esi
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
  105f4c:	39 75 10             	cmp    %esi,0x10(%ebp)
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  105f4f:	8b 7d 08             	mov    0x8(%ebp),%edi
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
    return oldsz;
  105f52:	89 f0                	mov    %esi,%eax
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
  105f54:	73 59                	jae    105faf <deallocuvm+0x6f>
    return oldsz;

  a = PGROUNDUP(newsz);
  105f56:	8b 5d 10             	mov    0x10(%ebp),%ebx
  105f59:	81 c3 ff 0f 00 00    	add    $0xfff,%ebx
  105f5f:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; a  < oldsz; a += PGSIZE){
  105f65:	39 de                	cmp    %ebx,%esi
  105f67:	76 43                	jbe    105fac <deallocuvm+0x6c>
  105f69:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    pte = walkpgdir(pgdir, (char*)a, 0);
  105f70:	31 c9                	xor    %ecx,%ecx
  105f72:	89 da                	mov    %ebx,%edx
  105f74:	89 f8                	mov    %edi,%eax
  105f76:	e8 c5 fc ff ff       	call   105c40 <walkpgdir>
    if(pte && (*pte & PTE_P) != 0){
  105f7b:	85 c0                	test   %eax,%eax
  105f7d:	74 23                	je     105fa2 <deallocuvm+0x62>
  105f7f:	8b 10                	mov    (%eax),%edx
  105f81:	f6 c2 01             	test   $0x1,%dl
  105f84:	74 1c                	je     105fa2 <deallocuvm+0x62>
      pa = PTE_ADDR(*pte);
      if(pa == 0)
  105f86:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
  105f8c:	74 29                	je     105fb7 <deallocuvm+0x77>
        panic("kfree");
      kfree((char*)pa);
  105f8e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  105f91:	89 14 24             	mov    %edx,(%esp)
  105f94:	e8 07 c3 ff ff       	call   1022a0 <kfree>
      *pte = 0;
  105f99:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  105f9c:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
  105fa2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  105fa8:	39 de                	cmp    %ebx,%esi
  105faa:	77 c4                	ja     105f70 <deallocuvm+0x30>
        panic("kfree");
      kfree((char*)pa);
      *pte = 0;
    }
  }
  return newsz;
  105fac:	8b 45 10             	mov    0x10(%ebp),%eax
}
  105faf:	83 c4 2c             	add    $0x2c,%esp
  105fb2:	5b                   	pop    %ebx
  105fb3:	5e                   	pop    %esi
  105fb4:	5f                   	pop    %edi
  105fb5:	5d                   	pop    %ebp
  105fb6:	c3                   	ret    
  for(; a  < oldsz; a += PGSIZE){
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(pte && (*pte & PTE_P) != 0){
      pa = PTE_ADDR(*pte);
      if(pa == 0)
        panic("kfree");
  105fb7:	c7 04 24 3e 66 10 00 	movl   $0x10663e,(%esp)
  105fbe:	e8 4d a9 ff ff       	call   100910 <panic>
  105fc3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  105fc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00105fd0 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
  105fd0:	55                   	push   %ebp
  105fd1:	89 e5                	mov    %esp,%ebp
  105fd3:	56                   	push   %esi
  105fd4:	53                   	push   %ebx
  105fd5:	83 ec 10             	sub    $0x10,%esp
  105fd8:	8b 5d 08             	mov    0x8(%ebp),%ebx
  uint i;

  if(pgdir == 0)
  105fdb:	85 db                	test   %ebx,%ebx
  105fdd:	74 59                	je     106038 <freevm+0x68>
    panic("freevm: no pgdir");
  deallocuvm(pgdir, USERTOP, 0);
  105fdf:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  105fe6:	00 
  105fe7:	31 f6                	xor    %esi,%esi
  105fe9:	c7 44 24 04 00 00 0a 	movl   $0xa0000,0x4(%esp)
  105ff0:	00 
  105ff1:	89 1c 24             	mov    %ebx,(%esp)
  105ff4:	e8 47 ff ff ff       	call   105f40 <deallocuvm>
  105ff9:	eb 10                	jmp    10600b <freevm+0x3b>
  105ffb:	90                   	nop
  105ffc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(i = 0; i < NPDENTRIES; i++){
  106000:	83 c6 01             	add    $0x1,%esi
  106003:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  106009:	74 1f                	je     10602a <freevm+0x5a>
    if(pgdir[i] & PTE_P)
  10600b:	8b 04 b3             	mov    (%ebx,%esi,4),%eax
  10600e:	a8 01                	test   $0x1,%al
  106010:	74 ee                	je     106000 <freevm+0x30>
      kfree((char*)PTE_ADDR(pgdir[i]));
  106012:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, USERTOP, 0);
  for(i = 0; i < NPDENTRIES; i++){
  106017:	83 c6 01             	add    $0x1,%esi
    if(pgdir[i] & PTE_P)
      kfree((char*)PTE_ADDR(pgdir[i]));
  10601a:	89 04 24             	mov    %eax,(%esp)
  10601d:	e8 7e c2 ff ff       	call   1022a0 <kfree>
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, USERTOP, 0);
  for(i = 0; i < NPDENTRIES; i++){
  106022:	81 fe 00 04 00 00    	cmp    $0x400,%esi
  106028:	75 e1                	jne    10600b <freevm+0x3b>
    if(pgdir[i] & PTE_P)
      kfree((char*)PTE_ADDR(pgdir[i]));
  }
  kfree((char*)pgdir);
  10602a:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
  10602d:	83 c4 10             	add    $0x10,%esp
  106030:	5b                   	pop    %ebx
  106031:	5e                   	pop    %esi
  106032:	5d                   	pop    %ebp
  deallocuvm(pgdir, USERTOP, 0);
  for(i = 0; i < NPDENTRIES; i++){
    if(pgdir[i] & PTE_P)
      kfree((char*)PTE_ADDR(pgdir[i]));
  }
  kfree((char*)pgdir);
  106033:	e9 68 c2 ff ff       	jmp    1022a0 <kfree>
freevm(pde_t *pgdir)
{
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  106038:	c7 04 24 ac 6c 10 00 	movl   $0x106cac,(%esp)
  10603f:	e8 cc a8 ff ff       	call   100910 <panic>
  106044:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  10604a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00106050 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
  106050:	55                   	push   %ebp
  106051:	89 e5                	mov    %esp,%ebp
  106053:	57                   	push   %edi
  106054:	56                   	push   %esi
  106055:	53                   	push   %ebx
  106056:	83 ec 2c             	sub    $0x2c,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i;
  char *mem;

  if((d = setupkvm()) == 0)
  106059:	e8 b2 fd ff ff       	call   105e10 <setupkvm>
  10605e:	85 c0                	test   %eax,%eax
  106060:	89 c6                	mov    %eax,%esi
  106062:	0f 84 84 00 00 00    	je     1060ec <copyuvm+0x9c>
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
  106068:	8b 45 0c             	mov    0xc(%ebp),%eax
  10606b:	85 c0                	test   %eax,%eax
  10606d:	74 7d                	je     1060ec <copyuvm+0x9c>
  10606f:	31 db                	xor    %ebx,%ebx
  106071:	eb 47                	jmp    1060ba <copyuvm+0x6a>
  106073:	90                   	nop
  106074:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)pa, PGSIZE);
  106078:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
  10607e:	89 54 24 04          	mov    %edx,0x4(%esp)
  106082:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  106089:	00 
  10608a:	89 04 24             	mov    %eax,(%esp)
  10608d:	e8 be db ff ff       	call   103c50 <memmove>
    if(mappages(d, (void*)i, PGSIZE, PADDR(mem), PTE_W|PTE_U) < 0)
  106092:	b9 00 10 00 00       	mov    $0x1000,%ecx
  106097:	89 da                	mov    %ebx,%edx
  106099:	89 f0                	mov    %esi,%eax
  10609b:	c7 44 24 04 06 00 00 	movl   $0x6,0x4(%esp)
  1060a2:	00 
  1060a3:	89 3c 24             	mov    %edi,(%esp)
  1060a6:	e8 e5 fc ff ff       	call   105d90 <mappages>
  1060ab:	85 c0                	test   %eax,%eax
  1060ad:	78 33                	js     1060e2 <copyuvm+0x92>
  uint pa, i;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
  1060af:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  1060b5:	39 5d 0c             	cmp    %ebx,0xc(%ebp)
  1060b8:	76 32                	jbe    1060ec <copyuvm+0x9c>
    if((pte = walkpgdir(pgdir, (void*)i, 0)) == 0)
  1060ba:	8b 45 08             	mov    0x8(%ebp),%eax
  1060bd:	31 c9                	xor    %ecx,%ecx
  1060bf:	89 da                	mov    %ebx,%edx
  1060c1:	e8 7a fb ff ff       	call   105c40 <walkpgdir>
  1060c6:	85 c0                	test   %eax,%eax
  1060c8:	74 2c                	je     1060f6 <copyuvm+0xa6>
      panic("copyuvm: pte should exist");
    if(!(*pte & PTE_P))
  1060ca:	8b 10                	mov    (%eax),%edx
  1060cc:	f6 c2 01             	test   $0x1,%dl
  1060cf:	74 31                	je     106102 <copyuvm+0xb2>
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    if((mem = kalloc()) == 0)
  1060d1:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  1060d4:	e8 87 c1 ff ff       	call   102260 <kalloc>
  1060d9:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  1060dc:	85 c0                	test   %eax,%eax
  1060de:	89 c7                	mov    %eax,%edi
  1060e0:	75 96                	jne    106078 <copyuvm+0x28>
      goto bad;
  }
  return d;

bad:
  freevm(d);
  1060e2:	89 34 24             	mov    %esi,(%esp)
  1060e5:	31 f6                	xor    %esi,%esi
  1060e7:	e8 e4 fe ff ff       	call   105fd0 <freevm>
  return 0;
}
  1060ec:	83 c4 2c             	add    $0x2c,%esp
  1060ef:	89 f0                	mov    %esi,%eax
  1060f1:	5b                   	pop    %ebx
  1060f2:	5e                   	pop    %esi
  1060f3:	5f                   	pop    %edi
  1060f4:	5d                   	pop    %ebp
  1060f5:	c3                   	ret    

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, (void*)i, 0)) == 0)
      panic("copyuvm: pte should exist");
  1060f6:	c7 04 24 bd 6c 10 00 	movl   $0x106cbd,(%esp)
  1060fd:	e8 0e a8 ff ff       	call   100910 <panic>
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
  106102:	c7 04 24 d7 6c 10 00 	movl   $0x106cd7,(%esp)
  106109:	e8 02 a8 ff ff       	call   100910 <panic>
  10610e:	66 90                	xchg   %ax,%ax

00106110 <allocuvm>:

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  106110:	55                   	push   %ebp
  char *mem;
  uint a;

  if(newsz > USERTOP)
  106111:	31 c0                	xor    %eax,%eax

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  106113:	89 e5                	mov    %esp,%ebp
  106115:	57                   	push   %edi
  106116:	56                   	push   %esi
  106117:	53                   	push   %ebx
  106118:	83 ec 2c             	sub    $0x2c,%esp
  10611b:	8b 75 10             	mov    0x10(%ebp),%esi
  10611e:	8b 7d 08             	mov    0x8(%ebp),%edi
  char *mem;
  uint a;

  if(newsz > USERTOP)
  106121:	81 fe 00 00 0a 00    	cmp    $0xa0000,%esi
  106127:	0f 87 8e 00 00 00    	ja     1061bb <allocuvm+0xab>
    return 0;
  if(newsz < oldsz)
    return oldsz;
  10612d:	8b 45 0c             	mov    0xc(%ebp),%eax
  char *mem;
  uint a;

  if(newsz > USERTOP)
    return 0;
  if(newsz < oldsz)
  106130:	39 c6                	cmp    %eax,%esi
  106132:	0f 82 83 00 00 00    	jb     1061bb <allocuvm+0xab>
    return oldsz;

  a = PGROUNDUP(oldsz);
  106138:	89 c3                	mov    %eax,%ebx
  10613a:	81 c3 ff 0f 00 00    	add    $0xfff,%ebx
  106140:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; a < newsz; a += PGSIZE){
  106146:	39 de                	cmp    %ebx,%esi
  106148:	77 47                	ja     106191 <allocuvm+0x81>
  10614a:	eb 7c                	jmp    1061c8 <allocuvm+0xb8>
  10614c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(mem == 0){
      cprintf("allocuvm out of memory\n");
      deallocuvm(pgdir, newsz, oldsz);
      return 0;
    }
    memset(mem, 0, PGSIZE);
  106150:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  106157:	00 
  106158:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  10615f:	00 
  106160:	89 04 24             	mov    %eax,(%esp)
  106163:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  106166:	e8 65 da ff ff       	call   103bd0 <memset>
    mappages(pgdir, (char*)a, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  10616b:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  10616e:	b9 00 10 00 00       	mov    $0x1000,%ecx
  106173:	89 f8                	mov    %edi,%eax
  106175:	c7 44 24 04 06 00 00 	movl   $0x6,0x4(%esp)
  10617c:	00 
  10617d:	89 14 24             	mov    %edx,(%esp)
  106180:	89 da                	mov    %ebx,%edx
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
  106182:	81 c3 00 10 00 00    	add    $0x1000,%ebx
      cprintf("allocuvm out of memory\n");
      deallocuvm(pgdir, newsz, oldsz);
      return 0;
    }
    memset(mem, 0, PGSIZE);
    mappages(pgdir, (char*)a, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  106188:	e8 03 fc ff ff       	call   105d90 <mappages>
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
  10618d:	39 de                	cmp    %ebx,%esi
  10618f:	76 37                	jbe    1061c8 <allocuvm+0xb8>
    mem = kalloc();
  106191:	e8 ca c0 ff ff       	call   102260 <kalloc>
    if(mem == 0){
  106196:	85 c0                	test   %eax,%eax
  106198:	75 b6                	jne    106150 <allocuvm+0x40>
      cprintf("allocuvm out of memory\n");
  10619a:	c7 04 24 f1 6c 10 00 	movl   $0x106cf1,(%esp)
  1061a1:	e8 8a a3 ff ff       	call   100530 <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
  1061a6:	8b 45 0c             	mov    0xc(%ebp),%eax
  1061a9:	89 74 24 04          	mov    %esi,0x4(%esp)
  1061ad:	89 3c 24             	mov    %edi,(%esp)
  1061b0:	89 44 24 08          	mov    %eax,0x8(%esp)
  1061b4:	e8 87 fd ff ff       	call   105f40 <deallocuvm>
  1061b9:	31 c0                	xor    %eax,%eax
    }
    memset(mem, 0, PGSIZE);
    mappages(pgdir, (char*)a, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  }
  return newsz;
}
  1061bb:	83 c4 2c             	add    $0x2c,%esp
  1061be:	5b                   	pop    %ebx
  1061bf:	5e                   	pop    %esi
  1061c0:	5f                   	pop    %edi
  1061c1:	5d                   	pop    %ebp
  1061c2:	c3                   	ret    
  1061c3:	90                   	nop
  1061c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  1061c8:	83 c4 2c             	add    $0x2c,%esp
      return 0;
    }
    memset(mem, 0, PGSIZE);
    mappages(pgdir, (char*)a, PGSIZE, PADDR(mem), PTE_W|PTE_U);
  }
  return newsz;
  1061cb:	89 f0                	mov    %esi,%eax
}
  1061cd:	5b                   	pop    %ebx
  1061ce:	5e                   	pop    %esi
  1061cf:	5f                   	pop    %edi
  1061d0:	5d                   	pop    %ebp
  1061d1:	c3                   	ret    
  1061d2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  1061d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

001061e0 <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
  1061e0:	55                   	push   %ebp
  1061e1:	89 e5                	mov    %esp,%ebp
  1061e3:	57                   	push   %edi
  1061e4:	56                   	push   %esi
  1061e5:	53                   	push   %ebx
  1061e6:	83 ec 2c             	sub    $0x2c,%esp
  1061e9:	8b 7d 0c             	mov    0xc(%ebp),%edi
  uint i, pa, n;
  pte_t *pte;

  if((uint)addr % PGSIZE != 0)
  1061ec:	f7 c7 ff 0f 00 00    	test   $0xfff,%edi
  1061f2:	0f 85 96 00 00 00    	jne    10628e <loaduvm+0xae>
    panic("loaduvm: addr must be page aligned");
  1061f8:	8b 75 18             	mov    0x18(%ebp),%esi
  1061fb:	31 db                	xor    %ebx,%ebx
  for(i = 0; i < sz; i += PGSIZE){
  1061fd:	85 f6                	test   %esi,%esi
  1061ff:	75 18                	jne    106219 <loaduvm+0x39>
  106201:	eb 75                	jmp    106278 <loaduvm+0x98>
  106203:	90                   	nop
  106204:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  106208:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  10620e:	81 ee 00 10 00 00    	sub    $0x1000,%esi
  106214:	39 5d 18             	cmp    %ebx,0x18(%ebp)
  106217:	76 5f                	jbe    106278 <loaduvm+0x98>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
  106219:	8b 45 08             	mov    0x8(%ebp),%eax
  10621c:	31 c9                	xor    %ecx,%ecx
  10621e:	8d 14 1f             	lea    (%edi,%ebx,1),%edx
  106221:	e8 1a fa ff ff       	call   105c40 <walkpgdir>
  106226:	85 c0                	test   %eax,%eax
  106228:	74 58                	je     106282 <loaduvm+0xa2>
      panic("loaduvm: address should exist");
    pa = PTE_ADDR(*pte);
  10622a:	8b 00                	mov    (%eax),%eax
    if(sz - i < PGSIZE)
  10622c:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
  106232:	ba 00 10 00 00       	mov    $0x1000,%edx
  106237:	0f 42 d6             	cmovb  %esi,%edx
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, (char*)pa, offset+i, n) != n)
  10623a:	8b 4d 14             	mov    0x14(%ebp),%ecx
  10623d:	89 54 24 0c          	mov    %edx,0xc(%esp)
  106241:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  106244:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  106249:	89 44 24 04          	mov    %eax,0x4(%esp)
  10624d:	8b 45 10             	mov    0x10(%ebp),%eax
  106250:	8d 0c 0b             	lea    (%ebx,%ecx,1),%ecx
  106253:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  106257:	89 04 24             	mov    %eax,(%esp)
  10625a:	e8 11 b1 ff ff       	call   101370 <readi>
  10625f:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  106262:	39 d0                	cmp    %edx,%eax
  106264:	74 a2                	je     106208 <loaduvm+0x28>
      return -1;
  }
  return 0;
}
  106266:	83 c4 2c             	add    $0x2c,%esp
    pa = PTE_ADDR(*pte);
    if(sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, (char*)pa, offset+i, n) != n)
  106269:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
      return -1;
  }
  return 0;
}
  10626e:	5b                   	pop    %ebx
  10626f:	5e                   	pop    %esi
  106270:	5f                   	pop    %edi
  106271:	5d                   	pop    %ebp
  106272:	c3                   	ret    
  106273:	90                   	nop
  106274:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  106278:	83 c4 2c             	add    $0x2c,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint)addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
  10627b:	31 c0                	xor    %eax,%eax
      n = PGSIZE;
    if(readi(ip, (char*)pa, offset+i, n) != n)
      return -1;
  }
  return 0;
}
  10627d:	5b                   	pop    %ebx
  10627e:	5e                   	pop    %esi
  10627f:	5f                   	pop    %edi
  106280:	5d                   	pop    %ebp
  106281:	c3                   	ret    

  if((uint)addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
      panic("loaduvm: address should exist");
  106282:	c7 04 24 09 6d 10 00 	movl   $0x106d09,(%esp)
  106289:	e8 82 a6 ff ff       	call   100910 <panic>
{
  uint i, pa, n;
  pte_t *pte;

  if((uint)addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  10628e:	c7 04 24 3c 6d 10 00 	movl   $0x106d3c,(%esp)
  106295:	e8 76 a6 ff ff       	call   100910 <panic>
  10629a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

001062a0 <switchuvm>:
}

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
  1062a0:	55                   	push   %ebp
  1062a1:	89 e5                	mov    %esp,%ebp
  1062a3:	53                   	push   %ebx
  1062a4:	83 ec 14             	sub    $0x14,%esp
  1062a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  pushcli();
  1062aa:	e8 91 d7 ff ff       	call   103a40 <pushcli>
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
  1062af:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  1062b5:	8d 50 08             	lea    0x8(%eax),%edx
  1062b8:	89 d1                	mov    %edx,%ecx
  1062ba:	66 89 90 a2 00 00 00 	mov    %dx,0xa2(%eax)
  1062c1:	c1 e9 10             	shr    $0x10,%ecx
  1062c4:	c1 ea 18             	shr    $0x18,%edx
  1062c7:	88 88 a4 00 00 00    	mov    %cl,0xa4(%eax)
  1062cd:	c6 80 a5 00 00 00 99 	movb   $0x99,0xa5(%eax)
  1062d4:	88 90 a7 00 00 00    	mov    %dl,0xa7(%eax)
  1062da:	66 c7 80 a0 00 00 00 	movw   $0x67,0xa0(%eax)
  1062e1:	67 00 
  1062e3:	c6 80 a6 00 00 00 40 	movb   $0x40,0xa6(%eax)
  cpu->gdt[SEG_TSS].s = 0;
  1062ea:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  1062f0:	80 a0 a5 00 00 00 ef 	andb   $0xef,0xa5(%eax)
  cpu->ts.ss0 = SEG_KDATA << 3;
  1062f7:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  1062fd:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
  106303:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  106309:	8b 50 08             	mov    0x8(%eax),%edx
  10630c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
  106312:	81 c2 00 10 00 00    	add    $0x1000,%edx
  106318:	89 50 0c             	mov    %edx,0xc(%eax)
}

static inline void
ltr(ushort sel)
{
  asm volatile("ltr %0" : : "r" (sel));
  10631b:	b8 30 00 00 00       	mov    $0x30,%eax
  106320:	0f 00 d8             	ltr    %ax
  ltr(SEG_TSS << 3);
  if(p->pgdir == 0)
  106323:	8b 43 04             	mov    0x4(%ebx),%eax
  106326:	85 c0                	test   %eax,%eax
  106328:	74 0d                	je     106337 <switchuvm+0x97>
}

static inline void
lcr3(uint val) 
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
  10632a:	0f 22 d8             	mov    %eax,%cr3
    panic("switchuvm: no pgdir");
  lcr3(PADDR(p->pgdir));  // switch to new address space
  popcli();
}
  10632d:	83 c4 14             	add    $0x14,%esp
  106330:	5b                   	pop    %ebx
  106331:	5d                   	pop    %ebp
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
  ltr(SEG_TSS << 3);
  if(p->pgdir == 0)
    panic("switchuvm: no pgdir");
  lcr3(PADDR(p->pgdir));  // switch to new address space
  popcli();
  106332:	e9 49 d7 ff ff       	jmp    103a80 <popcli>
  cpu->gdt[SEG_TSS].s = 0;
  cpu->ts.ss0 = SEG_KDATA << 3;
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
  ltr(SEG_TSS << 3);
  if(p->pgdir == 0)
    panic("switchuvm: no pgdir");
  106337:	c7 04 24 27 6d 10 00 	movl   $0x106d27,(%esp)
  10633e:	e8 cd a5 ff ff       	call   100910 <panic>
  106343:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  106349:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00106350 <seginit>:

// Set up CPU's kernel segment descriptors.
// Run once at boot time on each CPU.
void
seginit(void)
{
  106350:	55                   	push   %ebp
  106351:	89 e5                	mov    %esp,%ebp
  106353:	83 ec 18             	sub    $0x18,%esp

  // Map virtual addresses to linear addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
  106356:	e8 e5 c1 ff ff       	call   102540 <cpunum>
  10635b:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
  106361:	05 20 bb 10 00       	add    $0x10bb20,%eax
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
  106366:	8d 90 b4 00 00 00    	lea    0xb4(%eax),%edx
  10636c:	66 89 90 8a 00 00 00 	mov    %dx,0x8a(%eax)
  106373:	89 d1                	mov    %edx,%ecx
  106375:	c1 ea 18             	shr    $0x18,%edx
  106378:	88 90 8f 00 00 00    	mov    %dl,0x8f(%eax)
  10637e:	c1 e9 10             	shr    $0x10,%ecx

  lgdt(c->gdt, sizeof(c->gdt));
  106381:	8d 50 70             	lea    0x70(%eax),%edx
  // Map virtual addresses to linear addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
  106384:	66 c7 40 78 ff ff    	movw   $0xffff,0x78(%eax)
  10638a:	66 c7 40 7a 00 00    	movw   $0x0,0x7a(%eax)
  106390:	c6 40 7c 00          	movb   $0x0,0x7c(%eax)
  106394:	c6 40 7d 9a          	movb   $0x9a,0x7d(%eax)
  106398:	c6 40 7e cf          	movb   $0xcf,0x7e(%eax)
  10639c:	c6 40 7f 00          	movb   $0x0,0x7f(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
  1063a0:	66 c7 80 80 00 00 00 	movw   $0xffff,0x80(%eax)
  1063a7:	ff ff 
  1063a9:	66 c7 80 82 00 00 00 	movw   $0x0,0x82(%eax)
  1063b0:	00 00 
  1063b2:	c6 80 84 00 00 00 00 	movb   $0x0,0x84(%eax)
  1063b9:	c6 80 85 00 00 00 92 	movb   $0x92,0x85(%eax)
  1063c0:	c6 80 86 00 00 00 cf 	movb   $0xcf,0x86(%eax)
  1063c7:	c6 80 87 00 00 00 00 	movb   $0x0,0x87(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
  1063ce:	66 c7 80 90 00 00 00 	movw   $0xffff,0x90(%eax)
  1063d5:	ff ff 
  1063d7:	66 c7 80 92 00 00 00 	movw   $0x0,0x92(%eax)
  1063de:	00 00 
  1063e0:	c6 80 94 00 00 00 00 	movb   $0x0,0x94(%eax)
  1063e7:	c6 80 95 00 00 00 fa 	movb   $0xfa,0x95(%eax)
  1063ee:	c6 80 96 00 00 00 cf 	movb   $0xcf,0x96(%eax)
  1063f5:	c6 80 97 00 00 00 00 	movb   $0x0,0x97(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
  1063fc:	66 c7 80 98 00 00 00 	movw   $0xffff,0x98(%eax)
  106403:	ff ff 
  106405:	66 c7 80 9a 00 00 00 	movw   $0x0,0x9a(%eax)
  10640c:	00 00 
  10640e:	c6 80 9c 00 00 00 00 	movb   $0x0,0x9c(%eax)
  106415:	c6 80 9d 00 00 00 f2 	movb   $0xf2,0x9d(%eax)
  10641c:	c6 80 9e 00 00 00 cf 	movb   $0xcf,0x9e(%eax)
  106423:	c6 80 9f 00 00 00 00 	movb   $0x0,0x9f(%eax)

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
  10642a:	66 c7 80 88 00 00 00 	movw   $0x0,0x88(%eax)
  106431:	00 00 
  106433:	88 88 8c 00 00 00    	mov    %cl,0x8c(%eax)
  106439:	c6 80 8d 00 00 00 92 	movb   $0x92,0x8d(%eax)
  106440:	c6 80 8e 00 00 00 c0 	movb   $0xc0,0x8e(%eax)
static inline void
lgdt(struct segdesc *p, int size)
{
  volatile ushort pd[3];

  pd[0] = size-1;
  106447:	66 c7 45 f2 37 00    	movw   $0x37,-0xe(%ebp)
  pd[1] = (uint)p;
  10644d:	66 89 55 f4          	mov    %dx,-0xc(%ebp)
  pd[2] = (uint)p >> 16;
  106451:	c1 ea 10             	shr    $0x10,%edx
  106454:	66 89 55 f6          	mov    %dx,-0xa(%ebp)

  asm volatile("lgdt (%0)" : : "r" (pd));
  106458:	8d 55 f2             	lea    -0xe(%ebp),%edx
  10645b:	0f 01 12             	lgdtl  (%edx)
}

static inline void
loadgs(ushort v)
{
  asm volatile("movw %0, %%gs" : : "r" (v));
  10645e:	ba 18 00 00 00       	mov    $0x18,%edx
  106463:	8e ea                	mov    %edx,%gs

  lgdt(c->gdt, sizeof(c->gdt));
  loadgs(SEG_KCPU << 3);
  
  // Initialize cpu-local storage.
  cpu = c;
  106465:	65 a3 00 00 00 00    	mov    %eax,%gs:0x0
  proc = 0;
  10646b:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
  106472:	00 00 00 00 
}
  106476:	c9                   	leave  
  106477:	c3                   	ret    
