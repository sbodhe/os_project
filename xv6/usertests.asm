
_usertests:     file format elf32-i386


Disassembly of section .text:

00000000 <validateint>:
  printf(stdout, "sbrk test OK\n");
}

void
validateint(int *p)
{
       0:	55                   	push   %ebp
       1:	89 e5                	mov    %esp,%ebp
      "int %2\n\t"
      "mov %%ebx, %%esp" :
      "=a" (res) :
      "a" (SYS_sleep), "n" (T_SYSCALL), "c" (p) :
      "ebx");
}
       3:	5d                   	pop    %ebp
       4:	c3                   	ret    
       5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
       9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00000010 <bsstest>:

// does unintialized data start out zero?
char uninit[10000];
void
bsstest(void)
{
      10:	55                   	push   %ebp
      11:	89 e5                	mov    %esp,%ebp
      13:	83 ec 18             	sub    $0x18,%esp
  int i;

  printf(stdout, "bss test\n");
      16:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
      1b:	c7 44 24 04 88 38 00 	movl   $0x3888,0x4(%esp)
      22:	00 
      23:	89 04 24             	mov    %eax,(%esp)
      26:	e8 f5 34 00 00       	call   3520 <printf>
  for(i = 0; i < sizeof(uninit); i++){
    if(uninit[i] != '\0'){
      2b:	80 3d 00 4e 00 00 00 	cmpb   $0x0,0x4e00
      32:	75 36                	jne    6a <bsstest+0x5a>
      34:	b8 01 00 00 00       	mov    $0x1,%eax
      39:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      40:	80 b8 00 4e 00 00 00 	cmpb   $0x0,0x4e00(%eax)
      47:	75 21                	jne    6a <bsstest+0x5a>
bsstest(void)
{
  int i;

  printf(stdout, "bss test\n");
  for(i = 0; i < sizeof(uninit); i++){
      49:	83 c0 01             	add    $0x1,%eax
      4c:	3d 10 27 00 00       	cmp    $0x2710,%eax
      51:	75 ed                	jne    40 <bsstest+0x30>
    if(uninit[i] != '\0'){
      printf(stdout, "bss test failed\n");
      exit();
    }
  }
  printf(stdout, "bss test ok\n");
      53:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
      58:	c7 44 24 04 a3 38 00 	movl   $0x38a3,0x4(%esp)
      5f:	00 
      60:	89 04 24             	mov    %eax,(%esp)
      63:	e8 b8 34 00 00       	call   3520 <printf>
}
      68:	c9                   	leave  
      69:	c3                   	ret    
  int i;

  printf(stdout, "bss test\n");
  for(i = 0; i < sizeof(uninit); i++){
    if(uninit[i] != '\0'){
      printf(stdout, "bss test failed\n");
      6a:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
      6f:	c7 44 24 04 92 38 00 	movl   $0x3892,0x4(%esp)
      76:	00 
      77:	89 04 24             	mov    %eax,(%esp)
      7a:	e8 a1 34 00 00       	call   3520 <printf>
      exit();
      7f:	e8 64 33 00 00       	call   33e8 <exit>
      84:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      8a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

00000090 <opentest>:

// simple file system tests

void
opentest(void)
{
      90:	55                   	push   %ebp
      91:	89 e5                	mov    %esp,%ebp
      93:	83 ec 18             	sub    $0x18,%esp
  int fd;

  printf(stdout, "open test\n");
      96:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
      9b:	c7 44 24 04 b0 38 00 	movl   $0x38b0,0x4(%esp)
      a2:	00 
      a3:	89 04 24             	mov    %eax,(%esp)
      a6:	e8 75 34 00 00       	call   3520 <printf>
  fd = open("echo", 0);
      ab:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
      b2:	00 
      b3:	c7 04 24 bb 38 00 00 	movl   $0x38bb,(%esp)
      ba:	e8 69 33 00 00       	call   3428 <open>
  if(fd < 0){
      bf:	85 c0                	test   %eax,%eax
      c1:	78 37                	js     fa <opentest+0x6a>
    printf(stdout, "open echo failed!\n");
    exit();
  }
  close(fd);
      c3:	89 04 24             	mov    %eax,(%esp)
      c6:	e8 45 33 00 00       	call   3410 <close>
  fd = open("doesnotexist", 0);
      cb:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
      d2:	00 
      d3:	c7 04 24 d3 38 00 00 	movl   $0x38d3,(%esp)
      da:	e8 49 33 00 00       	call   3428 <open>
  if(fd >= 0){
      df:	85 c0                	test   %eax,%eax
      e1:	79 31                	jns    114 <opentest+0x84>
    printf(stdout, "open doesnotexist succeeded!\n");
    exit();
  }
  printf(stdout, "open test ok\n");
      e3:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
      e8:	c7 44 24 04 fe 38 00 	movl   $0x38fe,0x4(%esp)
      ef:	00 
      f0:	89 04 24             	mov    %eax,(%esp)
      f3:	e8 28 34 00 00       	call   3520 <printf>
}
      f8:	c9                   	leave  
      f9:	c3                   	ret    
  int fd;

  printf(stdout, "open test\n");
  fd = open("echo", 0);
  if(fd < 0){
    printf(stdout, "open echo failed!\n");
      fa:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
      ff:	c7 44 24 04 c0 38 00 	movl   $0x38c0,0x4(%esp)
     106:	00 
     107:	89 04 24             	mov    %eax,(%esp)
     10a:	e8 11 34 00 00       	call   3520 <printf>
    exit();
     10f:	e8 d4 32 00 00       	call   33e8 <exit>
  }
  close(fd);
  fd = open("doesnotexist", 0);
  if(fd >= 0){
    printf(stdout, "open doesnotexist succeeded!\n");
     114:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     119:	c7 44 24 04 e0 38 00 	movl   $0x38e0,0x4(%esp)
     120:	00 
     121:	89 04 24             	mov    %eax,(%esp)
     124:	e8 f7 33 00 00       	call   3520 <printf>
    exit();
     129:	e8 ba 32 00 00       	call   33e8 <exit>
     12e:	66 90                	xchg   %ax,%ax

00000130 <exectest>:
  printf(stdout, "mkdir test\n");
}

void
exectest(void)
{
     130:	55                   	push   %ebp
     131:	89 e5                	mov    %esp,%ebp
     133:	83 ec 18             	sub    $0x18,%esp
  printf(stdout, "exec test\n");
     136:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     13b:	c7 44 24 04 0c 39 00 	movl   $0x390c,0x4(%esp)
     142:	00 
     143:	89 04 24             	mov    %eax,(%esp)
     146:	e8 d5 33 00 00       	call   3520 <printf>
  if(exec("echo", echoargv) < 0){
     14b:	c7 44 24 04 c8 4d 00 	movl   $0x4dc8,0x4(%esp)
     152:	00 
     153:	c7 04 24 bb 38 00 00 	movl   $0x38bb,(%esp)
     15a:	e8 c1 32 00 00       	call   3420 <exec>
     15f:	85 c0                	test   %eax,%eax
     161:	78 02                	js     165 <exectest+0x35>
    printf(stdout, "exec echo failed\n");
    exit();
  }
}
     163:	c9                   	leave  
     164:	c3                   	ret    
void
exectest(void)
{
  printf(stdout, "exec test\n");
  if(exec("echo", echoargv) < 0){
    printf(stdout, "exec echo failed\n");
     165:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     16a:	c7 44 24 04 17 39 00 	movl   $0x3917,0x4(%esp)
     171:	00 
     172:	89 04 24             	mov    %eax,(%esp)
     175:	e8 a6 33 00 00       	call   3520 <printf>
    exit();
     17a:	e8 69 32 00 00       	call   33e8 <exit>
     17f:	90                   	nop

00000180 <bigargtest>:

// does exec do something sensible if the arguments
// are larger than a page?
void
bigargtest(void)
{
     180:	55                   	push   %ebp
     181:	89 e5                	mov    %esp,%ebp
     183:	53                   	push   %ebx
     184:	81 ec a4 00 00 00    	sub    $0xa4,%esp
  int pid, ppid;

  ppid = getpid();
     18a:	e8 d9 32 00 00       	call   3468 <getpid>
  pid = fork();
     18f:	e8 4c 32 00 00       	call   33e0 <fork>
  if(pid == 0){
     194:	83 f8 00             	cmp    $0x0,%eax
     197:	74 15                	je     1ae <bigargtest+0x2e>
    args[32] = 0;
    printf(stdout, "bigarg test\n");
    exec("echo", args);
    printf(stdout, "bigarg test ok\n");
    exit();
  } else if(pid < 0){
     199:	7c 72                	jl     20d <bigargtest+0x8d>
     19b:	90                   	nop
     19c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    printf(stdout, "bigargtest: fork failed\n");
    exit();
  }
  wait();
     1a0:	e8 4b 32 00 00       	call   33f0 <wait>
}
     1a5:	81 c4 a4 00 00 00    	add    $0xa4,%esp
     1ab:	5b                   	pop    %ebx
     1ac:	5d                   	pop    %ebp
     1ad:	c3                   	ret    
     1ae:	8d 9d 74 ff ff ff    	lea    -0x8c(%ebp),%ebx
     1b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  pid = fork();
  if(pid == 0){
    char *args[32+1];
    int i;
    for(i = 0; i < 32; i++)
      args[i] = "bigargs test: failed\n                                                                                                                     ";
     1b8:	c7 04 83 bc 46 00 00 	movl   $0x46bc,(%ebx,%eax,4)
  ppid = getpid();
  pid = fork();
  if(pid == 0){
    char *args[32+1];
    int i;
    for(i = 0; i < 32; i++)
     1bf:	83 c0 01             	add    $0x1,%eax
     1c2:	83 f8 20             	cmp    $0x20,%eax
     1c5:	75 f1                	jne    1b8 <bigargtest+0x38>
      args[i] = "bigargs test: failed\n                                                                                                                     ";
    args[32] = 0;
    printf(stdout, "bigarg test\n");
     1c7:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
  if(pid == 0){
    char *args[32+1];
    int i;
    for(i = 0; i < 32; i++)
      args[i] = "bigargs test: failed\n                                                                                                                     ";
    args[32] = 0;
     1cc:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    printf(stdout, "bigarg test\n");
     1d3:	c7 44 24 04 29 39 00 	movl   $0x3929,0x4(%esp)
     1da:	00 
     1db:	89 04 24             	mov    %eax,(%esp)
     1de:	e8 3d 33 00 00       	call   3520 <printf>
    exec("echo", args);
     1e3:	89 5c 24 04          	mov    %ebx,0x4(%esp)
     1e7:	c7 04 24 bb 38 00 00 	movl   $0x38bb,(%esp)
     1ee:	e8 2d 32 00 00       	call   3420 <exec>
    printf(stdout, "bigarg test ok\n");
     1f3:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     1f8:	c7 44 24 04 36 39 00 	movl   $0x3936,0x4(%esp)
     1ff:	00 
     200:	89 04 24             	mov    %eax,(%esp)
     203:	e8 18 33 00 00       	call   3520 <printf>
    exit();
     208:	e8 db 31 00 00       	call   33e8 <exit>
  } else if(pid < 0){
    printf(stdout, "bigargtest: fork failed\n");
     20d:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     212:	c7 44 24 04 46 39 00 	movl   $0x3946,0x4(%esp)
     219:	00 
     21a:	89 04 24             	mov    %eax,(%esp)
     21d:	e8 fe 32 00 00       	call   3520 <printf>
    exit();
     222:	e8 c1 31 00 00       	call   33e8 <exit>
     227:	89 f6                	mov    %esi,%esi
     229:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00000230 <forktest>:
// test that fork fails gracefully
// the forktest binary also does this, but it runs out of proc entries first.
// inside the bigger usertests binary, we run out of memory first.
void
forktest(void)
{
     230:	55                   	push   %ebp
     231:	89 e5                	mov    %esp,%ebp
     233:	53                   	push   %ebx
  int n, pid;

  printf(1, "fork test\n");
     234:	31 db                	xor    %ebx,%ebx
// test that fork fails gracefully
// the forktest binary also does this, but it runs out of proc entries first.
// inside the bigger usertests binary, we run out of memory first.
void
forktest(void)
{
     236:	83 ec 14             	sub    $0x14,%esp
  int n, pid;

  printf(1, "fork test\n");
     239:	c7 44 24 04 5f 39 00 	movl   $0x395f,0x4(%esp)
     240:	00 
     241:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     248:	e8 d3 32 00 00       	call   3520 <printf>
     24d:	eb 0e                	jmp    25d <forktest+0x2d>
     24f:	90                   	nop

  for(n=0; n<1000; n++){
    pid = fork();
    if(pid < 0)
      break;
    if(pid == 0)
     250:	74 6a                	je     2bc <forktest+0x8c>
{
  int n, pid;

  printf(1, "fork test\n");

  for(n=0; n<1000; n++){
     252:	83 c3 01             	add    $0x1,%ebx
     255:	81 fb e8 03 00 00    	cmp    $0x3e8,%ebx
     25b:	74 4b                	je     2a8 <forktest+0x78>
    pid = fork();
     25d:	e8 7e 31 00 00       	call   33e0 <fork>
    if(pid < 0)
     262:	83 f8 00             	cmp    $0x0,%eax
     265:	7d e9                	jge    250 <forktest+0x20>
  if(n == 1000){
    printf(1, "fork claimed to work 1000 times!\n");
    exit();
  }
  
  for(; n > 0; n--){
     267:	85 db                	test   %ebx,%ebx
     269:	74 13                	je     27e <forktest+0x4e>
     26b:	90                   	nop
     26c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(wait() < 0){
     270:	e8 7b 31 00 00       	call   33f0 <wait>
     275:	85 c0                	test   %eax,%eax
     277:	78 48                	js     2c1 <forktest+0x91>
  if(n == 1000){
    printf(1, "fork claimed to work 1000 times!\n");
    exit();
  }
  
  for(; n > 0; n--){
     279:	83 eb 01             	sub    $0x1,%ebx
     27c:	75 f2                	jne    270 <forktest+0x40>
     27e:	66 90                	xchg   %ax,%ax
      printf(1, "wait stopped early\n");
      exit();
    }
  }
  
  if(wait() != -1){
     280:	e8 6b 31 00 00       	call   33f0 <wait>
     285:	83 f8 ff             	cmp    $0xffffffff,%eax
     288:	75 50                	jne    2da <forktest+0xaa>
    printf(1, "wait got too many\n");
    exit();
  }
  
  printf(1, "fork test OK\n");
     28a:	c7 44 24 04 91 39 00 	movl   $0x3991,0x4(%esp)
     291:	00 
     292:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     299:	e8 82 32 00 00       	call   3520 <printf>
}
     29e:	83 c4 14             	add    $0x14,%esp
     2a1:	5b                   	pop    %ebx
     2a2:	5d                   	pop    %ebp
     2a3:	c3                   	ret    
     2a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(pid == 0)
      exit();
  }
  
  if(n == 1000){
    printf(1, "fork claimed to work 1000 times!\n");
     2a8:	c7 44 24 04 48 47 00 	movl   $0x4748,0x4(%esp)
     2af:	00 
     2b0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     2b7:	e8 64 32 00 00       	call   3520 <printf>
    exit();
     2bc:	e8 27 31 00 00       	call   33e8 <exit>
  }
  
  for(; n > 0; n--){
    if(wait() < 0){
      printf(1, "wait stopped early\n");
     2c1:	c7 44 24 04 6a 39 00 	movl   $0x396a,0x4(%esp)
     2c8:	00 
     2c9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     2d0:	e8 4b 32 00 00       	call   3520 <printf>
      exit();
     2d5:	e8 0e 31 00 00       	call   33e8 <exit>
    }
  }
  
  if(wait() != -1){
    printf(1, "wait got too many\n");
     2da:	c7 44 24 04 7e 39 00 	movl   $0x397e,0x4(%esp)
     2e1:	00 
     2e2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     2e9:	e8 32 32 00 00       	call   3520 <printf>
    exit();
     2ee:	e8 f5 30 00 00       	call   33e8 <exit>
     2f3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
     2f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00000300 <exitwait>:
}

// try to find any races between exit and wait
void
exitwait(void)
{
     300:	55                   	push   %ebp
     301:	89 e5                	mov    %esp,%ebp
     303:	56                   	push   %esi
     304:	31 f6                	xor    %esi,%esi
     306:	53                   	push   %ebx
     307:	83 ec 10             	sub    $0x10,%esp
     30a:	eb 17                	jmp    323 <exitwait+0x23>
     30c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    pid = fork();
    if(pid < 0){
      printf(1, "fork failed\n");
      return;
    }
    if(pid){
     310:	74 79                	je     38b <exitwait+0x8b>
      if(wait() != pid){
     312:	e8 d9 30 00 00       	call   33f0 <wait>
     317:	39 c3                	cmp    %eax,%ebx
     319:	75 35                	jne    350 <exitwait+0x50>
void
exitwait(void)
{
  int i, pid;

  for(i = 0; i < 100; i++){
     31b:	83 c6 01             	add    $0x1,%esi
     31e:	83 fe 64             	cmp    $0x64,%esi
     321:	74 4d                	je     370 <exitwait+0x70>
    pid = fork();
     323:	e8 b8 30 00 00       	call   33e0 <fork>
    if(pid < 0){
     328:	83 f8 00             	cmp    $0x0,%eax
exitwait(void)
{
  int i, pid;

  for(i = 0; i < 100; i++){
    pid = fork();
     32b:	89 c3                	mov    %eax,%ebx
    if(pid < 0){
     32d:	7d e1                	jge    310 <exitwait+0x10>
      printf(1, "fork failed\n");
     32f:	c7 44 24 04 52 39 00 	movl   $0x3952,0x4(%esp)
     336:	00 
     337:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     33e:	e8 dd 31 00 00       	call   3520 <printf>
    } else {
      exit();
    }
  }
  printf(1, "exitwait ok\n");
}
     343:	83 c4 10             	add    $0x10,%esp
     346:	5b                   	pop    %ebx
     347:	5e                   	pop    %esi
     348:	5d                   	pop    %ebp
     349:	c3                   	ret    
     34a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      printf(1, "fork failed\n");
      return;
    }
    if(pid){
      if(wait() != pid){
        printf(1, "wait wrong pid\n");
     350:	c7 44 24 04 9f 39 00 	movl   $0x399f,0x4(%esp)
     357:	00 
     358:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     35f:	e8 bc 31 00 00       	call   3520 <printf>
    } else {
      exit();
    }
  }
  printf(1, "exitwait ok\n");
}
     364:	83 c4 10             	add    $0x10,%esp
     367:	5b                   	pop    %ebx
     368:	5e                   	pop    %esi
     369:	5d                   	pop    %ebp
     36a:	c3                   	ret    
     36b:	90                   	nop
     36c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      }
    } else {
      exit();
    }
  }
  printf(1, "exitwait ok\n");
     370:	c7 44 24 04 af 39 00 	movl   $0x39af,0x4(%esp)
     377:	00 
     378:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     37f:	e8 9c 31 00 00       	call   3520 <printf>
}
     384:	83 c4 10             	add    $0x10,%esp
     387:	5b                   	pop    %ebx
     388:	5e                   	pop    %esi
     389:	5d                   	pop    %ebp
     38a:	c3                   	ret    
      if(wait() != pid){
        printf(1, "wait wrong pid\n");
        return;
      }
    } else {
      exit();
     38b:	e8 58 30 00 00       	call   33e8 <exit>

00000390 <validatetest>:
      "ebx");
}

void
validatetest(void)
{
     390:	55                   	push   %ebp
     391:	89 e5                	mov    %esp,%ebp
     393:	56                   	push   %esi
     394:	53                   	push   %ebx
  int hi, pid;
  uint p;

  printf(stdout, "validate test\n");
     395:	31 db                	xor    %ebx,%ebx
      "ebx");
}

void
validatetest(void)
{
     397:	83 ec 10             	sub    $0x10,%esp
  int hi, pid;
  uint p;

  printf(stdout, "validate test\n");
     39a:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     39f:	c7 44 24 04 bc 39 00 	movl   $0x39bc,0x4(%esp)
     3a6:	00 
     3a7:	89 04 24             	mov    %eax,(%esp)
     3aa:	e8 71 31 00 00       	call   3520 <printf>
     3af:	90                   	nop
  hi = 1100*1024;

  for(p = 0; p <= (uint)hi; p += 4096){
    if((pid = fork()) == 0){
     3b0:	e8 2b 30 00 00       	call   33e0 <fork>
     3b5:	85 c0                	test   %eax,%eax
     3b7:	89 c6                	mov    %eax,%esi
     3b9:	74 79                	je     434 <validatetest+0xa4>
      // try to crash the kernel by passing in a badly placed integer
      validateint((int*)p);
      exit();
    }
    sleep(0);
     3bb:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     3c2:	e8 b1 30 00 00       	call   3478 <sleep>
    sleep(0);
     3c7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     3ce:	e8 a5 30 00 00       	call   3478 <sleep>
    kill(pid);
     3d3:	89 34 24             	mov    %esi,(%esp)
     3d6:	e8 3d 30 00 00       	call   3418 <kill>
    wait();
     3db:	e8 10 30 00 00       	call   33f0 <wait>

    // try to crash the kernel by passing in a bad string pointer
    if(link("nosuchfile", (char*)p) != -1){
     3e0:	89 5c 24 04          	mov    %ebx,0x4(%esp)
     3e4:	c7 04 24 cb 39 00 00 	movl   $0x39cb,(%esp)
     3eb:	e8 58 30 00 00       	call   3448 <link>
     3f0:	83 f8 ff             	cmp    $0xffffffff,%eax
     3f3:	75 2a                	jne    41f <validatetest+0x8f>
  uint p;

  printf(stdout, "validate test\n");
  hi = 1100*1024;

  for(p = 0; p <= (uint)hi; p += 4096){
     3f5:	81 c3 00 10 00 00    	add    $0x1000,%ebx
     3fb:	81 fb 00 40 11 00    	cmp    $0x114000,%ebx
     401:	75 ad                	jne    3b0 <validatetest+0x20>
      printf(stdout, "link should not succeed\n");
      exit();
    }
  }

  printf(stdout, "validate ok\n");
     403:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     408:	c7 44 24 04 ef 39 00 	movl   $0x39ef,0x4(%esp)
     40f:	00 
     410:	89 04 24             	mov    %eax,(%esp)
     413:	e8 08 31 00 00       	call   3520 <printf>
}
     418:	83 c4 10             	add    $0x10,%esp
     41b:	5b                   	pop    %ebx
     41c:	5e                   	pop    %esi
     41d:	5d                   	pop    %ebp
     41e:	c3                   	ret    
    kill(pid);
    wait();

    // try to crash the kernel by passing in a bad string pointer
    if(link("nosuchfile", (char*)p) != -1){
      printf(stdout, "link should not succeed\n");
     41f:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     424:	c7 44 24 04 d6 39 00 	movl   $0x39d6,0x4(%esp)
     42b:	00 
     42c:	89 04 24             	mov    %eax,(%esp)
     42f:	e8 ec 30 00 00       	call   3520 <printf>
      exit();
     434:	e8 af 2f 00 00       	call   33e8 <exit>
     439:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00000440 <sbrktest>:
  printf(1, "fork test OK\n");
}

void
sbrktest(void)
{
     440:	55                   	push   %ebp
     441:	89 e5                	mov    %esp,%ebp
     443:	57                   	push   %edi
     444:	56                   	push   %esi

  printf(stdout, "sbrk test\n");
  oldbrk = sbrk(0);

  // can one sbrk() less than a page?
  a = sbrk(0);
     445:	31 f6                	xor    %esi,%esi
  printf(1, "fork test OK\n");
}

void
sbrktest(void)
{
     447:	53                   	push   %ebx
     448:	81 ec cc 00 00 00    	sub    $0xcc,%esp
  int fds[2], pid, pids[32], ppid;
  char *a, *b, *c, *lastaddr, *oldbrk, *p, scratch;
  uint amt;

  printf(stdout, "sbrk test\n");
     44e:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     453:	c7 44 24 04 fc 39 00 	movl   $0x39fc,0x4(%esp)
     45a:	00 
     45b:	89 04 24             	mov    %eax,(%esp)
     45e:	e8 bd 30 00 00       	call   3520 <printf>
  oldbrk = sbrk(0);
     463:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     46a:	e8 01 30 00 00       	call   3470 <sbrk>

  // can one sbrk() less than a page?
  a = sbrk(0);
     46f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  int fds[2], pid, pids[32], ppid;
  char *a, *b, *c, *lastaddr, *oldbrk, *p, scratch;
  uint amt;

  printf(stdout, "sbrk test\n");
  oldbrk = sbrk(0);
     476:	89 85 54 ff ff ff    	mov    %eax,-0xac(%ebp)

  // can one sbrk() less than a page?
  a = sbrk(0);
     47c:	e8 ef 2f 00 00       	call   3470 <sbrk>
     481:	89 c3                	mov    %eax,%ebx
     483:	90                   	nop
     484:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  int i;
  for(i = 0; i < 5000; i++){
    b = sbrk(1);
     488:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     48f:	e8 dc 2f 00 00       	call   3470 <sbrk>
    if(b != a){
     494:	39 c3                	cmp    %eax,%ebx
     496:	0f 85 80 02 00 00    	jne    71c <sbrktest+0x2dc>
  oldbrk = sbrk(0);

  // can one sbrk() less than a page?
  a = sbrk(0);
  int i;
  for(i = 0; i < 5000; i++){
     49c:	83 c6 01             	add    $0x1,%esi
    b = sbrk(1);
    if(b != a){
      printf(stdout, "sbrk test failed %d %x %x\n", i, a, b);
      exit();
    }
    *b = 1;
     49f:	c6 03 01             	movb   $0x1,(%ebx)
    a = b + 1;
     4a2:	83 c3 01             	add    $0x1,%ebx
  oldbrk = sbrk(0);

  // can one sbrk() less than a page?
  a = sbrk(0);
  int i;
  for(i = 0; i < 5000; i++){
     4a5:	81 fe 88 13 00 00    	cmp    $0x1388,%esi
     4ab:	75 db                	jne    488 <sbrktest+0x48>
      exit();
    }
    *b = 1;
    a = b + 1;
  }
  pid = fork();
     4ad:	e8 2e 2f 00 00       	call   33e0 <fork>
  if(pid < 0){
     4b2:	85 c0                	test   %eax,%eax
      exit();
    }
    *b = 1;
    a = b + 1;
  }
  pid = fork();
     4b4:	89 c6                	mov    %eax,%esi
  if(pid < 0){
     4b6:	0f 88 0e 04 00 00    	js     8ca <sbrktest+0x48a>
    printf(stdout, "sbrk test fork failed\n");
    exit();
  }
  c = sbrk(1);
     4bc:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  c = sbrk(1);
  if(c != a + 1){
     4c3:	83 c3 01             	add    $0x1,%ebx
  pid = fork();
  if(pid < 0){
    printf(stdout, "sbrk test fork failed\n");
    exit();
  }
  c = sbrk(1);
     4c6:	e8 a5 2f 00 00       	call   3470 <sbrk>
  c = sbrk(1);
     4cb:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     4d2:	e8 99 2f 00 00       	call   3470 <sbrk>
  if(c != a + 1){
     4d7:	39 d8                	cmp    %ebx,%eax
     4d9:	0f 85 d1 03 00 00    	jne    8b0 <sbrktest+0x470>
    printf(stdout, "sbrk test failed post-fork\n");
    exit();
  }
  if(pid == 0)
     4df:	85 f6                	test   %esi,%esi
     4e1:	0f 84 c4 03 00 00    	je     8ab <sbrktest+0x46b>
    exit();
  wait();
     4e7:	e8 04 2f 00 00       	call   33f0 <wait>

  // can one allocate the full 640K?
  a = sbrk(0);
     4ec:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     4f3:	e8 78 2f 00 00       	call   3470 <sbrk>
     4f8:	89 c3                	mov    %eax,%ebx
  amt = (640 * 1024) - (uint)a;
  p = sbrk(amt);
     4fa:	b8 00 00 0a 00       	mov    $0xa0000,%eax
     4ff:	29 d8                	sub    %ebx,%eax
     501:	89 04 24             	mov    %eax,(%esp)
     504:	e8 67 2f 00 00       	call   3470 <sbrk>
  if(p != a){
     509:	39 c3                	cmp    %eax,%ebx
     50b:	0f 85 7d 03 00 00    	jne    88e <sbrktest+0x44e>
    printf(stdout, "sbrk test failed 640K test, p %x a %x\n", p, a);
    exit();
  }
  lastaddr = (char*)(640 * 1024 - 1);
  *lastaddr = 99;
     511:	c6 05 ff ff 09 00 63 	movb   $0x63,0x9ffff

  // is one forbidden from allocating more than 640K?
  c = sbrk(4096);
     518:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
     51f:	e8 4c 2f 00 00       	call   3470 <sbrk>
  if(c != (char*)0xffffffff){
     524:	83 f8 ff             	cmp    $0xffffffff,%eax
     527:	0f 85 43 03 00 00    	jne    870 <sbrktest+0x430>
    printf(stdout, "sbrk allocated more than 640K, c %x\n", c);
    exit();
  }

  // can one de-allocate?
  a = sbrk(0);
     52d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     534:	e8 37 2f 00 00       	call   3470 <sbrk>
  c = sbrk(-4096);
     539:	c7 04 24 00 f0 ff ff 	movl   $0xfffff000,(%esp)
    printf(stdout, "sbrk allocated more than 640K, c %x\n", c);
    exit();
  }

  // can one de-allocate?
  a = sbrk(0);
     540:	89 c3                	mov    %eax,%ebx
  c = sbrk(-4096);
     542:	e8 29 2f 00 00       	call   3470 <sbrk>
  if(c == (char*)0xffffffff){
     547:	83 f8 ff             	cmp    $0xffffffff,%eax
     54a:	0f 84 06 03 00 00    	je     856 <sbrktest+0x416>
    printf(stdout, "sbrk could not deallocate\n");
    exit();
  }
  c = sbrk(0);
     550:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     557:	e8 14 2f 00 00       	call   3470 <sbrk>
  if(c != a - 4096){
     55c:	8d 93 00 f0 ff ff    	lea    -0x1000(%ebx),%edx
     562:	39 d0                	cmp    %edx,%eax
     564:	0f 85 ca 02 00 00    	jne    834 <sbrktest+0x3f4>
    printf(stdout, "sbrk deallocation produced wrong address, a %x c %x\n", a, c);
    exit();
  }

  // can one re-allocate that page?
  a = sbrk(0);
     56a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     571:	e8 fa 2e 00 00       	call   3470 <sbrk>
  c = sbrk(4096);
     576:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
    printf(stdout, "sbrk deallocation produced wrong address, a %x c %x\n", a, c);
    exit();
  }

  // can one re-allocate that page?
  a = sbrk(0);
     57d:	89 c3                	mov    %eax,%ebx
  c = sbrk(4096);
     57f:	e8 ec 2e 00 00       	call   3470 <sbrk>
  if(c != a || sbrk(0) != a + 4096){
     584:	39 c3                	cmp    %eax,%ebx
    exit();
  }

  // can one re-allocate that page?
  a = sbrk(0);
  c = sbrk(4096);
     586:	89 c6                	mov    %eax,%esi
  if(c != a || sbrk(0) != a + 4096){
     588:	0f 85 84 02 00 00    	jne    812 <sbrktest+0x3d2>
     58e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     595:	e8 d6 2e 00 00       	call   3470 <sbrk>
     59a:	8d 93 00 10 00 00    	lea    0x1000(%ebx),%edx
     5a0:	39 d0                	cmp    %edx,%eax
     5a2:	0f 85 6a 02 00 00    	jne    812 <sbrktest+0x3d2>
    printf(stdout, "sbrk re-allocation failed, a %x c %x\n", a, c);
    exit();
  }
  if(*lastaddr == 99){
     5a8:	80 3d ff ff 09 00 63 	cmpb   $0x63,0x9ffff
     5af:	0f 84 43 02 00 00    	je     7f8 <sbrktest+0x3b8>
    // should be zero
    printf(stdout, "sbrk de-allocation didn't really deallocate\n");
    exit();
  }

  c = sbrk(4096);
     5b5:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
  if(c != (char*)0xffffffff){
    printf(stdout, "sbrk was able to re-allocate beyond 640K, c %x\n", c);
    exit();
     5bc:	bb 00 00 0a 00       	mov    $0xa0000,%ebx
    // should be zero
    printf(stdout, "sbrk de-allocation didn't really deallocate\n");
    exit();
  }

  c = sbrk(4096);
     5c1:	e8 aa 2e 00 00       	call   3470 <sbrk>
  if(c != (char*)0xffffffff){
     5c6:	83 f8 ff             	cmp    $0xffffffff,%eax
     5c9:	0f 85 0b 02 00 00    	jne    7da <sbrktest+0x39a>
    exit();
  }

  // can we read the kernel's memory?
  for(a = (char*)(640*1024); a < (char*)2000000; a += 50000){
    ppid = getpid();
     5cf:	e8 94 2e 00 00       	call   3468 <getpid>
     5d4:	89 c6                	mov    %eax,%esi
    pid = fork();
     5d6:	e8 05 2e 00 00       	call   33e0 <fork>
    if(pid < 0){
     5db:	83 f8 00             	cmp    $0x0,%eax
     5de:	0f 8c dc 01 00 00    	jl     7c0 <sbrktest+0x380>
      printf(stdout, "fork failed\n");
      exit();
    }
    if(pid == 0){
     5e4:	0f 84 a9 01 00 00    	je     793 <sbrktest+0x353>
    printf(stdout, "sbrk was able to re-allocate beyond 640K, c %x\n", c);
    exit();
  }

  // can we read the kernel's memory?
  for(a = (char*)(640*1024); a < (char*)2000000; a += 50000){
     5ea:	81 c3 50 c3 00 00    	add    $0xc350,%ebx
    if(pid == 0){
      printf(stdout, "oops could read %x = %x\n", a, *a);
      kill(ppid);
      exit();
    }
    wait();
     5f0:	e8 fb 2d 00 00       	call   33f0 <wait>
    printf(stdout, "sbrk was able to re-allocate beyond 640K, c %x\n", c);
    exit();
  }

  // can we read the kernel's memory?
  for(a = (char*)(640*1024); a < (char*)2000000; a += 50000){
     5f5:	81 fb 70 99 1e 00    	cmp    $0x1e9970,%ebx
     5fb:	75 d2                	jne    5cf <sbrktest+0x18f>
    wait();
  }

  // if we run the system out of memory, does it clean up the last
  // failed allocation?
  sbrk(-(sbrk(0) - oldbrk));
     5fd:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     604:	e8 67 2e 00 00       	call   3470 <sbrk>
     609:	8b 95 54 ff ff ff    	mov    -0xac(%ebp),%edx
     60f:	29 c2                	sub    %eax,%edx
     611:	89 14 24             	mov    %edx,(%esp)
     614:	e8 57 2e 00 00       	call   3470 <sbrk>
  if(pipe(fds) != 0){
     619:	8d 45 dc             	lea    -0x24(%ebp),%eax
     61c:	89 04 24             	mov    %eax,(%esp)
     61f:	e8 d4 2d 00 00       	call   33f8 <pipe>
     624:	85 c0                	test   %eax,%eax
     626:	0f 85 4e 01 00 00    	jne    77a <sbrktest+0x33a>
    printf(1, "pipe() failed\n");
    exit();
     62c:	31 db                	xor    %ebx,%ebx
     62e:	8d bd 5c ff ff ff    	lea    -0xa4(%ebp),%edi
     634:	eb 2a                	jmp    660 <sbrktest+0x220>
      sbrk((640 * 1024) - (uint)sbrk(0));
      write(fds[1], "x", 1);
      // sit around until killed
      for(;;) sleep(1000);
    }
    if(pids[i] != -1)
     636:	83 f8 ff             	cmp    $0xffffffff,%eax
     639:	74 1a                	je     655 <sbrktest+0x215>
      read(fds[0], &scratch, 1);
     63b:	8d 45 e7             	lea    -0x19(%ebp),%eax
     63e:	89 44 24 04          	mov    %eax,0x4(%esp)
     642:	8b 45 dc             	mov    -0x24(%ebp),%eax
     645:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
     64c:	00 
     64d:	89 04 24             	mov    %eax,(%esp)
     650:	e8 ab 2d 00 00       	call   3400 <read>
  if(pipe(fds) != 0){
    printf(1, "pipe() failed\n");
    exit();
  }
  for(i = 0; i < sizeof(pids)/sizeof(pids[0]); i++){
    if((pids[i] = fork()) == 0){
     655:	89 34 9f             	mov    %esi,(%edi,%ebx,4)
  sbrk(-(sbrk(0) - oldbrk));
  if(pipe(fds) != 0){
    printf(1, "pipe() failed\n");
    exit();
  }
  for(i = 0; i < sizeof(pids)/sizeof(pids[0]); i++){
     658:	83 c3 01             	add    $0x1,%ebx
     65b:	83 fb 20             	cmp    $0x20,%ebx
     65e:	74 56                	je     6b6 <sbrktest+0x276>
    if((pids[i] = fork()) == 0){
     660:	e8 7b 2d 00 00       	call   33e0 <fork>
     665:	85 c0                	test   %eax,%eax
     667:	89 c6                	mov    %eax,%esi
     669:	75 cb                	jne    636 <sbrktest+0x1f6>
      // allocate the full 640K
      sbrk((640 * 1024) - (uint)sbrk(0));
     66b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     672:	e8 f9 2d 00 00       	call   3470 <sbrk>
     677:	ba 00 00 0a 00       	mov    $0xa0000,%edx
     67c:	29 c2                	sub    %eax,%edx
     67e:	89 14 24             	mov    %edx,(%esp)
     681:	e8 ea 2d 00 00       	call   3470 <sbrk>
      write(fds[1], "x", 1);
     686:	8b 45 e0             	mov    -0x20(%ebp),%eax
     689:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
     690:	00 
     691:	c7 44 24 04 c5 3f 00 	movl   $0x3fc5,0x4(%esp)
     698:	00 
     699:	89 04 24             	mov    %eax,(%esp)
     69c:	e8 67 2d 00 00       	call   3408 <write>
     6a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      // sit around until killed
      for(;;) sleep(1000);
     6a8:	c7 04 24 e8 03 00 00 	movl   $0x3e8,(%esp)
     6af:	e8 c4 2d 00 00       	call   3478 <sleep>
     6b4:	eb f2                	jmp    6a8 <sbrktest+0x268>
    if(pids[i] != -1)
      read(fds[0], &scratch, 1);
  }
  // if those failed allocations freed up the pages they did allocate,
  // we'll be able to allocate here
  c = sbrk(4096);
     6b6:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
     6bd:	30 db                	xor    %bl,%bl
     6bf:	e8 ac 2d 00 00       	call   3470 <sbrk>
     6c4:	89 c6                	mov    %eax,%esi
  for(i = 0; i < sizeof(pids)/sizeof(pids[0]); i++){
    if(pids[i] == -1)
     6c6:	8b 04 9f             	mov    (%edi,%ebx,4),%eax
     6c9:	83 f8 ff             	cmp    $0xffffffff,%eax
     6cc:	74 0d                	je     6db <sbrktest+0x29b>
      continue;
    kill(pids[i]);
     6ce:	89 04 24             	mov    %eax,(%esp)
     6d1:	e8 42 2d 00 00       	call   3418 <kill>
    wait();
     6d6:	e8 15 2d 00 00       	call   33f0 <wait>
      read(fds[0], &scratch, 1);
  }
  // if those failed allocations freed up the pages they did allocate,
  // we'll be able to allocate here
  c = sbrk(4096);
  for(i = 0; i < sizeof(pids)/sizeof(pids[0]); i++){
     6db:	83 c3 01             	add    $0x1,%ebx
     6de:	83 fb 20             	cmp    $0x20,%ebx
     6e1:	75 e3                	jne    6c6 <sbrktest+0x286>
    if(pids[i] == -1)
      continue;
    kill(pids[i]);
    wait();
  }
  if(c == (char*)0xffffffff){
     6e3:	83 fe ff             	cmp    $0xffffffff,%esi
     6e6:	74 78                	je     760 <sbrktest+0x320>
    printf(stdout, "failed sbrk leaked memory\n");
    exit();
  }

  if(sbrk(0) > oldbrk)
     6e8:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     6ef:	e8 7c 2d 00 00       	call   3470 <sbrk>
     6f4:	39 85 54 ff ff ff    	cmp    %eax,-0xac(%ebp)
     6fa:	72 46                	jb     742 <sbrktest+0x302>
    sbrk(-(sbrk(0) - oldbrk));

  printf(stdout, "sbrk test OK\n");
     6fc:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     701:	c7 44 24 04 b3 3a 00 	movl   $0x3ab3,0x4(%esp)
     708:	00 
     709:	89 04 24             	mov    %eax,(%esp)
     70c:	e8 0f 2e 00 00       	call   3520 <printf>
}
     711:	81 c4 cc 00 00 00    	add    $0xcc,%esp
     717:	5b                   	pop    %ebx
     718:	5e                   	pop    %esi
     719:	5f                   	pop    %edi
     71a:	5d                   	pop    %ebp
     71b:	c3                   	ret    
  a = sbrk(0);
  int i;
  for(i = 0; i < 5000; i++){
    b = sbrk(1);
    if(b != a){
      printf(stdout, "sbrk test failed %d %x %x\n", i, a, b);
     71c:	89 44 24 10          	mov    %eax,0x10(%esp)
     720:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     725:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
     729:	89 74 24 08          	mov    %esi,0x8(%esp)
     72d:	c7 44 24 04 07 3a 00 	movl   $0x3a07,0x4(%esp)
     734:	00 
     735:	89 04 24             	mov    %eax,(%esp)
     738:	e8 e3 2d 00 00       	call   3520 <printf>
      exit();
     73d:	e8 a6 2c 00 00       	call   33e8 <exit>
    printf(stdout, "failed sbrk leaked memory\n");
    exit();
  }

  if(sbrk(0) > oldbrk)
    sbrk(-(sbrk(0) - oldbrk));
     742:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     749:	e8 22 2d 00 00       	call   3470 <sbrk>
     74e:	8b 95 54 ff ff ff    	mov    -0xac(%ebp),%edx
     754:	29 c2                	sub    %eax,%edx
     756:	89 14 24             	mov    %edx,(%esp)
     759:	e8 12 2d 00 00       	call   3470 <sbrk>
     75e:	eb 9c                	jmp    6fc <sbrktest+0x2bc>
      continue;
    kill(pids[i]);
    wait();
  }
  if(c == (char*)0xffffffff){
    printf(stdout, "failed sbrk leaked memory\n");
     760:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     765:	c7 44 24 04 98 3a 00 	movl   $0x3a98,0x4(%esp)
     76c:	00 
     76d:	89 04 24             	mov    %eax,(%esp)
     770:	e8 ab 2d 00 00       	call   3520 <printf>
    exit();
     775:	e8 6e 2c 00 00       	call   33e8 <exit>

  // if we run the system out of memory, does it clean up the last
  // failed allocation?
  sbrk(-(sbrk(0) - oldbrk));
  if(pipe(fds) != 0){
    printf(1, "pipe() failed\n");
     77a:	c7 44 24 04 89 3a 00 	movl   $0x3a89,0x4(%esp)
     781:	00 
     782:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     789:	e8 92 2d 00 00       	call   3520 <printf>
    exit();
     78e:	e8 55 2c 00 00       	call   33e8 <exit>
    if(pid < 0){
      printf(stdout, "fork failed\n");
      exit();
    }
    if(pid == 0){
      printf(stdout, "oops could read %x = %x\n", a, *a);
     793:	0f be 03             	movsbl (%ebx),%eax
     796:	89 5c 24 08          	mov    %ebx,0x8(%esp)
     79a:	c7 44 24 04 70 3a 00 	movl   $0x3a70,0x4(%esp)
     7a1:	00 
     7a2:	89 44 24 0c          	mov    %eax,0xc(%esp)
     7a6:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     7ab:	89 04 24             	mov    %eax,(%esp)
     7ae:	e8 6d 2d 00 00       	call   3520 <printf>
      kill(ppid);
     7b3:	89 34 24             	mov    %esi,(%esp)
     7b6:	e8 5d 2c 00 00       	call   3418 <kill>
      exit();
     7bb:	e8 28 2c 00 00       	call   33e8 <exit>
  // can we read the kernel's memory?
  for(a = (char*)(640*1024); a < (char*)2000000; a += 50000){
    ppid = getpid();
    pid = fork();
    if(pid < 0){
      printf(stdout, "fork failed\n");
     7c0:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     7c5:	c7 44 24 04 52 39 00 	movl   $0x3952,0x4(%esp)
     7cc:	00 
     7cd:	89 04 24             	mov    %eax,(%esp)
     7d0:	e8 4b 2d 00 00       	call   3520 <printf>
      exit();
     7d5:	e8 0e 2c 00 00       	call   33e8 <exit>
    exit();
  }

  c = sbrk(4096);
  if(c != (char*)0xffffffff){
    printf(stdout, "sbrk was able to re-allocate beyond 640K, c %x\n", c);
     7da:	89 44 24 08          	mov    %eax,0x8(%esp)
     7de:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     7e3:	c7 44 24 04 4c 48 00 	movl   $0x484c,0x4(%esp)
     7ea:	00 
     7eb:	89 04 24             	mov    %eax,(%esp)
     7ee:	e8 2d 2d 00 00       	call   3520 <printf>
    exit();
     7f3:	e8 f0 2b 00 00       	call   33e8 <exit>
    printf(stdout, "sbrk re-allocation failed, a %x c %x\n", a, c);
    exit();
  }
  if(*lastaddr == 99){
    // should be zero
    printf(stdout, "sbrk de-allocation didn't really deallocate\n");
     7f8:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     7fd:	c7 44 24 04 1c 48 00 	movl   $0x481c,0x4(%esp)
     804:	00 
     805:	89 04 24             	mov    %eax,(%esp)
     808:	e8 13 2d 00 00       	call   3520 <printf>
    exit();
     80d:	e8 d6 2b 00 00       	call   33e8 <exit>

  // can one re-allocate that page?
  a = sbrk(0);
  c = sbrk(4096);
  if(c != a || sbrk(0) != a + 4096){
    printf(stdout, "sbrk re-allocation failed, a %x c %x\n", a, c);
     812:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     817:	89 74 24 0c          	mov    %esi,0xc(%esp)
     81b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
     81f:	c7 44 24 04 f4 47 00 	movl   $0x47f4,0x4(%esp)
     826:	00 
     827:	89 04 24             	mov    %eax,(%esp)
     82a:	e8 f1 2c 00 00       	call   3520 <printf>
    exit();
     82f:	e8 b4 2b 00 00       	call   33e8 <exit>
    printf(stdout, "sbrk could not deallocate\n");
    exit();
  }
  c = sbrk(0);
  if(c != a - 4096){
    printf(stdout, "sbrk deallocation produced wrong address, a %x c %x\n", a, c);
     834:	89 44 24 0c          	mov    %eax,0xc(%esp)
     838:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     83d:	89 5c 24 08          	mov    %ebx,0x8(%esp)
     841:	c7 44 24 04 bc 47 00 	movl   $0x47bc,0x4(%esp)
     848:	00 
     849:	89 04 24             	mov    %eax,(%esp)
     84c:	e8 cf 2c 00 00       	call   3520 <printf>
    exit();
     851:	e8 92 2b 00 00       	call   33e8 <exit>

  // can one de-allocate?
  a = sbrk(0);
  c = sbrk(-4096);
  if(c == (char*)0xffffffff){
    printf(stdout, "sbrk could not deallocate\n");
     856:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     85b:	c7 44 24 04 55 3a 00 	movl   $0x3a55,0x4(%esp)
     862:	00 
     863:	89 04 24             	mov    %eax,(%esp)
     866:	e8 b5 2c 00 00       	call   3520 <printf>
    exit();
     86b:	e8 78 2b 00 00       	call   33e8 <exit>
  *lastaddr = 99;

  // is one forbidden from allocating more than 640K?
  c = sbrk(4096);
  if(c != (char*)0xffffffff){
    printf(stdout, "sbrk allocated more than 640K, c %x\n", c);
     870:	89 44 24 08          	mov    %eax,0x8(%esp)
     874:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     879:	c7 44 24 04 94 47 00 	movl   $0x4794,0x4(%esp)
     880:	00 
     881:	89 04 24             	mov    %eax,(%esp)
     884:	e8 97 2c 00 00       	call   3520 <printf>
    exit();
     889:	e8 5a 2b 00 00       	call   33e8 <exit>
  // can one allocate the full 640K?
  a = sbrk(0);
  amt = (640 * 1024) - (uint)a;
  p = sbrk(amt);
  if(p != a){
    printf(stdout, "sbrk test failed 640K test, p %x a %x\n", p, a);
     88e:	89 44 24 08          	mov    %eax,0x8(%esp)
     892:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     897:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
     89b:	c7 44 24 04 6c 47 00 	movl   $0x476c,0x4(%esp)
     8a2:	00 
     8a3:	89 04 24             	mov    %eax,(%esp)
     8a6:	e8 75 2c 00 00       	call   3520 <printf>
    exit();
     8ab:	e8 38 2b 00 00       	call   33e8 <exit>
    exit();
  }
  c = sbrk(1);
  c = sbrk(1);
  if(c != a + 1){
    printf(stdout, "sbrk test failed post-fork\n");
     8b0:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     8b5:	c7 44 24 04 39 3a 00 	movl   $0x3a39,0x4(%esp)
     8bc:	00 
     8bd:	89 04 24             	mov    %eax,(%esp)
     8c0:	e8 5b 2c 00 00       	call   3520 <printf>
    exit();
     8c5:	e8 1e 2b 00 00       	call   33e8 <exit>
    *b = 1;
    a = b + 1;
  }
  pid = fork();
  if(pid < 0){
    printf(stdout, "sbrk test fork failed\n");
     8ca:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
     8cf:	c7 44 24 04 22 3a 00 	movl   $0x3a22,0x4(%esp)
     8d6:	00 
     8d7:	89 04 24             	mov    %eax,(%esp)
     8da:	e8 41 2c 00 00       	call   3520 <printf>
    exit();
     8df:	e8 04 2b 00 00       	call   33e8 <exit>
     8e4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
     8ea:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

000008f0 <preempt>:
}

// meant to be run w/ at most two CPUs
void
preempt(void)
{
     8f0:	55                   	push   %ebp
     8f1:	89 e5                	mov    %esp,%ebp
     8f3:	57                   	push   %edi
     8f4:	56                   	push   %esi
     8f5:	53                   	push   %ebx
     8f6:	83 ec 2c             	sub    $0x2c,%esp
  int pid1, pid2, pid3;
  int pfds[2];

  printf(1, "preempt: ");
     8f9:	c7 44 24 04 c1 3a 00 	movl   $0x3ac1,0x4(%esp)
     900:	00 
     901:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     908:	e8 13 2c 00 00       	call   3520 <printf>
  pid1 = fork();
     90d:	e8 ce 2a 00 00       	call   33e0 <fork>
  if(pid1 == 0)
     912:	85 c0                	test   %eax,%eax
{
  int pid1, pid2, pid3;
  int pfds[2];

  printf(1, "preempt: ");
  pid1 = fork();
     914:	89 c7                	mov    %eax,%edi
  if(pid1 == 0)
     916:	75 02                	jne    91a <preempt+0x2a>
     918:	eb fe                	jmp    918 <preempt+0x28>
     91a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    for(;;)
      ;

  pid2 = fork();
     920:	e8 bb 2a 00 00       	call   33e0 <fork>
  if(pid2 == 0)
     925:	85 c0                	test   %eax,%eax
  pid1 = fork();
  if(pid1 == 0)
    for(;;)
      ;

  pid2 = fork();
     927:	89 c6                	mov    %eax,%esi
  if(pid2 == 0)
     929:	75 02                	jne    92d <preempt+0x3d>
     92b:	eb fe                	jmp    92b <preempt+0x3b>
    for(;;)
      ;

  pipe(pfds);
     92d:	8d 45 e0             	lea    -0x20(%ebp),%eax
     930:	89 04 24             	mov    %eax,(%esp)
     933:	e8 c0 2a 00 00       	call   33f8 <pipe>
  pid3 = fork();
     938:	e8 a3 2a 00 00       	call   33e0 <fork>
  if(pid3 == 0){
     93d:	85 c0                	test   %eax,%eax
  if(pid2 == 0)
    for(;;)
      ;

  pipe(pfds);
  pid3 = fork();
     93f:	89 c3                	mov    %eax,%ebx
  if(pid3 == 0){
     941:	75 4c                	jne    98f <preempt+0x9f>
    close(pfds[0]);
     943:	8b 45 e0             	mov    -0x20(%ebp),%eax
     946:	89 04 24             	mov    %eax,(%esp)
     949:	e8 c2 2a 00 00       	call   3410 <close>
    if(write(pfds[1], "x", 1) != 1)
     94e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
     951:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
     958:	00 
     959:	c7 44 24 04 c5 3f 00 	movl   $0x3fc5,0x4(%esp)
     960:	00 
     961:	89 04 24             	mov    %eax,(%esp)
     964:	e8 9f 2a 00 00       	call   3408 <write>
     969:	83 f8 01             	cmp    $0x1,%eax
     96c:	74 14                	je     982 <preempt+0x92>
      printf(1, "preempt write error");
     96e:	c7 44 24 04 cb 3a 00 	movl   $0x3acb,0x4(%esp)
     975:	00 
     976:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     97d:	e8 9e 2b 00 00       	call   3520 <printf>
    close(pfds[1]);
     982:	8b 45 e4             	mov    -0x1c(%ebp),%eax
     985:	89 04 24             	mov    %eax,(%esp)
     988:	e8 83 2a 00 00       	call   3410 <close>
     98d:	eb fe                	jmp    98d <preempt+0x9d>
    for(;;)
      ;
  }

  close(pfds[1]);
     98f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
     992:	89 04 24             	mov    %eax,(%esp)
     995:	e8 76 2a 00 00       	call   3410 <close>
  if(read(pfds[0], buf, sizeof(buf)) != 1){
     99a:	8b 45 e0             	mov    -0x20(%ebp),%eax
     99d:	c7 44 24 08 00 08 00 	movl   $0x800,0x8(%esp)
     9a4:	00 
     9a5:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
     9ac:	00 
     9ad:	89 04 24             	mov    %eax,(%esp)
     9b0:	e8 4b 2a 00 00       	call   3400 <read>
     9b5:	83 f8 01             	cmp    $0x1,%eax
     9b8:	74 1c                	je     9d6 <preempt+0xe6>
    printf(1, "preempt read error");
     9ba:	c7 44 24 04 df 3a 00 	movl   $0x3adf,0x4(%esp)
     9c1:	00 
     9c2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     9c9:	e8 52 2b 00 00       	call   3520 <printf>
  printf(1, "wait... ");
  wait();
  wait();
  wait();
  printf(1, "preempt ok\n");
}
     9ce:	83 c4 2c             	add    $0x2c,%esp
     9d1:	5b                   	pop    %ebx
     9d2:	5e                   	pop    %esi
     9d3:	5f                   	pop    %edi
     9d4:	5d                   	pop    %ebp
     9d5:	c3                   	ret    
  close(pfds[1]);
  if(read(pfds[0], buf, sizeof(buf)) != 1){
    printf(1, "preempt read error");
    return;
  }
  close(pfds[0]);
     9d6:	8b 45 e0             	mov    -0x20(%ebp),%eax
     9d9:	89 04 24             	mov    %eax,(%esp)
     9dc:	e8 2f 2a 00 00       	call   3410 <close>
  printf(1, "kill... ");
     9e1:	c7 44 24 04 f2 3a 00 	movl   $0x3af2,0x4(%esp)
     9e8:	00 
     9e9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     9f0:	e8 2b 2b 00 00       	call   3520 <printf>
  kill(pid1);
     9f5:	89 3c 24             	mov    %edi,(%esp)
     9f8:	e8 1b 2a 00 00       	call   3418 <kill>
  kill(pid2);
     9fd:	89 34 24             	mov    %esi,(%esp)
     a00:	e8 13 2a 00 00       	call   3418 <kill>
  kill(pid3);
     a05:	89 1c 24             	mov    %ebx,(%esp)
     a08:	e8 0b 2a 00 00       	call   3418 <kill>
  printf(1, "wait... ");
     a0d:	c7 44 24 04 fb 3a 00 	movl   $0x3afb,0x4(%esp)
     a14:	00 
     a15:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     a1c:	e8 ff 2a 00 00       	call   3520 <printf>
  wait();
     a21:	e8 ca 29 00 00       	call   33f0 <wait>
  wait();
     a26:	e8 c5 29 00 00       	call   33f0 <wait>
     a2b:	90                   	nop
     a2c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  wait();
     a30:	e8 bb 29 00 00       	call   33f0 <wait>
  printf(1, "preempt ok\n");
     a35:	c7 44 24 04 04 3b 00 	movl   $0x3b04,0x4(%esp)
     a3c:	00 
     a3d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     a44:	e8 d7 2a 00 00       	call   3520 <printf>
     a49:	eb 83                	jmp    9ce <preempt+0xde>
     a4b:	90                   	nop
     a4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000a50 <pipe1>:

// simple fork and pipe read/write

void
pipe1(void)
{
     a50:	55                   	push   %ebp
     a51:	89 e5                	mov    %esp,%ebp
     a53:	57                   	push   %edi
     a54:	56                   	push   %esi
     a55:	53                   	push   %ebx
     a56:	83 ec 2c             	sub    $0x2c,%esp
  int fds[2], pid;
  int seq, i, n, cc, total;

  if(pipe(fds) != 0){
     a59:	8d 45 e0             	lea    -0x20(%ebp),%eax
     a5c:	89 04 24             	mov    %eax,(%esp)
     a5f:	e8 94 29 00 00       	call   33f8 <pipe>
     a64:	85 c0                	test   %eax,%eax
     a66:	0f 85 4e 01 00 00    	jne    bba <pipe1+0x16a>
    printf(1, "pipe() failed\n");
    exit();
  }
  pid = fork();
     a6c:	e8 6f 29 00 00       	call   33e0 <fork>
  seq = 0;
  if(pid == 0){
     a71:	83 f8 00             	cmp    $0x0,%eax
     a74:	0f 84 80 00 00 00    	je     afa <pipe1+0xaa>
        printf(1, "pipe1 oops 1\n");
        exit();
      }
    }
    exit();
  } else if(pid > 0){
     a7a:	0f 8e 53 01 00 00    	jle    bd3 <pipe1+0x183>
    close(fds[1]);
     a80:	8b 45 e4             	mov    -0x1c(%ebp),%eax
     a83:	31 ff                	xor    %edi,%edi
     a85:	be 01 00 00 00       	mov    $0x1,%esi
     a8a:	31 db                	xor    %ebx,%ebx
     a8c:	89 04 24             	mov    %eax,(%esp)
     a8f:	e8 7c 29 00 00       	call   3410 <close>
    total = 0;
    cc = 1;
    while((n = read(fds[0], buf, cc)) > 0){
     a94:	8b 45 e0             	mov    -0x20(%ebp),%eax
     a97:	89 74 24 08          	mov    %esi,0x8(%esp)
     a9b:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
     aa2:	00 
     aa3:	89 04 24             	mov    %eax,(%esp)
     aa6:	e8 55 29 00 00       	call   3400 <read>
     aab:	85 c0                	test   %eax,%eax
     aad:	0f 8e a5 00 00 00    	jle    b58 <pipe1+0x108>
     ab3:	31 d2                	xor    %edx,%edx
     ab5:	8d 76 00             	lea    0x0(%esi),%esi
      for(i = 0; i < n; i++){
        if((buf[i] & 0xff) != (seq++ & 0xff)){
     ab8:	38 9a 20 75 00 00    	cmp    %bl,0x7520(%edx)
     abe:	75 1e                	jne    ade <pipe1+0x8e>
  } else if(pid > 0){
    close(fds[1]);
    total = 0;
    cc = 1;
    while((n = read(fds[0], buf, cc)) > 0){
      for(i = 0; i < n; i++){
     ac0:	83 c2 01             	add    $0x1,%edx
        if((buf[i] & 0xff) != (seq++ & 0xff)){
     ac3:	83 c3 01             	add    $0x1,%ebx
  } else if(pid > 0){
    close(fds[1]);
    total = 0;
    cc = 1;
    while((n = read(fds[0], buf, cc)) > 0){
      for(i = 0; i < n; i++){
     ac6:	39 d0                	cmp    %edx,%eax
     ac8:	7f ee                	jg     ab8 <pipe1+0x68>
          printf(1, "pipe1 oops 2\n");
          return;
        }
      }
      total += n;
      cc = cc * 2;
     aca:	01 f6                	add    %esi,%esi
      if(cc > sizeof(buf))
     acc:	ba 00 08 00 00       	mov    $0x800,%edx
     ad1:	81 fe 01 08 00 00    	cmp    $0x801,%esi
     ad7:	0f 43 f2             	cmovae %edx,%esi
        if((buf[i] & 0xff) != (seq++ & 0xff)){
          printf(1, "pipe1 oops 2\n");
          return;
        }
      }
      total += n;
     ada:	01 c7                	add    %eax,%edi
     adc:	eb b6                	jmp    a94 <pipe1+0x44>
    total = 0;
    cc = 1;
    while((n = read(fds[0], buf, cc)) > 0){
      for(i = 0; i < n; i++){
        if((buf[i] & 0xff) != (seq++ & 0xff)){
          printf(1, "pipe1 oops 2\n");
     ade:	c7 44 24 04 1e 3b 00 	movl   $0x3b1e,0x4(%esp)
     ae5:	00 
     ae6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     aed:	e8 2e 2a 00 00       	call   3520 <printf>
  } else {
    printf(1, "fork() failed\n");
    exit();
  }
  printf(1, "pipe1 ok\n");
}
     af2:	83 c4 2c             	add    $0x2c,%esp
     af5:	5b                   	pop    %ebx
     af6:	5e                   	pop    %esi
     af7:	5f                   	pop    %edi
     af8:	5d                   	pop    %ebp
     af9:	c3                   	ret    
    exit();
  }
  pid = fork();
  seq = 0;
  if(pid == 0){
    close(fds[0]);
     afa:	8b 45 e0             	mov    -0x20(%ebp),%eax
     afd:	31 db                	xor    %ebx,%ebx
     aff:	89 04 24             	mov    %eax,(%esp)
     b02:	e8 09 29 00 00       	call   3410 <close>
    for(n = 0; n < 5; n++){
     b07:	31 c0                	xor    %eax,%eax
     b09:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      for(i = 0; i < 1033; i++)
        buf[i] = seq++;
     b10:	8d 14 03             	lea    (%ebx,%eax,1),%edx
     b13:	88 90 20 75 00 00    	mov    %dl,0x7520(%eax)
  pid = fork();
  seq = 0;
  if(pid == 0){
    close(fds[0]);
    for(n = 0; n < 5; n++){
      for(i = 0; i < 1033; i++)
     b19:	83 c0 01             	add    $0x1,%eax
     b1c:	3d 09 04 00 00       	cmp    $0x409,%eax
     b21:	75 ed                	jne    b10 <pipe1+0xc0>
        buf[i] = seq++;
      if(write(fds[1], buf, 1033) != 1033){
     b23:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  pid = fork();
  seq = 0;
  if(pid == 0){
    close(fds[0]);
    for(n = 0; n < 5; n++){
      for(i = 0; i < 1033; i++)
     b26:	81 c3 09 04 00 00    	add    $0x409,%ebx
        buf[i] = seq++;
      if(write(fds[1], buf, 1033) != 1033){
     b2c:	c7 44 24 08 09 04 00 	movl   $0x409,0x8(%esp)
     b33:	00 
     b34:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
     b3b:	00 
     b3c:	89 04 24             	mov    %eax,(%esp)
     b3f:	e8 c4 28 00 00       	call   3408 <write>
     b44:	3d 09 04 00 00       	cmp    $0x409,%eax
     b49:	75 56                	jne    ba1 <pipe1+0x151>
  }
  pid = fork();
  seq = 0;
  if(pid == 0){
    close(fds[0]);
    for(n = 0; n < 5; n++){
     b4b:	81 fb 2d 14 00 00    	cmp    $0x142d,%ebx
     b51:	75 b4                	jne    b07 <pipe1+0xb7>
      printf(1, "pipe1 oops 3 total %d\n", total);
    close(fds[0]);
    wait();
  } else {
    printf(1, "fork() failed\n");
    exit();
     b53:	e8 90 28 00 00       	call   33e8 <exit>
      total += n;
      cc = cc * 2;
      if(cc > sizeof(buf))
        cc = sizeof(buf);
    }
    if(total != 5 * 1033)
     b58:	81 ff 2d 14 00 00    	cmp    $0x142d,%edi
     b5e:	74 18                	je     b78 <pipe1+0x128>
      printf(1, "pipe1 oops 3 total %d\n", total);
     b60:	89 7c 24 08          	mov    %edi,0x8(%esp)
     b64:	c7 44 24 04 2c 3b 00 	movl   $0x3b2c,0x4(%esp)
     b6b:	00 
     b6c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     b73:	e8 a8 29 00 00       	call   3520 <printf>
    close(fds[0]);
     b78:	8b 45 e0             	mov    -0x20(%ebp),%eax
     b7b:	89 04 24             	mov    %eax,(%esp)
     b7e:	e8 8d 28 00 00       	call   3410 <close>
    wait();
     b83:	e8 68 28 00 00       	call   33f0 <wait>
  } else {
    printf(1, "fork() failed\n");
    exit();
  }
  printf(1, "pipe1 ok\n");
     b88:	c7 44 24 04 43 3b 00 	movl   $0x3b43,0x4(%esp)
     b8f:	00 
     b90:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     b97:	e8 84 29 00 00       	call   3520 <printf>
     b9c:	e9 51 ff ff ff       	jmp    af2 <pipe1+0xa2>
    close(fds[0]);
    for(n = 0; n < 5; n++){
      for(i = 0; i < 1033; i++)
        buf[i] = seq++;
      if(write(fds[1], buf, 1033) != 1033){
        printf(1, "pipe1 oops 1\n");
     ba1:	c7 44 24 04 10 3b 00 	movl   $0x3b10,0x4(%esp)
     ba8:	00 
     ba9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     bb0:	e8 6b 29 00 00       	call   3520 <printf>
        exit();
     bb5:	e8 2e 28 00 00       	call   33e8 <exit>
{
  int fds[2], pid;
  int seq, i, n, cc, total;

  if(pipe(fds) != 0){
    printf(1, "pipe() failed\n");
     bba:	c7 44 24 04 89 3a 00 	movl   $0x3a89,0x4(%esp)
     bc1:	00 
     bc2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     bc9:	e8 52 29 00 00       	call   3520 <printf>
    exit();
     bce:	e8 15 28 00 00       	call   33e8 <exit>
    if(total != 5 * 1033)
      printf(1, "pipe1 oops 3 total %d\n", total);
    close(fds[0]);
    wait();
  } else {
    printf(1, "fork() failed\n");
     bd3:	c7 44 24 04 4d 3b 00 	movl   $0x3b4d,0x4(%esp)
     bda:	00 
     bdb:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     be2:	e8 39 29 00 00       	call   3520 <printf>
     be7:	e9 67 ff ff ff       	jmp    b53 <pipe1+0x103>
     bec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000bf0 <fourteen>:
  printf(1, "bigfile test ok\n");
}

void
fourteen(void)
{
     bf0:	55                   	push   %ebp
     bf1:	89 e5                	mov    %esp,%ebp
     bf3:	83 ec 18             	sub    $0x18,%esp
  int fd;

  // DIRSIZ is 14.
  printf(1, "fourteen test\n");
     bf6:	c7 44 24 04 5c 3b 00 	movl   $0x3b5c,0x4(%esp)
     bfd:	00 
     bfe:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     c05:	e8 16 29 00 00       	call   3520 <printf>

  if(mkdir("12345678901234") != 0){
     c0a:	c7 04 24 97 3b 00 00 	movl   $0x3b97,(%esp)
     c11:	e8 3a 28 00 00       	call   3450 <mkdir>
     c16:	85 c0                	test   %eax,%eax
     c18:	0f 85 92 00 00 00    	jne    cb0 <fourteen+0xc0>
    printf(1, "mkdir 12345678901234 failed\n");
    exit();
  }
  if(mkdir("12345678901234/123456789012345") != 0){
     c1e:	c7 04 24 7c 48 00 00 	movl   $0x487c,(%esp)
     c25:	e8 26 28 00 00       	call   3450 <mkdir>
     c2a:	85 c0                	test   %eax,%eax
     c2c:	0f 85 fb 00 00 00    	jne    d2d <fourteen+0x13d>
    printf(1, "mkdir 12345678901234/123456789012345 failed\n");
    exit();
  }
  fd = open("123456789012345/123456789012345/123456789012345", O_CREATE);
     c32:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
     c39:	00 
     c3a:	c7 04 24 cc 48 00 00 	movl   $0x48cc,(%esp)
     c41:	e8 e2 27 00 00       	call   3428 <open>
  if(fd < 0){
     c46:	85 c0                	test   %eax,%eax
     c48:	0f 88 c6 00 00 00    	js     d14 <fourteen+0x124>
    printf(1, "create 123456789012345/123456789012345/123456789012345 failed\n");
    exit();
  }
  close(fd);
     c4e:	89 04 24             	mov    %eax,(%esp)
     c51:	e8 ba 27 00 00       	call   3410 <close>
  fd = open("12345678901234/12345678901234/12345678901234", 0);
     c56:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
     c5d:	00 
     c5e:	c7 04 24 3c 49 00 00 	movl   $0x493c,(%esp)
     c65:	e8 be 27 00 00       	call   3428 <open>
  if(fd < 0){
     c6a:	85 c0                	test   %eax,%eax
     c6c:	0f 88 89 00 00 00    	js     cfb <fourteen+0x10b>
    printf(1, "open 12345678901234/12345678901234/12345678901234 failed\n");
    exit();
  }
  close(fd);
     c72:	89 04 24             	mov    %eax,(%esp)
     c75:	e8 96 27 00 00       	call   3410 <close>

  if(mkdir("12345678901234/12345678901234") == 0){
     c7a:	c7 04 24 88 3b 00 00 	movl   $0x3b88,(%esp)
     c81:	e8 ca 27 00 00       	call   3450 <mkdir>
     c86:	85 c0                	test   %eax,%eax
     c88:	74 58                	je     ce2 <fourteen+0xf2>
    printf(1, "mkdir 12345678901234/12345678901234 succeeded!\n");
    exit();
  }
  if(mkdir("123456789012345/12345678901234") == 0){
     c8a:	c7 04 24 d8 49 00 00 	movl   $0x49d8,(%esp)
     c91:	e8 ba 27 00 00       	call   3450 <mkdir>
     c96:	85 c0                	test   %eax,%eax
     c98:	74 2f                	je     cc9 <fourteen+0xd9>
    printf(1, "mkdir 12345678901234/123456789012345 succeeded!\n");
    exit();
  }

  printf(1, "fourteen ok\n");
     c9a:	c7 44 24 04 a6 3b 00 	movl   $0x3ba6,0x4(%esp)
     ca1:	00 
     ca2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     ca9:	e8 72 28 00 00       	call   3520 <printf>
}
     cae:	c9                   	leave  
     caf:	c3                   	ret    

  // DIRSIZ is 14.
  printf(1, "fourteen test\n");

  if(mkdir("12345678901234") != 0){
    printf(1, "mkdir 12345678901234 failed\n");
     cb0:	c7 44 24 04 6b 3b 00 	movl   $0x3b6b,0x4(%esp)
     cb7:	00 
     cb8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     cbf:	e8 5c 28 00 00       	call   3520 <printf>
    exit();
     cc4:	e8 1f 27 00 00       	call   33e8 <exit>
  if(mkdir("12345678901234/12345678901234") == 0){
    printf(1, "mkdir 12345678901234/12345678901234 succeeded!\n");
    exit();
  }
  if(mkdir("123456789012345/12345678901234") == 0){
    printf(1, "mkdir 12345678901234/123456789012345 succeeded!\n");
     cc9:	c7 44 24 04 f8 49 00 	movl   $0x49f8,0x4(%esp)
     cd0:	00 
     cd1:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     cd8:	e8 43 28 00 00       	call   3520 <printf>
    exit();
     cdd:	e8 06 27 00 00       	call   33e8 <exit>
    exit();
  }
  close(fd);

  if(mkdir("12345678901234/12345678901234") == 0){
    printf(1, "mkdir 12345678901234/12345678901234 succeeded!\n");
     ce2:	c7 44 24 04 a8 49 00 	movl   $0x49a8,0x4(%esp)
     ce9:	00 
     cea:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     cf1:	e8 2a 28 00 00       	call   3520 <printf>
    exit();
     cf6:	e8 ed 26 00 00       	call   33e8 <exit>
    exit();
  }
  close(fd);
  fd = open("12345678901234/12345678901234/12345678901234", 0);
  if(fd < 0){
    printf(1, "open 12345678901234/12345678901234/12345678901234 failed\n");
     cfb:	c7 44 24 04 6c 49 00 	movl   $0x496c,0x4(%esp)
     d02:	00 
     d03:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     d0a:	e8 11 28 00 00       	call   3520 <printf>
    exit();
     d0f:	e8 d4 26 00 00       	call   33e8 <exit>
    printf(1, "mkdir 12345678901234/123456789012345 failed\n");
    exit();
  }
  fd = open("123456789012345/123456789012345/123456789012345", O_CREATE);
  if(fd < 0){
    printf(1, "create 123456789012345/123456789012345/123456789012345 failed\n");
     d14:	c7 44 24 04 fc 48 00 	movl   $0x48fc,0x4(%esp)
     d1b:	00 
     d1c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     d23:	e8 f8 27 00 00       	call   3520 <printf>
    exit();
     d28:	e8 bb 26 00 00       	call   33e8 <exit>
  if(mkdir("12345678901234") != 0){
    printf(1, "mkdir 12345678901234 failed\n");
    exit();
  }
  if(mkdir("12345678901234/123456789012345") != 0){
    printf(1, "mkdir 12345678901234/123456789012345 failed\n");
     d2d:	c7 44 24 04 9c 48 00 	movl   $0x489c,0x4(%esp)
     d34:	00 
     d35:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     d3c:	e8 df 27 00 00       	call   3520 <printf>
    exit();
     d41:	e8 a2 26 00 00       	call   33e8 <exit>
     d46:	8d 76 00             	lea    0x0(%esi),%esi
     d49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00000d50 <iref>:
}

// test that iput() is called at the end of _namei()
void
iref(void)
{
     d50:	55                   	push   %ebp
     d51:	89 e5                	mov    %esp,%ebp
     d53:	53                   	push   %ebx
  int i, fd;

  printf(1, "empty file name\n");
     d54:	31 db                	xor    %ebx,%ebx
}

// test that iput() is called at the end of _namei()
void
iref(void)
{
     d56:	83 ec 14             	sub    $0x14,%esp
  int i, fd;

  printf(1, "empty file name\n");
     d59:	c7 44 24 04 b3 3b 00 	movl   $0x3bb3,0x4(%esp)
     d60:	00 
     d61:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     d68:	e8 b3 27 00 00       	call   3520 <printf>
     d6d:	8d 76 00             	lea    0x0(%esi),%esi

  // the 50 is NINODE
  for(i = 0; i < 50 + 1; i++){
    if(mkdir("irefd") != 0){
     d70:	c7 04 24 c4 3b 00 00 	movl   $0x3bc4,(%esp)
     d77:	e8 d4 26 00 00       	call   3450 <mkdir>
     d7c:	85 c0                	test   %eax,%eax
     d7e:	0f 85 b2 00 00 00    	jne    e36 <iref+0xe6>
      printf(1, "mkdir irefd failed\n");
      exit();
    }
    if(chdir("irefd") != 0){
     d84:	c7 04 24 c4 3b 00 00 	movl   $0x3bc4,(%esp)
     d8b:	e8 c8 26 00 00       	call   3458 <chdir>
     d90:	85 c0                	test   %eax,%eax
     d92:	0f 85 b7 00 00 00    	jne    e4f <iref+0xff>
      printf(1, "chdir irefd failed\n");
      exit();
    }

    mkdir("");
     d98:	c7 04 24 7f 46 00 00 	movl   $0x467f,(%esp)
     d9f:	e8 ac 26 00 00       	call   3450 <mkdir>
    link("README", "");
     da4:	c7 44 24 04 7f 46 00 	movl   $0x467f,0x4(%esp)
     dab:	00 
     dac:	c7 04 24 f2 3b 00 00 	movl   $0x3bf2,(%esp)
     db3:	e8 90 26 00 00       	call   3448 <link>
    fd = open("", O_CREATE);
     db8:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
     dbf:	00 
     dc0:	c7 04 24 7f 46 00 00 	movl   $0x467f,(%esp)
     dc7:	e8 5c 26 00 00       	call   3428 <open>
    if(fd >= 0)
     dcc:	85 c0                	test   %eax,%eax
     dce:	78 08                	js     dd8 <iref+0x88>
      close(fd);
     dd0:	89 04 24             	mov    %eax,(%esp)
     dd3:	e8 38 26 00 00       	call   3410 <close>
    fd = open("xx", O_CREATE);
     dd8:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
     ddf:	00 
     de0:	c7 04 24 c4 3f 00 00 	movl   $0x3fc4,(%esp)
     de7:	e8 3c 26 00 00       	call   3428 <open>
    if(fd >= 0)
     dec:	85 c0                	test   %eax,%eax
     dee:	78 08                	js     df8 <iref+0xa8>
      close(fd);
     df0:	89 04 24             	mov    %eax,(%esp)
     df3:	e8 18 26 00 00       	call   3410 <close>
  int i, fd;

  printf(1, "empty file name\n");

  // the 50 is NINODE
  for(i = 0; i < 50 + 1; i++){
     df8:	83 c3 01             	add    $0x1,%ebx
    if(fd >= 0)
      close(fd);
    fd = open("xx", O_CREATE);
    if(fd >= 0)
      close(fd);
    unlink("xx");
     dfb:	c7 04 24 c4 3f 00 00 	movl   $0x3fc4,(%esp)
     e02:	e8 31 26 00 00       	call   3438 <unlink>
  int i, fd;

  printf(1, "empty file name\n");

  // the 50 is NINODE
  for(i = 0; i < 50 + 1; i++){
     e07:	83 fb 33             	cmp    $0x33,%ebx
     e0a:	0f 85 60 ff ff ff    	jne    d70 <iref+0x20>
    if(fd >= 0)
      close(fd);
    unlink("xx");
  }

  chdir("/");
     e10:	c7 04 24 f9 3b 00 00 	movl   $0x3bf9,(%esp)
     e17:	e8 3c 26 00 00       	call   3458 <chdir>
  printf(1, "empty file name OK\n");
     e1c:	c7 44 24 04 fb 3b 00 	movl   $0x3bfb,0x4(%esp)
     e23:	00 
     e24:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     e2b:	e8 f0 26 00 00       	call   3520 <printf>
}
     e30:	83 c4 14             	add    $0x14,%esp
     e33:	5b                   	pop    %ebx
     e34:	5d                   	pop    %ebp
     e35:	c3                   	ret    
  printf(1, "empty file name\n");

  // the 50 is NINODE
  for(i = 0; i < 50 + 1; i++){
    if(mkdir("irefd") != 0){
      printf(1, "mkdir irefd failed\n");
     e36:	c7 44 24 04 ca 3b 00 	movl   $0x3bca,0x4(%esp)
     e3d:	00 
     e3e:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     e45:	e8 d6 26 00 00       	call   3520 <printf>
      exit();
     e4a:	e8 99 25 00 00       	call   33e8 <exit>
    }
    if(chdir("irefd") != 0){
      printf(1, "chdir irefd failed\n");
     e4f:	c7 44 24 04 de 3b 00 	movl   $0x3bde,0x4(%esp)
     e56:	00 
     e57:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     e5e:	e8 bd 26 00 00       	call   3520 <printf>
      exit();
     e63:	e8 80 25 00 00       	call   33e8 <exit>
     e68:	90                   	nop
     e69:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00000e70 <dirfile>:
  printf(1, "rmdot ok\n");
}

void
dirfile(void)
{
     e70:	55                   	push   %ebp
     e71:	89 e5                	mov    %esp,%ebp
     e73:	53                   	push   %ebx
     e74:	83 ec 14             	sub    $0x14,%esp
  int fd;

  printf(1, "dir vs file\n");
     e77:	c7 44 24 04 0f 3c 00 	movl   $0x3c0f,0x4(%esp)
     e7e:	00 
     e7f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     e86:	e8 95 26 00 00       	call   3520 <printf>

  fd = open("dirfile", O_CREATE);
     e8b:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
     e92:	00 
     e93:	c7 04 24 1c 3c 00 00 	movl   $0x3c1c,(%esp)
     e9a:	e8 89 25 00 00       	call   3428 <open>
  if(fd < 0){
     e9f:	85 c0                	test   %eax,%eax
     ea1:	0f 88 4e 01 00 00    	js     ff5 <dirfile+0x185>
    printf(1, "create dirfile failed\n");
    exit();
  }
  close(fd);
     ea7:	89 04 24             	mov    %eax,(%esp)
     eaa:	e8 61 25 00 00       	call   3410 <close>
  if(chdir("dirfile") == 0){
     eaf:	c7 04 24 1c 3c 00 00 	movl   $0x3c1c,(%esp)
     eb6:	e8 9d 25 00 00       	call   3458 <chdir>
     ebb:	85 c0                	test   %eax,%eax
     ebd:	0f 84 19 01 00 00    	je     fdc <dirfile+0x16c>
    printf(1, "chdir dirfile succeeded!\n");
    exit();
  }
  fd = open("dirfile/xx", 0);
     ec3:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
     eca:	00 
     ecb:	c7 04 24 55 3c 00 00 	movl   $0x3c55,(%esp)
     ed2:	e8 51 25 00 00       	call   3428 <open>
  if(fd >= 0){
     ed7:	85 c0                	test   %eax,%eax
     ed9:	0f 89 e4 00 00 00    	jns    fc3 <dirfile+0x153>
    printf(1, "create dirfile/xx succeeded!\n");
    exit();
  }
  fd = open("dirfile/xx", O_CREATE);
     edf:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
     ee6:	00 
     ee7:	c7 04 24 55 3c 00 00 	movl   $0x3c55,(%esp)
     eee:	e8 35 25 00 00       	call   3428 <open>
  if(fd >= 0){
     ef3:	85 c0                	test   %eax,%eax
     ef5:	0f 89 c8 00 00 00    	jns    fc3 <dirfile+0x153>
    printf(1, "create dirfile/xx succeeded!\n");
    exit();
  }
  if(mkdir("dirfile/xx") == 0){
     efb:	c7 04 24 55 3c 00 00 	movl   $0x3c55,(%esp)
     f02:	e8 49 25 00 00       	call   3450 <mkdir>
     f07:	85 c0                	test   %eax,%eax
     f09:	0f 84 7c 01 00 00    	je     108b <dirfile+0x21b>
    printf(1, "mkdir dirfile/xx succeeded!\n");
    exit();
  }
  if(unlink("dirfile/xx") == 0){
     f0f:	c7 04 24 55 3c 00 00 	movl   $0x3c55,(%esp)
     f16:	e8 1d 25 00 00       	call   3438 <unlink>
     f1b:	85 c0                	test   %eax,%eax
     f1d:	0f 84 4f 01 00 00    	je     1072 <dirfile+0x202>
    printf(1, "unlink dirfile/xx succeeded!\n");
    exit();
  }
  if(link("README", "dirfile/xx") == 0){
     f23:	c7 44 24 04 55 3c 00 	movl   $0x3c55,0x4(%esp)
     f2a:	00 
     f2b:	c7 04 24 f2 3b 00 00 	movl   $0x3bf2,(%esp)
     f32:	e8 11 25 00 00       	call   3448 <link>
     f37:	85 c0                	test   %eax,%eax
     f39:	0f 84 1a 01 00 00    	je     1059 <dirfile+0x1e9>
    printf(1, "link to dirfile/xx succeeded!\n");
    exit();
  }
  if(unlink("dirfile") != 0){
     f3f:	c7 04 24 1c 3c 00 00 	movl   $0x3c1c,(%esp)
     f46:	e8 ed 24 00 00       	call   3438 <unlink>
     f4b:	85 c0                	test   %eax,%eax
     f4d:	0f 85 ed 00 00 00    	jne    1040 <dirfile+0x1d0>
    printf(1, "unlink dirfile failed!\n");
    exit();
  }

  fd = open(".", O_RDWR);
     f53:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
     f5a:	00 
     f5b:	c7 04 24 e2 3e 00 00 	movl   $0x3ee2,(%esp)
     f62:	e8 c1 24 00 00       	call   3428 <open>
  if(fd >= 0){
     f67:	85 c0                	test   %eax,%eax
     f69:	0f 89 b8 00 00 00    	jns    1027 <dirfile+0x1b7>
    printf(1, "open . for writing succeeded!\n");
    exit();
  }
  fd = open(".", 0);
     f6f:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
     f76:	00 
     f77:	c7 04 24 e2 3e 00 00 	movl   $0x3ee2,(%esp)
     f7e:	e8 a5 24 00 00       	call   3428 <open>
  if(write(fd, "x", 1) > 0){
     f83:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
     f8a:	00 
     f8b:	c7 44 24 04 c5 3f 00 	movl   $0x3fc5,0x4(%esp)
     f92:	00 
  fd = open(".", O_RDWR);
  if(fd >= 0){
    printf(1, "open . for writing succeeded!\n");
    exit();
  }
  fd = open(".", 0);
     f93:	89 c3                	mov    %eax,%ebx
  if(write(fd, "x", 1) > 0){
     f95:	89 04 24             	mov    %eax,(%esp)
     f98:	e8 6b 24 00 00       	call   3408 <write>
     f9d:	85 c0                	test   %eax,%eax
     f9f:	7f 6d                	jg     100e <dirfile+0x19e>
    printf(1, "write . succeeded!\n");
    exit();
  }
  close(fd);
     fa1:	89 1c 24             	mov    %ebx,(%esp)
     fa4:	e8 67 24 00 00       	call   3410 <close>

  printf(1, "dir vs file OK\n");
     fa9:	c7 44 24 04 e5 3c 00 	movl   $0x3ce5,0x4(%esp)
     fb0:	00 
     fb1:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     fb8:	e8 63 25 00 00       	call   3520 <printf>
}
     fbd:	83 c4 14             	add    $0x14,%esp
     fc0:	5b                   	pop    %ebx
     fc1:	5d                   	pop    %ebp
     fc2:	c3                   	ret    
    printf(1, "create dirfile/xx succeeded!\n");
    exit();
  }
  fd = open("dirfile/xx", O_CREATE);
  if(fd >= 0){
    printf(1, "create dirfile/xx succeeded!\n");
     fc3:	c7 44 24 04 60 3c 00 	movl   $0x3c60,0x4(%esp)
     fca:	00 
     fcb:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     fd2:	e8 49 25 00 00       	call   3520 <printf>
    exit();
     fd7:	e8 0c 24 00 00       	call   33e8 <exit>
    printf(1, "create dirfile failed\n");
    exit();
  }
  close(fd);
  if(chdir("dirfile") == 0){
    printf(1, "chdir dirfile succeeded!\n");
     fdc:	c7 44 24 04 3b 3c 00 	movl   $0x3c3b,0x4(%esp)
     fe3:	00 
     fe4:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
     feb:	e8 30 25 00 00       	call   3520 <printf>
    exit();
     ff0:	e8 f3 23 00 00       	call   33e8 <exit>

  printf(1, "dir vs file\n");

  fd = open("dirfile", O_CREATE);
  if(fd < 0){
    printf(1, "create dirfile failed\n");
     ff5:	c7 44 24 04 24 3c 00 	movl   $0x3c24,0x4(%esp)
     ffc:	00 
     ffd:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1004:	e8 17 25 00 00       	call   3520 <printf>
    exit();
    1009:	e8 da 23 00 00       	call   33e8 <exit>
    printf(1, "open . for writing succeeded!\n");
    exit();
  }
  fd = open(".", 0);
  if(write(fd, "x", 1) > 0){
    printf(1, "write . succeeded!\n");
    100e:	c7 44 24 04 d1 3c 00 	movl   $0x3cd1,0x4(%esp)
    1015:	00 
    1016:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    101d:	e8 fe 24 00 00       	call   3520 <printf>
    exit();
    1022:	e8 c1 23 00 00       	call   33e8 <exit>
    exit();
  }

  fd = open(".", O_RDWR);
  if(fd >= 0){
    printf(1, "open . for writing succeeded!\n");
    1027:	c7 44 24 04 4c 4a 00 	movl   $0x4a4c,0x4(%esp)
    102e:	00 
    102f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1036:	e8 e5 24 00 00       	call   3520 <printf>
    exit();
    103b:	e8 a8 23 00 00       	call   33e8 <exit>
  if(link("README", "dirfile/xx") == 0){
    printf(1, "link to dirfile/xx succeeded!\n");
    exit();
  }
  if(unlink("dirfile") != 0){
    printf(1, "unlink dirfile failed!\n");
    1040:	c7 44 24 04 b9 3c 00 	movl   $0x3cb9,0x4(%esp)
    1047:	00 
    1048:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    104f:	e8 cc 24 00 00       	call   3520 <printf>
    exit();
    1054:	e8 8f 23 00 00       	call   33e8 <exit>
  if(unlink("dirfile/xx") == 0){
    printf(1, "unlink dirfile/xx succeeded!\n");
    exit();
  }
  if(link("README", "dirfile/xx") == 0){
    printf(1, "link to dirfile/xx succeeded!\n");
    1059:	c7 44 24 04 2c 4a 00 	movl   $0x4a2c,0x4(%esp)
    1060:	00 
    1061:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1068:	e8 b3 24 00 00       	call   3520 <printf>
    exit();
    106d:	e8 76 23 00 00       	call   33e8 <exit>
  if(mkdir("dirfile/xx") == 0){
    printf(1, "mkdir dirfile/xx succeeded!\n");
    exit();
  }
  if(unlink("dirfile/xx") == 0){
    printf(1, "unlink dirfile/xx succeeded!\n");
    1072:	c7 44 24 04 9b 3c 00 	movl   $0x3c9b,0x4(%esp)
    1079:	00 
    107a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1081:	e8 9a 24 00 00       	call   3520 <printf>
    exit();
    1086:	e8 5d 23 00 00       	call   33e8 <exit>
  if(fd >= 0){
    printf(1, "create dirfile/xx succeeded!\n");
    exit();
  }
  if(mkdir("dirfile/xx") == 0){
    printf(1, "mkdir dirfile/xx succeeded!\n");
    108b:	c7 44 24 04 7e 3c 00 	movl   $0x3c7e,0x4(%esp)
    1092:	00 
    1093:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    109a:	e8 81 24 00 00       	call   3520 <printf>
    exit();
    109f:	e8 44 23 00 00       	call   33e8 <exit>
    10a4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    10aa:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

000010b0 <rmdot>:
  printf(1, "fourteen ok\n");
}

void
rmdot(void)
{
    10b0:	55                   	push   %ebp
    10b1:	89 e5                	mov    %esp,%ebp
    10b3:	83 ec 18             	sub    $0x18,%esp
  printf(1, "rmdot test\n");
    10b6:	c7 44 24 04 f5 3c 00 	movl   $0x3cf5,0x4(%esp)
    10bd:	00 
    10be:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    10c5:	e8 56 24 00 00       	call   3520 <printf>
  if(mkdir("dots") != 0){
    10ca:	c7 04 24 01 3d 00 00 	movl   $0x3d01,(%esp)
    10d1:	e8 7a 23 00 00       	call   3450 <mkdir>
    10d6:	85 c0                	test   %eax,%eax
    10d8:	0f 85 9a 00 00 00    	jne    1178 <rmdot+0xc8>
    printf(1, "mkdir dots failed\n");
    exit();
  }
  if(chdir("dots") != 0){
    10de:	c7 04 24 01 3d 00 00 	movl   $0x3d01,(%esp)
    10e5:	e8 6e 23 00 00       	call   3458 <chdir>
    10ea:	85 c0                	test   %eax,%eax
    10ec:	0f 85 35 01 00 00    	jne    1227 <rmdot+0x177>
    printf(1, "chdir dots failed\n");
    exit();
  }
  if(unlink(".") == 0){
    10f2:	c7 04 24 e2 3e 00 00 	movl   $0x3ee2,(%esp)
    10f9:	e8 3a 23 00 00       	call   3438 <unlink>
    10fe:	85 c0                	test   %eax,%eax
    1100:	0f 84 08 01 00 00    	je     120e <rmdot+0x15e>
    printf(1, "rm . worked!\n");
    exit();
  }
  if(unlink("..") == 0){
    1106:	c7 04 24 e1 3e 00 00 	movl   $0x3ee1,(%esp)
    110d:	e8 26 23 00 00       	call   3438 <unlink>
    1112:	85 c0                	test   %eax,%eax
    1114:	0f 84 db 00 00 00    	je     11f5 <rmdot+0x145>
    printf(1, "rm .. worked!\n");
    exit();
  }
  if(chdir("/") != 0){
    111a:	c7 04 24 f9 3b 00 00 	movl   $0x3bf9,(%esp)
    1121:	e8 32 23 00 00       	call   3458 <chdir>
    1126:	85 c0                	test   %eax,%eax
    1128:	0f 85 ae 00 00 00    	jne    11dc <rmdot+0x12c>
    printf(1, "chdir / failed\n");
    exit();
  }
  if(unlink("dots/.") == 0){
    112e:	c7 04 24 59 3d 00 00 	movl   $0x3d59,(%esp)
    1135:	e8 fe 22 00 00       	call   3438 <unlink>
    113a:	85 c0                	test   %eax,%eax
    113c:	0f 84 81 00 00 00    	je     11c3 <rmdot+0x113>
    printf(1, "unlink dots/. worked!\n");
    exit();
  }
  if(unlink("dots/..") == 0){
    1142:	c7 04 24 77 3d 00 00 	movl   $0x3d77,(%esp)
    1149:	e8 ea 22 00 00       	call   3438 <unlink>
    114e:	85 c0                	test   %eax,%eax
    1150:	74 58                	je     11aa <rmdot+0xfa>
    printf(1, "unlink dots/.. worked!\n");
    exit();
  }
  if(unlink("dots") != 0){
    1152:	c7 04 24 01 3d 00 00 	movl   $0x3d01,(%esp)
    1159:	e8 da 22 00 00       	call   3438 <unlink>
    115e:	85 c0                	test   %eax,%eax
    1160:	75 2f                	jne    1191 <rmdot+0xe1>
    printf(1, "unlink dots failed!\n");
    exit();
  }
  printf(1, "rmdot ok\n");
    1162:	c7 44 24 04 ac 3d 00 	movl   $0x3dac,0x4(%esp)
    1169:	00 
    116a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1171:	e8 aa 23 00 00       	call   3520 <printf>
}
    1176:	c9                   	leave  
    1177:	c3                   	ret    
void
rmdot(void)
{
  printf(1, "rmdot test\n");
  if(mkdir("dots") != 0){
    printf(1, "mkdir dots failed\n");
    1178:	c7 44 24 04 06 3d 00 	movl   $0x3d06,0x4(%esp)
    117f:	00 
    1180:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1187:	e8 94 23 00 00       	call   3520 <printf>
    exit();
    118c:	e8 57 22 00 00       	call   33e8 <exit>
  if(unlink("dots/..") == 0){
    printf(1, "unlink dots/.. worked!\n");
    exit();
  }
  if(unlink("dots") != 0){
    printf(1, "unlink dots failed!\n");
    1191:	c7 44 24 04 97 3d 00 	movl   $0x3d97,0x4(%esp)
    1198:	00 
    1199:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    11a0:	e8 7b 23 00 00       	call   3520 <printf>
    exit();
    11a5:	e8 3e 22 00 00       	call   33e8 <exit>
  if(unlink("dots/.") == 0){
    printf(1, "unlink dots/. worked!\n");
    exit();
  }
  if(unlink("dots/..") == 0){
    printf(1, "unlink dots/.. worked!\n");
    11aa:	c7 44 24 04 7f 3d 00 	movl   $0x3d7f,0x4(%esp)
    11b1:	00 
    11b2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    11b9:	e8 62 23 00 00       	call   3520 <printf>
    exit();
    11be:	e8 25 22 00 00       	call   33e8 <exit>
  if(chdir("/") != 0){
    printf(1, "chdir / failed\n");
    exit();
  }
  if(unlink("dots/.") == 0){
    printf(1, "unlink dots/. worked!\n");
    11c3:	c7 44 24 04 60 3d 00 	movl   $0x3d60,0x4(%esp)
    11ca:	00 
    11cb:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    11d2:	e8 49 23 00 00       	call   3520 <printf>
    exit();
    11d7:	e8 0c 22 00 00       	call   33e8 <exit>
  if(unlink("..") == 0){
    printf(1, "rm .. worked!\n");
    exit();
  }
  if(chdir("/") != 0){
    printf(1, "chdir / failed\n");
    11dc:	c7 44 24 04 49 3d 00 	movl   $0x3d49,0x4(%esp)
    11e3:	00 
    11e4:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    11eb:	e8 30 23 00 00       	call   3520 <printf>
    exit();
    11f0:	e8 f3 21 00 00       	call   33e8 <exit>
  if(unlink(".") == 0){
    printf(1, "rm . worked!\n");
    exit();
  }
  if(unlink("..") == 0){
    printf(1, "rm .. worked!\n");
    11f5:	c7 44 24 04 3a 3d 00 	movl   $0x3d3a,0x4(%esp)
    11fc:	00 
    11fd:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1204:	e8 17 23 00 00       	call   3520 <printf>
    exit();
    1209:	e8 da 21 00 00       	call   33e8 <exit>
  if(chdir("dots") != 0){
    printf(1, "chdir dots failed\n");
    exit();
  }
  if(unlink(".") == 0){
    printf(1, "rm . worked!\n");
    120e:	c7 44 24 04 2c 3d 00 	movl   $0x3d2c,0x4(%esp)
    1215:	00 
    1216:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    121d:	e8 fe 22 00 00       	call   3520 <printf>
    exit();
    1222:	e8 c1 21 00 00       	call   33e8 <exit>
  if(mkdir("dots") != 0){
    printf(1, "mkdir dots failed\n");
    exit();
  }
  if(chdir("dots") != 0){
    printf(1, "chdir dots failed\n");
    1227:	c7 44 24 04 19 3d 00 	movl   $0x3d19,0x4(%esp)
    122e:	00 
    122f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1236:	e8 e5 22 00 00       	call   3520 <printf>
    exit();
    123b:	e8 a8 21 00 00       	call   33e8 <exit>

00001240 <subdir>:
  printf(1, "bigdir ok\n");
}

void
subdir(void)
{
    1240:	55                   	push   %ebp
    1241:	89 e5                	mov    %esp,%ebp
    1243:	53                   	push   %ebx
    1244:	83 ec 14             	sub    $0x14,%esp
  int fd, cc;

  printf(1, "subdir test\n");
    1247:	c7 44 24 04 b6 3d 00 	movl   $0x3db6,0x4(%esp)
    124e:	00 
    124f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1256:	e8 c5 22 00 00       	call   3520 <printf>

  unlink("ff");
    125b:	c7 04 24 3f 3e 00 00 	movl   $0x3e3f,(%esp)
    1262:	e8 d1 21 00 00       	call   3438 <unlink>
  if(mkdir("dd") != 0){
    1267:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    126e:	e8 dd 21 00 00       	call   3450 <mkdir>
    1273:	85 c0                	test   %eax,%eax
    1275:	0f 85 07 06 00 00    	jne    1882 <subdir+0x642>
    printf(1, "subdir mkdir dd failed\n");
    exit();
  }

  fd = open("dd/ff", O_CREATE | O_RDWR);
    127b:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    1282:	00 
    1283:	c7 04 24 15 3e 00 00 	movl   $0x3e15,(%esp)
    128a:	e8 99 21 00 00       	call   3428 <open>
  if(fd < 0){
    128f:	85 c0                	test   %eax,%eax
  if(mkdir("dd") != 0){
    printf(1, "subdir mkdir dd failed\n");
    exit();
  }

  fd = open("dd/ff", O_CREATE | O_RDWR);
    1291:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    1293:	0f 88 d0 05 00 00    	js     1869 <subdir+0x629>
    printf(1, "create dd/ff failed\n");
    exit();
  }
  write(fd, "ff", 2);
    1299:	c7 44 24 08 02 00 00 	movl   $0x2,0x8(%esp)
    12a0:	00 
    12a1:	c7 44 24 04 3f 3e 00 	movl   $0x3e3f,0x4(%esp)
    12a8:	00 
    12a9:	89 04 24             	mov    %eax,(%esp)
    12ac:	e8 57 21 00 00       	call   3408 <write>
  close(fd);
    12b1:	89 1c 24             	mov    %ebx,(%esp)
    12b4:	e8 57 21 00 00       	call   3410 <close>
  
  if(unlink("dd") >= 0){
    12b9:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    12c0:	e8 73 21 00 00       	call   3438 <unlink>
    12c5:	85 c0                	test   %eax,%eax
    12c7:	0f 89 83 05 00 00    	jns    1850 <subdir+0x610>
    printf(1, "unlink dd (non-empty dir) succeeded!\n");
    exit();
  }

  if(mkdir("/dd/dd") != 0){
    12cd:	c7 04 24 f0 3d 00 00 	movl   $0x3df0,(%esp)
    12d4:	e8 77 21 00 00       	call   3450 <mkdir>
    12d9:	85 c0                	test   %eax,%eax
    12db:	0f 85 56 05 00 00    	jne    1837 <subdir+0x5f7>
    printf(1, "subdir mkdir dd/dd failed\n");
    exit();
  }

  fd = open("dd/dd/ff", O_CREATE | O_RDWR);
    12e1:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    12e8:	00 
    12e9:	c7 04 24 12 3e 00 00 	movl   $0x3e12,(%esp)
    12f0:	e8 33 21 00 00       	call   3428 <open>
  if(fd < 0){
    12f5:	85 c0                	test   %eax,%eax
  if(mkdir("/dd/dd") != 0){
    printf(1, "subdir mkdir dd/dd failed\n");
    exit();
  }

  fd = open("dd/dd/ff", O_CREATE | O_RDWR);
    12f7:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    12f9:	0f 88 25 04 00 00    	js     1724 <subdir+0x4e4>
    printf(1, "create dd/dd/ff failed\n");
    exit();
  }
  write(fd, "FF", 2);
    12ff:	c7 44 24 08 02 00 00 	movl   $0x2,0x8(%esp)
    1306:	00 
    1307:	c7 44 24 04 33 3e 00 	movl   $0x3e33,0x4(%esp)
    130e:	00 
    130f:	89 04 24             	mov    %eax,(%esp)
    1312:	e8 f1 20 00 00       	call   3408 <write>
  close(fd);
    1317:	89 1c 24             	mov    %ebx,(%esp)
    131a:	e8 f1 20 00 00       	call   3410 <close>

  fd = open("dd/dd/../ff", 0);
    131f:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    1326:	00 
    1327:	c7 04 24 36 3e 00 00 	movl   $0x3e36,(%esp)
    132e:	e8 f5 20 00 00       	call   3428 <open>
  if(fd < 0){
    1333:	85 c0                	test   %eax,%eax
    exit();
  }
  write(fd, "FF", 2);
  close(fd);

  fd = open("dd/dd/../ff", 0);
    1335:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    1337:	0f 88 ce 03 00 00    	js     170b <subdir+0x4cb>
    printf(1, "open dd/dd/../ff failed\n");
    exit();
  }
  cc = read(fd, buf, sizeof(buf));
    133d:	c7 44 24 08 00 08 00 	movl   $0x800,0x8(%esp)
    1344:	00 
    1345:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    134c:	00 
    134d:	89 04 24             	mov    %eax,(%esp)
    1350:	e8 ab 20 00 00       	call   3400 <read>
  if(cc != 2 || buf[0] != 'f'){
    1355:	83 f8 02             	cmp    $0x2,%eax
    1358:	0f 85 fe 02 00 00    	jne    165c <subdir+0x41c>
    135e:	80 3d 20 75 00 00 66 	cmpb   $0x66,0x7520
    1365:	0f 85 f1 02 00 00    	jne    165c <subdir+0x41c>
    printf(1, "dd/dd/../ff wrong content\n");
    exit();
  }
  close(fd);
    136b:	89 1c 24             	mov    %ebx,(%esp)
    136e:	e8 9d 20 00 00       	call   3410 <close>

  if(link("dd/dd/ff", "dd/dd/ffff") != 0){
    1373:	c7 44 24 04 76 3e 00 	movl   $0x3e76,0x4(%esp)
    137a:	00 
    137b:	c7 04 24 12 3e 00 00 	movl   $0x3e12,(%esp)
    1382:	e8 c1 20 00 00       	call   3448 <link>
    1387:	85 c0                	test   %eax,%eax
    1389:	0f 85 c7 03 00 00    	jne    1756 <subdir+0x516>
    printf(1, "link dd/dd/ff dd/dd/ffff failed\n");
    exit();
  }

  if(unlink("dd/dd/ff") != 0){
    138f:	c7 04 24 12 3e 00 00 	movl   $0x3e12,(%esp)
    1396:	e8 9d 20 00 00       	call   3438 <unlink>
    139b:	85 c0                	test   %eax,%eax
    139d:	0f 85 eb 02 00 00    	jne    168e <subdir+0x44e>
    printf(1, "unlink dd/dd/ff failed\n");
    exit();
  }
  if(open("dd/dd/ff", O_RDONLY) >= 0){
    13a3:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    13aa:	00 
    13ab:	c7 04 24 12 3e 00 00 	movl   $0x3e12,(%esp)
    13b2:	e8 71 20 00 00       	call   3428 <open>
    13b7:	85 c0                	test   %eax,%eax
    13b9:	0f 89 5f 04 00 00    	jns    181e <subdir+0x5de>
    printf(1, "open (unlinked) dd/dd/ff succeeded\n");
    exit();
  }

  if(chdir("dd") != 0){
    13bf:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    13c6:	e8 8d 20 00 00       	call   3458 <chdir>
    13cb:	85 c0                	test   %eax,%eax
    13cd:	0f 85 32 04 00 00    	jne    1805 <subdir+0x5c5>
    printf(1, "chdir dd failed\n");
    exit();
  }
  if(chdir("dd/../../dd") != 0){
    13d3:	c7 04 24 aa 3e 00 00 	movl   $0x3eaa,(%esp)
    13da:	e8 79 20 00 00       	call   3458 <chdir>
    13df:	85 c0                	test   %eax,%eax
    13e1:	0f 85 8e 02 00 00    	jne    1675 <subdir+0x435>
    printf(1, "chdir dd/../../dd failed\n");
    exit();
  }
  if(chdir("dd/../../../dd") != 0){
    13e7:	c7 04 24 d0 3e 00 00 	movl   $0x3ed0,(%esp)
    13ee:	e8 65 20 00 00       	call   3458 <chdir>
    13f3:	85 c0                	test   %eax,%eax
    13f5:	0f 85 7a 02 00 00    	jne    1675 <subdir+0x435>
    printf(1, "chdir dd/../../dd failed\n");
    exit();
  }
  if(chdir("./..") != 0){
    13fb:	c7 04 24 df 3e 00 00 	movl   $0x3edf,(%esp)
    1402:	e8 51 20 00 00       	call   3458 <chdir>
    1407:	85 c0                	test   %eax,%eax
    1409:	0f 85 2e 03 00 00    	jne    173d <subdir+0x4fd>
    printf(1, "chdir ./.. failed\n");
    exit();
  }

  fd = open("dd/dd/ffff", 0);
    140f:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    1416:	00 
    1417:	c7 04 24 76 3e 00 00 	movl   $0x3e76,(%esp)
    141e:	e8 05 20 00 00       	call   3428 <open>
  if(fd < 0){
    1423:	85 c0                	test   %eax,%eax
  if(chdir("./..") != 0){
    printf(1, "chdir ./.. failed\n");
    exit();
  }

  fd = open("dd/dd/ffff", 0);
    1425:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    1427:	0f 88 81 05 00 00    	js     19ae <subdir+0x76e>
    printf(1, "open dd/dd/ffff failed\n");
    exit();
  }
  if(read(fd, buf, sizeof(buf)) != 2){
    142d:	c7 44 24 08 00 08 00 	movl   $0x800,0x8(%esp)
    1434:	00 
    1435:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    143c:	00 
    143d:	89 04 24             	mov    %eax,(%esp)
    1440:	e8 bb 1f 00 00       	call   3400 <read>
    1445:	83 f8 02             	cmp    $0x2,%eax
    1448:	0f 85 47 05 00 00    	jne    1995 <subdir+0x755>
    printf(1, "read dd/dd/ffff wrong len\n");
    exit();
  }
  close(fd);
    144e:	89 1c 24             	mov    %ebx,(%esp)
    1451:	e8 ba 1f 00 00       	call   3410 <close>

  if(open("dd/dd/ff", O_RDONLY) >= 0){
    1456:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    145d:	00 
    145e:	c7 04 24 12 3e 00 00 	movl   $0x3e12,(%esp)
    1465:	e8 be 1f 00 00       	call   3428 <open>
    146a:	85 c0                	test   %eax,%eax
    146c:	0f 89 4e 02 00 00    	jns    16c0 <subdir+0x480>
    printf(1, "open (unlinked) dd/dd/ff succeeded!\n");
    exit();
  }

  if(open("dd/ff/ff", O_CREATE|O_RDWR) >= 0){
    1472:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    1479:	00 
    147a:	c7 04 24 2a 3f 00 00 	movl   $0x3f2a,(%esp)
    1481:	e8 a2 1f 00 00       	call   3428 <open>
    1486:	85 c0                	test   %eax,%eax
    1488:	0f 89 19 02 00 00    	jns    16a7 <subdir+0x467>
    printf(1, "create dd/ff/ff succeeded!\n");
    exit();
  }
  if(open("dd/xx/ff", O_CREATE|O_RDWR) >= 0){
    148e:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    1495:	00 
    1496:	c7 04 24 4f 3f 00 00 	movl   $0x3f4f,(%esp)
    149d:	e8 86 1f 00 00       	call   3428 <open>
    14a2:	85 c0                	test   %eax,%eax
    14a4:	0f 89 42 03 00 00    	jns    17ec <subdir+0x5ac>
    printf(1, "create dd/xx/ff succeeded!\n");
    exit();
  }
  if(open("dd", O_CREATE) >= 0){
    14aa:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
    14b1:	00 
    14b2:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    14b9:	e8 6a 1f 00 00       	call   3428 <open>
    14be:	85 c0                	test   %eax,%eax
    14c0:	0f 89 0d 03 00 00    	jns    17d3 <subdir+0x593>
    printf(1, "create dd succeeded!\n");
    exit();
  }
  if(open("dd", O_RDWR) >= 0){
    14c6:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
    14cd:	00 
    14ce:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    14d5:	e8 4e 1f 00 00       	call   3428 <open>
    14da:	85 c0                	test   %eax,%eax
    14dc:	0f 89 d8 02 00 00    	jns    17ba <subdir+0x57a>
    printf(1, "open dd rdwr succeeded!\n");
    exit();
  }
  if(open("dd", O_WRONLY) >= 0){
    14e2:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
    14e9:	00 
    14ea:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    14f1:	e8 32 1f 00 00       	call   3428 <open>
    14f6:	85 c0                	test   %eax,%eax
    14f8:	0f 89 a3 02 00 00    	jns    17a1 <subdir+0x561>
    printf(1, "open dd wronly succeeded!\n");
    exit();
  }
  if(link("dd/ff/ff", "dd/dd/xx") == 0){
    14fe:	c7 44 24 04 be 3f 00 	movl   $0x3fbe,0x4(%esp)
    1505:	00 
    1506:	c7 04 24 2a 3f 00 00 	movl   $0x3f2a,(%esp)
    150d:	e8 36 1f 00 00       	call   3448 <link>
    1512:	85 c0                	test   %eax,%eax
    1514:	0f 84 6e 02 00 00    	je     1788 <subdir+0x548>
    printf(1, "link dd/ff/ff dd/dd/xx succeeded!\n");
    exit();
  }
  if(link("dd/xx/ff", "dd/dd/xx") == 0){
    151a:	c7 44 24 04 be 3f 00 	movl   $0x3fbe,0x4(%esp)
    1521:	00 
    1522:	c7 04 24 4f 3f 00 00 	movl   $0x3f4f,(%esp)
    1529:	e8 1a 1f 00 00       	call   3448 <link>
    152e:	85 c0                	test   %eax,%eax
    1530:	0f 84 39 02 00 00    	je     176f <subdir+0x52f>
    printf(1, "link dd/xx/ff dd/dd/xx succeeded!\n");
    exit();
  }
  if(link("dd/ff", "dd/dd/ffff") == 0){
    1536:	c7 44 24 04 76 3e 00 	movl   $0x3e76,0x4(%esp)
    153d:	00 
    153e:	c7 04 24 15 3e 00 00 	movl   $0x3e15,(%esp)
    1545:	e8 fe 1e 00 00       	call   3448 <link>
    154a:	85 c0                	test   %eax,%eax
    154c:	0f 84 a0 01 00 00    	je     16f2 <subdir+0x4b2>
    printf(1, "link dd/ff dd/dd/ffff succeeded!\n");
    exit();
  }
  if(mkdir("dd/ff/ff") == 0){
    1552:	c7 04 24 2a 3f 00 00 	movl   $0x3f2a,(%esp)
    1559:	e8 f2 1e 00 00       	call   3450 <mkdir>
    155e:	85 c0                	test   %eax,%eax
    1560:	0f 84 73 01 00 00    	je     16d9 <subdir+0x499>
    printf(1, "mkdir dd/ff/ff succeeded!\n");
    exit();
  }
  if(mkdir("dd/xx/ff") == 0){
    1566:	c7 04 24 4f 3f 00 00 	movl   $0x3f4f,(%esp)
    156d:	e8 de 1e 00 00       	call   3450 <mkdir>
    1572:	85 c0                	test   %eax,%eax
    1574:	0f 84 02 04 00 00    	je     197c <subdir+0x73c>
    printf(1, "mkdir dd/xx/ff succeeded!\n");
    exit();
  }
  if(mkdir("dd/dd/ffff") == 0){
    157a:	c7 04 24 76 3e 00 00 	movl   $0x3e76,(%esp)
    1581:	e8 ca 1e 00 00       	call   3450 <mkdir>
    1586:	85 c0                	test   %eax,%eax
    1588:	0f 84 d5 03 00 00    	je     1963 <subdir+0x723>
    printf(1, "mkdir dd/dd/ffff succeeded!\n");
    exit();
  }
  if(unlink("dd/xx/ff") == 0){
    158e:	c7 04 24 4f 3f 00 00 	movl   $0x3f4f,(%esp)
    1595:	e8 9e 1e 00 00       	call   3438 <unlink>
    159a:	85 c0                	test   %eax,%eax
    159c:	0f 84 a8 03 00 00    	je     194a <subdir+0x70a>
    printf(1, "unlink dd/xx/ff succeeded!\n");
    exit();
  }
  if(unlink("dd/ff/ff") == 0){
    15a2:	c7 04 24 2a 3f 00 00 	movl   $0x3f2a,(%esp)
    15a9:	e8 8a 1e 00 00       	call   3438 <unlink>
    15ae:	85 c0                	test   %eax,%eax
    15b0:	0f 84 7b 03 00 00    	je     1931 <subdir+0x6f1>
    printf(1, "unlink dd/ff/ff succeeded!\n");
    exit();
  }
  if(chdir("dd/ff") == 0){
    15b6:	c7 04 24 15 3e 00 00 	movl   $0x3e15,(%esp)
    15bd:	e8 96 1e 00 00       	call   3458 <chdir>
    15c2:	85 c0                	test   %eax,%eax
    15c4:	0f 84 4e 03 00 00    	je     1918 <subdir+0x6d8>
    printf(1, "chdir dd/ff succeeded!\n");
    exit();
  }
  if(chdir("dd/xx") == 0){
    15ca:	c7 04 24 c1 3f 00 00 	movl   $0x3fc1,(%esp)
    15d1:	e8 82 1e 00 00       	call   3458 <chdir>
    15d6:	85 c0                	test   %eax,%eax
    15d8:	0f 84 21 03 00 00    	je     18ff <subdir+0x6bf>
    printf(1, "chdir dd/xx succeeded!\n");
    exit();
  }

  if(unlink("dd/dd/ffff") != 0){
    15de:	c7 04 24 76 3e 00 00 	movl   $0x3e76,(%esp)
    15e5:	e8 4e 1e 00 00       	call   3438 <unlink>
    15ea:	85 c0                	test   %eax,%eax
    15ec:	0f 85 9c 00 00 00    	jne    168e <subdir+0x44e>
    printf(1, "unlink dd/dd/ff failed\n");
    exit();
  }
  if(unlink("dd/ff") != 0){
    15f2:	c7 04 24 15 3e 00 00 	movl   $0x3e15,(%esp)
    15f9:	e8 3a 1e 00 00       	call   3438 <unlink>
    15fe:	85 c0                	test   %eax,%eax
    1600:	0f 85 e0 02 00 00    	jne    18e6 <subdir+0x6a6>
    printf(1, "unlink dd/ff failed\n");
    exit();
  }
  if(unlink("dd") == 0){
    1606:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    160d:	e8 26 1e 00 00       	call   3438 <unlink>
    1612:	85 c0                	test   %eax,%eax
    1614:	0f 84 b3 02 00 00    	je     18cd <subdir+0x68d>
    printf(1, "unlink non-empty dd succeeded!\n");
    exit();
  }
  if(unlink("dd/dd") < 0){
    161a:	c7 04 24 f1 3d 00 00 	movl   $0x3df1,(%esp)
    1621:	e8 12 1e 00 00       	call   3438 <unlink>
    1626:	85 c0                	test   %eax,%eax
    1628:	0f 88 86 02 00 00    	js     18b4 <subdir+0x674>
    printf(1, "unlink dd/dd failed\n");
    exit();
  }
  if(unlink("dd") < 0){
    162e:	c7 04 24 dc 3e 00 00 	movl   $0x3edc,(%esp)
    1635:	e8 fe 1d 00 00       	call   3438 <unlink>
    163a:	85 c0                	test   %eax,%eax
    163c:	0f 88 59 02 00 00    	js     189b <subdir+0x65b>
    printf(1, "unlink dd failed\n");
    exit();
  }

  printf(1, "subdir ok\n");
    1642:	c7 44 24 04 be 40 00 	movl   $0x40be,0x4(%esp)
    1649:	00 
    164a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1651:	e8 ca 1e 00 00       	call   3520 <printf>
}
    1656:	83 c4 14             	add    $0x14,%esp
    1659:	5b                   	pop    %ebx
    165a:	5d                   	pop    %ebp
    165b:	c3                   	ret    
    printf(1, "open dd/dd/../ff failed\n");
    exit();
  }
  cc = read(fd, buf, sizeof(buf));
  if(cc != 2 || buf[0] != 'f'){
    printf(1, "dd/dd/../ff wrong content\n");
    165c:	c7 44 24 04 5b 3e 00 	movl   $0x3e5b,0x4(%esp)
    1663:	00 
    1664:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    166b:	e8 b0 1e 00 00       	call   3520 <printf>
    exit();
    1670:	e8 73 1d 00 00       	call   33e8 <exit>
  if(chdir("dd/../../dd") != 0){
    printf(1, "chdir dd/../../dd failed\n");
    exit();
  }
  if(chdir("dd/../../../dd") != 0){
    printf(1, "chdir dd/../../dd failed\n");
    1675:	c7 44 24 04 b6 3e 00 	movl   $0x3eb6,0x4(%esp)
    167c:	00 
    167d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1684:	e8 97 1e 00 00       	call   3520 <printf>
    exit();
    1689:	e8 5a 1d 00 00       	call   33e8 <exit>
    printf(1, "chdir dd/xx succeeded!\n");
    exit();
  }

  if(unlink("dd/dd/ffff") != 0){
    printf(1, "unlink dd/dd/ff failed\n");
    168e:	c7 44 24 04 81 3e 00 	movl   $0x3e81,0x4(%esp)
    1695:	00 
    1696:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    169d:	e8 7e 1e 00 00       	call   3520 <printf>
    exit();
    16a2:	e8 41 1d 00 00       	call   33e8 <exit>
    printf(1, "open (unlinked) dd/dd/ff succeeded!\n");
    exit();
  }

  if(open("dd/ff/ff", O_CREATE|O_RDWR) >= 0){
    printf(1, "create dd/ff/ff succeeded!\n");
    16a7:	c7 44 24 04 33 3f 00 	movl   $0x3f33,0x4(%esp)
    16ae:	00 
    16af:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    16b6:	e8 65 1e 00 00       	call   3520 <printf>
    exit();
    16bb:	e8 28 1d 00 00       	call   33e8 <exit>
    exit();
  }
  close(fd);

  if(open("dd/dd/ff", O_RDONLY) >= 0){
    printf(1, "open (unlinked) dd/dd/ff succeeded!\n");
    16c0:	c7 44 24 04 dc 4a 00 	movl   $0x4adc,0x4(%esp)
    16c7:	00 
    16c8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    16cf:	e8 4c 1e 00 00       	call   3520 <printf>
    exit();
    16d4:	e8 0f 1d 00 00       	call   33e8 <exit>
  if(link("dd/ff", "dd/dd/ffff") == 0){
    printf(1, "link dd/ff dd/dd/ffff succeeded!\n");
    exit();
  }
  if(mkdir("dd/ff/ff") == 0){
    printf(1, "mkdir dd/ff/ff succeeded!\n");
    16d9:	c7 44 24 04 c7 3f 00 	movl   $0x3fc7,0x4(%esp)
    16e0:	00 
    16e1:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    16e8:	e8 33 1e 00 00       	call   3520 <printf>
    exit();
    16ed:	e8 f6 1c 00 00       	call   33e8 <exit>
  if(link("dd/xx/ff", "dd/dd/xx") == 0){
    printf(1, "link dd/xx/ff dd/dd/xx succeeded!\n");
    exit();
  }
  if(link("dd/ff", "dd/dd/ffff") == 0){
    printf(1, "link dd/ff dd/dd/ffff succeeded!\n");
    16f2:	c7 44 24 04 4c 4b 00 	movl   $0x4b4c,0x4(%esp)
    16f9:	00 
    16fa:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1701:	e8 1a 1e 00 00       	call   3520 <printf>
    exit();
    1706:	e8 dd 1c 00 00       	call   33e8 <exit>
  write(fd, "FF", 2);
  close(fd);

  fd = open("dd/dd/../ff", 0);
  if(fd < 0){
    printf(1, "open dd/dd/../ff failed\n");
    170b:	c7 44 24 04 42 3e 00 	movl   $0x3e42,0x4(%esp)
    1712:	00 
    1713:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    171a:	e8 01 1e 00 00       	call   3520 <printf>
    exit();
    171f:	e8 c4 1c 00 00       	call   33e8 <exit>
    exit();
  }

  fd = open("dd/dd/ff", O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "create dd/dd/ff failed\n");
    1724:	c7 44 24 04 1b 3e 00 	movl   $0x3e1b,0x4(%esp)
    172b:	00 
    172c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1733:	e8 e8 1d 00 00       	call   3520 <printf>
    exit();
    1738:	e8 ab 1c 00 00       	call   33e8 <exit>
  if(chdir("dd/../../../dd") != 0){
    printf(1, "chdir dd/../../dd failed\n");
    exit();
  }
  if(chdir("./..") != 0){
    printf(1, "chdir ./.. failed\n");
    173d:	c7 44 24 04 e4 3e 00 	movl   $0x3ee4,0x4(%esp)
    1744:	00 
    1745:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    174c:	e8 cf 1d 00 00       	call   3520 <printf>
    exit();
    1751:	e8 92 1c 00 00       	call   33e8 <exit>
    exit();
  }
  close(fd);

  if(link("dd/dd/ff", "dd/dd/ffff") != 0){
    printf(1, "link dd/dd/ff dd/dd/ffff failed\n");
    1756:	c7 44 24 04 94 4a 00 	movl   $0x4a94,0x4(%esp)
    175d:	00 
    175e:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1765:	e8 b6 1d 00 00       	call   3520 <printf>
    exit();
    176a:	e8 79 1c 00 00       	call   33e8 <exit>
  if(link("dd/ff/ff", "dd/dd/xx") == 0){
    printf(1, "link dd/ff/ff dd/dd/xx succeeded!\n");
    exit();
  }
  if(link("dd/xx/ff", "dd/dd/xx") == 0){
    printf(1, "link dd/xx/ff dd/dd/xx succeeded!\n");
    176f:	c7 44 24 04 28 4b 00 	movl   $0x4b28,0x4(%esp)
    1776:	00 
    1777:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    177e:	e8 9d 1d 00 00       	call   3520 <printf>
    exit();
    1783:	e8 60 1c 00 00       	call   33e8 <exit>
  if(open("dd", O_WRONLY) >= 0){
    printf(1, "open dd wronly succeeded!\n");
    exit();
  }
  if(link("dd/ff/ff", "dd/dd/xx") == 0){
    printf(1, "link dd/ff/ff dd/dd/xx succeeded!\n");
    1788:	c7 44 24 04 04 4b 00 	movl   $0x4b04,0x4(%esp)
    178f:	00 
    1790:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1797:	e8 84 1d 00 00       	call   3520 <printf>
    exit();
    179c:	e8 47 1c 00 00       	call   33e8 <exit>
  if(open("dd", O_RDWR) >= 0){
    printf(1, "open dd rdwr succeeded!\n");
    exit();
  }
  if(open("dd", O_WRONLY) >= 0){
    printf(1, "open dd wronly succeeded!\n");
    17a1:	c7 44 24 04 a3 3f 00 	movl   $0x3fa3,0x4(%esp)
    17a8:	00 
    17a9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    17b0:	e8 6b 1d 00 00       	call   3520 <printf>
    exit();
    17b5:	e8 2e 1c 00 00       	call   33e8 <exit>
  if(open("dd", O_CREATE) >= 0){
    printf(1, "create dd succeeded!\n");
    exit();
  }
  if(open("dd", O_RDWR) >= 0){
    printf(1, "open dd rdwr succeeded!\n");
    17ba:	c7 44 24 04 8a 3f 00 	movl   $0x3f8a,0x4(%esp)
    17c1:	00 
    17c2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    17c9:	e8 52 1d 00 00       	call   3520 <printf>
    exit();
    17ce:	e8 15 1c 00 00       	call   33e8 <exit>
  if(open("dd/xx/ff", O_CREATE|O_RDWR) >= 0){
    printf(1, "create dd/xx/ff succeeded!\n");
    exit();
  }
  if(open("dd", O_CREATE) >= 0){
    printf(1, "create dd succeeded!\n");
    17d3:	c7 44 24 04 74 3f 00 	movl   $0x3f74,0x4(%esp)
    17da:	00 
    17db:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    17e2:	e8 39 1d 00 00       	call   3520 <printf>
    exit();
    17e7:	e8 fc 1b 00 00       	call   33e8 <exit>
  if(open("dd/ff/ff", O_CREATE|O_RDWR) >= 0){
    printf(1, "create dd/ff/ff succeeded!\n");
    exit();
  }
  if(open("dd/xx/ff", O_CREATE|O_RDWR) >= 0){
    printf(1, "create dd/xx/ff succeeded!\n");
    17ec:	c7 44 24 04 58 3f 00 	movl   $0x3f58,0x4(%esp)
    17f3:	00 
    17f4:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    17fb:	e8 20 1d 00 00       	call   3520 <printf>
    exit();
    1800:	e8 e3 1b 00 00       	call   33e8 <exit>
    printf(1, "open (unlinked) dd/dd/ff succeeded\n");
    exit();
  }

  if(chdir("dd") != 0){
    printf(1, "chdir dd failed\n");
    1805:	c7 44 24 04 99 3e 00 	movl   $0x3e99,0x4(%esp)
    180c:	00 
    180d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1814:	e8 07 1d 00 00       	call   3520 <printf>
    exit();
    1819:	e8 ca 1b 00 00       	call   33e8 <exit>
  if(unlink("dd/dd/ff") != 0){
    printf(1, "unlink dd/dd/ff failed\n");
    exit();
  }
  if(open("dd/dd/ff", O_RDONLY) >= 0){
    printf(1, "open (unlinked) dd/dd/ff succeeded\n");
    181e:	c7 44 24 04 b8 4a 00 	movl   $0x4ab8,0x4(%esp)
    1825:	00 
    1826:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    182d:	e8 ee 1c 00 00       	call   3520 <printf>
    exit();
    1832:	e8 b1 1b 00 00       	call   33e8 <exit>
    printf(1, "unlink dd (non-empty dir) succeeded!\n");
    exit();
  }

  if(mkdir("/dd/dd") != 0){
    printf(1, "subdir mkdir dd/dd failed\n");
    1837:	c7 44 24 04 f7 3d 00 	movl   $0x3df7,0x4(%esp)
    183e:	00 
    183f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1846:	e8 d5 1c 00 00       	call   3520 <printf>
    exit();
    184b:	e8 98 1b 00 00       	call   33e8 <exit>
  }
  write(fd, "ff", 2);
  close(fd);
  
  if(unlink("dd") >= 0){
    printf(1, "unlink dd (non-empty dir) succeeded!\n");
    1850:	c7 44 24 04 6c 4a 00 	movl   $0x4a6c,0x4(%esp)
    1857:	00 
    1858:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    185f:	e8 bc 1c 00 00       	call   3520 <printf>
    exit();
    1864:	e8 7f 1b 00 00       	call   33e8 <exit>
    exit();
  }

  fd = open("dd/ff", O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "create dd/ff failed\n");
    1869:	c7 44 24 04 db 3d 00 	movl   $0x3ddb,0x4(%esp)
    1870:	00 
    1871:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1878:	e8 a3 1c 00 00       	call   3520 <printf>
    exit();
    187d:	e8 66 1b 00 00       	call   33e8 <exit>

  printf(1, "subdir test\n");

  unlink("ff");
  if(mkdir("dd") != 0){
    printf(1, "subdir mkdir dd failed\n");
    1882:	c7 44 24 04 c3 3d 00 	movl   $0x3dc3,0x4(%esp)
    1889:	00 
    188a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1891:	e8 8a 1c 00 00       	call   3520 <printf>
    exit();
    1896:	e8 4d 1b 00 00       	call   33e8 <exit>
  if(unlink("dd/dd") < 0){
    printf(1, "unlink dd/dd failed\n");
    exit();
  }
  if(unlink("dd") < 0){
    printf(1, "unlink dd failed\n");
    189b:	c7 44 24 04 ac 40 00 	movl   $0x40ac,0x4(%esp)
    18a2:	00 
    18a3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    18aa:	e8 71 1c 00 00       	call   3520 <printf>
    exit();
    18af:	e8 34 1b 00 00       	call   33e8 <exit>
  if(unlink("dd") == 0){
    printf(1, "unlink non-empty dd succeeded!\n");
    exit();
  }
  if(unlink("dd/dd") < 0){
    printf(1, "unlink dd/dd failed\n");
    18b4:	c7 44 24 04 97 40 00 	movl   $0x4097,0x4(%esp)
    18bb:	00 
    18bc:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    18c3:	e8 58 1c 00 00       	call   3520 <printf>
    exit();
    18c8:	e8 1b 1b 00 00       	call   33e8 <exit>
  if(unlink("dd/ff") != 0){
    printf(1, "unlink dd/ff failed\n");
    exit();
  }
  if(unlink("dd") == 0){
    printf(1, "unlink non-empty dd succeeded!\n");
    18cd:	c7 44 24 04 70 4b 00 	movl   $0x4b70,0x4(%esp)
    18d4:	00 
    18d5:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    18dc:	e8 3f 1c 00 00       	call   3520 <printf>
    exit();
    18e1:	e8 02 1b 00 00       	call   33e8 <exit>
  if(unlink("dd/dd/ffff") != 0){
    printf(1, "unlink dd/dd/ff failed\n");
    exit();
  }
  if(unlink("dd/ff") != 0){
    printf(1, "unlink dd/ff failed\n");
    18e6:	c7 44 24 04 82 40 00 	movl   $0x4082,0x4(%esp)
    18ed:	00 
    18ee:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    18f5:	e8 26 1c 00 00       	call   3520 <printf>
    exit();
    18fa:	e8 e9 1a 00 00       	call   33e8 <exit>
  if(chdir("dd/ff") == 0){
    printf(1, "chdir dd/ff succeeded!\n");
    exit();
  }
  if(chdir("dd/xx") == 0){
    printf(1, "chdir dd/xx succeeded!\n");
    18ff:	c7 44 24 04 6a 40 00 	movl   $0x406a,0x4(%esp)
    1906:	00 
    1907:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    190e:	e8 0d 1c 00 00       	call   3520 <printf>
    exit();
    1913:	e8 d0 1a 00 00       	call   33e8 <exit>
  if(unlink("dd/ff/ff") == 0){
    printf(1, "unlink dd/ff/ff succeeded!\n");
    exit();
  }
  if(chdir("dd/ff") == 0){
    printf(1, "chdir dd/ff succeeded!\n");
    1918:	c7 44 24 04 52 40 00 	movl   $0x4052,0x4(%esp)
    191f:	00 
    1920:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1927:	e8 f4 1b 00 00       	call   3520 <printf>
    exit();
    192c:	e8 b7 1a 00 00       	call   33e8 <exit>
  if(unlink("dd/xx/ff") == 0){
    printf(1, "unlink dd/xx/ff succeeded!\n");
    exit();
  }
  if(unlink("dd/ff/ff") == 0){
    printf(1, "unlink dd/ff/ff succeeded!\n");
    1931:	c7 44 24 04 36 40 00 	movl   $0x4036,0x4(%esp)
    1938:	00 
    1939:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1940:	e8 db 1b 00 00       	call   3520 <printf>
    exit();
    1945:	e8 9e 1a 00 00       	call   33e8 <exit>
  if(mkdir("dd/dd/ffff") == 0){
    printf(1, "mkdir dd/dd/ffff succeeded!\n");
    exit();
  }
  if(unlink("dd/xx/ff") == 0){
    printf(1, "unlink dd/xx/ff succeeded!\n");
    194a:	c7 44 24 04 1a 40 00 	movl   $0x401a,0x4(%esp)
    1951:	00 
    1952:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1959:	e8 c2 1b 00 00       	call   3520 <printf>
    exit();
    195e:	e8 85 1a 00 00       	call   33e8 <exit>
  if(mkdir("dd/xx/ff") == 0){
    printf(1, "mkdir dd/xx/ff succeeded!\n");
    exit();
  }
  if(mkdir("dd/dd/ffff") == 0){
    printf(1, "mkdir dd/dd/ffff succeeded!\n");
    1963:	c7 44 24 04 fd 3f 00 	movl   $0x3ffd,0x4(%esp)
    196a:	00 
    196b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1972:	e8 a9 1b 00 00       	call   3520 <printf>
    exit();
    1977:	e8 6c 1a 00 00       	call   33e8 <exit>
  if(mkdir("dd/ff/ff") == 0){
    printf(1, "mkdir dd/ff/ff succeeded!\n");
    exit();
  }
  if(mkdir("dd/xx/ff") == 0){
    printf(1, "mkdir dd/xx/ff succeeded!\n");
    197c:	c7 44 24 04 e2 3f 00 	movl   $0x3fe2,0x4(%esp)
    1983:	00 
    1984:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    198b:	e8 90 1b 00 00       	call   3520 <printf>
    exit();
    1990:	e8 53 1a 00 00       	call   33e8 <exit>
  if(fd < 0){
    printf(1, "open dd/dd/ffff failed\n");
    exit();
  }
  if(read(fd, buf, sizeof(buf)) != 2){
    printf(1, "read dd/dd/ffff wrong len\n");
    1995:	c7 44 24 04 0f 3f 00 	movl   $0x3f0f,0x4(%esp)
    199c:	00 
    199d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    19a4:	e8 77 1b 00 00       	call   3520 <printf>
    exit();
    19a9:	e8 3a 1a 00 00       	call   33e8 <exit>
    exit();
  }

  fd = open("dd/dd/ffff", 0);
  if(fd < 0){
    printf(1, "open dd/dd/ffff failed\n");
    19ae:	c7 44 24 04 f7 3e 00 	movl   $0x3ef7,0x4(%esp)
    19b5:	00 
    19b6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    19bd:	e8 5e 1b 00 00       	call   3520 <printf>
    exit();
    19c2:	e8 21 1a 00 00       	call   33e8 <exit>
    19c7:	89 f6                	mov    %esi,%esi
    19c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

000019d0 <bigdir>:
}

// directory that uses indirect blocks
void
bigdir(void)
{
    19d0:	55                   	push   %ebp
    19d1:	89 e5                	mov    %esp,%ebp
    19d3:	56                   	push   %esi
    19d4:	53                   	push   %ebx
    19d5:	83 ec 20             	sub    $0x20,%esp
  int i, fd;
  char name[10];

  printf(1, "bigdir test\n");
    19d8:	c7 44 24 04 c9 40 00 	movl   $0x40c9,0x4(%esp)
    19df:	00 
    19e0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    19e7:	e8 34 1b 00 00       	call   3520 <printf>
  unlink("bd");
    19ec:	c7 04 24 d6 40 00 00 	movl   $0x40d6,(%esp)
    19f3:	e8 40 1a 00 00       	call   3438 <unlink>

  fd = open("bd", O_CREATE);
    19f8:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
    19ff:	00 
    1a00:	c7 04 24 d6 40 00 00 	movl   $0x40d6,(%esp)
    1a07:	e8 1c 1a 00 00       	call   3428 <open>
  if(fd < 0){
    1a0c:	85 c0                	test   %eax,%eax
    1a0e:	0f 88 e6 00 00 00    	js     1afa <bigdir+0x12a>
    printf(1, "bigdir create failed\n");
    exit();
  }
  close(fd);
    1a14:	89 04 24             	mov    %eax,(%esp)
    1a17:	31 db                	xor    %ebx,%ebx
    1a19:	e8 f2 19 00 00       	call   3410 <close>
    1a1e:	8d 75 ee             	lea    -0x12(%ebp),%esi
    1a21:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

  for(i = 0; i < 500; i++){
    name[0] = 'x';
    name[1] = '0' + (i / 64);
    1a28:	89 d8                	mov    %ebx,%eax
    1a2a:	c1 f8 06             	sar    $0x6,%eax
    1a2d:	83 c0 30             	add    $0x30,%eax
    1a30:	88 45 ef             	mov    %al,-0x11(%ebp)
    name[2] = '0' + (i % 64);
    1a33:	89 d8                	mov    %ebx,%eax
    1a35:	83 e0 3f             	and    $0x3f,%eax
    1a38:	83 c0 30             	add    $0x30,%eax
    exit();
  }
  close(fd);

  for(i = 0; i < 500; i++){
    name[0] = 'x';
    1a3b:	c6 45 ee 78          	movb   $0x78,-0x12(%ebp)
    name[1] = '0' + (i / 64);
    name[2] = '0' + (i % 64);
    1a3f:	88 45 f0             	mov    %al,-0x10(%ebp)
    name[3] = '\0';
    1a42:	c6 45 f1 00          	movb   $0x0,-0xf(%ebp)
    if(link("bd", name) != 0){
    1a46:	89 74 24 04          	mov    %esi,0x4(%esp)
    1a4a:	c7 04 24 d6 40 00 00 	movl   $0x40d6,(%esp)
    1a51:	e8 f2 19 00 00       	call   3448 <link>
    1a56:	85 c0                	test   %eax,%eax
    1a58:	75 6e                	jne    1ac8 <bigdir+0xf8>
    printf(1, "bigdir create failed\n");
    exit();
  }
  close(fd);

  for(i = 0; i < 500; i++){
    1a5a:	83 c3 01             	add    $0x1,%ebx
    1a5d:	81 fb f4 01 00 00    	cmp    $0x1f4,%ebx
    1a63:	75 c3                	jne    1a28 <bigdir+0x58>
      printf(1, "bigdir link failed\n");
      exit();
    }
  }

  unlink("bd");
    1a65:	c7 04 24 d6 40 00 00 	movl   $0x40d6,(%esp)
    1a6c:	66 31 db             	xor    %bx,%bx
    1a6f:	e8 c4 19 00 00       	call   3438 <unlink>
    1a74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(i = 0; i < 500; i++){
    name[0] = 'x';
    name[1] = '0' + (i / 64);
    1a78:	89 d8                	mov    %ebx,%eax
    1a7a:	c1 f8 06             	sar    $0x6,%eax
    1a7d:	83 c0 30             	add    $0x30,%eax
    1a80:	88 45 ef             	mov    %al,-0x11(%ebp)
    name[2] = '0' + (i % 64);
    1a83:	89 d8                	mov    %ebx,%eax
    1a85:	83 e0 3f             	and    $0x3f,%eax
    1a88:	83 c0 30             	add    $0x30,%eax
    }
  }

  unlink("bd");
  for(i = 0; i < 500; i++){
    name[0] = 'x';
    1a8b:	c6 45 ee 78          	movb   $0x78,-0x12(%ebp)
    name[1] = '0' + (i / 64);
    name[2] = '0' + (i % 64);
    1a8f:	88 45 f0             	mov    %al,-0x10(%ebp)
    name[3] = '\0';
    1a92:	c6 45 f1 00          	movb   $0x0,-0xf(%ebp)
    if(unlink(name) != 0){
    1a96:	89 34 24             	mov    %esi,(%esp)
    1a99:	e8 9a 19 00 00       	call   3438 <unlink>
    1a9e:	85 c0                	test   %eax,%eax
    1aa0:	75 3f                	jne    1ae1 <bigdir+0x111>
      exit();
    }
  }

  unlink("bd");
  for(i = 0; i < 500; i++){
    1aa2:	83 c3 01             	add    $0x1,%ebx
    1aa5:	81 fb f4 01 00 00    	cmp    $0x1f4,%ebx
    1aab:	75 cb                	jne    1a78 <bigdir+0xa8>
      printf(1, "bigdir unlink failed");
      exit();
    }
  }

  printf(1, "bigdir ok\n");
    1aad:	c7 44 24 04 18 41 00 	movl   $0x4118,0x4(%esp)
    1ab4:	00 
    1ab5:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1abc:	e8 5f 1a 00 00       	call   3520 <printf>
}
    1ac1:	83 c4 20             	add    $0x20,%esp
    1ac4:	5b                   	pop    %ebx
    1ac5:	5e                   	pop    %esi
    1ac6:	5d                   	pop    %ebp
    1ac7:	c3                   	ret    
    name[0] = 'x';
    name[1] = '0' + (i / 64);
    name[2] = '0' + (i % 64);
    name[3] = '\0';
    if(link("bd", name) != 0){
      printf(1, "bigdir link failed\n");
    1ac8:	c7 44 24 04 ef 40 00 	movl   $0x40ef,0x4(%esp)
    1acf:	00 
    1ad0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1ad7:	e8 44 1a 00 00       	call   3520 <printf>
      exit();
    1adc:	e8 07 19 00 00       	call   33e8 <exit>
    name[0] = 'x';
    name[1] = '0' + (i / 64);
    name[2] = '0' + (i % 64);
    name[3] = '\0';
    if(unlink(name) != 0){
      printf(1, "bigdir unlink failed");
    1ae1:	c7 44 24 04 03 41 00 	movl   $0x4103,0x4(%esp)
    1ae8:	00 
    1ae9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1af0:	e8 2b 1a 00 00       	call   3520 <printf>
      exit();
    1af5:	e8 ee 18 00 00       	call   33e8 <exit>
  printf(1, "bigdir test\n");
  unlink("bd");

  fd = open("bd", O_CREATE);
  if(fd < 0){
    printf(1, "bigdir create failed\n");
    1afa:	c7 44 24 04 d9 40 00 	movl   $0x40d9,0x4(%esp)
    1b01:	00 
    1b02:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1b09:	e8 12 1a 00 00       	call   3520 <printf>
    exit();
    1b0e:	e8 d5 18 00 00       	call   33e8 <exit>
    1b13:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    1b19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00001b20 <linktest>:
  printf(1, "unlinkread ok\n");
}

void
linktest(void)
{
    1b20:	55                   	push   %ebp
    1b21:	89 e5                	mov    %esp,%ebp
    1b23:	53                   	push   %ebx
    1b24:	83 ec 14             	sub    $0x14,%esp
  int fd;

  printf(1, "linktest\n");
    1b27:	c7 44 24 04 23 41 00 	movl   $0x4123,0x4(%esp)
    1b2e:	00 
    1b2f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1b36:	e8 e5 19 00 00       	call   3520 <printf>

  unlink("lf1");
    1b3b:	c7 04 24 2d 41 00 00 	movl   $0x412d,(%esp)
    1b42:	e8 f1 18 00 00       	call   3438 <unlink>
  unlink("lf2");
    1b47:	c7 04 24 31 41 00 00 	movl   $0x4131,(%esp)
    1b4e:	e8 e5 18 00 00       	call   3438 <unlink>

  fd = open("lf1", O_CREATE|O_RDWR);
    1b53:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    1b5a:	00 
    1b5b:	c7 04 24 2d 41 00 00 	movl   $0x412d,(%esp)
    1b62:	e8 c1 18 00 00       	call   3428 <open>
  if(fd < 0){
    1b67:	85 c0                	test   %eax,%eax
  printf(1, "linktest\n");

  unlink("lf1");
  unlink("lf2");

  fd = open("lf1", O_CREATE|O_RDWR);
    1b69:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    1b6b:	0f 88 26 01 00 00    	js     1c97 <linktest+0x177>
    printf(1, "create lf1 failed\n");
    exit();
  }
  if(write(fd, "hello", 5) != 5){
    1b71:	c7 44 24 08 05 00 00 	movl   $0x5,0x8(%esp)
    1b78:	00 
    1b79:	c7 44 24 04 48 41 00 	movl   $0x4148,0x4(%esp)
    1b80:	00 
    1b81:	89 04 24             	mov    %eax,(%esp)
    1b84:	e8 7f 18 00 00       	call   3408 <write>
    1b89:	83 f8 05             	cmp    $0x5,%eax
    1b8c:	0f 85 cd 01 00 00    	jne    1d5f <linktest+0x23f>
    printf(1, "write lf1 failed\n");
    exit();
  }
  close(fd);
    1b92:	89 1c 24             	mov    %ebx,(%esp)
    1b95:	e8 76 18 00 00       	call   3410 <close>

  if(link("lf1", "lf2") < 0){
    1b9a:	c7 44 24 04 31 41 00 	movl   $0x4131,0x4(%esp)
    1ba1:	00 
    1ba2:	c7 04 24 2d 41 00 00 	movl   $0x412d,(%esp)
    1ba9:	e8 9a 18 00 00       	call   3448 <link>
    1bae:	85 c0                	test   %eax,%eax
    1bb0:	0f 88 90 01 00 00    	js     1d46 <linktest+0x226>
    printf(1, "link lf1 lf2 failed\n");
    exit();
  }
  unlink("lf1");
    1bb6:	c7 04 24 2d 41 00 00 	movl   $0x412d,(%esp)
    1bbd:	e8 76 18 00 00       	call   3438 <unlink>

  if(open("lf1", 0) >= 0){
    1bc2:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    1bc9:	00 
    1bca:	c7 04 24 2d 41 00 00 	movl   $0x412d,(%esp)
    1bd1:	e8 52 18 00 00       	call   3428 <open>
    1bd6:	85 c0                	test   %eax,%eax
    1bd8:	0f 89 4f 01 00 00    	jns    1d2d <linktest+0x20d>
    printf(1, "unlinked lf1 but it is still there!\n");
    exit();
  }

  fd = open("lf2", 0);
    1bde:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    1be5:	00 
    1be6:	c7 04 24 31 41 00 00 	movl   $0x4131,(%esp)
    1bed:	e8 36 18 00 00       	call   3428 <open>
  if(fd < 0){
    1bf2:	85 c0                	test   %eax,%eax
  if(open("lf1", 0) >= 0){
    printf(1, "unlinked lf1 but it is still there!\n");
    exit();
  }

  fd = open("lf2", 0);
    1bf4:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    1bf6:	0f 88 18 01 00 00    	js     1d14 <linktest+0x1f4>
    printf(1, "open lf2 failed\n");
    exit();
  }
  if(read(fd, buf, sizeof(buf)) != 5){
    1bfc:	c7 44 24 08 00 08 00 	movl   $0x800,0x8(%esp)
    1c03:	00 
    1c04:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    1c0b:	00 
    1c0c:	89 04 24             	mov    %eax,(%esp)
    1c0f:	e8 ec 17 00 00       	call   3400 <read>
    1c14:	83 f8 05             	cmp    $0x5,%eax
    1c17:	0f 85 de 00 00 00    	jne    1cfb <linktest+0x1db>
    printf(1, "read lf2 failed\n");
    exit();
  }
  close(fd);
    1c1d:	89 1c 24             	mov    %ebx,(%esp)
    1c20:	e8 eb 17 00 00       	call   3410 <close>

  if(link("lf2", "lf2") >= 0){
    1c25:	c7 44 24 04 31 41 00 	movl   $0x4131,0x4(%esp)
    1c2c:	00 
    1c2d:	c7 04 24 31 41 00 00 	movl   $0x4131,(%esp)
    1c34:	e8 0f 18 00 00       	call   3448 <link>
    1c39:	85 c0                	test   %eax,%eax
    1c3b:	0f 89 a1 00 00 00    	jns    1ce2 <linktest+0x1c2>
    printf(1, "link lf2 lf2 succeeded! oops\n");
    exit();
  }

  unlink("lf2");
    1c41:	c7 04 24 31 41 00 00 	movl   $0x4131,(%esp)
    1c48:	e8 eb 17 00 00       	call   3438 <unlink>
  if(link("lf2", "lf1") >= 0){
    1c4d:	c7 44 24 04 2d 41 00 	movl   $0x412d,0x4(%esp)
    1c54:	00 
    1c55:	c7 04 24 31 41 00 00 	movl   $0x4131,(%esp)
    1c5c:	e8 e7 17 00 00       	call   3448 <link>
    1c61:	85 c0                	test   %eax,%eax
    1c63:	79 64                	jns    1cc9 <linktest+0x1a9>
    printf(1, "link non-existant succeeded! oops\n");
    exit();
  }

  if(link(".", "lf1") >= 0){
    1c65:	c7 44 24 04 2d 41 00 	movl   $0x412d,0x4(%esp)
    1c6c:	00 
    1c6d:	c7 04 24 e2 3e 00 00 	movl   $0x3ee2,(%esp)
    1c74:	e8 cf 17 00 00       	call   3448 <link>
    1c79:	85 c0                	test   %eax,%eax
    1c7b:	79 33                	jns    1cb0 <linktest+0x190>
    printf(1, "link . lf1 succeeded! oops\n");
    exit();
  }

  printf(1, "linktest ok\n");
    1c7d:	c7 44 24 04 d1 41 00 	movl   $0x41d1,0x4(%esp)
    1c84:	00 
    1c85:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1c8c:	e8 8f 18 00 00       	call   3520 <printf>
}
    1c91:	83 c4 14             	add    $0x14,%esp
    1c94:	5b                   	pop    %ebx
    1c95:	5d                   	pop    %ebp
    1c96:	c3                   	ret    
  unlink("lf1");
  unlink("lf2");

  fd = open("lf1", O_CREATE|O_RDWR);
  if(fd < 0){
    printf(1, "create lf1 failed\n");
    1c97:	c7 44 24 04 35 41 00 	movl   $0x4135,0x4(%esp)
    1c9e:	00 
    1c9f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1ca6:	e8 75 18 00 00       	call   3520 <printf>
    exit();
    1cab:	e8 38 17 00 00       	call   33e8 <exit>
    printf(1, "link non-existant succeeded! oops\n");
    exit();
  }

  if(link(".", "lf1") >= 0){
    printf(1, "link . lf1 succeeded! oops\n");
    1cb0:	c7 44 24 04 b5 41 00 	movl   $0x41b5,0x4(%esp)
    1cb7:	00 
    1cb8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1cbf:	e8 5c 18 00 00       	call   3520 <printf>
    exit();
    1cc4:	e8 1f 17 00 00       	call   33e8 <exit>
    exit();
  }

  unlink("lf2");
  if(link("lf2", "lf1") >= 0){
    printf(1, "link non-existant succeeded! oops\n");
    1cc9:	c7 44 24 04 b8 4b 00 	movl   $0x4bb8,0x4(%esp)
    1cd0:	00 
    1cd1:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1cd8:	e8 43 18 00 00       	call   3520 <printf>
    exit();
    1cdd:	e8 06 17 00 00       	call   33e8 <exit>
    exit();
  }
  close(fd);

  if(link("lf2", "lf2") >= 0){
    printf(1, "link lf2 lf2 succeeded! oops\n");
    1ce2:	c7 44 24 04 97 41 00 	movl   $0x4197,0x4(%esp)
    1ce9:	00 
    1cea:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1cf1:	e8 2a 18 00 00       	call   3520 <printf>
    exit();
    1cf6:	e8 ed 16 00 00       	call   33e8 <exit>
  if(fd < 0){
    printf(1, "open lf2 failed\n");
    exit();
  }
  if(read(fd, buf, sizeof(buf)) != 5){
    printf(1, "read lf2 failed\n");
    1cfb:	c7 44 24 04 86 41 00 	movl   $0x4186,0x4(%esp)
    1d02:	00 
    1d03:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1d0a:	e8 11 18 00 00       	call   3520 <printf>
    exit();
    1d0f:	e8 d4 16 00 00       	call   33e8 <exit>
    exit();
  }

  fd = open("lf2", 0);
  if(fd < 0){
    printf(1, "open lf2 failed\n");
    1d14:	c7 44 24 04 75 41 00 	movl   $0x4175,0x4(%esp)
    1d1b:	00 
    1d1c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1d23:	e8 f8 17 00 00       	call   3520 <printf>
    exit();
    1d28:	e8 bb 16 00 00       	call   33e8 <exit>
    exit();
  }
  unlink("lf1");

  if(open("lf1", 0) >= 0){
    printf(1, "unlinked lf1 but it is still there!\n");
    1d2d:	c7 44 24 04 90 4b 00 	movl   $0x4b90,0x4(%esp)
    1d34:	00 
    1d35:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1d3c:	e8 df 17 00 00       	call   3520 <printf>
    exit();
    1d41:	e8 a2 16 00 00       	call   33e8 <exit>
    exit();
  }
  close(fd);

  if(link("lf1", "lf2") < 0){
    printf(1, "link lf1 lf2 failed\n");
    1d46:	c7 44 24 04 60 41 00 	movl   $0x4160,0x4(%esp)
    1d4d:	00 
    1d4e:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1d55:	e8 c6 17 00 00       	call   3520 <printf>
    exit();
    1d5a:	e8 89 16 00 00       	call   33e8 <exit>
  if(fd < 0){
    printf(1, "create lf1 failed\n");
    exit();
  }
  if(write(fd, "hello", 5) != 5){
    printf(1, "write lf1 failed\n");
    1d5f:	c7 44 24 04 4e 41 00 	movl   $0x414e,0x4(%esp)
    1d66:	00 
    1d67:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1d6e:	e8 ad 17 00 00       	call   3520 <printf>
    exit();
    1d73:	e8 70 16 00 00       	call   33e8 <exit>
    1d78:	90                   	nop
    1d79:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00001d80 <unlinkread>:
}

// can I unlink a file and still read it?
void
unlinkread(void)
{
    1d80:	55                   	push   %ebp
    1d81:	89 e5                	mov    %esp,%ebp
    1d83:	56                   	push   %esi
    1d84:	53                   	push   %ebx
    1d85:	83 ec 10             	sub    $0x10,%esp
  int fd, fd1;

  printf(1, "unlinkread test\n");
    1d88:	c7 44 24 04 de 41 00 	movl   $0x41de,0x4(%esp)
    1d8f:	00 
    1d90:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1d97:	e8 84 17 00 00       	call   3520 <printf>
  fd = open("unlinkread", O_CREATE | O_RDWR);
    1d9c:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    1da3:	00 
    1da4:	c7 04 24 ef 41 00 00 	movl   $0x41ef,(%esp)
    1dab:	e8 78 16 00 00       	call   3428 <open>
  if(fd < 0){
    1db0:	85 c0                	test   %eax,%eax
unlinkread(void)
{
  int fd, fd1;

  printf(1, "unlinkread test\n");
  fd = open("unlinkread", O_CREATE | O_RDWR);
    1db2:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    1db4:	0f 88 fe 00 00 00    	js     1eb8 <unlinkread+0x138>
    printf(1, "create unlinkread failed\n");
    exit();
  }
  write(fd, "hello", 5);
    1dba:	c7 44 24 08 05 00 00 	movl   $0x5,0x8(%esp)
    1dc1:	00 
    1dc2:	c7 44 24 04 48 41 00 	movl   $0x4148,0x4(%esp)
    1dc9:	00 
    1dca:	89 04 24             	mov    %eax,(%esp)
    1dcd:	e8 36 16 00 00       	call   3408 <write>
  close(fd);
    1dd2:	89 1c 24             	mov    %ebx,(%esp)
    1dd5:	e8 36 16 00 00       	call   3410 <close>

  fd = open("unlinkread", O_RDWR);
    1dda:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
    1de1:	00 
    1de2:	c7 04 24 ef 41 00 00 	movl   $0x41ef,(%esp)
    1de9:	e8 3a 16 00 00       	call   3428 <open>
  if(fd < 0){
    1dee:	85 c0                	test   %eax,%eax
    exit();
  }
  write(fd, "hello", 5);
  close(fd);

  fd = open("unlinkread", O_RDWR);
    1df0:	89 c3                	mov    %eax,%ebx
  if(fd < 0){
    1df2:	0f 88 3d 01 00 00    	js     1f35 <unlinkread+0x1b5>
    printf(1, "open unlinkread failed\n");
    exit();
  }
  if(unlink("unlinkread") != 0){
    1df8:	c7 04 24 ef 41 00 00 	movl   $0x41ef,(%esp)
    1dff:	e8 34 16 00 00       	call   3438 <unlink>
    1e04:	85 c0                	test   %eax,%eax
    1e06:	0f 85 10 01 00 00    	jne    1f1c <unlinkread+0x19c>
    printf(1, "unlink unlinkread failed\n");
    exit();
  }

  fd1 = open("unlinkread", O_CREATE | O_RDWR);
    1e0c:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    1e13:	00 
    1e14:	c7 04 24 ef 41 00 00 	movl   $0x41ef,(%esp)
    1e1b:	e8 08 16 00 00       	call   3428 <open>
  write(fd1, "yyy", 3);
    1e20:	c7 44 24 08 03 00 00 	movl   $0x3,0x8(%esp)
    1e27:	00 
    1e28:	c7 44 24 04 46 42 00 	movl   $0x4246,0x4(%esp)
    1e2f:	00 
  if(unlink("unlinkread") != 0){
    printf(1, "unlink unlinkread failed\n");
    exit();
  }

  fd1 = open("unlinkread", O_CREATE | O_RDWR);
    1e30:	89 c6                	mov    %eax,%esi
  write(fd1, "yyy", 3);
    1e32:	89 04 24             	mov    %eax,(%esp)
    1e35:	e8 ce 15 00 00       	call   3408 <write>
  close(fd1);
    1e3a:	89 34 24             	mov    %esi,(%esp)
    1e3d:	e8 ce 15 00 00       	call   3410 <close>

  if(read(fd, buf, sizeof(buf)) != 5){
    1e42:	c7 44 24 08 00 08 00 	movl   $0x800,0x8(%esp)
    1e49:	00 
    1e4a:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    1e51:	00 
    1e52:	89 1c 24             	mov    %ebx,(%esp)
    1e55:	e8 a6 15 00 00       	call   3400 <read>
    1e5a:	83 f8 05             	cmp    $0x5,%eax
    1e5d:	0f 85 a0 00 00 00    	jne    1f03 <unlinkread+0x183>
    printf(1, "unlinkread read failed");
    exit();
  }
  if(buf[0] != 'h'){
    1e63:	80 3d 20 75 00 00 68 	cmpb   $0x68,0x7520
    1e6a:	75 7e                	jne    1eea <unlinkread+0x16a>
    printf(1, "unlinkread wrong data\n");
    exit();
  }
  if(write(fd, buf, 10) != 10){
    1e6c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
    1e73:	00 
    1e74:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    1e7b:	00 
    1e7c:	89 1c 24             	mov    %ebx,(%esp)
    1e7f:	e8 84 15 00 00       	call   3408 <write>
    1e84:	83 f8 0a             	cmp    $0xa,%eax
    1e87:	75 48                	jne    1ed1 <unlinkread+0x151>
    printf(1, "unlinkread write failed\n");
    exit();
  }
  close(fd);
    1e89:	89 1c 24             	mov    %ebx,(%esp)
    1e8c:	e8 7f 15 00 00       	call   3410 <close>
  unlink("unlinkread");
    1e91:	c7 04 24 ef 41 00 00 	movl   $0x41ef,(%esp)
    1e98:	e8 9b 15 00 00       	call   3438 <unlink>
  printf(1, "unlinkread ok\n");
    1e9d:	c7 44 24 04 91 42 00 	movl   $0x4291,0x4(%esp)
    1ea4:	00 
    1ea5:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1eac:	e8 6f 16 00 00       	call   3520 <printf>
}
    1eb1:	83 c4 10             	add    $0x10,%esp
    1eb4:	5b                   	pop    %ebx
    1eb5:	5e                   	pop    %esi
    1eb6:	5d                   	pop    %ebp
    1eb7:	c3                   	ret    
  int fd, fd1;

  printf(1, "unlinkread test\n");
  fd = open("unlinkread", O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "create unlinkread failed\n");
    1eb8:	c7 44 24 04 fa 41 00 	movl   $0x41fa,0x4(%esp)
    1ebf:	00 
    1ec0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1ec7:	e8 54 16 00 00       	call   3520 <printf>
    exit();
    1ecc:	e8 17 15 00 00       	call   33e8 <exit>
  if(buf[0] != 'h'){
    printf(1, "unlinkread wrong data\n");
    exit();
  }
  if(write(fd, buf, 10) != 10){
    printf(1, "unlinkread write failed\n");
    1ed1:	c7 44 24 04 78 42 00 	movl   $0x4278,0x4(%esp)
    1ed8:	00 
    1ed9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1ee0:	e8 3b 16 00 00       	call   3520 <printf>
    exit();
    1ee5:	e8 fe 14 00 00       	call   33e8 <exit>
  if(read(fd, buf, sizeof(buf)) != 5){
    printf(1, "unlinkread read failed");
    exit();
  }
  if(buf[0] != 'h'){
    printf(1, "unlinkread wrong data\n");
    1eea:	c7 44 24 04 61 42 00 	movl   $0x4261,0x4(%esp)
    1ef1:	00 
    1ef2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1ef9:	e8 22 16 00 00       	call   3520 <printf>
    exit();
    1efe:	e8 e5 14 00 00       	call   33e8 <exit>
  fd1 = open("unlinkread", O_CREATE | O_RDWR);
  write(fd1, "yyy", 3);
  close(fd1);

  if(read(fd, buf, sizeof(buf)) != 5){
    printf(1, "unlinkread read failed");
    1f03:	c7 44 24 04 4a 42 00 	movl   $0x424a,0x4(%esp)
    1f0a:	00 
    1f0b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1f12:	e8 09 16 00 00       	call   3520 <printf>
    exit();
    1f17:	e8 cc 14 00 00       	call   33e8 <exit>
  if(fd < 0){
    printf(1, "open unlinkread failed\n");
    exit();
  }
  if(unlink("unlinkread") != 0){
    printf(1, "unlink unlinkread failed\n");
    1f1c:	c7 44 24 04 2c 42 00 	movl   $0x422c,0x4(%esp)
    1f23:	00 
    1f24:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1f2b:	e8 f0 15 00 00       	call   3520 <printf>
    exit();
    1f30:	e8 b3 14 00 00       	call   33e8 <exit>
  write(fd, "hello", 5);
  close(fd);

  fd = open("unlinkread", O_RDWR);
  if(fd < 0){
    printf(1, "open unlinkread failed\n");
    1f35:	c7 44 24 04 14 42 00 	movl   $0x4214,0x4(%esp)
    1f3c:	00 
    1f3d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1f44:	e8 d7 15 00 00       	call   3520 <printf>
    exit();
    1f49:	e8 9a 14 00 00       	call   33e8 <exit>
    1f4e:	66 90                	xchg   %ax,%ax

00001f50 <createdelete>:
}

// two processes create and delete different files in same directory
void
createdelete(void)
{
    1f50:	55                   	push   %ebp
    1f51:	89 e5                	mov    %esp,%ebp
    1f53:	57                   	push   %edi
    1f54:	56                   	push   %esi
    1f55:	53                   	push   %ebx
    1f56:	83 ec 4c             	sub    $0x4c,%esp
  enum { N = 20 };
  int pid, i, fd;
  char name[32];

  printf(1, "createdelete test\n");
    1f59:	c7 44 24 04 a0 42 00 	movl   $0x42a0,0x4(%esp)
    1f60:	00 
    1f61:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1f68:	e8 b3 15 00 00       	call   3520 <printf>
  pid = fork();
    1f6d:	e8 6e 14 00 00       	call   33e0 <fork>
  if(pid < 0){
    1f72:	85 c0                	test   %eax,%eax
  enum { N = 20 };
  int pid, i, fd;
  char name[32];

  printf(1, "createdelete test\n");
  pid = fork();
    1f74:	89 45 c4             	mov    %eax,-0x3c(%ebp)
  if(pid < 0){
    1f77:	0f 88 12 02 00 00    	js     218f <createdelete+0x23f>
    printf(1, "fork failed\n");
    exit();
  }

  name[0] = pid ? 'p' : 'c';
    1f7d:	83 7d c4 01          	cmpl   $0x1,-0x3c(%ebp)
  name[2] = '\0';
    1f81:	bf 01 00 00 00       	mov    $0x1,%edi
    1f86:	c6 45 ca 00          	movb   $0x0,-0x36(%ebp)
    1f8a:	8d 75 c8             	lea    -0x38(%ebp),%esi
  if(pid < 0){
    printf(1, "fork failed\n");
    exit();
  }

  name[0] = pid ? 'p' : 'c';
    1f8d:	19 c0                	sbb    %eax,%eax
  name[2] = '\0';
    1f8f:	31 db                	xor    %ebx,%ebx
  if(pid < 0){
    printf(1, "fork failed\n");
    exit();
  }

  name[0] = pid ? 'p' : 'c';
    1f91:	83 e0 f3             	and    $0xfffffff3,%eax
    1f94:	83 c0 70             	add    $0x70,%eax
    1f97:	88 45 c8             	mov    %al,-0x38(%ebp)
    1f9a:	eb 0f                	jmp    1fab <createdelete+0x5b>
    1f9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  name[2] = '\0';
  for(i = 0; i < N; i++){
    1fa0:	83 ff 13             	cmp    $0x13,%edi
    1fa3:	7f 6b                	jg     2010 <createdelete+0xc0>
    printf(1, "fork failed\n");
    exit();
  }

  name[0] = pid ? 'p' : 'c';
  name[2] = '\0';
    1fa5:	83 c3 01             	add    $0x1,%ebx
    1fa8:	83 c7 01             	add    $0x1,%edi
  for(i = 0; i < N; i++){
    name[1] = '0' + i;
    1fab:	8d 43 30             	lea    0x30(%ebx),%eax
    1fae:	88 45 c9             	mov    %al,-0x37(%ebp)
    fd = open(name, O_CREATE | O_RDWR);
    1fb1:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    1fb8:	00 
    1fb9:	89 34 24             	mov    %esi,(%esp)
    1fbc:	e8 67 14 00 00       	call   3428 <open>
    if(fd < 0){
    1fc1:	85 c0                	test   %eax,%eax
    1fc3:	0f 88 3e 01 00 00    	js     2107 <createdelete+0x1b7>
      printf(1, "create failed\n");
      exit();
    }
    close(fd);
    1fc9:	89 04 24             	mov    %eax,(%esp)
    1fcc:	e8 3f 14 00 00       	call   3410 <close>
    if(i > 0 && (i % 2 ) == 0){
    1fd1:	85 db                	test   %ebx,%ebx
    1fd3:	74 d0                	je     1fa5 <createdelete+0x55>
    1fd5:	f6 c3 01             	test   $0x1,%bl
    1fd8:	75 c6                	jne    1fa0 <createdelete+0x50>
      name[1] = '0' + (i / 2);
    1fda:	89 d8                	mov    %ebx,%eax
    1fdc:	d1 f8                	sar    %eax
    1fde:	83 c0 30             	add    $0x30,%eax
    1fe1:	88 45 c9             	mov    %al,-0x37(%ebp)
      if(unlink(name) < 0){
    1fe4:	89 34 24             	mov    %esi,(%esp)
    1fe7:	e8 4c 14 00 00       	call   3438 <unlink>
    1fec:	85 c0                	test   %eax,%eax
    1fee:	79 b0                	jns    1fa0 <createdelete+0x50>
        printf(1, "unlink failed\n");
    1ff0:	c7 44 24 04 b3 42 00 	movl   $0x42b3,0x4(%esp)
    1ff7:	00 
    1ff8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    1fff:	e8 1c 15 00 00       	call   3520 <printf>
        exit();
    2004:	e8 df 13 00 00       	call   33e8 <exit>
    2009:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      }
    }
  }

  if(pid==0)
    2010:	8b 45 c4             	mov    -0x3c(%ebp),%eax
    2013:	85 c0                	test   %eax,%eax
    2015:	0f 84 6f 01 00 00    	je     218a <createdelete+0x23a>
    exit();
  else
    wait();
    201b:	e8 d0 13 00 00       	call   33f0 <wait>
    2020:	31 db                	xor    %ebx,%ebx
    2022:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

  for(i = 0; i < N; i++){
    name[0] = 'p';
    2028:	8d 7b 30             	lea    0x30(%ebx),%edi
    name[1] = '0' + i;
    202b:	89 f8                	mov    %edi,%eax
    exit();
  else
    wait();

  for(i = 0; i < N; i++){
    name[0] = 'p';
    202d:	c6 45 c8 70          	movb   $0x70,-0x38(%ebp)
    name[1] = '0' + i;
    2031:	88 45 c9             	mov    %al,-0x37(%ebp)
    fd = open(name, 0);
    2034:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    203b:	00 
    203c:	89 34 24             	mov    %esi,(%esp)
    203f:	e8 e4 13 00 00       	call   3428 <open>
    if((i == 0 || i >= N/2) && fd < 0){
    2044:	83 fb 09             	cmp    $0x9,%ebx
    2047:	0f 9f c1             	setg   %cl
    204a:	85 db                	test   %ebx,%ebx
    204c:	0f 94 c2             	sete   %dl
    204f:	08 d1                	or     %dl,%cl
    2051:	88 4d c3             	mov    %cl,-0x3d(%ebp)
    2054:	74 08                	je     205e <createdelete+0x10e>
    2056:	85 c0                	test   %eax,%eax
    2058:	0f 88 14 01 00 00    	js     2172 <createdelete+0x222>
      printf(1, "oops createdelete %s didn't exist\n", name);
      exit();
    } else if((i >= 1 && i < N/2) && fd >= 0){
    205e:	8d 53 ff             	lea    -0x1(%ebx),%edx
    2061:	83 fa 08             	cmp    $0x8,%edx
    2064:	89 c2                	mov    %eax,%edx
    2066:	f7 d2                	not    %edx
    2068:	0f 96 45 c4          	setbe  -0x3c(%ebp)
    206c:	c1 ea 1f             	shr    $0x1f,%edx
    206f:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
    2073:	0f 85 b7 00 00 00    	jne    2130 <createdelete+0x1e0>
      printf(1, "oops createdelete %s did exist\n", name);
      exit();
    }
    if(fd >= 0)
    2079:	84 d2                	test   %dl,%dl
    207b:	74 08                	je     2085 <createdelete+0x135>
      close(fd);
    207d:	89 04 24             	mov    %eax,(%esp)
    2080:	e8 8b 13 00 00       	call   3410 <close>

    name[0] = 'c';
    name[1] = '0' + i;
    2085:	89 f8                	mov    %edi,%eax
      exit();
    }
    if(fd >= 0)
      close(fd);

    name[0] = 'c';
    2087:	c6 45 c8 63          	movb   $0x63,-0x38(%ebp)
    name[1] = '0' + i;
    208b:	88 45 c9             	mov    %al,-0x37(%ebp)
    fd = open(name, 0);
    208e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    2095:	00 
    2096:	89 34 24             	mov    %esi,(%esp)
    2099:	e8 8a 13 00 00       	call   3428 <open>
    if((i == 0 || i >= N/2) && fd < 0){
    209e:	80 7d c3 00          	cmpb   $0x0,-0x3d(%ebp)
    20a2:	74 08                	je     20ac <createdelete+0x15c>
    20a4:	85 c0                	test   %eax,%eax
    20a6:	0f 88 a9 00 00 00    	js     2155 <createdelete+0x205>
      printf(1, "oops createdelete %s didn't exist\n", name);
      exit();
    } else if((i >= 1 && i < N/2) && fd >= 0){
    20ac:	85 c0                	test   %eax,%eax
    20ae:	66 90                	xchg   %ax,%ax
    20b0:	79 6e                	jns    2120 <createdelete+0x1d0>
  if(pid==0)
    exit();
  else
    wait();

  for(i = 0; i < N; i++){
    20b2:	83 c3 01             	add    $0x1,%ebx
    20b5:	83 fb 14             	cmp    $0x14,%ebx
    20b8:	0f 85 6a ff ff ff    	jne    2028 <createdelete+0xd8>
    20be:	bb 30 00 00 00       	mov    $0x30,%ebx
    20c3:	90                   	nop
    20c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      close(fd);
  }

  for(i = 0; i < N; i++){
    name[0] = 'p';
    name[1] = '0' + i;
    20c8:	88 5d c9             	mov    %bl,-0x37(%ebp)
    unlink(name);
    name[0] = 'c';
    unlink(name);
    20cb:	83 c3 01             	add    $0x1,%ebx
    if(fd >= 0)
      close(fd);
  }

  for(i = 0; i < N; i++){
    name[0] = 'p';
    20ce:	c6 45 c8 70          	movb   $0x70,-0x38(%ebp)
    name[1] = '0' + i;
    unlink(name);
    20d2:	89 34 24             	mov    %esi,(%esp)
    20d5:	e8 5e 13 00 00       	call   3438 <unlink>
    name[0] = 'c';
    20da:	c6 45 c8 63          	movb   $0x63,-0x38(%ebp)
    unlink(name);
    20de:	89 34 24             	mov    %esi,(%esp)
    20e1:	e8 52 13 00 00       	call   3438 <unlink>
    }
    if(fd >= 0)
      close(fd);
  }

  for(i = 0; i < N; i++){
    20e6:	80 fb 44             	cmp    $0x44,%bl
    20e9:	75 dd                	jne    20c8 <createdelete+0x178>
    unlink(name);
    name[0] = 'c';
    unlink(name);
  }

  printf(1, "createdelete ok\n");
    20eb:	c7 44 24 04 c2 42 00 	movl   $0x42c2,0x4(%esp)
    20f2:	00 
    20f3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    20fa:	e8 21 14 00 00       	call   3520 <printf>
}
    20ff:	83 c4 4c             	add    $0x4c,%esp
    2102:	5b                   	pop    %ebx
    2103:	5e                   	pop    %esi
    2104:	5f                   	pop    %edi
    2105:	5d                   	pop    %ebp
    2106:	c3                   	ret    
  name[2] = '\0';
  for(i = 0; i < N; i++){
    name[1] = '0' + i;
    fd = open(name, O_CREATE | O_RDWR);
    if(fd < 0){
      printf(1, "create failed\n");
    2107:	c7 44 24 04 e0 40 00 	movl   $0x40e0,0x4(%esp)
    210e:	00 
    210f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2116:	e8 05 14 00 00       	call   3520 <printf>
      exit();
    211b:	e8 c8 12 00 00       	call   33e8 <exit>
    name[1] = '0' + i;
    fd = open(name, 0);
    if((i == 0 || i >= N/2) && fd < 0){
      printf(1, "oops createdelete %s didn't exist\n", name);
      exit();
    } else if((i >= 1 && i < N/2) && fd >= 0){
    2120:	80 7d c4 00          	cmpb   $0x0,-0x3c(%ebp)
    2124:	75 12                	jne    2138 <createdelete+0x1e8>
      printf(1, "oops createdelete %s did exist\n", name);
      exit();
    }
    if(fd >= 0)
      close(fd);
    2126:	89 04 24             	mov    %eax,(%esp)
    2129:	e8 e2 12 00 00       	call   3410 <close>
    212e:	eb 82                	jmp    20b2 <createdelete+0x162>
    name[1] = '0' + i;
    fd = open(name, 0);
    if((i == 0 || i >= N/2) && fd < 0){
      printf(1, "oops createdelete %s didn't exist\n", name);
      exit();
    } else if((i >= 1 && i < N/2) && fd >= 0){
    2130:	84 d2                	test   %dl,%dl
    2132:	0f 84 4d ff ff ff    	je     2085 <createdelete+0x135>
    fd = open(name, 0);
    if((i == 0 || i >= N/2) && fd < 0){
      printf(1, "oops createdelete %s didn't exist\n", name);
      exit();
    } else if((i >= 1 && i < N/2) && fd >= 0){
      printf(1, "oops createdelete %s did exist\n", name);
    2138:	89 74 24 08          	mov    %esi,0x8(%esp)
    213c:	c7 44 24 04 00 4c 00 	movl   $0x4c00,0x4(%esp)
    2143:	00 
    2144:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    214b:	e8 d0 13 00 00       	call   3520 <printf>
      exit();
    2150:	e8 93 12 00 00       	call   33e8 <exit>

    name[0] = 'c';
    name[1] = '0' + i;
    fd = open(name, 0);
    if((i == 0 || i >= N/2) && fd < 0){
      printf(1, "oops createdelete %s didn't exist\n", name);
    2155:	89 74 24 08          	mov    %esi,0x8(%esp)
    2159:	c7 44 24 04 dc 4b 00 	movl   $0x4bdc,0x4(%esp)
    2160:	00 
    2161:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2168:	e8 b3 13 00 00       	call   3520 <printf>
      exit();
    216d:	e8 76 12 00 00       	call   33e8 <exit>
  for(i = 0; i < N; i++){
    name[0] = 'p';
    name[1] = '0' + i;
    fd = open(name, 0);
    if((i == 0 || i >= N/2) && fd < 0){
      printf(1, "oops createdelete %s didn't exist\n", name);
    2172:	89 74 24 08          	mov    %esi,0x8(%esp)
    2176:	c7 44 24 04 dc 4b 00 	movl   $0x4bdc,0x4(%esp)
    217d:	00 
    217e:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2185:	e8 96 13 00 00       	call   3520 <printf>
      exit();
    218a:	e8 59 12 00 00       	call   33e8 <exit>
  char name[32];

  printf(1, "createdelete test\n");
  pid = fork();
  if(pid < 0){
    printf(1, "fork failed\n");
    218f:	c7 44 24 04 52 39 00 	movl   $0x3952,0x4(%esp)
    2196:	00 
    2197:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    219e:	e8 7d 13 00 00       	call   3520 <printf>
    exit();
    21a3:	e8 40 12 00 00       	call   33e8 <exit>
    21a8:	90                   	nop
    21a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

000021b0 <dirtest>:
  }
  printf(stdout, "many creates, followed by unlink; ok\n");
}

void dirtest(void)
{
    21b0:	55                   	push   %ebp
    21b1:	89 e5                	mov    %esp,%ebp
    21b3:	83 ec 18             	sub    $0x18,%esp
  printf(stdout, "mkdir test\n");
    21b6:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    21bb:	c7 44 24 04 d3 42 00 	movl   $0x42d3,0x4(%esp)
    21c2:	00 
    21c3:	89 04 24             	mov    %eax,(%esp)
    21c6:	e8 55 13 00 00       	call   3520 <printf>

  if(mkdir("dir0") < 0){
    21cb:	c7 04 24 df 42 00 00 	movl   $0x42df,(%esp)
    21d2:	e8 79 12 00 00       	call   3450 <mkdir>
    21d7:	85 c0                	test   %eax,%eax
    21d9:	78 4b                	js     2226 <dirtest+0x76>
    printf(stdout, "mkdir failed\n");
    exit();
  }

  if(chdir("dir0") < 0){
    21db:	c7 04 24 df 42 00 00 	movl   $0x42df,(%esp)
    21e2:	e8 71 12 00 00       	call   3458 <chdir>
    21e7:	85 c0                	test   %eax,%eax
    21e9:	0f 88 85 00 00 00    	js     2274 <dirtest+0xc4>
    printf(stdout, "chdir dir0 failed\n");
    exit();
  }

  if(chdir("..") < 0){
    21ef:	c7 04 24 e1 3e 00 00 	movl   $0x3ee1,(%esp)
    21f6:	e8 5d 12 00 00       	call   3458 <chdir>
    21fb:	85 c0                	test   %eax,%eax
    21fd:	78 5b                	js     225a <dirtest+0xaa>
    printf(stdout, "chdir .. failed\n");
    exit();
  }

  if(unlink("dir0") < 0){
    21ff:	c7 04 24 df 42 00 00 	movl   $0x42df,(%esp)
    2206:	e8 2d 12 00 00       	call   3438 <unlink>
    220b:	85 c0                	test   %eax,%eax
    220d:	78 31                	js     2240 <dirtest+0x90>
    printf(stdout, "unlink dir0 failed\n");
    exit();
  }
  printf(stdout, "mkdir test\n");
    220f:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2214:	c7 44 24 04 d3 42 00 	movl   $0x42d3,0x4(%esp)
    221b:	00 
    221c:	89 04 24             	mov    %eax,(%esp)
    221f:	e8 fc 12 00 00       	call   3520 <printf>
}
    2224:	c9                   	leave  
    2225:	c3                   	ret    
void dirtest(void)
{
  printf(stdout, "mkdir test\n");

  if(mkdir("dir0") < 0){
    printf(stdout, "mkdir failed\n");
    2226:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    222b:	c7 44 24 04 e4 42 00 	movl   $0x42e4,0x4(%esp)
    2232:	00 
    2233:	89 04 24             	mov    %eax,(%esp)
    2236:	e8 e5 12 00 00       	call   3520 <printf>
    exit();
    223b:	e8 a8 11 00 00       	call   33e8 <exit>
    printf(stdout, "chdir .. failed\n");
    exit();
  }

  if(unlink("dir0") < 0){
    printf(stdout, "unlink dir0 failed\n");
    2240:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2245:	c7 44 24 04 16 43 00 	movl   $0x4316,0x4(%esp)
    224c:	00 
    224d:	89 04 24             	mov    %eax,(%esp)
    2250:	e8 cb 12 00 00       	call   3520 <printf>
    exit();
    2255:	e8 8e 11 00 00       	call   33e8 <exit>
    printf(stdout, "chdir dir0 failed\n");
    exit();
  }

  if(chdir("..") < 0){
    printf(stdout, "chdir .. failed\n");
    225a:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    225f:	c7 44 24 04 05 43 00 	movl   $0x4305,0x4(%esp)
    2266:	00 
    2267:	89 04 24             	mov    %eax,(%esp)
    226a:	e8 b1 12 00 00       	call   3520 <printf>
    exit();
    226f:	e8 74 11 00 00       	call   33e8 <exit>
    printf(stdout, "mkdir failed\n");
    exit();
  }

  if(chdir("dir0") < 0){
    printf(stdout, "chdir dir0 failed\n");
    2274:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2279:	c7 44 24 04 f2 42 00 	movl   $0x42f2,0x4(%esp)
    2280:	00 
    2281:	89 04 24             	mov    %eax,(%esp)
    2284:	e8 97 12 00 00       	call   3520 <printf>
    exit();
    2289:	e8 5a 11 00 00       	call   33e8 <exit>
    228e:	66 90                	xchg   %ax,%ax

00002290 <createtest>:
  printf(stdout, "big files ok\n");
}

void
createtest(void)
{
    2290:	55                   	push   %ebp
    2291:	89 e5                	mov    %esp,%ebp
    2293:	53                   	push   %ebx
  int i, fd;

  printf(stdout, "many creates, followed by unlink test\n");

  name[0] = 'a';
  name[2] = '\0';
    2294:	bb 30 00 00 00       	mov    $0x30,%ebx
  printf(stdout, "big files ok\n");
}

void
createtest(void)
{
    2299:	83 ec 14             	sub    $0x14,%esp
  int i, fd;

  printf(stdout, "many creates, followed by unlink test\n");
    229c:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    22a1:	c7 44 24 04 20 4c 00 	movl   $0x4c20,0x4(%esp)
    22a8:	00 
    22a9:	89 04 24             	mov    %eax,(%esp)
    22ac:	e8 6f 12 00 00       	call   3520 <printf>

  name[0] = 'a';
    22b1:	c6 05 20 7d 00 00 61 	movb   $0x61,0x7d20
  name[2] = '\0';
    22b8:	c6 05 22 7d 00 00 00 	movb   $0x0,0x7d22
    22bf:	90                   	nop
  for(i = 0; i < 52; i++){
    name[1] = '0' + i;
    22c0:	88 1d 21 7d 00 00    	mov    %bl,0x7d21
    fd = open(name, O_CREATE|O_RDWR);
    close(fd);
    22c6:	83 c3 01             	add    $0x1,%ebx

  name[0] = 'a';
  name[2] = '\0';
  for(i = 0; i < 52; i++){
    name[1] = '0' + i;
    fd = open(name, O_CREATE|O_RDWR);
    22c9:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    22d0:	00 
    22d1:	c7 04 24 20 7d 00 00 	movl   $0x7d20,(%esp)
    22d8:	e8 4b 11 00 00       	call   3428 <open>
    close(fd);
    22dd:	89 04 24             	mov    %eax,(%esp)
    22e0:	e8 2b 11 00 00       	call   3410 <close>

  printf(stdout, "many creates, followed by unlink test\n");

  name[0] = 'a';
  name[2] = '\0';
  for(i = 0; i < 52; i++){
    22e5:	80 fb 64             	cmp    $0x64,%bl
    22e8:	75 d6                	jne    22c0 <createtest+0x30>
    name[1] = '0' + i;
    fd = open(name, O_CREATE|O_RDWR);
    close(fd);
  }
  name[0] = 'a';
    22ea:	c6 05 20 7d 00 00 61 	movb   $0x61,0x7d20
  name[2] = '\0';
    22f1:	bb 30 00 00 00       	mov    $0x30,%ebx
    22f6:	c6 05 22 7d 00 00 00 	movb   $0x0,0x7d22
    22fd:	8d 76 00             	lea    0x0(%esi),%esi
  for(i = 0; i < 52; i++){
    name[1] = '0' + i;
    2300:	88 1d 21 7d 00 00    	mov    %bl,0x7d21
    unlink(name);
    2306:	83 c3 01             	add    $0x1,%ebx
    2309:	c7 04 24 20 7d 00 00 	movl   $0x7d20,(%esp)
    2310:	e8 23 11 00 00       	call   3438 <unlink>
    fd = open(name, O_CREATE|O_RDWR);
    close(fd);
  }
  name[0] = 'a';
  name[2] = '\0';
  for(i = 0; i < 52; i++){
    2315:	80 fb 64             	cmp    $0x64,%bl
    2318:	75 e6                	jne    2300 <createtest+0x70>
    name[1] = '0' + i;
    unlink(name);
  }
  printf(stdout, "many creates, followed by unlink; ok\n");
    231a:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    231f:	c7 44 24 04 48 4c 00 	movl   $0x4c48,0x4(%esp)
    2326:	00 
    2327:	89 04 24             	mov    %eax,(%esp)
    232a:	e8 f1 11 00 00       	call   3520 <printf>
}
    232f:	83 c4 14             	add    $0x14,%esp
    2332:	5b                   	pop    %ebx
    2333:	5d                   	pop    %ebp
    2334:	c3                   	ret    
    2335:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    2339:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00002340 <writetest1>:
  printf(stdout, "small file test ok\n");
}

void
writetest1(void)
{
    2340:	55                   	push   %ebp
    2341:	89 e5                	mov    %esp,%ebp
    2343:	56                   	push   %esi
    2344:	53                   	push   %ebx
    2345:	83 ec 10             	sub    $0x10,%esp
  int i, fd, n;

  printf(stdout, "big files test\n");
    2348:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    234d:	c7 44 24 04 2a 43 00 	movl   $0x432a,0x4(%esp)
    2354:	00 
    2355:	89 04 24             	mov    %eax,(%esp)
    2358:	e8 c3 11 00 00       	call   3520 <printf>

  fd = open("big", O_CREATE|O_RDWR);
    235d:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    2364:	00 
    2365:	c7 04 24 a4 43 00 00 	movl   $0x43a4,(%esp)
    236c:	e8 b7 10 00 00       	call   3428 <open>
  if(fd < 0){
    2371:	85 c0                	test   %eax,%eax
{
  int i, fd, n;

  printf(stdout, "big files test\n");

  fd = open("big", O_CREATE|O_RDWR);
    2373:	89 c6                	mov    %eax,%esi
  if(fd < 0){
    2375:	0f 88 7a 01 00 00    	js     24f5 <writetest1+0x1b5>
    printf(stdout, "error: creat big failed!\n");
    exit();
    237b:	31 db                	xor    %ebx,%ebx
    237d:	8d 76 00             	lea    0x0(%esi),%esi
  }

  for(i = 0; i < MAXFILE; i++){
    ((int*)buf)[0] = i;
    2380:	89 1d 20 75 00 00    	mov    %ebx,0x7520
    if(write(fd, buf, 512) != 512){
    2386:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
    238d:	00 
    238e:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    2395:	00 
    2396:	89 34 24             	mov    %esi,(%esp)
    2399:	e8 6a 10 00 00       	call   3408 <write>
    239e:	3d 00 02 00 00       	cmp    $0x200,%eax
    23a3:	0f 85 b2 00 00 00    	jne    245b <writetest1+0x11b>
  if(fd < 0){
    printf(stdout, "error: creat big failed!\n");
    exit();
  }

  for(i = 0; i < MAXFILE; i++){
    23a9:	83 c3 01             	add    $0x1,%ebx
    23ac:	81 fb 8c 00 00 00    	cmp    $0x8c,%ebx
    23b2:	75 cc                	jne    2380 <writetest1+0x40>
      printf(stdout, "error: write big file failed\n", i);
      exit();
    }
  }

  close(fd);
    23b4:	89 34 24             	mov    %esi,(%esp)
    23b7:	e8 54 10 00 00       	call   3410 <close>

  fd = open("big", O_RDONLY);
    23bc:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    23c3:	00 
    23c4:	c7 04 24 a4 43 00 00 	movl   $0x43a4,(%esp)
    23cb:	e8 58 10 00 00       	call   3428 <open>
  if(fd < 0){
    23d0:	85 c0                	test   %eax,%eax
    }
  }

  close(fd);

  fd = open("big", O_RDONLY);
    23d2:	89 c6                	mov    %eax,%esi
  if(fd < 0){
    23d4:	0f 88 01 01 00 00    	js     24db <writetest1+0x19b>
    printf(stdout, "error: open big failed!\n");
    exit();
    23da:	31 db                	xor    %ebx,%ebx
    23dc:	eb 1d                	jmp    23fb <writetest1+0xbb>
    23de:	66 90                	xchg   %ax,%ax
      if(n == MAXFILE - 1){
        printf(stdout, "read only %d blocks from big", n);
        exit();
      }
      break;
    } else if(i != 512){
    23e0:	3d 00 02 00 00       	cmp    $0x200,%eax
    23e5:	0f 85 b0 00 00 00    	jne    249b <writetest1+0x15b>
      printf(stdout, "read failed %d\n", i);
      exit();
    }
    if(((int*)buf)[0] != n){
    23eb:	a1 20 75 00 00       	mov    0x7520,%eax
    23f0:	39 d8                	cmp    %ebx,%eax
    23f2:	0f 85 81 00 00 00    	jne    2479 <writetest1+0x139>
      printf(stdout, "read content of block %d is %d\n",
             n, ((int*)buf)[0]);
      exit();
    }
    n++;
    23f8:	83 c3 01             	add    $0x1,%ebx
    exit();
  }

  n = 0;
  for(;;){
    i = read(fd, buf, 512);
    23fb:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
    2402:	00 
    2403:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    240a:	00 
    240b:	89 34 24             	mov    %esi,(%esp)
    240e:	e8 ed 0f 00 00       	call   3400 <read>
    if(i == 0){
    2413:	85 c0                	test   %eax,%eax
    2415:	75 c9                	jne    23e0 <writetest1+0xa0>
      if(n == MAXFILE - 1){
    2417:	81 fb 8b 00 00 00    	cmp    $0x8b,%ebx
    241d:	0f 84 96 00 00 00    	je     24b9 <writetest1+0x179>
             n, ((int*)buf)[0]);
      exit();
    }
    n++;
  }
  close(fd);
    2423:	89 34 24             	mov    %esi,(%esp)
    2426:	e8 e5 0f 00 00       	call   3410 <close>
  if(unlink("big") < 0){
    242b:	c7 04 24 a4 43 00 00 	movl   $0x43a4,(%esp)
    2432:	e8 01 10 00 00       	call   3438 <unlink>
    2437:	85 c0                	test   %eax,%eax
    2439:	0f 88 d0 00 00 00    	js     250f <writetest1+0x1cf>
    printf(stdout, "unlink big failed\n");
    exit();
  }
  printf(stdout, "big files ok\n");
    243f:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2444:	c7 44 24 04 cb 43 00 	movl   $0x43cb,0x4(%esp)
    244b:	00 
    244c:	89 04 24             	mov    %eax,(%esp)
    244f:	e8 cc 10 00 00       	call   3520 <printf>
}
    2454:	83 c4 10             	add    $0x10,%esp
    2457:	5b                   	pop    %ebx
    2458:	5e                   	pop    %esi
    2459:	5d                   	pop    %ebp
    245a:	c3                   	ret    
  }

  for(i = 0; i < MAXFILE; i++){
    ((int*)buf)[0] = i;
    if(write(fd, buf, 512) != 512){
      printf(stdout, "error: write big file failed\n", i);
    245b:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2460:	89 5c 24 08          	mov    %ebx,0x8(%esp)
    2464:	c7 44 24 04 54 43 00 	movl   $0x4354,0x4(%esp)
    246b:	00 
    246c:	89 04 24             	mov    %eax,(%esp)
    246f:	e8 ac 10 00 00       	call   3520 <printf>
      exit();
    2474:	e8 6f 0f 00 00       	call   33e8 <exit>
    } else if(i != 512){
      printf(stdout, "read failed %d\n", i);
      exit();
    }
    if(((int*)buf)[0] != n){
      printf(stdout, "read content of block %d is %d\n",
    2479:	89 44 24 0c          	mov    %eax,0xc(%esp)
    247d:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2482:	89 5c 24 08          	mov    %ebx,0x8(%esp)
    2486:	c7 44 24 04 70 4c 00 	movl   $0x4c70,0x4(%esp)
    248d:	00 
    248e:	89 04 24             	mov    %eax,(%esp)
    2491:	e8 8a 10 00 00       	call   3520 <printf>
             n, ((int*)buf)[0]);
      exit();
    2496:	e8 4d 0f 00 00       	call   33e8 <exit>
        printf(stdout, "read only %d blocks from big", n);
        exit();
      }
      break;
    } else if(i != 512){
      printf(stdout, "read failed %d\n", i);
    249b:	89 44 24 08          	mov    %eax,0x8(%esp)
    249f:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    24a4:	c7 44 24 04 a8 43 00 	movl   $0x43a8,0x4(%esp)
    24ab:	00 
    24ac:	89 04 24             	mov    %eax,(%esp)
    24af:	e8 6c 10 00 00       	call   3520 <printf>
      exit();
    24b4:	e8 2f 0f 00 00       	call   33e8 <exit>
  n = 0;
  for(;;){
    i = read(fd, buf, 512);
    if(i == 0){
      if(n == MAXFILE - 1){
        printf(stdout, "read only %d blocks from big", n);
    24b9:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    24be:	c7 44 24 08 8b 00 00 	movl   $0x8b,0x8(%esp)
    24c5:	00 
    24c6:	c7 44 24 04 8b 43 00 	movl   $0x438b,0x4(%esp)
    24cd:	00 
    24ce:	89 04 24             	mov    %eax,(%esp)
    24d1:	e8 4a 10 00 00       	call   3520 <printf>
        exit();
    24d6:	e8 0d 0f 00 00       	call   33e8 <exit>

  close(fd);

  fd = open("big", O_RDONLY);
  if(fd < 0){
    printf(stdout, "error: open big failed!\n");
    24db:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    24e0:	c7 44 24 04 72 43 00 	movl   $0x4372,0x4(%esp)
    24e7:	00 
    24e8:	89 04 24             	mov    %eax,(%esp)
    24eb:	e8 30 10 00 00       	call   3520 <printf>
    exit();
    24f0:	e8 f3 0e 00 00       	call   33e8 <exit>

  printf(stdout, "big files test\n");

  fd = open("big", O_CREATE|O_RDWR);
  if(fd < 0){
    printf(stdout, "error: creat big failed!\n");
    24f5:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    24fa:	c7 44 24 04 3a 43 00 	movl   $0x433a,0x4(%esp)
    2501:	00 
    2502:	89 04 24             	mov    %eax,(%esp)
    2505:	e8 16 10 00 00       	call   3520 <printf>
    exit();
    250a:	e8 d9 0e 00 00       	call   33e8 <exit>
    }
    n++;
  }
  close(fd);
  if(unlink("big") < 0){
    printf(stdout, "unlink big failed\n");
    250f:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2514:	c7 44 24 04 b8 43 00 	movl   $0x43b8,0x4(%esp)
    251b:	00 
    251c:	89 04 24             	mov    %eax,(%esp)
    251f:	e8 fc 0f 00 00       	call   3520 <printf>
    exit();
    2524:	e8 bf 0e 00 00       	call   33e8 <exit>
    2529:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00002530 <writetest>:
  printf(stdout, "open test ok\n");
}

void
writetest(void)
{
    2530:	55                   	push   %ebp
    2531:	89 e5                	mov    %esp,%ebp
    2533:	56                   	push   %esi
    2534:	53                   	push   %ebx
    2535:	83 ec 10             	sub    $0x10,%esp
  int fd;
  int i;

  printf(stdout, "small file test\n");
    2538:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    253d:	c7 44 24 04 d9 43 00 	movl   $0x43d9,0x4(%esp)
    2544:	00 
    2545:	89 04 24             	mov    %eax,(%esp)
    2548:	e8 d3 0f 00 00       	call   3520 <printf>
  fd = open("small", O_CREATE|O_RDWR);
    254d:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    2554:	00 
    2555:	c7 04 24 ea 43 00 00 	movl   $0x43ea,(%esp)
    255c:	e8 c7 0e 00 00       	call   3428 <open>
  if(fd >= 0){
    2561:	85 c0                	test   %eax,%eax
{
  int fd;
  int i;

  printf(stdout, "small file test\n");
  fd = open("small", O_CREATE|O_RDWR);
    2563:	89 c6                	mov    %eax,%esi
  if(fd >= 0){
    2565:	0f 88 b1 01 00 00    	js     271c <writetest+0x1ec>
    printf(stdout, "creat small succeeded; ok\n");
    256b:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2570:	31 db                	xor    %ebx,%ebx
    2572:	c7 44 24 04 f0 43 00 	movl   $0x43f0,0x4(%esp)
    2579:	00 
    257a:	89 04 24             	mov    %eax,(%esp)
    257d:	e8 9e 0f 00 00       	call   3520 <printf>
    2582:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  } else {
    printf(stdout, "error: creat small failed!\n");
    exit();
  }
  for(i = 0; i < 100; i++){
    if(write(fd, "aaaaaaaaaa", 10) != 10){
    2588:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
    258f:	00 
    2590:	c7 44 24 04 27 44 00 	movl   $0x4427,0x4(%esp)
    2597:	00 
    2598:	89 34 24             	mov    %esi,(%esp)
    259b:	e8 68 0e 00 00       	call   3408 <write>
    25a0:	83 f8 0a             	cmp    $0xa,%eax
    25a3:	0f 85 e9 00 00 00    	jne    2692 <writetest+0x162>
      printf(stdout, "error: write aa %d new file failed\n", i);
      exit();
    }
    if(write(fd, "bbbbbbbbbb", 10) != 10){
    25a9:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
    25b0:	00 
    25b1:	c7 44 24 04 32 44 00 	movl   $0x4432,0x4(%esp)
    25b8:	00 
    25b9:	89 34 24             	mov    %esi,(%esp)
    25bc:	e8 47 0e 00 00       	call   3408 <write>
    25c1:	83 f8 0a             	cmp    $0xa,%eax
    25c4:	0f 85 e6 00 00 00    	jne    26b0 <writetest+0x180>
    printf(stdout, "creat small succeeded; ok\n");
  } else {
    printf(stdout, "error: creat small failed!\n");
    exit();
  }
  for(i = 0; i < 100; i++){
    25ca:	83 c3 01             	add    $0x1,%ebx
    25cd:	83 fb 64             	cmp    $0x64,%ebx
    25d0:	75 b6                	jne    2588 <writetest+0x58>
    if(write(fd, "bbbbbbbbbb", 10) != 10){
      printf(stdout, "error: write bb %d new file failed\n", i);
      exit();
    }
  }
  printf(stdout, "writes ok\n");
    25d2:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    25d7:	c7 44 24 04 3d 44 00 	movl   $0x443d,0x4(%esp)
    25de:	00 
    25df:	89 04 24             	mov    %eax,(%esp)
    25e2:	e8 39 0f 00 00       	call   3520 <printf>
  close(fd);
    25e7:	89 34 24             	mov    %esi,(%esp)
    25ea:	e8 21 0e 00 00       	call   3410 <close>
  fd = open("small", O_RDONLY);
    25ef:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    25f6:	00 
    25f7:	c7 04 24 ea 43 00 00 	movl   $0x43ea,(%esp)
    25fe:	e8 25 0e 00 00       	call   3428 <open>
  if(fd >= 0){
    2603:	85 c0                	test   %eax,%eax
      exit();
    }
  }
  printf(stdout, "writes ok\n");
  close(fd);
  fd = open("small", O_RDONLY);
    2605:	89 c3                	mov    %eax,%ebx
  if(fd >= 0){
    2607:	0f 88 c1 00 00 00    	js     26ce <writetest+0x19e>
    printf(stdout, "open small succeeded ok\n");
    260d:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2612:	c7 44 24 04 48 44 00 	movl   $0x4448,0x4(%esp)
    2619:	00 
    261a:	89 04 24             	mov    %eax,(%esp)
    261d:	e8 fe 0e 00 00       	call   3520 <printf>
  } else {
    printf(stdout, "error: open small failed!\n");
    exit();
  }
  i = read(fd, buf, 2000);
    2622:	c7 44 24 08 d0 07 00 	movl   $0x7d0,0x8(%esp)
    2629:	00 
    262a:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    2631:	00 
    2632:	89 1c 24             	mov    %ebx,(%esp)
    2635:	e8 c6 0d 00 00       	call   3400 <read>
  if(i == 2000){
    263a:	3d d0 07 00 00       	cmp    $0x7d0,%eax
    263f:	0f 85 a3 00 00 00    	jne    26e8 <writetest+0x1b8>
    printf(stdout, "read succeeded ok\n");
    2645:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    264a:	c7 44 24 04 7c 44 00 	movl   $0x447c,0x4(%esp)
    2651:	00 
    2652:	89 04 24             	mov    %eax,(%esp)
    2655:	e8 c6 0e 00 00       	call   3520 <printf>
  } else {
    printf(stdout, "read failed\n");
    exit();
  }
  close(fd);
    265a:	89 1c 24             	mov    %ebx,(%esp)
    265d:	e8 ae 0d 00 00       	call   3410 <close>

  if(unlink("small") < 0){
    2662:	c7 04 24 ea 43 00 00 	movl   $0x43ea,(%esp)
    2669:	e8 ca 0d 00 00       	call   3438 <unlink>
    266e:	85 c0                	test   %eax,%eax
    2670:	0f 88 8c 00 00 00    	js     2702 <writetest+0x1d2>
    printf(stdout, "unlink small failed\n");
    exit();
  }
  printf(stdout, "small file test ok\n");
    2676:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    267b:	c7 44 24 04 a4 44 00 	movl   $0x44a4,0x4(%esp)
    2682:	00 
    2683:	89 04 24             	mov    %eax,(%esp)
    2686:	e8 95 0e 00 00       	call   3520 <printf>
}
    268b:	83 c4 10             	add    $0x10,%esp
    268e:	5b                   	pop    %ebx
    268f:	5e                   	pop    %esi
    2690:	5d                   	pop    %ebp
    2691:	c3                   	ret    
    printf(stdout, "error: creat small failed!\n");
    exit();
  }
  for(i = 0; i < 100; i++){
    if(write(fd, "aaaaaaaaaa", 10) != 10){
      printf(stdout, "error: write aa %d new file failed\n", i);
    2692:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2697:	89 5c 24 08          	mov    %ebx,0x8(%esp)
    269b:	c7 44 24 04 90 4c 00 	movl   $0x4c90,0x4(%esp)
    26a2:	00 
    26a3:	89 04 24             	mov    %eax,(%esp)
    26a6:	e8 75 0e 00 00       	call   3520 <printf>
      exit();
    26ab:	e8 38 0d 00 00       	call   33e8 <exit>
    }
    if(write(fd, "bbbbbbbbbb", 10) != 10){
      printf(stdout, "error: write bb %d new file failed\n", i);
    26b0:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    26b5:	89 5c 24 08          	mov    %ebx,0x8(%esp)
    26b9:	c7 44 24 04 b4 4c 00 	movl   $0x4cb4,0x4(%esp)
    26c0:	00 
    26c1:	89 04 24             	mov    %eax,(%esp)
    26c4:	e8 57 0e 00 00       	call   3520 <printf>
      exit();
    26c9:	e8 1a 0d 00 00       	call   33e8 <exit>
  close(fd);
  fd = open("small", O_RDONLY);
  if(fd >= 0){
    printf(stdout, "open small succeeded ok\n");
  } else {
    printf(stdout, "error: open small failed!\n");
    26ce:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    26d3:	c7 44 24 04 61 44 00 	movl   $0x4461,0x4(%esp)
    26da:	00 
    26db:	89 04 24             	mov    %eax,(%esp)
    26de:	e8 3d 0e 00 00       	call   3520 <printf>
    exit();
    26e3:	e8 00 0d 00 00       	call   33e8 <exit>
  }
  i = read(fd, buf, 2000);
  if(i == 2000){
    printf(stdout, "read succeeded ok\n");
  } else {
    printf(stdout, "read failed\n");
    26e8:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    26ed:	c7 44 24 04 07 42 00 	movl   $0x4207,0x4(%esp)
    26f4:	00 
    26f5:	89 04 24             	mov    %eax,(%esp)
    26f8:	e8 23 0e 00 00       	call   3520 <printf>
    exit();
    26fd:	e8 e6 0c 00 00       	call   33e8 <exit>
  }
  close(fd);

  if(unlink("small") < 0){
    printf(stdout, "unlink small failed\n");
    2702:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2707:	c7 44 24 04 8f 44 00 	movl   $0x448f,0x4(%esp)
    270e:	00 
    270f:	89 04 24             	mov    %eax,(%esp)
    2712:	e8 09 0e 00 00       	call   3520 <printf>
    exit();
    2717:	e8 cc 0c 00 00       	call   33e8 <exit>
  printf(stdout, "small file test\n");
  fd = open("small", O_CREATE|O_RDWR);
  if(fd >= 0){
    printf(stdout, "creat small succeeded; ok\n");
  } else {
    printf(stdout, "error: creat small failed!\n");
    271c:	a1 dc 4d 00 00       	mov    0x4ddc,%eax
    2721:	c7 44 24 04 0b 44 00 	movl   $0x440b,0x4(%esp)
    2728:	00 
    2729:	89 04 24             	mov    %eax,(%esp)
    272c:	e8 ef 0d 00 00       	call   3520 <printf>
    exit();
    2731:	e8 b2 0c 00 00       	call   33e8 <exit>
    2736:	8d 76 00             	lea    0x0(%esi),%esi
    2739:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00002740 <bigfile>:
  printf(1, "subdir ok\n");
}

void
bigfile(void)
{
    2740:	55                   	push   %ebp
    2741:	89 e5                	mov    %esp,%ebp
    2743:	57                   	push   %edi
    2744:	56                   	push   %esi
    2745:	53                   	push   %ebx
    2746:	83 ec 1c             	sub    $0x1c,%esp
  int fd, i, total, cc;

  printf(1, "bigfile test\n");
    2749:	c7 44 24 04 b8 44 00 	movl   $0x44b8,0x4(%esp)
    2750:	00 
    2751:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2758:	e8 c3 0d 00 00       	call   3520 <printf>

  unlink("bigfile");
    275d:	c7 04 24 d4 44 00 00 	movl   $0x44d4,(%esp)
    2764:	e8 cf 0c 00 00       	call   3438 <unlink>
  fd = open("bigfile", O_CREATE | O_RDWR);
    2769:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    2770:	00 
    2771:	c7 04 24 d4 44 00 00 	movl   $0x44d4,(%esp)
    2778:	e8 ab 0c 00 00       	call   3428 <open>
  if(fd < 0){
    277d:	85 c0                	test   %eax,%eax
  int fd, i, total, cc;

  printf(1, "bigfile test\n");

  unlink("bigfile");
  fd = open("bigfile", O_CREATE | O_RDWR);
    277f:	89 c6                	mov    %eax,%esi
  if(fd < 0){
    2781:	0f 88 7f 01 00 00    	js     2906 <bigfile+0x1c6>
    printf(1, "cannot create bigfile");
    exit();
    2787:	31 db                	xor    %ebx,%ebx
    2789:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  }
  for(i = 0; i < 20; i++){
    memset(buf, i, 600);
    2790:	c7 44 24 08 58 02 00 	movl   $0x258,0x8(%esp)
    2797:	00 
    2798:	89 5c 24 04          	mov    %ebx,0x4(%esp)
    279c:	c7 04 24 20 75 00 00 	movl   $0x7520,(%esp)
    27a3:	e8 b8 0a 00 00       	call   3260 <memset>
    if(write(fd, buf, 600) != 600){
    27a8:	c7 44 24 08 58 02 00 	movl   $0x258,0x8(%esp)
    27af:	00 
    27b0:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    27b7:	00 
    27b8:	89 34 24             	mov    %esi,(%esp)
    27bb:	e8 48 0c 00 00       	call   3408 <write>
    27c0:	3d 58 02 00 00       	cmp    $0x258,%eax
    27c5:	0f 85 09 01 00 00    	jne    28d4 <bigfile+0x194>
  fd = open("bigfile", O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "cannot create bigfile");
    exit();
  }
  for(i = 0; i < 20; i++){
    27cb:	83 c3 01             	add    $0x1,%ebx
    27ce:	83 fb 14             	cmp    $0x14,%ebx
    27d1:	75 bd                	jne    2790 <bigfile+0x50>
    if(write(fd, buf, 600) != 600){
      printf(1, "write bigfile failed\n");
      exit();
    }
  }
  close(fd);
    27d3:	89 34 24             	mov    %esi,(%esp)
    27d6:	e8 35 0c 00 00       	call   3410 <close>

  fd = open("bigfile", 0);
    27db:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    27e2:	00 
    27e3:	c7 04 24 d4 44 00 00 	movl   $0x44d4,(%esp)
    27ea:	e8 39 0c 00 00       	call   3428 <open>
  if(fd < 0){
    27ef:	85 c0                	test   %eax,%eax
      exit();
    }
  }
  close(fd);

  fd = open("bigfile", 0);
    27f1:	89 c7                	mov    %eax,%edi
  if(fd < 0){
    27f3:	0f 88 f4 00 00 00    	js     28ed <bigfile+0x1ad>
    printf(1, "cannot open bigfile\n");
    exit();
    27f9:	31 f6                	xor    %esi,%esi
    27fb:	31 db                	xor    %ebx,%ebx
    27fd:	eb 2f                	jmp    282e <bigfile+0xee>
    27ff:	90                   	nop
      printf(1, "read bigfile failed\n");
      exit();
    }
    if(cc == 0)
      break;
    if(cc != 300){
    2800:	3d 2c 01 00 00       	cmp    $0x12c,%eax
    2805:	0f 85 97 00 00 00    	jne    28a2 <bigfile+0x162>
      printf(1, "short read bigfile\n");
      exit();
    }
    if(buf[0] != i/2 || buf[299] != i/2){
    280b:	0f be 05 20 75 00 00 	movsbl 0x7520,%eax
    2812:	89 da                	mov    %ebx,%edx
    2814:	d1 fa                	sar    %edx
    2816:	39 d0                	cmp    %edx,%eax
    2818:	75 6f                	jne    2889 <bigfile+0x149>
    281a:	0f be 15 4b 76 00 00 	movsbl 0x764b,%edx
    2821:	39 d0                	cmp    %edx,%eax
    2823:	75 64                	jne    2889 <bigfile+0x149>
      printf(1, "read bigfile wrong data\n");
      exit();
    }
    total += cc;
    2825:	81 c6 2c 01 00 00    	add    $0x12c,%esi
  if(fd < 0){
    printf(1, "cannot open bigfile\n");
    exit();
  }
  total = 0;
  for(i = 0; ; i++){
    282b:	83 c3 01             	add    $0x1,%ebx
    cc = read(fd, buf, 300);
    282e:	c7 44 24 08 2c 01 00 	movl   $0x12c,0x8(%esp)
    2835:	00 
    2836:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    283d:	00 
    283e:	89 3c 24             	mov    %edi,(%esp)
    2841:	e8 ba 0b 00 00       	call   3400 <read>
    if(cc < 0){
    2846:	83 f8 00             	cmp    $0x0,%eax
    2849:	7c 70                	jl     28bb <bigfile+0x17b>
      printf(1, "read bigfile failed\n");
      exit();
    }
    if(cc == 0)
    284b:	75 b3                	jne    2800 <bigfile+0xc0>
      printf(1, "read bigfile wrong data\n");
      exit();
    }
    total += cc;
  }
  close(fd);
    284d:	89 3c 24             	mov    %edi,(%esp)
    2850:	e8 bb 0b 00 00       	call   3410 <close>
  if(total != 20*600){
    2855:	81 fe e0 2e 00 00    	cmp    $0x2ee0,%esi
    285b:	0f 85 be 00 00 00    	jne    291f <bigfile+0x1df>
    printf(1, "read bigfile wrong total\n");
    exit();
  }
  unlink("bigfile");
    2861:	c7 04 24 d4 44 00 00 	movl   $0x44d4,(%esp)
    2868:	e8 cb 0b 00 00       	call   3438 <unlink>

  printf(1, "bigfile test ok\n");
    286d:	c7 44 24 04 63 45 00 	movl   $0x4563,0x4(%esp)
    2874:	00 
    2875:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    287c:	e8 9f 0c 00 00       	call   3520 <printf>
}
    2881:	83 c4 1c             	add    $0x1c,%esp
    2884:	5b                   	pop    %ebx
    2885:	5e                   	pop    %esi
    2886:	5f                   	pop    %edi
    2887:	5d                   	pop    %ebp
    2888:	c3                   	ret    
    if(cc != 300){
      printf(1, "short read bigfile\n");
      exit();
    }
    if(buf[0] != i/2 || buf[299] != i/2){
      printf(1, "read bigfile wrong data\n");
    2889:	c7 44 24 04 30 45 00 	movl   $0x4530,0x4(%esp)
    2890:	00 
    2891:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2898:	e8 83 0c 00 00       	call   3520 <printf>
      exit();
    289d:	e8 46 0b 00 00       	call   33e8 <exit>
      exit();
    }
    if(cc == 0)
      break;
    if(cc != 300){
      printf(1, "short read bigfile\n");
    28a2:	c7 44 24 04 1c 45 00 	movl   $0x451c,0x4(%esp)
    28a9:	00 
    28aa:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    28b1:	e8 6a 0c 00 00       	call   3520 <printf>
      exit();
    28b6:	e8 2d 0b 00 00       	call   33e8 <exit>
  }
  total = 0;
  for(i = 0; ; i++){
    cc = read(fd, buf, 300);
    if(cc < 0){
      printf(1, "read bigfile failed\n");
    28bb:	c7 44 24 04 07 45 00 	movl   $0x4507,0x4(%esp)
    28c2:	00 
    28c3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    28ca:	e8 51 0c 00 00       	call   3520 <printf>
      exit();
    28cf:	e8 14 0b 00 00       	call   33e8 <exit>
    exit();
  }
  for(i = 0; i < 20; i++){
    memset(buf, i, 600);
    if(write(fd, buf, 600) != 600){
      printf(1, "write bigfile failed\n");
    28d4:	c7 44 24 04 dc 44 00 	movl   $0x44dc,0x4(%esp)
    28db:	00 
    28dc:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    28e3:	e8 38 0c 00 00       	call   3520 <printf>
      exit();
    28e8:	e8 fb 0a 00 00       	call   33e8 <exit>
  }
  close(fd);

  fd = open("bigfile", 0);
  if(fd < 0){
    printf(1, "cannot open bigfile\n");
    28ed:	c7 44 24 04 f2 44 00 	movl   $0x44f2,0x4(%esp)
    28f4:	00 
    28f5:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    28fc:	e8 1f 0c 00 00       	call   3520 <printf>
    exit();
    2901:	e8 e2 0a 00 00       	call   33e8 <exit>
  printf(1, "bigfile test\n");

  unlink("bigfile");
  fd = open("bigfile", O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "cannot create bigfile");
    2906:	c7 44 24 04 c6 44 00 	movl   $0x44c6,0x4(%esp)
    290d:	00 
    290e:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2915:	e8 06 0c 00 00       	call   3520 <printf>
    exit();
    291a:	e8 c9 0a 00 00       	call   33e8 <exit>
    }
    total += cc;
  }
  close(fd);
  if(total != 20*600){
    printf(1, "read bigfile wrong total\n");
    291f:	c7 44 24 04 49 45 00 	movl   $0x4549,0x4(%esp)
    2926:	00 
    2927:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    292e:	e8 ed 0b 00 00       	call   3520 <printf>
    exit();
    2933:	e8 b0 0a 00 00       	call   33e8 <exit>
    2938:	90                   	nop
    2939:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

00002940 <concreate>:
}

// test concurrent create and unlink of the same file
void
concreate(void)
{
    2940:	55                   	push   %ebp
    2941:	89 e5                	mov    %esp,%ebp
    2943:	57                   	push   %edi
    2944:	56                   	push   %esi
    2945:	53                   	push   %ebx
    char name[14];
  } de;

  printf(1, "concreate test\n");
  file[0] = 'C';
  file[2] = '\0';
    2946:	31 db                	xor    %ebx,%ebx
}

// test concurrent create and unlink of the same file
void
concreate(void)
{
    2948:	83 ec 6c             	sub    $0x6c,%esp
  struct {
    ushort inum;
    char name[14];
  } de;

  printf(1, "concreate test\n");
    294b:	c7 44 24 04 74 45 00 	movl   $0x4574,0x4(%esp)
    2952:	00 
    2953:	8d 7d e5             	lea    -0x1b(%ebp),%edi
    2956:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    295d:	e8 be 0b 00 00       	call   3520 <printf>
  file[0] = 'C';
    2962:	c6 45 e5 43          	movb   $0x43,-0x1b(%ebp)
  file[2] = '\0';
    2966:	c6 45 e7 00          	movb   $0x0,-0x19(%ebp)
    296a:	eb 4f                	jmp    29bb <concreate+0x7b>
    296c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(i = 0; i < 40; i++){
    file[1] = '0' + i;
    unlink(file);
    pid = fork();
    if(pid && (i % 3) == 1){
    2970:	b8 56 55 55 55       	mov    $0x55555556,%eax
    2975:	f7 eb                	imul   %ebx
    2977:	89 d8                	mov    %ebx,%eax
    2979:	c1 f8 1f             	sar    $0x1f,%eax
    297c:	29 c2                	sub    %eax,%edx
    297e:	8d 04 52             	lea    (%edx,%edx,2),%eax
    2981:	89 da                	mov    %ebx,%edx
    2983:	29 c2                	sub    %eax,%edx
    2985:	83 fa 01             	cmp    $0x1,%edx
    2988:	74 7e                	je     2a08 <concreate+0xc8>
      link("C0", file);
    } else if(pid == 0 && (i % 5) == 1){
      link("C0", file);
    } else {
      fd = open(file, O_CREATE | O_RDWR);
    298a:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    2991:	00 
    2992:	89 3c 24             	mov    %edi,(%esp)
    2995:	e8 8e 0a 00 00       	call   3428 <open>
      if(fd < 0){
    299a:	85 c0                	test   %eax,%eax
    299c:	0f 88 db 01 00 00    	js     2b7d <concreate+0x23d>
        printf(1, "concreate create %s failed\n", file);
        exit();
      }
      close(fd);
    29a2:	89 04 24             	mov    %eax,(%esp)
    29a5:	e8 66 0a 00 00       	call   3410 <close>
    }
    if(pid == 0)
    29aa:	85 f6                	test   %esi,%esi
    29ac:	74 52                	je     2a00 <concreate+0xc0>
  } de;

  printf(1, "concreate test\n");
  file[0] = 'C';
  file[2] = '\0';
  for(i = 0; i < 40; i++){
    29ae:	83 c3 01             	add    $0x1,%ebx
      close(fd);
    }
    if(pid == 0)
      exit();
    else
      wait();
    29b1:	e8 3a 0a 00 00       	call   33f0 <wait>
  } de;

  printf(1, "concreate test\n");
  file[0] = 'C';
  file[2] = '\0';
  for(i = 0; i < 40; i++){
    29b6:	83 fb 28             	cmp    $0x28,%ebx
    29b9:	74 6d                	je     2a28 <concreate+0xe8>
    file[1] = '0' + i;
    29bb:	8d 43 30             	lea    0x30(%ebx),%eax
    29be:	88 45 e6             	mov    %al,-0x1a(%ebp)
    unlink(file);
    29c1:	89 3c 24             	mov    %edi,(%esp)
    29c4:	e8 6f 0a 00 00       	call   3438 <unlink>
    pid = fork();
    29c9:	e8 12 0a 00 00       	call   33e0 <fork>
    if(pid && (i % 3) == 1){
    29ce:	85 c0                	test   %eax,%eax
  file[0] = 'C';
  file[2] = '\0';
  for(i = 0; i < 40; i++){
    file[1] = '0' + i;
    unlink(file);
    pid = fork();
    29d0:	89 c6                	mov    %eax,%esi
    if(pid && (i % 3) == 1){
    29d2:	75 9c                	jne    2970 <concreate+0x30>
      link("C0", file);
    } else if(pid == 0 && (i % 5) == 1){
    29d4:	b8 67 66 66 66       	mov    $0x66666667,%eax
    29d9:	f7 eb                	imul   %ebx
    29db:	89 d8                	mov    %ebx,%eax
    29dd:	c1 f8 1f             	sar    $0x1f,%eax
    29e0:	d1 fa                	sar    %edx
    29e2:	29 c2                	sub    %eax,%edx
    29e4:	8d 04 92             	lea    (%edx,%edx,4),%eax
    29e7:	89 da                	mov    %ebx,%edx
    29e9:	29 c2                	sub    %eax,%edx
    29eb:	83 fa 01             	cmp    $0x1,%edx
    29ee:	75 9a                	jne    298a <concreate+0x4a>
      link("C0", file);
    29f0:	89 7c 24 04          	mov    %edi,0x4(%esp)
    29f4:	c7 04 24 84 45 00 00 	movl   $0x4584,(%esp)
    29fb:	e8 48 0a 00 00       	call   3448 <link>
      continue;
    if(de.name[0] == 'C' && de.name[2] == '\0'){
      i = de.name[1] - '0';
      if(i < 0 || i >= sizeof(fa)){
        printf(1, "concreate weird file %s\n", de.name);
        exit();
    2a00:	e8 e3 09 00 00       	call   33e8 <exit>
    2a05:	8d 76 00             	lea    0x0(%esi),%esi
  } de;

  printf(1, "concreate test\n");
  file[0] = 'C';
  file[2] = '\0';
  for(i = 0; i < 40; i++){
    2a08:	83 c3 01             	add    $0x1,%ebx
    file[1] = '0' + i;
    unlink(file);
    pid = fork();
    if(pid && (i % 3) == 1){
      link("C0", file);
    2a0b:	89 7c 24 04          	mov    %edi,0x4(%esp)
    2a0f:	c7 04 24 84 45 00 00 	movl   $0x4584,(%esp)
    2a16:	e8 2d 0a 00 00       	call   3448 <link>
      close(fd);
    }
    if(pid == 0)
      exit();
    else
      wait();
    2a1b:	e8 d0 09 00 00       	call   33f0 <wait>
  } de;

  printf(1, "concreate test\n");
  file[0] = 'C';
  file[2] = '\0';
  for(i = 0; i < 40; i++){
    2a20:	83 fb 28             	cmp    $0x28,%ebx
    2a23:	75 96                	jne    29bb <concreate+0x7b>
    2a25:	8d 76 00             	lea    0x0(%esi),%esi
      exit();
    else
      wait();
  }

  memset(fa, 0, sizeof(fa));
    2a28:	8d 45 ac             	lea    -0x54(%ebp),%eax
    2a2b:	c7 44 24 08 28 00 00 	movl   $0x28,0x8(%esp)
    2a32:	00 
    2a33:	8d 75 d4             	lea    -0x2c(%ebp),%esi
    2a36:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    2a3d:	00 
    2a3e:	89 04 24             	mov    %eax,(%esp)
    2a41:	e8 1a 08 00 00       	call   3260 <memset>
  fd = open(".", 0);
    2a46:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    2a4d:	00 
    2a4e:	c7 04 24 e2 3e 00 00 	movl   $0x3ee2,(%esp)
    2a55:	e8 ce 09 00 00       	call   3428 <open>
    2a5a:	c7 45 a4 00 00 00 00 	movl   $0x0,-0x5c(%ebp)
    2a61:	89 c3                	mov    %eax,%ebx
    2a63:	90                   	nop
    2a64:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  n = 0;
  while(read(fd, &de, sizeof(de)) > 0){
    2a68:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
    2a6f:	00 
    2a70:	89 74 24 04          	mov    %esi,0x4(%esp)
    2a74:	89 1c 24             	mov    %ebx,(%esp)
    2a77:	e8 84 09 00 00       	call   3400 <read>
    2a7c:	85 c0                	test   %eax,%eax
    2a7e:	7e 40                	jle    2ac0 <concreate+0x180>
    if(de.inum == 0)
    2a80:	66 83 7d d4 00       	cmpw   $0x0,-0x2c(%ebp)
    2a85:	74 e1                	je     2a68 <concreate+0x128>
      continue;
    if(de.name[0] == 'C' && de.name[2] == '\0'){
    2a87:	80 7d d6 43          	cmpb   $0x43,-0x2a(%ebp)
    2a8b:	75 db                	jne    2a68 <concreate+0x128>
    2a8d:	80 7d d8 00          	cmpb   $0x0,-0x28(%ebp)
    2a91:	75 d5                	jne    2a68 <concreate+0x128>
      i = de.name[1] - '0';
    2a93:	0f be 45 d7          	movsbl -0x29(%ebp),%eax
    2a97:	83 e8 30             	sub    $0x30,%eax
      if(i < 0 || i >= sizeof(fa)){
    2a9a:	83 f8 27             	cmp    $0x27,%eax
    2a9d:	0f 87 f7 00 00 00    	ja     2b9a <concreate+0x25a>
        printf(1, "concreate weird file %s\n", de.name);
        exit();
      }
      if(fa[i]){
    2aa3:	80 7c 05 ac 00       	cmpb   $0x0,-0x54(%ebp,%eax,1)
    2aa8:	0f 85 25 01 00 00    	jne    2bd3 <concreate+0x293>
        printf(1, "concreate duplicate file %s\n", de.name);
        exit();
      }
      fa[i] = 1;
    2aae:	c6 44 05 ac 01       	movb   $0x1,-0x54(%ebp,%eax,1)
      n++;
    2ab3:	83 45 a4 01          	addl   $0x1,-0x5c(%ebp)
    2ab7:	eb af                	jmp    2a68 <concreate+0x128>
    2ab9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    }
  }
  close(fd);
    2ac0:	89 1c 24             	mov    %ebx,(%esp)
    2ac3:	e8 48 09 00 00       	call   3410 <close>

  if(n != 40){
    2ac8:	83 7d a4 28          	cmpl   $0x28,-0x5c(%ebp)
    2acc:	0f 85 e8 00 00 00    	jne    2bba <concreate+0x27a>
    printf(1, "concreate not enough files in directory listing\n");
    exit();
    2ad2:	31 db                	xor    %ebx,%ebx
    2ad4:	eb 39                	jmp    2b0f <concreate+0x1cf>
    2ad6:	66 90                	xchg   %ax,%ax
    pid = fork();
    if(pid < 0){
      printf(1, "fork failed\n");
      exit();
    }
    if(((i % 3) == 0 && pid == 0) ||
    2ad8:	83 f8 01             	cmp    $0x1,%eax
    2adb:	75 5e                	jne    2b3b <concreate+0x1fb>
    2add:	85 f6                	test   %esi,%esi
    2adf:	90                   	nop
    2ae0:	74 59                	je     2b3b <concreate+0x1fb>
       ((i % 3) == 1 && pid != 0)){
      fd = open(file, 0);
    2ae2:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    2ae9:	00 
    2aea:	89 3c 24             	mov    %edi,(%esp)
    2aed:	e8 36 09 00 00       	call   3428 <open>
      close(fd);
    2af2:	89 04 24             	mov    %eax,(%esp)
    2af5:	e8 16 09 00 00       	call   3410 <close>
    } else {
      unlink(file);
    }
    if(pid == 0)
    2afa:	85 f6                	test   %esi,%esi
    2afc:	0f 84 fe fe ff ff    	je     2a00 <concreate+0xc0>
  if(n != 40){
    printf(1, "concreate not enough files in directory listing\n");
    exit();
  }

  for(i = 0; i < 40; i++){
    2b02:	83 c3 01             	add    $0x1,%ebx
      unlink(file);
    }
    if(pid == 0)
      exit();
    else
      wait();
    2b05:	e8 e6 08 00 00       	call   33f0 <wait>
  if(n != 40){
    printf(1, "concreate not enough files in directory listing\n");
    exit();
  }

  for(i = 0; i < 40; i++){
    2b0a:	83 fb 28             	cmp    $0x28,%ebx
    2b0d:	74 39                	je     2b48 <concreate+0x208>
    file[1] = '0' + i;
    2b0f:	8d 43 30             	lea    0x30(%ebx),%eax
    2b12:	88 45 e6             	mov    %al,-0x1a(%ebp)
    pid = fork();
    2b15:	e8 c6 08 00 00       	call   33e0 <fork>
    if(pid < 0){
    2b1a:	85 c0                	test   %eax,%eax
    exit();
  }

  for(i = 0; i < 40; i++){
    file[1] = '0' + i;
    pid = fork();
    2b1c:	89 c6                	mov    %eax,%esi
    if(pid < 0){
    2b1e:	78 44                	js     2b64 <concreate+0x224>
      printf(1, "fork failed\n");
      exit();
    }
    if(((i % 3) == 0 && pid == 0) ||
    2b20:	b8 56 55 55 55       	mov    $0x55555556,%eax
    2b25:	f7 eb                	imul   %ebx
    2b27:	89 d8                	mov    %ebx,%eax
    2b29:	c1 f8 1f             	sar    $0x1f,%eax
    2b2c:	29 c2                	sub    %eax,%edx
    2b2e:	89 d8                	mov    %ebx,%eax
    2b30:	8d 14 52             	lea    (%edx,%edx,2),%edx
    2b33:	29 d0                	sub    %edx,%eax
    2b35:	75 a1                	jne    2ad8 <concreate+0x198>
    2b37:	85 f6                	test   %esi,%esi
    2b39:	74 a7                	je     2ae2 <concreate+0x1a2>
       ((i % 3) == 1 && pid != 0)){
      fd = open(file, 0);
      close(fd);
    } else {
      unlink(file);
    2b3b:	89 3c 24             	mov    %edi,(%esp)
    2b3e:	e8 f5 08 00 00       	call   3438 <unlink>
    2b43:	eb b5                	jmp    2afa <concreate+0x1ba>
    2b45:	8d 76 00             	lea    0x0(%esi),%esi
      exit();
    else
      wait();
  }

  printf(1, "concreate ok\n");
    2b48:	c7 44 24 04 d9 45 00 	movl   $0x45d9,0x4(%esp)
    2b4f:	00 
    2b50:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2b57:	e8 c4 09 00 00       	call   3520 <printf>
}
    2b5c:	83 c4 6c             	add    $0x6c,%esp
    2b5f:	5b                   	pop    %ebx
    2b60:	5e                   	pop    %esi
    2b61:	5f                   	pop    %edi
    2b62:	5d                   	pop    %ebp
    2b63:	c3                   	ret    

  for(i = 0; i < 40; i++){
    file[1] = '0' + i;
    pid = fork();
    if(pid < 0){
      printf(1, "fork failed\n");
    2b64:	c7 44 24 04 52 39 00 	movl   $0x3952,0x4(%esp)
    2b6b:	00 
    2b6c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2b73:	e8 a8 09 00 00       	call   3520 <printf>
      exit();
    2b78:	e8 6b 08 00 00       	call   33e8 <exit>
    } else if(pid == 0 && (i % 5) == 1){
      link("C0", file);
    } else {
      fd = open(file, O_CREATE | O_RDWR);
      if(fd < 0){
        printf(1, "concreate create %s failed\n", file);
    2b7d:	89 7c 24 08          	mov    %edi,0x8(%esp)
    2b81:	c7 44 24 04 87 45 00 	movl   $0x4587,0x4(%esp)
    2b88:	00 
    2b89:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2b90:	e8 8b 09 00 00       	call   3520 <printf>
        exit();
    2b95:	e8 4e 08 00 00       	call   33e8 <exit>
    if(de.inum == 0)
      continue;
    if(de.name[0] == 'C' && de.name[2] == '\0'){
      i = de.name[1] - '0';
      if(i < 0 || i >= sizeof(fa)){
        printf(1, "concreate weird file %s\n", de.name);
    2b9a:	8d 45 d6             	lea    -0x2a(%ebp),%eax
    2b9d:	89 44 24 08          	mov    %eax,0x8(%esp)
    2ba1:	c7 44 24 04 a3 45 00 	movl   $0x45a3,0x4(%esp)
    2ba8:	00 
    2ba9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2bb0:	e8 6b 09 00 00       	call   3520 <printf>
    2bb5:	e9 46 fe ff ff       	jmp    2a00 <concreate+0xc0>
    }
  }
  close(fd);

  if(n != 40){
    printf(1, "concreate not enough files in directory listing\n");
    2bba:	c7 44 24 04 d8 4c 00 	movl   $0x4cd8,0x4(%esp)
    2bc1:	00 
    2bc2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2bc9:	e8 52 09 00 00       	call   3520 <printf>
    exit();
    2bce:	e8 15 08 00 00       	call   33e8 <exit>
      if(i < 0 || i >= sizeof(fa)){
        printf(1, "concreate weird file %s\n", de.name);
        exit();
      }
      if(fa[i]){
        printf(1, "concreate duplicate file %s\n", de.name);
    2bd3:	8d 45 d6             	lea    -0x2a(%ebp),%eax
    2bd6:	89 44 24 08          	mov    %eax,0x8(%esp)
    2bda:	c7 44 24 04 bc 45 00 	movl   $0x45bc,0x4(%esp)
    2be1:	00 
    2be2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2be9:	e8 32 09 00 00       	call   3520 <printf>
        exit();
    2bee:	e8 f5 07 00 00       	call   33e8 <exit>
    2bf3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    2bf9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00002c00 <twofiles>:

// two processes write two different files at the same
// time, to test block allocation.
void
twofiles(void)
{
    2c00:	55                   	push   %ebp
    2c01:	89 e5                	mov    %esp,%ebp
    2c03:	57                   	push   %edi
    2c04:	56                   	push   %esi
    2c05:	53                   	push   %ebx
    2c06:	83 ec 2c             	sub    $0x2c,%esp
  int fd, pid, i, j, n, total;
  char *fname;

  printf(1, "twofiles test\n");
    2c09:	c7 44 24 04 e7 45 00 	movl   $0x45e7,0x4(%esp)
    2c10:	00 
    2c11:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2c18:	e8 03 09 00 00       	call   3520 <printf>

  unlink("f1");
    2c1d:	c7 04 24 2e 41 00 00 	movl   $0x412e,(%esp)
    2c24:	e8 0f 08 00 00       	call   3438 <unlink>
  unlink("f2");
    2c29:	c7 04 24 32 41 00 00 	movl   $0x4132,(%esp)
    2c30:	e8 03 08 00 00       	call   3438 <unlink>

  pid = fork();
    2c35:	e8 a6 07 00 00       	call   33e0 <fork>
  if(pid < 0){
    2c3a:	83 f8 00             	cmp    $0x0,%eax
  printf(1, "twofiles test\n");

  unlink("f1");
  unlink("f2");

  pid = fork();
    2c3d:	89 c7                	mov    %eax,%edi
  if(pid < 0){
    2c3f:	0f 8c 6c 01 00 00    	jl     2db1 <twofiles+0x1b1>
    printf(1, "fork failed\n");
    return;
  }

  fname = pid ? "f1" : "f2";
    2c45:	ba 2e 41 00 00       	mov    $0x412e,%edx
    2c4a:	b8 32 41 00 00       	mov    $0x4132,%eax
    2c4f:	0f 45 c2             	cmovne %edx,%eax
  fd = open(fname, O_CREATE | O_RDWR);
    2c52:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    2c59:	00 
    2c5a:	89 04 24             	mov    %eax,(%esp)
    2c5d:	e8 c6 07 00 00       	call   3428 <open>
  if(fd < 0){
    2c62:	85 c0                	test   %eax,%eax
    printf(1, "fork failed\n");
    return;
  }

  fname = pid ? "f1" : "f2";
  fd = open(fname, O_CREATE | O_RDWR);
    2c64:	89 c6                	mov    %eax,%esi
  if(fd < 0){
    2c66:	0f 88 95 01 00 00    	js     2e01 <twofiles+0x201>
    printf(1, "create failed\n");
    exit();
  }

  memset(buf, pid?'p':'c', 512);
    2c6c:	83 ff 01             	cmp    $0x1,%edi
    2c6f:	19 c0                	sbb    %eax,%eax
    2c71:	31 db                	xor    %ebx,%ebx
    2c73:	83 e0 f3             	and    $0xfffffff3,%eax
    2c76:	83 c0 70             	add    $0x70,%eax
    2c79:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
    2c80:	00 
    2c81:	89 44 24 04          	mov    %eax,0x4(%esp)
    2c85:	c7 04 24 20 75 00 00 	movl   $0x7520,(%esp)
    2c8c:	e8 cf 05 00 00       	call   3260 <memset>
    2c91:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(i = 0; i < 12; i++){
    if((n = write(fd, buf, 500)) != 500){
    2c98:	c7 44 24 08 f4 01 00 	movl   $0x1f4,0x8(%esp)
    2c9f:	00 
    2ca0:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    2ca7:	00 
    2ca8:	89 34 24             	mov    %esi,(%esp)
    2cab:	e8 58 07 00 00       	call   3408 <write>
    2cb0:	3d f4 01 00 00       	cmp    $0x1f4,%eax
    2cb5:	0f 85 29 01 00 00    	jne    2de4 <twofiles+0x1e4>
    printf(1, "create failed\n");
    exit();
  }

  memset(buf, pid?'p':'c', 512);
  for(i = 0; i < 12; i++){
    2cbb:	83 c3 01             	add    $0x1,%ebx
    2cbe:	83 fb 0c             	cmp    $0xc,%ebx
    2cc1:	75 d5                	jne    2c98 <twofiles+0x98>
    if((n = write(fd, buf, 500)) != 500){
      printf(1, "write failed %d\n", n);
      exit();
    }
  }
  close(fd);
    2cc3:	89 34 24             	mov    %esi,(%esp)
    2cc6:	e8 45 07 00 00       	call   3410 <close>
  if(pid)
    2ccb:	85 ff                	test   %edi,%edi
    2ccd:	0f 84 d9 00 00 00    	je     2dac <twofiles+0x1ac>
    wait();
    2cd3:	e8 18 07 00 00       	call   33f0 <wait>
    2cd8:	30 db                	xor    %bl,%bl
    2cda:	b8 32 41 00 00       	mov    $0x4132,%eax
  else
    exit();

  for(i = 0; i < 2; i++){
    fd = open(i?"f1":"f2", 0);
    2cdf:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    2ce6:	00 
    2ce7:	31 f6                	xor    %esi,%esi
    2ce9:	89 04 24             	mov    %eax,(%esp)
    2cec:	e8 37 07 00 00       	call   3428 <open>
    2cf1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    2cf4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    total = 0;
    while((n = read(fd, buf, sizeof(buf))) > 0){
    2cf8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    2cfb:	c7 44 24 08 00 08 00 	movl   $0x800,0x8(%esp)
    2d02:	00 
    2d03:	c7 44 24 04 20 75 00 	movl   $0x7520,0x4(%esp)
    2d0a:	00 
    2d0b:	89 04 24             	mov    %eax,(%esp)
    2d0e:	e8 ed 06 00 00       	call   3400 <read>
    2d13:	85 c0                	test   %eax,%eax
    2d15:	7e 2a                	jle    2d41 <twofiles+0x141>
    2d17:	31 c9                	xor    %ecx,%ecx
    2d19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      for(j = 0; j < n; j++){
        if(buf[j] != (i?'p':'c')){
    2d20:	83 fb 01             	cmp    $0x1,%ebx
    2d23:	0f be b9 20 75 00 00 	movsbl 0x7520(%ecx),%edi
    2d2a:	19 d2                	sbb    %edx,%edx
    2d2c:	83 e2 f3             	and    $0xfffffff3,%edx
    2d2f:	83 c2 70             	add    $0x70,%edx
    2d32:	39 d7                	cmp    %edx,%edi
    2d34:	75 62                	jne    2d98 <twofiles+0x198>

  for(i = 0; i < 2; i++){
    fd = open(i?"f1":"f2", 0);
    total = 0;
    while((n = read(fd, buf, sizeof(buf))) > 0){
      for(j = 0; j < n; j++){
    2d36:	83 c1 01             	add    $0x1,%ecx
    2d39:	39 c8                	cmp    %ecx,%eax
    2d3b:	7f e3                	jg     2d20 <twofiles+0x120>
        if(buf[j] != (i?'p':'c')){
          printf(1, "wrong char\n");
          exit();
        }
      }
      total += n;
    2d3d:	01 c6                	add    %eax,%esi
    2d3f:	eb b7                	jmp    2cf8 <twofiles+0xf8>
    }
    close(fd);
    2d41:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    2d44:	89 04 24             	mov    %eax,(%esp)
    2d47:	e8 c4 06 00 00       	call   3410 <close>
    if(total != 12*500){
    2d4c:	81 fe 70 17 00 00    	cmp    $0x1770,%esi
    2d52:	75 73                	jne    2dc7 <twofiles+0x1c7>
  if(pid)
    wait();
  else
    exit();

  for(i = 0; i < 2; i++){
    2d54:	83 fb 01             	cmp    $0x1,%ebx
      total += n;
    }
    close(fd);
    if(total != 12*500){
      printf(1, "wrong length %d\n", total);
      exit();
    2d57:	b8 2e 41 00 00       	mov    $0x412e,%eax
  if(pid)
    wait();
  else
    exit();

  for(i = 0; i < 2; i++){
    2d5c:	75 30                	jne    2d8e <twofiles+0x18e>
      printf(1, "wrong length %d\n", total);
      exit();
    }
  }

  unlink("f1");
    2d5e:	89 04 24             	mov    %eax,(%esp)
    2d61:	e8 d2 06 00 00       	call   3438 <unlink>
  unlink("f2");
    2d66:	c7 04 24 32 41 00 00 	movl   $0x4132,(%esp)
    2d6d:	e8 c6 06 00 00       	call   3438 <unlink>

  printf(1, "twofiles ok\n");
    2d72:	c7 44 24 04 24 46 00 	movl   $0x4624,0x4(%esp)
    2d79:	00 
    2d7a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2d81:	e8 9a 07 00 00       	call   3520 <printf>
}
    2d86:	83 c4 2c             	add    $0x2c,%esp
    2d89:	5b                   	pop    %ebx
    2d8a:	5e                   	pop    %esi
    2d8b:	5f                   	pop    %edi
    2d8c:	5d                   	pop    %ebp
    2d8d:	c3                   	ret    
  }
  close(fd);
  if(pid)
    wait();
  else
    exit();
    2d8e:	bb 01 00 00 00       	mov    $0x1,%ebx
    2d93:	e9 47 ff ff ff       	jmp    2cdf <twofiles+0xdf>
    fd = open(i?"f1":"f2", 0);
    total = 0;
    while((n = read(fd, buf, sizeof(buf))) > 0){
      for(j = 0; j < n; j++){
        if(buf[j] != (i?'p':'c')){
          printf(1, "wrong char\n");
    2d98:	c7 44 24 04 07 46 00 	movl   $0x4607,0x4(%esp)
    2d9f:	00 
    2da0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2da7:	e8 74 07 00 00       	call   3520 <printf>
          exit();
    2dac:	e8 37 06 00 00       	call   33e8 <exit>
  unlink("f1");
  unlink("f2");

  pid = fork();
  if(pid < 0){
    printf(1, "fork failed\n");
    2db1:	c7 44 24 04 52 39 00 	movl   $0x3952,0x4(%esp)
    2db8:	00 
    2db9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2dc0:	e8 5b 07 00 00       	call   3520 <printf>
    return;
    2dc5:	eb bf                	jmp    2d86 <twofiles+0x186>
      }
      total += n;
    }
    close(fd);
    if(total != 12*500){
      printf(1, "wrong length %d\n", total);
    2dc7:	89 74 24 08          	mov    %esi,0x8(%esp)
    2dcb:	c7 44 24 04 13 46 00 	movl   $0x4613,0x4(%esp)
    2dd2:	00 
    2dd3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2dda:	e8 41 07 00 00       	call   3520 <printf>
      exit();
    2ddf:	e8 04 06 00 00       	call   33e8 <exit>
  }

  memset(buf, pid?'p':'c', 512);
  for(i = 0; i < 12; i++){
    if((n = write(fd, buf, 500)) != 500){
      printf(1, "write failed %d\n", n);
    2de4:	89 44 24 08          	mov    %eax,0x8(%esp)
    2de8:	c7 44 24 04 f6 45 00 	movl   $0x45f6,0x4(%esp)
    2def:	00 
    2df0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2df7:	e8 24 07 00 00       	call   3520 <printf>
      exit();
    2dfc:	e8 e7 05 00 00       	call   33e8 <exit>
  }

  fname = pid ? "f1" : "f2";
  fd = open(fname, O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "create failed\n");
    2e01:	c7 44 24 04 e0 40 00 	movl   $0x40e0,0x4(%esp)
    2e08:	00 
    2e09:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2e10:	e8 0b 07 00 00       	call   3520 <printf>
    exit();
    2e15:	e8 ce 05 00 00       	call   33e8 <exit>
    2e1a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00002e20 <sharedfd>:

// two processes write to the same file descriptor
// is the offset shared? does inode locking work?
void
sharedfd(void)
{
    2e20:	55                   	push   %ebp
    2e21:	89 e5                	mov    %esp,%ebp
    2e23:	57                   	push   %edi
    2e24:	56                   	push   %esi
    2e25:	53                   	push   %ebx
    2e26:	83 ec 3c             	sub    $0x3c,%esp
  int fd, pid, i, n, nc, np;
  char buf[10];

  unlink("sharedfd");
    2e29:	c7 04 24 31 46 00 00 	movl   $0x4631,(%esp)
    2e30:	e8 03 06 00 00       	call   3438 <unlink>
  fd = open("sharedfd", O_CREATE|O_RDWR);
    2e35:	c7 44 24 04 02 02 00 	movl   $0x202,0x4(%esp)
    2e3c:	00 
    2e3d:	c7 04 24 31 46 00 00 	movl   $0x4631,(%esp)
    2e44:	e8 df 05 00 00       	call   3428 <open>
  if(fd < 0){
    2e49:	85 c0                	test   %eax,%eax
{
  int fd, pid, i, n, nc, np;
  char buf[10];

  unlink("sharedfd");
  fd = open("sharedfd", O_CREATE|O_RDWR);
    2e4b:	89 c7                	mov    %eax,%edi
  if(fd < 0){
    2e4d:	0f 88 55 01 00 00    	js     2fa8 <sharedfd+0x188>
    printf(1, "fstests: cannot open sharedfd for writing");
    return;
  }
  pid = fork();
    2e53:	e8 88 05 00 00       	call   33e0 <fork>
  memset(buf, pid==0?'c':'p', sizeof(buf));
    2e58:	8d 75 de             	lea    -0x22(%ebp),%esi
    2e5b:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
    2e62:	00 
    2e63:	89 34 24             	mov    %esi,(%esp)
    2e66:	83 f8 01             	cmp    $0x1,%eax
  fd = open("sharedfd", O_CREATE|O_RDWR);
  if(fd < 0){
    printf(1, "fstests: cannot open sharedfd for writing");
    return;
  }
  pid = fork();
    2e69:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  memset(buf, pid==0?'c':'p', sizeof(buf));
    2e6c:	19 c0                	sbb    %eax,%eax
    2e6e:	31 db                	xor    %ebx,%ebx
    2e70:	83 e0 f3             	and    $0xfffffff3,%eax
    2e73:	83 c0 70             	add    $0x70,%eax
    2e76:	89 44 24 04          	mov    %eax,0x4(%esp)
    2e7a:	e8 e1 03 00 00       	call   3260 <memset>
    2e7f:	eb 12                	jmp    2e93 <sharedfd+0x73>
    2e81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(i = 0; i < 1000; i++){
    2e88:	83 c3 01             	add    $0x1,%ebx
    2e8b:	81 fb e8 03 00 00    	cmp    $0x3e8,%ebx
    2e91:	74 2d                	je     2ec0 <sharedfd+0xa0>
    if(write(fd, buf, sizeof(buf)) != sizeof(buf)){
    2e93:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
    2e9a:	00 
    2e9b:	89 74 24 04          	mov    %esi,0x4(%esp)
    2e9f:	89 3c 24             	mov    %edi,(%esp)
    2ea2:	e8 61 05 00 00       	call   3408 <write>
    2ea7:	83 f8 0a             	cmp    $0xa,%eax
    2eaa:	74 dc                	je     2e88 <sharedfd+0x68>
      printf(1, "fstests: write sharedfd failed\n");
    2eac:	c7 44 24 04 38 4d 00 	movl   $0x4d38,0x4(%esp)
    2eb3:	00 
    2eb4:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2ebb:	e8 60 06 00 00       	call   3520 <printf>
      break;
    }
  }
  if(pid == 0)
    2ec0:	8b 55 d4             	mov    -0x2c(%ebp),%edx
    2ec3:	85 d2                	test   %edx,%edx
    2ec5:	0f 84 0f 01 00 00    	je     2fda <sharedfd+0x1ba>
    exit();
  else
    wait();
    2ecb:	e8 20 05 00 00       	call   33f0 <wait>
  close(fd);
  fd = open("sharedfd", 0);
  if(fd < 0){
    2ed0:	31 db                	xor    %ebx,%ebx
  }
  if(pid == 0)
    exit();
  else
    wait();
  close(fd);
    2ed2:	89 3c 24             	mov    %edi,(%esp)
  fd = open("sharedfd", 0);
  if(fd < 0){
    2ed5:	31 ff                	xor    %edi,%edi
  }
  if(pid == 0)
    exit();
  else
    wait();
  close(fd);
    2ed7:	e8 34 05 00 00       	call   3410 <close>
  fd = open("sharedfd", 0);
    2edc:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    2ee3:	00 
    2ee4:	c7 04 24 31 46 00 00 	movl   $0x4631,(%esp)
    2eeb:	e8 38 05 00 00       	call   3428 <open>
  if(fd < 0){
    2ef0:	85 c0                	test   %eax,%eax
  if(pid == 0)
    exit();
  else
    wait();
  close(fd);
  fd = open("sharedfd", 0);
    2ef2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  if(fd < 0){
    2ef5:	0f 88 c9 00 00 00    	js     2fc4 <sharedfd+0x1a4>
    2efb:	90                   	nop
    2efc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    printf(1, "fstests: cannot open sharedfd for reading\n");
    return;
  }
  nc = np = 0;
  while((n = read(fd, buf, sizeof(buf))) > 0){
    2f00:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    2f03:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
    2f0a:	00 
    2f0b:	89 74 24 04          	mov    %esi,0x4(%esp)
    2f0f:	89 04 24             	mov    %eax,(%esp)
    2f12:	e8 e9 04 00 00       	call   3400 <read>
    2f17:	85 c0                	test   %eax,%eax
    2f19:	7e 26                	jle    2f41 <sharedfd+0x121>
    wait();
  close(fd);
  fd = open("sharedfd", 0);
  if(fd < 0){
    printf(1, "fstests: cannot open sharedfd for reading\n");
    return;
    2f1b:	31 c0                	xor    %eax,%eax
    2f1d:	eb 14                	jmp    2f33 <sharedfd+0x113>
    2f1f:	90                   	nop
  while((n = read(fd, buf, sizeof(buf))) > 0){
    for(i = 0; i < sizeof(buf); i++){
      if(buf[i] == 'c')
        nc++;
      if(buf[i] == 'p')
        np++;
    2f20:	80 fa 70             	cmp    $0x70,%dl
    2f23:	0f 94 c2             	sete   %dl
    2f26:	0f b6 d2             	movzbl %dl,%edx
    2f29:	01 d3                	add    %edx,%ebx
    printf(1, "fstests: cannot open sharedfd for reading\n");
    return;
  }
  nc = np = 0;
  while((n = read(fd, buf, sizeof(buf))) > 0){
    for(i = 0; i < sizeof(buf); i++){
    2f2b:	83 c0 01             	add    $0x1,%eax
    2f2e:	83 f8 0a             	cmp    $0xa,%eax
    2f31:	74 cd                	je     2f00 <sharedfd+0xe0>
      if(buf[i] == 'c')
    2f33:	0f b6 14 06          	movzbl (%esi,%eax,1),%edx
    2f37:	80 fa 63             	cmp    $0x63,%dl
    2f3a:	75 e4                	jne    2f20 <sharedfd+0x100>
        nc++;
    2f3c:	83 c7 01             	add    $0x1,%edi
    2f3f:	eb ea                	jmp    2f2b <sharedfd+0x10b>
      if(buf[i] == 'p')
        np++;
    }
  }
  close(fd);
    2f41:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    2f44:	89 04 24             	mov    %eax,(%esp)
    2f47:	e8 c4 04 00 00       	call   3410 <close>
  unlink("sharedfd");
    2f4c:	c7 04 24 31 46 00 00 	movl   $0x4631,(%esp)
    2f53:	e8 e0 04 00 00       	call   3438 <unlink>
  if(nc == 10000 && np == 10000)
    2f58:	81 fb 10 27 00 00    	cmp    $0x2710,%ebx
    2f5e:	74 24                	je     2f84 <sharedfd+0x164>
    printf(1, "sharedfd ok\n");
  else
    printf(1, "sharedfd oops %d %d\n", nc, np);
    2f60:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
    2f64:	89 7c 24 08          	mov    %edi,0x8(%esp)
    2f68:	c7 44 24 04 47 46 00 	movl   $0x4647,0x4(%esp)
    2f6f:	00 
    2f70:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2f77:	e8 a4 05 00 00       	call   3520 <printf>
}
    2f7c:	83 c4 3c             	add    $0x3c,%esp
    2f7f:	5b                   	pop    %ebx
    2f80:	5e                   	pop    %esi
    2f81:	5f                   	pop    %edi
    2f82:	5d                   	pop    %ebp
    2f83:	c3                   	ret    
        np++;
    }
  }
  close(fd);
  unlink("sharedfd");
  if(nc == 10000 && np == 10000)
    2f84:	81 ff 10 27 00 00    	cmp    $0x2710,%edi
    2f8a:	75 d4                	jne    2f60 <sharedfd+0x140>
    printf(1, "sharedfd ok\n");
    2f8c:	c7 44 24 04 3a 46 00 	movl   $0x463a,0x4(%esp)
    2f93:	00 
    2f94:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2f9b:	e8 80 05 00 00       	call   3520 <printf>
  else
    printf(1, "sharedfd oops %d %d\n", nc, np);
}
    2fa0:	83 c4 3c             	add    $0x3c,%esp
    2fa3:	5b                   	pop    %ebx
    2fa4:	5e                   	pop    %esi
    2fa5:	5f                   	pop    %edi
    2fa6:	5d                   	pop    %ebp
    2fa7:	c3                   	ret    
  char buf[10];

  unlink("sharedfd");
  fd = open("sharedfd", O_CREATE|O_RDWR);
  if(fd < 0){
    printf(1, "fstests: cannot open sharedfd for writing");
    2fa8:	c7 44 24 04 0c 4d 00 	movl   $0x4d0c,0x4(%esp)
    2faf:	00 
    2fb0:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2fb7:	e8 64 05 00 00       	call   3520 <printf>
  unlink("sharedfd");
  if(nc == 10000 && np == 10000)
    printf(1, "sharedfd ok\n");
  else
    printf(1, "sharedfd oops %d %d\n", nc, np);
}
    2fbc:	83 c4 3c             	add    $0x3c,%esp
    2fbf:	5b                   	pop    %ebx
    2fc0:	5e                   	pop    %esi
    2fc1:	5f                   	pop    %edi
    2fc2:	5d                   	pop    %ebp
    2fc3:	c3                   	ret    
  else
    wait();
  close(fd);
  fd = open("sharedfd", 0);
  if(fd < 0){
    printf(1, "fstests: cannot open sharedfd for reading\n");
    2fc4:	c7 44 24 04 58 4d 00 	movl   $0x4d58,0x4(%esp)
    2fcb:	00 
    2fcc:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2fd3:	e8 48 05 00 00       	call   3520 <printf>
    return;
    2fd8:	eb a2                	jmp    2f7c <sharedfd+0x15c>
      printf(1, "fstests: write sharedfd failed\n");
      break;
    }
  }
  if(pid == 0)
    exit();
    2fda:	e8 09 04 00 00       	call   33e8 <exit>
    2fdf:	90                   	nop

00002fe0 <mem>:
  printf(1, "exitwait ok\n");
}

void
mem(void)
{
    2fe0:	55                   	push   %ebp
    2fe1:	89 e5                	mov    %esp,%ebp
    2fe3:	57                   	push   %edi
    2fe4:	56                   	push   %esi
    2fe5:	53                   	push   %ebx
  void *m1, *m2;
  int pid, ppid;

  printf(1, "mem test\n");
  ppid = getpid();
  if((pid = fork()) == 0){
    2fe6:	31 db                	xor    %ebx,%ebx
  printf(1, "exitwait ok\n");
}

void
mem(void)
{
    2fe8:	83 ec 1c             	sub    $0x1c,%esp
  void *m1, *m2;
  int pid, ppid;

  printf(1, "mem test\n");
    2feb:	c7 44 24 04 5c 46 00 	movl   $0x465c,0x4(%esp)
    2ff2:	00 
    2ff3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    2ffa:	e8 21 05 00 00       	call   3520 <printf>
  ppid = getpid();
    2fff:	e8 64 04 00 00       	call   3468 <getpid>
    3004:	89 c6                	mov    %eax,%esi
  if((pid = fork()) == 0){
    3006:	e8 d5 03 00 00       	call   33e0 <fork>
    300b:	85 c0                	test   %eax,%eax
    300d:	74 0d                	je     301c <mem+0x3c>
    300f:	90                   	nop
    3010:	eb 5f                	jmp    3071 <mem+0x91>
    3012:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    m1 = 0;
    while((m2 = malloc(10001)) != 0){
      *(char**)m2 = m1;
    3018:	89 18                	mov    %ebx,(%eax)
    301a:	89 c3                	mov    %eax,%ebx

  printf(1, "mem test\n");
  ppid = getpid();
  if((pid = fork()) == 0){
    m1 = 0;
    while((m2 = malloc(10001)) != 0){
    301c:	c7 04 24 11 27 00 00 	movl   $0x2711,(%esp)
    3023:	e8 78 07 00 00       	call   37a0 <malloc>
    3028:	85 c0                	test   %eax,%eax
    302a:	75 ec                	jne    3018 <mem+0x38>
      *(char**)m2 = m1;
      m1 = m2;
    }
    while(m1){
    302c:	85 db                	test   %ebx,%ebx
    302e:	74 10                	je     3040 <mem+0x60>
      m2 = *(char**)m1;
    3030:	8b 3b                	mov    (%ebx),%edi
      free(m1);
    3032:	89 1c 24             	mov    %ebx,(%esp)
    3035:	e8 d6 06 00 00       	call   3710 <free>
    303a:	89 fb                	mov    %edi,%ebx
    m1 = 0;
    while((m2 = malloc(10001)) != 0){
      *(char**)m2 = m1;
      m1 = m2;
    }
    while(m1){
    303c:	85 db                	test   %ebx,%ebx
    303e:	75 f0                	jne    3030 <mem+0x50>
      m2 = *(char**)m1;
      free(m1);
      m1 = m2;
    }
    m1 = malloc(1024*20);
    3040:	c7 04 24 00 50 00 00 	movl   $0x5000,(%esp)
    3047:	e8 54 07 00 00       	call   37a0 <malloc>
    if(m1 == 0){
    304c:	85 c0                	test   %eax,%eax
    304e:	74 30                	je     3080 <mem+0xa0>
      printf(1, "couldn't allocate mem?!!\n");
      kill(ppid);
      exit();
    }
    free(m1);
    3050:	89 04 24             	mov    %eax,(%esp)
    3053:	e8 b8 06 00 00       	call   3710 <free>
    printf(1, "mem ok\n");
    3058:	c7 44 24 04 80 46 00 	movl   $0x4680,0x4(%esp)
    305f:	00 
    3060:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    3067:	e8 b4 04 00 00       	call   3520 <printf>
    exit();
    306c:	e8 77 03 00 00       	call   33e8 <exit>
  } else {
    wait();
  }
}
    3071:	83 c4 1c             	add    $0x1c,%esp
    3074:	5b                   	pop    %ebx
    3075:	5e                   	pop    %esi
    3076:	5f                   	pop    %edi
    3077:	5d                   	pop    %ebp
    }
    free(m1);
    printf(1, "mem ok\n");
    exit();
  } else {
    wait();
    3078:	e9 73 03 00 00       	jmp    33f0 <wait>
    307d:	8d 76 00             	lea    0x0(%esi),%esi
      free(m1);
      m1 = m2;
    }
    m1 = malloc(1024*20);
    if(m1 == 0){
      printf(1, "couldn't allocate mem?!!\n");
    3080:	c7 44 24 04 66 46 00 	movl   $0x4666,0x4(%esp)
    3087:	00 
    3088:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    308f:	e8 8c 04 00 00       	call   3520 <printf>
      kill(ppid);
    3094:	89 34 24             	mov    %esi,(%esp)
    3097:	e8 7c 03 00 00       	call   3418 <kill>
      exit();
    309c:	e8 47 03 00 00       	call   33e8 <exit>
    30a1:	eb 0d                	jmp    30b0 <main>
    30a3:	90                   	nop
    30a4:	90                   	nop
    30a5:	90                   	nop
    30a6:	90                   	nop
    30a7:	90                   	nop
    30a8:	90                   	nop
    30a9:	90                   	nop
    30aa:	90                   	nop
    30ab:	90                   	nop
    30ac:	90                   	nop
    30ad:	90                   	nop
    30ae:	90                   	nop
    30af:	90                   	nop

000030b0 <main>:
  wait();
}

int
main(int argc, char *argv[])
{
    30b0:	55                   	push   %ebp
    30b1:	89 e5                	mov    %esp,%ebp
    30b3:	83 e4 f0             	and    $0xfffffff0,%esp
    30b6:	83 ec 10             	sub    $0x10,%esp
  printf(1, "usertests starting\n");
    30b9:	c7 44 24 04 88 46 00 	movl   $0x4688,0x4(%esp)
    30c0:	00 
    30c1:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    30c8:	e8 53 04 00 00       	call   3520 <printf>

  if(open("usertests.ran", 0) >= 0){
    30cd:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    30d4:	00 
    30d5:	c7 04 24 9c 46 00 00 	movl   $0x469c,(%esp)
    30dc:	e8 47 03 00 00       	call   3428 <open>
    30e1:	85 c0                	test   %eax,%eax
    30e3:	78 1b                	js     3100 <main+0x50>
    printf(1, "already ran user tests -- rebuild fs.img\n");
    30e5:	c7 44 24 04 84 4d 00 	movl   $0x4d84,0x4(%esp)
    30ec:	00 
    30ed:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    30f4:	e8 27 04 00 00       	call   3520 <printf>
    exit();
    30f9:	e8 ea 02 00 00       	call   33e8 <exit>
    30fe:	66 90                	xchg   %ax,%ax
  }
  close(open("usertests.ran", O_CREATE));
    3100:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
    3107:	00 
    3108:	c7 04 24 9c 46 00 00 	movl   $0x469c,(%esp)
    310f:	e8 14 03 00 00       	call   3428 <open>
    3114:	89 04 24             	mov    %eax,(%esp)
    3117:	e8 f4 02 00 00       	call   3410 <close>

  bigargtest();
    311c:	e8 5f d0 ff ff       	call   180 <bigargtest>
  bsstest();
    3121:	e8 ea ce ff ff       	call   10 <bsstest>
  sbrktest();
    3126:	e8 15 d3 ff ff       	call   440 <sbrktest>
    312b:	90                   	nop
    312c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  validatetest();
    3130:	e8 5b d2 ff ff       	call   390 <validatetest>

  opentest();
    3135:	e8 56 cf ff ff       	call   90 <opentest>
  writetest();
    313a:	e8 f1 f3 ff ff       	call   2530 <writetest>
    313f:	90                   	nop
  writetest1();
    3140:	e8 fb f1 ff ff       	call   2340 <writetest1>
  createtest();
    3145:	e8 46 f1 ff ff       	call   2290 <createtest>

  mem();
    314a:	e8 91 fe ff ff       	call   2fe0 <mem>
    314f:	90                   	nop
  pipe1();
    3150:	e8 fb d8 ff ff       	call   a50 <pipe1>
  preempt();
    3155:	e8 96 d7 ff ff       	call   8f0 <preempt>
  exitwait();
    315a:	e8 a1 d1 ff ff       	call   300 <exitwait>
    315f:	90                   	nop

  rmdot();
    3160:	e8 4b df ff ff       	call   10b0 <rmdot>
  fourteen();
    3165:	e8 86 da ff ff       	call   bf0 <fourteen>
  bigfile();
    316a:	e8 d1 f5 ff ff       	call   2740 <bigfile>
    316f:	90                   	nop
  subdir();
    3170:	e8 cb e0 ff ff       	call   1240 <subdir>
  concreate();
    3175:	e8 c6 f7 ff ff       	call   2940 <concreate>
  linktest();
    317a:	e8 a1 e9 ff ff       	call   1b20 <linktest>
    317f:	90                   	nop
  unlinkread();
    3180:	e8 fb eb ff ff       	call   1d80 <unlinkread>
  createdelete();
    3185:	e8 c6 ed ff ff       	call   1f50 <createdelete>
  twofiles();
    318a:	e8 71 fa ff ff       	call   2c00 <twofiles>
    318f:	90                   	nop
  sharedfd();
    3190:	e8 8b fc ff ff       	call   2e20 <sharedfd>
  dirfile();
    3195:	e8 d6 dc ff ff       	call   e70 <dirfile>
  iref();
    319a:	e8 b1 db ff ff       	call   d50 <iref>
    319f:	90                   	nop
  forktest();
    31a0:	e8 8b d0 ff ff       	call   230 <forktest>
  bigdir(); // slow
    31a5:	e8 26 e8 ff ff       	call   19d0 <bigdir>

  exectest();
    31aa:	e8 81 cf ff ff       	call   130 <exectest>
    31af:	90                   	nop

  exit();
    31b0:	e8 33 02 00 00       	call   33e8 <exit>
    31b5:	90                   	nop
    31b6:	90                   	nop
    31b7:	90                   	nop
    31b8:	90                   	nop
    31b9:	90                   	nop
    31ba:	90                   	nop
    31bb:	90                   	nop
    31bc:	90                   	nop
    31bd:	90                   	nop
    31be:	90                   	nop
    31bf:	90                   	nop

000031c0 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
    31c0:	55                   	push   %ebp
    31c1:	31 d2                	xor    %edx,%edx
    31c3:	89 e5                	mov    %esp,%ebp
    31c5:	8b 45 08             	mov    0x8(%ebp),%eax
    31c8:	53                   	push   %ebx
    31c9:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    31cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
    31d0:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
    31d4:	88 0c 10             	mov    %cl,(%eax,%edx,1)
    31d7:	83 c2 01             	add    $0x1,%edx
    31da:	84 c9                	test   %cl,%cl
    31dc:	75 f2                	jne    31d0 <strcpy+0x10>
    ;
  return os;
}
    31de:	5b                   	pop    %ebx
    31df:	5d                   	pop    %ebp
    31e0:	c3                   	ret    
    31e1:	eb 0d                	jmp    31f0 <strcmp>
    31e3:	90                   	nop
    31e4:	90                   	nop
    31e5:	90                   	nop
    31e6:	90                   	nop
    31e7:	90                   	nop
    31e8:	90                   	nop
    31e9:	90                   	nop
    31ea:	90                   	nop
    31eb:	90                   	nop
    31ec:	90                   	nop
    31ed:	90                   	nop
    31ee:	90                   	nop
    31ef:	90                   	nop

000031f0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
    31f0:	55                   	push   %ebp
    31f1:	89 e5                	mov    %esp,%ebp
    31f3:	8b 4d 08             	mov    0x8(%ebp),%ecx
    31f6:	53                   	push   %ebx
    31f7:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
    31fa:	0f b6 01             	movzbl (%ecx),%eax
    31fd:	84 c0                	test   %al,%al
    31ff:	75 14                	jne    3215 <strcmp+0x25>
    3201:	eb 25                	jmp    3228 <strcmp+0x38>
    3203:	90                   	nop
    3204:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    p++, q++;
    3208:	83 c1 01             	add    $0x1,%ecx
    320b:	83 c2 01             	add    $0x1,%edx
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
    320e:	0f b6 01             	movzbl (%ecx),%eax
    3211:	84 c0                	test   %al,%al
    3213:	74 13                	je     3228 <strcmp+0x38>
    3215:	0f b6 1a             	movzbl (%edx),%ebx
    3218:	38 d8                	cmp    %bl,%al
    321a:	74 ec                	je     3208 <strcmp+0x18>
    321c:	0f b6 db             	movzbl %bl,%ebx
    321f:	0f b6 c0             	movzbl %al,%eax
    3222:	29 d8                	sub    %ebx,%eax
    p++, q++;
  return (uchar)*p - (uchar)*q;
}
    3224:	5b                   	pop    %ebx
    3225:	5d                   	pop    %ebp
    3226:	c3                   	ret    
    3227:	90                   	nop
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
    3228:	0f b6 1a             	movzbl (%edx),%ebx
    322b:	31 c0                	xor    %eax,%eax
    322d:	0f b6 db             	movzbl %bl,%ebx
    3230:	29 d8                	sub    %ebx,%eax
    p++, q++;
  return (uchar)*p - (uchar)*q;
}
    3232:	5b                   	pop    %ebx
    3233:	5d                   	pop    %ebp
    3234:	c3                   	ret    
    3235:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    3239:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00003240 <strlen>:

uint
strlen(char *s)
{
    3240:	55                   	push   %ebp
  int n;

  for(n = 0; s[n]; n++)
    3241:	31 d2                	xor    %edx,%edx
  return (uchar)*p - (uchar)*q;
}

uint
strlen(char *s)
{
    3243:	89 e5                	mov    %esp,%ebp
  int n;

  for(n = 0; s[n]; n++)
    3245:	31 c0                	xor    %eax,%eax
  return (uchar)*p - (uchar)*q;
}

uint
strlen(char *s)
{
    3247:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
    324a:	80 39 00             	cmpb   $0x0,(%ecx)
    324d:	74 0c                	je     325b <strlen+0x1b>
    324f:	90                   	nop
    3250:	83 c2 01             	add    $0x1,%edx
    3253:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
    3257:	89 d0                	mov    %edx,%eax
    3259:	75 f5                	jne    3250 <strlen+0x10>
    ;
  return n;
}
    325b:	5d                   	pop    %ebp
    325c:	c3                   	ret    
    325d:	8d 76 00             	lea    0x0(%esi),%esi

00003260 <memset>:

void*
memset(void *dst, int c, uint n)
{
    3260:	55                   	push   %ebp
    3261:	89 e5                	mov    %esp,%ebp
    3263:	8b 55 08             	mov    0x8(%ebp),%edx
    3266:	57                   	push   %edi
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
    3267:	8b 4d 10             	mov    0x10(%ebp),%ecx
    326a:	8b 45 0c             	mov    0xc(%ebp),%eax
    326d:	89 d7                	mov    %edx,%edi
    326f:	fc                   	cld    
    3270:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
    3272:	89 d0                	mov    %edx,%eax
    3274:	5f                   	pop    %edi
    3275:	5d                   	pop    %ebp
    3276:	c3                   	ret    
    3277:	89 f6                	mov    %esi,%esi
    3279:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00003280 <strchr>:

char*
strchr(const char *s, char c)
{
    3280:	55                   	push   %ebp
    3281:	89 e5                	mov    %esp,%ebp
    3283:	8b 45 08             	mov    0x8(%ebp),%eax
    3286:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
    328a:	0f b6 10             	movzbl (%eax),%edx
    328d:	84 d2                	test   %dl,%dl
    328f:	75 11                	jne    32a2 <strchr+0x22>
    3291:	eb 15                	jmp    32a8 <strchr+0x28>
    3293:	90                   	nop
    3294:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    3298:	83 c0 01             	add    $0x1,%eax
    329b:	0f b6 10             	movzbl (%eax),%edx
    329e:	84 d2                	test   %dl,%dl
    32a0:	74 06                	je     32a8 <strchr+0x28>
    if(*s == c)
    32a2:	38 ca                	cmp    %cl,%dl
    32a4:	75 f2                	jne    3298 <strchr+0x18>
      return (char*)s;
  return 0;
}
    32a6:	5d                   	pop    %ebp
    32a7:	c3                   	ret    
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
    32a8:	31 c0                	xor    %eax,%eax
    if(*s == c)
      return (char*)s;
  return 0;
}
    32aa:	5d                   	pop    %ebp
    32ab:	90                   	nop
    32ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    32b0:	c3                   	ret    
    32b1:	eb 0d                	jmp    32c0 <atoi>
    32b3:	90                   	nop
    32b4:	90                   	nop
    32b5:	90                   	nop
    32b6:	90                   	nop
    32b7:	90                   	nop
    32b8:	90                   	nop
    32b9:	90                   	nop
    32ba:	90                   	nop
    32bb:	90                   	nop
    32bc:	90                   	nop
    32bd:	90                   	nop
    32be:	90                   	nop
    32bf:	90                   	nop

000032c0 <atoi>:
  return r;
}

int
atoi(const char *s)
{
    32c0:	55                   	push   %ebp
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
    32c1:	31 c0                	xor    %eax,%eax
  return r;
}

int
atoi(const char *s)
{
    32c3:	89 e5                	mov    %esp,%ebp
    32c5:	8b 4d 08             	mov    0x8(%ebp),%ecx
    32c8:	53                   	push   %ebx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
    32c9:	0f b6 11             	movzbl (%ecx),%edx
    32cc:	8d 5a d0             	lea    -0x30(%edx),%ebx
    32cf:	80 fb 09             	cmp    $0x9,%bl
    32d2:	77 1c                	ja     32f0 <atoi+0x30>
    32d4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    n = n*10 + *s++ - '0';
    32d8:	0f be d2             	movsbl %dl,%edx
    32db:	83 c1 01             	add    $0x1,%ecx
    32de:	8d 04 80             	lea    (%eax,%eax,4),%eax
    32e1:	8d 44 42 d0          	lea    -0x30(%edx,%eax,2),%eax
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
    32e5:	0f b6 11             	movzbl (%ecx),%edx
    32e8:	8d 5a d0             	lea    -0x30(%edx),%ebx
    32eb:	80 fb 09             	cmp    $0x9,%bl
    32ee:	76 e8                	jbe    32d8 <atoi+0x18>
    n = n*10 + *s++ - '0';
  return n;
}
    32f0:	5b                   	pop    %ebx
    32f1:	5d                   	pop    %ebp
    32f2:	c3                   	ret    
    32f3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    32f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

00003300 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
    3300:	55                   	push   %ebp
    3301:	89 e5                	mov    %esp,%ebp
    3303:	56                   	push   %esi
    3304:	8b 45 08             	mov    0x8(%ebp),%eax
    3307:	53                   	push   %ebx
    3308:	8b 5d 10             	mov    0x10(%ebp),%ebx
    330b:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
    330e:	85 db                	test   %ebx,%ebx
    3310:	7e 14                	jle    3326 <memmove+0x26>
    n = n*10 + *s++ - '0';
  return n;
}

void*
memmove(void *vdst, void *vsrc, int n)
    3312:	31 d2                	xor    %edx,%edx
    3314:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
    *dst++ = *src++;
    3318:	0f b6 0c 16          	movzbl (%esi,%edx,1),%ecx
    331c:	88 0c 10             	mov    %cl,(%eax,%edx,1)
    331f:	83 c2 01             	add    $0x1,%edx
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
    3322:	39 da                	cmp    %ebx,%edx
    3324:	75 f2                	jne    3318 <memmove+0x18>
    *dst++ = *src++;
  return vdst;
}
    3326:	5b                   	pop    %ebx
    3327:	5e                   	pop    %esi
    3328:	5d                   	pop    %ebp
    3329:	c3                   	ret    
    332a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

00003330 <stat>:
  return buf;
}

int
stat(char *n, struct stat *st)
{
    3330:	55                   	push   %ebp
    3331:	89 e5                	mov    %esp,%ebp
    3333:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    3336:	8b 45 08             	mov    0x8(%ebp),%eax
  return buf;
}

int
stat(char *n, struct stat *st)
{
    3339:	89 5d f8             	mov    %ebx,-0x8(%ebp)
    333c:	89 75 fc             	mov    %esi,-0x4(%ebp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
  if(fd < 0)
    333f:	be ff ff ff ff       	mov    $0xffffffff,%esi
stat(char *n, struct stat *st)
{
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    3344:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
    334b:	00 
    334c:	89 04 24             	mov    %eax,(%esp)
    334f:	e8 d4 00 00 00       	call   3428 <open>
  if(fd < 0)
    3354:	85 c0                	test   %eax,%eax
stat(char *n, struct stat *st)
{
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    3356:	89 c3                	mov    %eax,%ebx
  if(fd < 0)
    3358:	78 19                	js     3373 <stat+0x43>
    return -1;
  r = fstat(fd, st);
    335a:	8b 45 0c             	mov    0xc(%ebp),%eax
    335d:	89 1c 24             	mov    %ebx,(%esp)
    3360:	89 44 24 04          	mov    %eax,0x4(%esp)
    3364:	e8 d7 00 00 00       	call   3440 <fstat>
  close(fd);
    3369:	89 1c 24             	mov    %ebx,(%esp)
  int r;

  fd = open(n, O_RDONLY);
  if(fd < 0)
    return -1;
  r = fstat(fd, st);
    336c:	89 c6                	mov    %eax,%esi
  close(fd);
    336e:	e8 9d 00 00 00       	call   3410 <close>
  return r;
}
    3373:	89 f0                	mov    %esi,%eax
    3375:	8b 5d f8             	mov    -0x8(%ebp),%ebx
    3378:	8b 75 fc             	mov    -0x4(%ebp),%esi
    337b:	89 ec                	mov    %ebp,%esp
    337d:	5d                   	pop    %ebp
    337e:	c3                   	ret    
    337f:	90                   	nop

00003380 <gets>:
  return 0;
}

char*
gets(char *buf, int max)
{
    3380:	55                   	push   %ebp
    3381:	89 e5                	mov    %esp,%ebp
    3383:	57                   	push   %edi
    3384:	56                   	push   %esi
    3385:	31 f6                	xor    %esi,%esi
    3387:	53                   	push   %ebx
    3388:	83 ec 2c             	sub    $0x2c,%esp
    338b:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    338e:	eb 06                	jmp    3396 <gets+0x16>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
    buf[i++] = c;
    if(c == '\n' || c == '\r')
    3390:	3c 0a                	cmp    $0xa,%al
    3392:	74 39                	je     33cd <gets+0x4d>
    3394:	89 de                	mov    %ebx,%esi
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    3396:	8d 5e 01             	lea    0x1(%esi),%ebx
    3399:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
    339c:	7d 31                	jge    33cf <gets+0x4f>
    cc = read(0, &c, 1);
    339e:	8d 45 e7             	lea    -0x19(%ebp),%eax
    33a1:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
    33a8:	00 
    33a9:	89 44 24 04          	mov    %eax,0x4(%esp)
    33ad:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
    33b4:	e8 47 00 00 00       	call   3400 <read>
    if(cc < 1)
    33b9:	85 c0                	test   %eax,%eax
    33bb:	7e 12                	jle    33cf <gets+0x4f>
      break;
    buf[i++] = c;
    33bd:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
    33c1:	88 44 1f ff          	mov    %al,-0x1(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
    33c5:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
    33c9:	3c 0d                	cmp    $0xd,%al
    33cb:	75 c3                	jne    3390 <gets+0x10>
    33cd:	89 de                	mov    %ebx,%esi
      break;
  }
  buf[i] = '\0';
    33cf:	c6 04 37 00          	movb   $0x0,(%edi,%esi,1)
  return buf;
}
    33d3:	89 f8                	mov    %edi,%eax
    33d5:	83 c4 2c             	add    $0x2c,%esp
    33d8:	5b                   	pop    %ebx
    33d9:	5e                   	pop    %esi
    33da:	5f                   	pop    %edi
    33db:	5d                   	pop    %ebp
    33dc:	c3                   	ret    
    33dd:	90                   	nop
    33de:	90                   	nop
    33df:	90                   	nop

000033e0 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
    33e0:	b8 01 00 00 00       	mov    $0x1,%eax
    33e5:	cd 40                	int    $0x40
    33e7:	c3                   	ret    

000033e8 <exit>:
SYSCALL(exit)
    33e8:	b8 02 00 00 00       	mov    $0x2,%eax
    33ed:	cd 40                	int    $0x40
    33ef:	c3                   	ret    

000033f0 <wait>:
SYSCALL(wait)
    33f0:	b8 03 00 00 00       	mov    $0x3,%eax
    33f5:	cd 40                	int    $0x40
    33f7:	c3                   	ret    

000033f8 <pipe>:
SYSCALL(pipe)
    33f8:	b8 04 00 00 00       	mov    $0x4,%eax
    33fd:	cd 40                	int    $0x40
    33ff:	c3                   	ret    

00003400 <read>:
SYSCALL(read)
    3400:	b8 06 00 00 00       	mov    $0x6,%eax
    3405:	cd 40                	int    $0x40
    3407:	c3                   	ret    

00003408 <write>:
SYSCALL(write)
    3408:	b8 05 00 00 00       	mov    $0x5,%eax
    340d:	cd 40                	int    $0x40
    340f:	c3                   	ret    

00003410 <close>:
SYSCALL(close)
    3410:	b8 07 00 00 00       	mov    $0x7,%eax
    3415:	cd 40                	int    $0x40
    3417:	c3                   	ret    

00003418 <kill>:
SYSCALL(kill)
    3418:	b8 08 00 00 00       	mov    $0x8,%eax
    341d:	cd 40                	int    $0x40
    341f:	c3                   	ret    

00003420 <exec>:
SYSCALL(exec)
    3420:	b8 09 00 00 00       	mov    $0x9,%eax
    3425:	cd 40                	int    $0x40
    3427:	c3                   	ret    

00003428 <open>:
SYSCALL(open)
    3428:	b8 0a 00 00 00       	mov    $0xa,%eax
    342d:	cd 40                	int    $0x40
    342f:	c3                   	ret    

00003430 <mknod>:
SYSCALL(mknod)
    3430:	b8 0b 00 00 00       	mov    $0xb,%eax
    3435:	cd 40                	int    $0x40
    3437:	c3                   	ret    

00003438 <unlink>:
SYSCALL(unlink)
    3438:	b8 0c 00 00 00       	mov    $0xc,%eax
    343d:	cd 40                	int    $0x40
    343f:	c3                   	ret    

00003440 <fstat>:
SYSCALL(fstat)
    3440:	b8 0d 00 00 00       	mov    $0xd,%eax
    3445:	cd 40                	int    $0x40
    3447:	c3                   	ret    

00003448 <link>:
SYSCALL(link)
    3448:	b8 0e 00 00 00       	mov    $0xe,%eax
    344d:	cd 40                	int    $0x40
    344f:	c3                   	ret    

00003450 <mkdir>:
SYSCALL(mkdir)
    3450:	b8 0f 00 00 00       	mov    $0xf,%eax
    3455:	cd 40                	int    $0x40
    3457:	c3                   	ret    

00003458 <chdir>:
SYSCALL(chdir)
    3458:	b8 10 00 00 00       	mov    $0x10,%eax
    345d:	cd 40                	int    $0x40
    345f:	c3                   	ret    

00003460 <dup>:
SYSCALL(dup)
    3460:	b8 11 00 00 00       	mov    $0x11,%eax
    3465:	cd 40                	int    $0x40
    3467:	c3                   	ret    

00003468 <getpid>:
SYSCALL(getpid)
    3468:	b8 12 00 00 00       	mov    $0x12,%eax
    346d:	cd 40                	int    $0x40
    346f:	c3                   	ret    

00003470 <sbrk>:
SYSCALL(sbrk)
    3470:	b8 13 00 00 00       	mov    $0x13,%eax
    3475:	cd 40                	int    $0x40
    3477:	c3                   	ret    

00003478 <sleep>:
SYSCALL(sleep)
    3478:	b8 14 00 00 00       	mov    $0x14,%eax
    347d:	cd 40                	int    $0x40
    347f:	c3                   	ret    

00003480 <uptime>:
SYSCALL(uptime)
    3480:	b8 15 00 00 00       	mov    $0x15,%eax
    3485:	cd 40                	int    $0x40
    3487:	c3                   	ret    
    3488:	90                   	nop
    3489:	90                   	nop
    348a:	90                   	nop
    348b:	90                   	nop
    348c:	90                   	nop
    348d:	90                   	nop
    348e:	90                   	nop
    348f:	90                   	nop

00003490 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
    3490:	55                   	push   %ebp
    3491:	89 e5                	mov    %esp,%ebp
    3493:	57                   	push   %edi
    3494:	89 cf                	mov    %ecx,%edi
    3496:	56                   	push   %esi
    3497:	89 c6                	mov    %eax,%esi
    3499:	53                   	push   %ebx
    349a:	83 ec 4c             	sub    $0x4c,%esp
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    349d:	8b 4d 08             	mov    0x8(%ebp),%ecx
    34a0:	85 c9                	test   %ecx,%ecx
    34a2:	74 04                	je     34a8 <printint+0x18>
    34a4:	85 d2                	test   %edx,%edx
    34a6:	78 68                	js     3510 <printint+0x80>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
    34a8:	89 d0                	mov    %edx,%eax
    34aa:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%ebp)
    34b1:	31 c9                	xor    %ecx,%ecx
    34b3:	8d 5d d7             	lea    -0x29(%ebp),%ebx
    34b6:	66 90                	xchg   %ax,%ax
  }

  i = 0;
  do{
    buf[i++] = digits[x % base];
    34b8:	31 d2                	xor    %edx,%edx
    34ba:	f7 f7                	div    %edi
    34bc:	0f b6 92 b7 4d 00 00 	movzbl 0x4db7(%edx),%edx
    34c3:	88 14 0b             	mov    %dl,(%ebx,%ecx,1)
    34c6:	83 c1 01             	add    $0x1,%ecx
  }while((x /= base) != 0);
    34c9:	85 c0                	test   %eax,%eax
    34cb:	75 eb                	jne    34b8 <printint+0x28>
  if(neg)
    34cd:	8b 45 c4             	mov    -0x3c(%ebp),%eax
    34d0:	85 c0                	test   %eax,%eax
    34d2:	74 08                	je     34dc <printint+0x4c>
    buf[i++] = '-';
    34d4:	c6 44 0d d7 2d       	movb   $0x2d,-0x29(%ebp,%ecx,1)
    34d9:	83 c1 01             	add    $0x1,%ecx

  while(--i >= 0)
    34dc:	8d 79 ff             	lea    -0x1(%ecx),%edi
    34df:	90                   	nop
    34e0:	0f b6 04 3b          	movzbl (%ebx,%edi,1),%eax
    34e4:	83 ef 01             	sub    $0x1,%edi
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    34e7:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
    34ee:	00 
    34ef:	89 34 24             	mov    %esi,(%esp)
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
    34f2:	88 45 e7             	mov    %al,-0x19(%ebp)
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    34f5:	8d 45 e7             	lea    -0x19(%ebp),%eax
    34f8:	89 44 24 04          	mov    %eax,0x4(%esp)
    34fc:	e8 07 ff ff ff       	call   3408 <write>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
    3501:	83 ff ff             	cmp    $0xffffffff,%edi
    3504:	75 da                	jne    34e0 <printint+0x50>
    putc(fd, buf[i]);
}
    3506:	83 c4 4c             	add    $0x4c,%esp
    3509:	5b                   	pop    %ebx
    350a:	5e                   	pop    %esi
    350b:	5f                   	pop    %edi
    350c:	5d                   	pop    %ebp
    350d:	c3                   	ret    
    350e:	66 90                	xchg   %ax,%ax
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
    3510:	89 d0                	mov    %edx,%eax
    3512:	f7 d8                	neg    %eax
    3514:	c7 45 c4 01 00 00 00 	movl   $0x1,-0x3c(%ebp)
    351b:	eb 94                	jmp    34b1 <printint+0x21>
    351d:	8d 76 00             	lea    0x0(%esi),%esi

00003520 <printf>:
}

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
    3520:	55                   	push   %ebp
    3521:	89 e5                	mov    %esp,%ebp
    3523:	57                   	push   %edi
    3524:	56                   	push   %esi
    3525:	53                   	push   %ebx
    3526:	83 ec 3c             	sub    $0x3c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
    3529:	8b 45 0c             	mov    0xc(%ebp),%eax
    352c:	0f b6 10             	movzbl (%eax),%edx
    352f:	84 d2                	test   %dl,%dl
    3531:	0f 84 c1 00 00 00    	je     35f8 <printf+0xd8>
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
    3537:	8d 4d 10             	lea    0x10(%ebp),%ecx
    353a:	31 ff                	xor    %edi,%edi
    353c:	89 4d d4             	mov    %ecx,-0x2c(%ebp)
    353f:	31 db                	xor    %ebx,%ebx
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    3541:	8d 75 e7             	lea    -0x19(%ebp),%esi
    3544:	eb 1e                	jmp    3564 <printf+0x44>
    3546:	66 90                	xchg   %ax,%ax
  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
    3548:	83 fa 25             	cmp    $0x25,%edx
    354b:	0f 85 af 00 00 00    	jne    3600 <printf+0xe0>
    3551:	66 bf 25 00          	mov    $0x25,%di
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
    3555:	83 c3 01             	add    $0x1,%ebx
    3558:	0f b6 14 18          	movzbl (%eax,%ebx,1),%edx
    355c:	84 d2                	test   %dl,%dl
    355e:	0f 84 94 00 00 00    	je     35f8 <printf+0xd8>
    c = fmt[i] & 0xff;
    if(state == 0){
    3564:	85 ff                	test   %edi,%edi
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
    c = fmt[i] & 0xff;
    3566:	0f b6 d2             	movzbl %dl,%edx
    if(state == 0){
    3569:	74 dd                	je     3548 <printf+0x28>
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
      }
    } else if(state == '%'){
    356b:	83 ff 25             	cmp    $0x25,%edi
    356e:	75 e5                	jne    3555 <printf+0x35>
      if(c == 'd'){
    3570:	83 fa 64             	cmp    $0x64,%edx
    3573:	0f 84 3f 01 00 00    	je     36b8 <printf+0x198>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
    3579:	83 fa 70             	cmp    $0x70,%edx
    357c:	0f 84 a6 00 00 00    	je     3628 <printf+0x108>
    3582:	83 fa 78             	cmp    $0x78,%edx
    3585:	0f 84 9d 00 00 00    	je     3628 <printf+0x108>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
    358b:	83 fa 73             	cmp    $0x73,%edx
    358e:	66 90                	xchg   %ax,%ax
    3590:	0f 84 ba 00 00 00    	je     3650 <printf+0x130>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
    3596:	83 fa 63             	cmp    $0x63,%edx
    3599:	0f 84 41 01 00 00    	je     36e0 <printf+0x1c0>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
    359f:	83 fa 25             	cmp    $0x25,%edx
    35a2:	0f 84 00 01 00 00    	je     36a8 <printf+0x188>
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    35a8:	8b 4d 08             	mov    0x8(%ebp),%ecx
    35ab:	89 55 cc             	mov    %edx,-0x34(%ebp)
    35ae:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
    35b2:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
    35b9:	00 
    35ba:	89 74 24 04          	mov    %esi,0x4(%esp)
    35be:	89 0c 24             	mov    %ecx,(%esp)
    35c1:	e8 42 fe ff ff       	call   3408 <write>
    35c6:	8b 55 cc             	mov    -0x34(%ebp),%edx
    35c9:	88 55 e7             	mov    %dl,-0x19(%ebp)
    35cc:	8b 45 08             	mov    0x8(%ebp),%eax
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
    35cf:	83 c3 01             	add    $0x1,%ebx
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    35d2:	31 ff                	xor    %edi,%edi
    35d4:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
    35db:	00 
    35dc:	89 74 24 04          	mov    %esi,0x4(%esp)
    35e0:	89 04 24             	mov    %eax,(%esp)
    35e3:	e8 20 fe ff ff       	call   3408 <write>
    35e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
    35eb:	0f b6 14 18          	movzbl (%eax,%ebx,1),%edx
    35ef:	84 d2                	test   %dl,%dl
    35f1:	0f 85 6d ff ff ff    	jne    3564 <printf+0x44>
    35f7:	90                   	nop
        putc(fd, c);
      }
      state = 0;
    }
  }
}
    35f8:	83 c4 3c             	add    $0x3c,%esp
    35fb:	5b                   	pop    %ebx
    35fc:	5e                   	pop    %esi
    35fd:	5f                   	pop    %edi
    35fe:	5d                   	pop    %ebp
    35ff:	c3                   	ret    
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    3600:	8b 45 08             	mov    0x8(%ebp),%eax
  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
    3603:	88 55 e7             	mov    %dl,-0x19(%ebp)
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    3606:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
    360d:	00 
    360e:	89 74 24 04          	mov    %esi,0x4(%esp)
    3612:	89 04 24             	mov    %eax,(%esp)
    3615:	e8 ee fd ff ff       	call   3408 <write>
    361a:	8b 45 0c             	mov    0xc(%ebp),%eax
    361d:	e9 33 ff ff ff       	jmp    3555 <printf+0x35>
    3622:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
        printint(fd, *ap, 16, 0);
    3628:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    362b:	b9 10 00 00 00       	mov    $0x10,%ecx
        ap++;
    3630:	31 ff                	xor    %edi,%edi
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
        printint(fd, *ap, 16, 0);
    3632:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
    3639:	8b 10                	mov    (%eax),%edx
    363b:	8b 45 08             	mov    0x8(%ebp),%eax
    363e:	e8 4d fe ff ff       	call   3490 <printint>
    3643:	8b 45 0c             	mov    0xc(%ebp),%eax
        ap++;
    3646:	83 45 d4 04          	addl   $0x4,-0x2c(%ebp)
    364a:	e9 06 ff ff ff       	jmp    3555 <printf+0x35>
    364f:	90                   	nop
      } else if(c == 's'){
        s = (char*)*ap;
    3650:	8b 55 d4             	mov    -0x2c(%ebp),%edx
        ap++;
        if(s == 0)
    3653:	b9 b0 4d 00 00       	mov    $0x4db0,%ecx
        ap++;
      } else if(c == 'x' || c == 'p'){
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
        s = (char*)*ap;
    3658:	8b 3a                	mov    (%edx),%edi
        ap++;
    365a:	83 c2 04             	add    $0x4,%edx
    365d:	89 55 d4             	mov    %edx,-0x2c(%ebp)
        if(s == 0)
    3660:	85 ff                	test   %edi,%edi
    3662:	0f 44 f9             	cmove  %ecx,%edi
          s = "(null)";
        while(*s != 0){
    3665:	0f b6 17             	movzbl (%edi),%edx
    3668:	84 d2                	test   %dl,%dl
    366a:	74 33                	je     369f <printf+0x17f>
    366c:	89 5d d0             	mov    %ebx,-0x30(%ebp)
    366f:	8b 5d 08             	mov    0x8(%ebp),%ebx
    3672:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
          putc(fd, *s);
          s++;
    3678:	83 c7 01             	add    $0x1,%edi
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
    367b:	88 55 e7             	mov    %dl,-0x19(%ebp)
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    367e:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
    3685:	00 
    3686:	89 74 24 04          	mov    %esi,0x4(%esp)
    368a:	89 1c 24             	mov    %ebx,(%esp)
    368d:	e8 76 fd ff ff       	call   3408 <write>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
    3692:	0f b6 17             	movzbl (%edi),%edx
    3695:	84 d2                	test   %dl,%dl
    3697:	75 df                	jne    3678 <printf+0x158>
    3699:	8b 5d d0             	mov    -0x30(%ebp),%ebx
    369c:	8b 45 0c             	mov    0xc(%ebp),%eax
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    369f:	31 ff                	xor    %edi,%edi
    36a1:	e9 af fe ff ff       	jmp    3555 <printf+0x35>
    36a6:	66 90                	xchg   %ax,%ax
          s++;
        }
      } else if(c == 'c'){
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
    36a8:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
    36ac:	e9 1b ff ff ff       	jmp    35cc <printf+0xac>
    36b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      } else {
        putc(fd, c);
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
    36b8:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    36bb:	b9 0a 00 00 00       	mov    $0xa,%ecx
        ap++;
    36c0:	66 31 ff             	xor    %di,%di
      } else {
        putc(fd, c);
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
    36c3:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
    36ca:	8b 10                	mov    (%eax),%edx
    36cc:	8b 45 08             	mov    0x8(%ebp),%eax
    36cf:	e8 bc fd ff ff       	call   3490 <printint>
    36d4:	8b 45 0c             	mov    0xc(%ebp),%eax
        ap++;
    36d7:	83 45 d4 04          	addl   $0x4,-0x2c(%ebp)
    36db:	e9 75 fe ff ff       	jmp    3555 <printf+0x35>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
    36e0:	8b 55 d4             	mov    -0x2c(%ebp),%edx
        putc(fd, *ap);
        ap++;
    36e3:	31 ff                	xor    %edi,%edi
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    36e5:	8b 4d 08             	mov    0x8(%ebp),%ecx
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
    36e8:	8b 02                	mov    (%edx),%eax
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    36ea:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
    36f1:	00 
    36f2:	89 74 24 04          	mov    %esi,0x4(%esp)
    36f6:	89 0c 24             	mov    %ecx,(%esp)
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
    36f9:	88 45 e7             	mov    %al,-0x19(%ebp)
#include "user.h"

static void
putc(int fd, char c)
{
  write(fd, &c, 1);
    36fc:	e8 07 fd ff ff       	call   3408 <write>
    3701:	8b 45 0c             	mov    0xc(%ebp),%eax
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
        putc(fd, *ap);
        ap++;
    3704:	83 45 d4 04          	addl   $0x4,-0x2c(%ebp)
    3708:	e9 48 fe ff ff       	jmp    3555 <printf+0x35>
    370d:	90                   	nop
    370e:	90                   	nop
    370f:	90                   	nop

00003710 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
    3710:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    3711:	a1 e8 4d 00 00       	mov    0x4de8,%eax
static Header base;
static Header *freep;

void
free(void *ap)
{
    3716:	89 e5                	mov    %esp,%ebp
    3718:	57                   	push   %edi
    3719:	56                   	push   %esi
    371a:	53                   	push   %ebx
    371b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
    371e:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    3721:	39 c8                	cmp    %ecx,%eax
    3723:	73 1d                	jae    3742 <free+0x32>
    3725:	8d 76 00             	lea    0x0(%esi),%esi
    3728:	8b 10                	mov    (%eax),%edx
    372a:	39 d1                	cmp    %edx,%ecx
    372c:	72 1a                	jb     3748 <free+0x38>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
    372e:	39 d0                	cmp    %edx,%eax
    3730:	72 08                	jb     373a <free+0x2a>
    3732:	39 c8                	cmp    %ecx,%eax
    3734:	72 12                	jb     3748 <free+0x38>
    3736:	39 d1                	cmp    %edx,%ecx
    3738:	72 0e                	jb     3748 <free+0x38>
    373a:	89 d0                	mov    %edx,%eax
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    373c:	39 c8                	cmp    %ecx,%eax
    373e:	66 90                	xchg   %ax,%ax
    3740:	72 e6                	jb     3728 <free+0x18>
    3742:	8b 10                	mov    (%eax),%edx
    3744:	eb e8                	jmp    372e <free+0x1e>
    3746:	66 90                	xchg   %ax,%ax
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
    3748:	8b 71 04             	mov    0x4(%ecx),%esi
    374b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
    374e:	39 d7                	cmp    %edx,%edi
    3750:	74 19                	je     376b <free+0x5b>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
    3752:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
    3755:	8b 50 04             	mov    0x4(%eax),%edx
    3758:	8d 34 d0             	lea    (%eax,%edx,8),%esi
    375b:	39 ce                	cmp    %ecx,%esi
    375d:	74 23                	je     3782 <free+0x72>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
    375f:	89 08                	mov    %ecx,(%eax)
  freep = p;
    3761:	a3 e8 4d 00 00       	mov    %eax,0x4de8
}
    3766:	5b                   	pop    %ebx
    3767:	5e                   	pop    %esi
    3768:	5f                   	pop    %edi
    3769:	5d                   	pop    %ebp
    376a:	c3                   	ret    
  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
    bp->s.size += p->s.ptr->s.size;
    376b:	03 72 04             	add    0x4(%edx),%esi
    376e:	89 71 04             	mov    %esi,0x4(%ecx)
    bp->s.ptr = p->s.ptr->s.ptr;
    3771:	8b 10                	mov    (%eax),%edx
    3773:	8b 12                	mov    (%edx),%edx
    3775:	89 53 f8             	mov    %edx,-0x8(%ebx)
  } else
    bp->s.ptr = p->s.ptr;
  if(p + p->s.size == bp){
    3778:	8b 50 04             	mov    0x4(%eax),%edx
    377b:	8d 34 d0             	lea    (%eax,%edx,8),%esi
    377e:	39 ce                	cmp    %ecx,%esi
    3780:	75 dd                	jne    375f <free+0x4f>
    p->s.size += bp->s.size;
    3782:	03 51 04             	add    0x4(%ecx),%edx
    3785:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
    3788:	8b 53 f8             	mov    -0x8(%ebx),%edx
    378b:	89 10                	mov    %edx,(%eax)
  } else
    p->s.ptr = bp;
  freep = p;
    378d:	a3 e8 4d 00 00       	mov    %eax,0x4de8
}
    3792:	5b                   	pop    %ebx
    3793:	5e                   	pop    %esi
    3794:	5f                   	pop    %edi
    3795:	5d                   	pop    %ebp
    3796:	c3                   	ret    
    3797:	89 f6                	mov    %esi,%esi
    3799:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

000037a0 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
    37a0:	55                   	push   %ebp
    37a1:	89 e5                	mov    %esp,%ebp
    37a3:	57                   	push   %edi
    37a4:	56                   	push   %esi
    37a5:	53                   	push   %ebx
    37a6:	83 ec 2c             	sub    $0x2c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
    37a9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((prevp = freep) == 0){
    37ac:	8b 0d e8 4d 00 00    	mov    0x4de8,%ecx
malloc(uint nbytes)
{
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
    37b2:	83 c3 07             	add    $0x7,%ebx
    37b5:	c1 eb 03             	shr    $0x3,%ebx
    37b8:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
    37bb:	85 c9                	test   %ecx,%ecx
    37bd:	0f 84 9b 00 00 00    	je     385e <malloc+0xbe>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
    37c3:	8b 01                	mov    (%ecx),%eax
    if(p->s.size >= nunits){
    37c5:	8b 50 04             	mov    0x4(%eax),%edx
    37c8:	39 d3                	cmp    %edx,%ebx
    37ca:	76 27                	jbe    37f3 <malloc+0x53>
        p->s.size -= nunits;
        p += p->s.size;
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    37cc:	8d 3c dd 00 00 00 00 	lea    0x0(,%ebx,8),%edi
morecore(uint nu)
{
  char *p;
  Header *hp;

  if(nu < 4096)
    37d3:	be 00 80 00 00       	mov    $0x8000,%esi
    37d8:	89 7d e4             	mov    %edi,-0x1c(%ebp)
    37db:	90                   	nop
    37dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
    37e0:	3b 05 e8 4d 00 00    	cmp    0x4de8,%eax
    37e6:	74 30                	je     3818 <malloc+0x78>
    37e8:	89 c1                	mov    %eax,%ecx
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
    37ea:	8b 01                	mov    (%ecx),%eax
    if(p->s.size >= nunits){
    37ec:	8b 50 04             	mov    0x4(%eax),%edx
    37ef:	39 d3                	cmp    %edx,%ebx
    37f1:	77 ed                	ja     37e0 <malloc+0x40>
      if(p->s.size == nunits)
    37f3:	39 d3                	cmp    %edx,%ebx
    37f5:	74 61                	je     3858 <malloc+0xb8>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
    37f7:	29 da                	sub    %ebx,%edx
    37f9:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
    37fc:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
    37ff:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
    3802:	89 0d e8 4d 00 00    	mov    %ecx,0x4de8
      return (void*)(p + 1);
    3808:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
    380b:	83 c4 2c             	add    $0x2c,%esp
    380e:	5b                   	pop    %ebx
    380f:	5e                   	pop    %esi
    3810:	5f                   	pop    %edi
    3811:	5d                   	pop    %ebp
    3812:	c3                   	ret    
    3813:	90                   	nop
    3814:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
morecore(uint nu)
{
  char *p;
  Header *hp;

  if(nu < 4096)
    3818:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    381b:	81 fb 00 10 00 00    	cmp    $0x1000,%ebx
    3821:	bf 00 10 00 00       	mov    $0x1000,%edi
    3826:	0f 43 fb             	cmovae %ebx,%edi
    3829:	0f 42 c6             	cmovb  %esi,%eax
    nu = 4096;
  p = sbrk(nu * sizeof(Header));
    382c:	89 04 24             	mov    %eax,(%esp)
    382f:	e8 3c fc ff ff       	call   3470 <sbrk>
  if(p == (char*)-1)
    3834:	83 f8 ff             	cmp    $0xffffffff,%eax
    3837:	74 18                	je     3851 <malloc+0xb1>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
    3839:	89 78 04             	mov    %edi,0x4(%eax)
  free((void*)(hp + 1));
    383c:	83 c0 08             	add    $0x8,%eax
    383f:	89 04 24             	mov    %eax,(%esp)
    3842:	e8 c9 fe ff ff       	call   3710 <free>
  return freep;
    3847:	8b 0d e8 4d 00 00    	mov    0x4de8,%ecx
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
    384d:	85 c9                	test   %ecx,%ecx
    384f:	75 99                	jne    37ea <malloc+0x4a>
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
    if(p->s.size >= nunits){
    3851:	31 c0                	xor    %eax,%eax
    3853:	eb b6                	jmp    380b <malloc+0x6b>
    3855:	8d 76 00             	lea    0x0(%esi),%esi
      if(p->s.size == nunits)
        prevp->s.ptr = p->s.ptr;
    3858:	8b 10                	mov    (%eax),%edx
    385a:	89 11                	mov    %edx,(%ecx)
    385c:	eb a4                	jmp    3802 <malloc+0x62>
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    385e:	c7 05 e8 4d 00 00 e0 	movl   $0x4de0,0x4de8
    3865:	4d 00 00 
    base.s.size = 0;
    3868:	b9 e0 4d 00 00       	mov    $0x4de0,%ecx
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    386d:	c7 05 e0 4d 00 00 e0 	movl   $0x4de0,0x4de0
    3874:	4d 00 00 
    base.s.size = 0;
    3877:	c7 05 e4 4d 00 00 00 	movl   $0x0,0x4de4
    387e:	00 00 00 
    3881:	e9 3d ff ff ff       	jmp    37c3 <malloc+0x23>
